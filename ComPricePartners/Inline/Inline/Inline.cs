﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;

namespace Inline
{
	class Inline : SupplierBase
	{
		public Inline(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
		}

		private const string UrlLogin = "http://www.inline-online.ru/pwd/login.aspx?returnUrl=/es3/";
		private const string SearchLogin = "http://www.inline-online.ru/esnew/webservices/catalogservice.asmx/GoodsCatalog";
		private const string LogoutUrl = "http://www.inline-online.ru/esnew/logout.aspx";

		private Dictionary<string, string> LoginOnInline()
		{
			var sessionParam = new Dictionary<string, string>();

			var requestAuthenticationFirst = (HttpWebRequest) WebRequest.Create(UrlLogin);
			requestAuthenticationFirst.Method = WebRequestMethods.Http.Get;

			requestAuthenticationFirst.Host = "www.inline-online.ru";
			requestAuthenticationFirst.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0";
			requestAuthenticationFirst.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			requestAuthenticationFirst.CookieContainer = new CookieContainer();
			requestAuthenticationFirst.KeepAlive = true;
			requestAuthenticationFirst.AllowAutoRedirect = false;

			var responseAuthenticationFirst = (HttpWebResponse) requestAuthenticationFirst.GetResponse();

			using (var streamResponseSearch = new StreamReader(responseAuthenticationFirst.GetResponseStream(), Encoding.UTF8))
			{
				var responseHtml = streamResponseSearch.ReadToEnd();

				if (!string.IsNullOrEmpty(responseHtml))
				{
					var searchTemplate = "id=\"__VIEWSTATE\" value=\"";
					var startIndex = responseHtml.IndexOf(searchTemplate, StringComparison.Ordinal);
					var endIndex = responseHtml.IndexOf("\" />", startIndex, StringComparison.Ordinal);
					var sessionId = responseHtml.Substring((startIndex + searchTemplate.Length), endIndex - (startIndex + searchTemplate.Length));
					sessionParam.Add("__VIEWSTATE", sessionId);

					searchTemplate = "id=\"__VIEWSTATEGENERATOR\" value=\"";
					startIndex = responseHtml.IndexOf(searchTemplate, StringComparison.Ordinal);
					endIndex = responseHtml.IndexOf("\" />", startIndex, StringComparison.Ordinal);
					sessionId = responseHtml.Substring((startIndex + searchTemplate.Length), endIndex - (startIndex + searchTemplate.Length));
					sessionParam.Add("__VIEWSTATEGENERATOR", sessionId);

					searchTemplate = "id=\"__EVENTVALIDATION\" value=\"";
					startIndex = responseHtml.IndexOf(searchTemplate, StringComparison.Ordinal);
					endIndex = responseHtml.IndexOf("\" />", startIndex, StringComparison.Ordinal);
					sessionId = responseHtml.Substring((startIndex + searchTemplate.Length), endIndex - (startIndex + searchTemplate.Length));
					sessionParam.Add("__EVENTVALIDATION", sessionId);
				}
			}

			if (sessionParam.Count == 3)
			{
				var requestAuthentication = (HttpWebRequest) WebRequest.Create(UrlLogin);
				requestAuthentication.Method = WebRequestMethods.Http.Post;

				requestAuthentication.Host = "www.inline-online.ru";
				requestAuthentication.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0";
				requestAuthentication.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
				requestAuthentication.CookieContainer = new CookieContainer();
				requestAuthentication.KeepAlive = true;
				requestAuthentication.AllowAutoRedirect = false;
				requestAuthentication.Referer = "http://www.inline-online.ru/pwd/login.aspx?returnUrl=/es3/";

				var postData = "__VIEWSTATE=" + sessionParam["__VIEWSTATE"];
				postData += "&__VIEWSTATEGENERATOR=" + sessionParam["__VIEWSTATEGENERATOR"];
				postData += "&__EVENTVALIDATION=" + sessionParam["__EVENTVALIDATION"];
				postData += "&ctl00$main$textLogin=" + SupplierLogin;
				postData += "&ctl00$main$textPassword=" + SupplierPassword;
				postData += "&ctl00$main$buttonEnter=Войти";
				var data = Encoding.UTF8.GetBytes(postData);

				requestAuthentication.ContentType = "application/x-www-form-urlencoded";
				requestAuthentication.ContentLength = data.Length;

				using (var stream = requestAuthentication.GetRequestStream())
					stream.Write(data, 0, data.Length);

				var responseAuthentication = (HttpWebResponse) requestAuthentication.GetResponse();
				if (responseAuthentication.Cookies != null && responseAuthentication.Cookies.Count > 0)
				{
					sessionParam.Add("ASP.NET_SessionId", responseAuthentication.Cookies["ASP.NET_SessionId"]?.Value);
					sessionParam.Add(".ASPXAUTH", responseAuthentication.Cookies[".ASPXAUTH"]?.Value);
				}

				responseAuthentication.Close();
			}

			responseAuthenticationFirst.Close();

			return sessionParam;
		}

		private void LogoutOnInline(Dictionary<string, string> param)
		{
			var reqLogout = (HttpWebRequest) WebRequest.Create(LogoutUrl);
			reqLogout.Method = WebRequestMethods.Http.Get;

			reqLogout.Host = "www.inline-online.ru";
			reqLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0";
			reqLogout.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			reqLogout.KeepAlive = true;
			reqLogout.AllowAutoRedirect = true;

			reqLogout.CookieContainer = new CookieContainer();
			var uriSearch = new Uri(SearchLogin);
			reqLogout.CookieContainer.Add(uriSearch, new Cookie(".ASPXAUTH", param[".ASPXAUTH"]));
			reqLogout.CookieContainer.Add(uriSearch, new Cookie("ASP.NET_SessionId", param["ASP.NET_SessionId"]));

			var respAuthFirst = (HttpWebResponse) reqLogout.GetResponse();
			respAuthFirst.Close();
		}

		[DataContract]
		internal class Param
		{
			[DataMember(Name = "available", Order = 1)]
			internal string paramBindGrid = "0";

			[DataMember(Name = "ctg", Order = 2)] internal object ctg = null;

			[DataMember(Name = "mode", Order = 3)] internal int mode = 0;

			[DataMember(Name = "oem", Order = 4)] internal object oem = null;

			[DataMember(Name = "search", Order = 5)]
			internal string search = string.Empty;

			[DataMember(Name = "sort", Order = 6)] internal object sort = null;

			[DataMember(Name = "sortDir", Order = 7)]
			internal object sortDir = null;

			[DataMember(Name = "startRow", Order = 8)]
			internal object startRow = null;
		}

		public override string Name { get; } = "Inl";
		public override string Header { get; } = "Inl";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var inlineResult = new List<ComPriceProduct>();
			var sessionParam = LoginOnInline();
			if (sessionParam.Count == 5)
			{
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					var reqSearch = (HttpWebRequest) WebRequest.Create(SearchLogin);
					reqSearch.Method = WebRequestMethods.Http.Post;

					reqSearch.CookieContainer = new CookieContainer();
					var uriSearch = new Uri(SearchLogin);
					reqSearch.CookieContainer.Add(uriSearch, new Cookie(".ASPXAUTH", sessionParam[".ASPXAUTH"]));
					reqSearch.CookieContainer.Add(uriSearch, new Cookie("ASP.NET_SessionId", sessionParam["ASP.NET_SessionId"]));

					reqSearch.Host = "www.inline-online.ru";
					reqSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0";
					reqSearch.Accept = "*/*";
					reqSearch.KeepAlive = true;
					reqSearch.AllowAutoRedirect = false;

					var json = new DataContractJsonSerializer(typeof(Param)).ToJson(new Param {search = article});

					var data = Encoding.UTF8.GetBytes(json);
					reqSearch.ContentType = "application/json; charset=utf-8";
					reqSearch.ContentLength = data.Length;

					using (var stream = reqSearch.GetRequestStream())
						stream.Write(data, 0, data.Length);

					var respSearch = (HttpWebResponse) reqSearch.GetResponse();

					using (var streamResponseSearch = new StreamReader(respSearch.GetResponseStream(), Encoding.UTF8))
					{
						var responseHtml = streamResponseSearch.ReadToEnd();
						if (!string.IsNullOrEmpty(responseHtml))
						{
							var jsonResponse = JObject.Parse(responseHtml);
							if (jsonResponse != null && jsonResponse.Count > 0)
							{
								var htmlContent = jsonResponse.First.First.ToString();
								var jsonHtmlContent = JObject.Parse(htmlContent);
								var value = jsonHtmlContent.Values().ToList()[0].ToString();

								if (!string.IsNullOrEmpty(value))
								{
									var html = new HtmlDocument();
									html.LoadHtml(value);
									var a = html.DocumentNode.Descendants();
									var table = html.DocumentNode.Descendants().Where(x => x.Name == "table").ToList();
									if (table.Any())
									{
										var productInfoTr = table.ToList()[0].Descendants().Where(
											x => x.Name == "tr" && (!string.IsNullOrEmpty(x.Id) || (x.HasAttributes && x.Attributes["class"] != null && x.Attributes["class"].Value.Equals("h1")))
										).ToList();

										var vendor = "";

										foreach (var product in productInfoTr)
										{
											if (product.HasAttributes && product.Attributes["class"] != null &&
											    product.Attributes["class"].Value.Equals("h1"))
											{
												var tagB = product.Descendants().Where(x => x.Name == "b").ToList();
												if (tagB.Last() != null)
													vendor = tagB.Last().InnerText;
											}
											else
											{
												var tagTd = product.Descendants().Where(x => x.Name == "td")
													.ToList();

												var name =
													tagTd[1].Descendants().Where(x => x.Name == "a").ToList()[0]
														.InnerText;

												if (!string.IsNullOrEmpty(name) && name.Contains("(комплект"))
													continue;

												var costHtml = tagTd[2].InnerText.Replace(" ", string.Empty);
												var costDouble = costHtml.ToDouble() / SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];

												var quantity = 0;
												if (tagTd[4].OuterHtml.Contains("available"))
													quantity = 1;

												if (IsEqualArticle)
												{
													var isGarbage = true;
													var nameSplit = name.Split(' ');
													if (nameSplit.Length > 0)
													{
														foreach (var split in nameSplit)
														{
															if (article.Equals(split) || ("(" + article + ")").Equals(split) || ("[" + article + "]").Equals(split))
															{
																isGarbage = false;
																break;
															}
														}
													}

													if (isGarbage)
														continue;
												}

												var inlineItem = new ComPriceProduct(article, name, vendor, quantity, costDouble);

												var isAddItem = false;
												var comPriceProduct = inlineResult.FirstOrDefault(inlineProduct => inlineProduct.Article.Equals(article));
												if (comPriceProduct != null)
												{
													if (inlineItem.Price < comPriceProduct.Price)
													{
														if (inlineItem.Quantity == comPriceProduct.Quantity)
															comPriceProduct.Price = inlineItem.Price;
														else if (inlineItem.Quantity > 0)
														{
															comPriceProduct.Quantity = inlineItem.Quantity;
															comPriceProduct.Price = inlineItem.Price;
														}
													}
													else
													{
														if (inlineItem.Quantity > 0 && comPriceProduct.Quantity < 1)
														{
															comPriceProduct.Quantity = inlineItem.Quantity;
															comPriceProduct.Price = inlineItem.Price;
														}
													}

													isAddItem = true;
												}

												if (!isAddItem)
												{
													SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(inlineItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
													inlineResult.Add(inlineItem);
												}
											}
										}
									}
								}
							}
						}
					}

					respSearch.Close();
				}

				LogoutOnInline(sessionParam);
			}

			return inlineResult;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

		protected override string LoginSupplierFieldName { get; } = "InlineLogin";

		protected override string PasswordSupplierFieldName { get; } = "InlinePassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "InlineInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "InlineUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "InlineSpecifyDeliveryDay";
	}
}