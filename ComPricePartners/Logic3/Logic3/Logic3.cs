﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;

namespace Logic3
{
    class Logic3 : SupplierBase
    {
        public Logic3(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
        {
            PriceListFileExtension = ".xlsx";
        }

        private readonly string _urlLogin = "https://3logic.ru/?login=yes";

        private readonly string _urlSearch = "https://3logic.ru/search/?q=";

        private readonly string _urlLogout = "https://3logic.ru/?logout=yes";

        private Dictionary<string, string> LoginOn3Logic()
        {
            var cookies = new Dictionary<string, string>();

            var requestLogin = (HttpWebRequest)WebRequest.Create(_urlLogin);
            requestLogin.Method = WebRequestMethods.Http.Post;

            requestLogin.Host = "3logic.ru";
            requestLogin.UserAgent =
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063";
            requestLogin.Accept = "text/html, application/xhtml+xml, image/jxr, */*";
            requestLogin.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            requestLogin.Headers.Add("Accept-Language", "ru,en-US;q=0.7,en;q=0.3");

            requestLogin.KeepAlive = true;
            requestLogin.AllowAutoRedirect = false;
            requestLogin.CookieContainer = new CookieContainer();

            var postData = "AUTH_FORM=Y";
            postData += "&backurl=/";
            postData += "&Login=Войти";
            postData += "&TYPE=AUTH";
            postData += "&USER_LOGIN=" + SupplierLogin;
            postData += "&USER_PASSWORD=" + SupplierPassword;
            var data = Encoding.Default.GetBytes(postData);

            requestLogin.ContentType = "application/x-www-form-urlencoded";
            requestLogin.ContentLength = data.Length;
            using (var stream = requestLogin.GetRequestStream())
                stream.Write(data, 0, data.Length);

            var responseLogin = (HttpWebResponse)requestLogin.GetResponse();

            // STRIX-GTX1070-O8G-GAMING
            if (responseLogin.Cookies?["PHPSESSID"] != null)
                cookies.Add("PHPSESSID", responseLogin.Cookies["PHPSESSID"].Value);

            responseLogin.Close();

            return cookies;
        }

        private void LogoutOn3Logic(Dictionary<string, string> cookies)
        {
            var requestLogout = (HttpWebRequest)WebRequest.Create(_urlLogout);
            requestLogout.Method = WebRequestMethods.Http.Get;

            requestLogout.Host = "3logic.ru";
            requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063";
            requestLogout.Accept = "text/html, application/xhtml+xml, image/jxr, */*";
            requestLogout.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            requestLogout.Headers.Add("Accept-Language", "ru,en-US;q=0.7,en;q=0.3");
            requestLogout.KeepAlive = true;
            requestLogout.AllowAutoRedirect = false;
            requestLogout.CookieContainer = new CookieContainer();

            var uriLogout = new Uri(_urlLogout);
            requestLogout.CookieContainer.Add(uriLogout, new Cookie("PHPSESSID", cookies["PHPSESSID"]));

            var responseLogout = (HttpWebResponse)requestLogout.GetResponse();
            responseLogout.Close();
        }

        public override string Name { get; } = "3log";

        public override string Header { get; } = "3Log";

        protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
        {
            var logics = new List<ComPriceProduct>();

            var cookies = LoginOn3Logic();
            if (cookies != null && cookies.Count > 0)
            {
                foreach (var article in articles)
                {
                    if (cancellationToken.IsCancellationRequested)
                        throw new Exception("TerminateSearch");

                    var requestSearch = (HttpWebRequest)WebRequest.Create(_urlSearch + WebUtility.UrlEncode(article));
                    requestSearch.Method = WebRequestMethods.Http.Get;

                    requestSearch.Host = "3logic.ru";
                    requestSearch.UserAgent =
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586";
                    requestSearch.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

                    requestSearch.KeepAlive = true;
                    requestSearch.AllowAutoRedirect = true;
                    requestSearch.Referer = "http://3logic.ru/?";

                    requestSearch.CookieContainer = new CookieContainer();
                    var uriBindGrid = new Uri(_urlSearch);
                    requestSearch.CookieContainer.Add(uriBindGrid, new Cookie("PHPSESSID", cookies["PHPSESSID"]));

                    var responseSearch = (HttpWebResponse)requestSearch.GetResponse();
                    using (var streamResponseSearch = new StreamReader(responseSearch.GetResponseStream(), Encoding.UTF8))
                    {
                        var responseHtml = streamResponseSearch.ReadToEnd();
                        if (!string.IsNullOrEmpty(responseHtml))
                        {
                            var addProduction = false;
                            var html = new HtmlDocument();
                            html.LoadHtml(responseHtml);
                            var tdWithArticle = html.DocumentNode.Descendants()
                                .Where(x => x.Name == "td" && x.InnerText.ToUpper().Contains(article.ToUpper()))
                                .ToList();

                            if (tdWithArticle.Any())
                            {
                                var trs = tdWithArticle.ToList();
                                foreach (var tr in trs)
                                {
                                    var item = new ComPriceProduct();
                                    var td = tr.ParentNode.Descendants().Where(x => x.Name == "td").ToList();
                                    var count = td.Count;
                                    if (count >= 7)
                                    {
                                        if (td[2].InnerText.ToUpper().Contains(article.ToUpper()))
                                        {
                                            item.Article = article;
                                            item.Name = td[3].InnerText.Replace("&nbsp;", string.Empty).Replace("&nbsp;", string.Empty).Trim();
                                            item.Brand = td[1].InnerText.Replace("\t", string.Empty).Replace("\n", string.Empty).Replace("&nbsp;", string.Empty).Trim();
                                            var priceCellValue = td[7].InnerHtml;
                                            if (string.IsNullOrEmpty(priceCellValue) || priceCellValue.ToLower().Contains("заказ") || priceCellValue.ToLower().Contains("р"))
                                                item.Quantity = 0;
                                            else if (priceCellValue.ToLower().Contains("+"))
                                                item.Quantity = 2;
                                            else
                                                item.Quantity = priceCellValue.ToInt();

                                            var priceString = td[5].InnerText.Replace("$", string.Empty).Replace(",", string.Empty).Replace("&nbsp;", string.Empty).Trim();
                                            item.Price = priceString.ToDouble();
                                            if (item.Price <= 0.01)
                                                item.Price = 0;
                                        }
                                    }

                                    if (!item.IsEmpty())
                                    {
                                        SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                                        logics.Add(item);
                                        addProduction = true;
                                        break;
                                    }
                                }
                            }

                            if (!addProduction)
                            {
                                var item = new ComPriceProduct(article);
                                SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                                logics.Add(item);
                            }
                        }
                    }

                    responseSearch.Close();
                }

                LogoutOn3Logic(cookies);
            }

            return logics;
        }

        protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
        {
            var logics = new List<ComPriceProduct>();

            var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
            if (string.IsNullOrEmpty(filePath))
                filePath = DownloadPrice();

            if (!string.IsNullOrEmpty(filePath))
            {
                var excelProcessingRequest = new ExcelProcessingRequest { FilePath = filePath, Name = 7, Manufacturer = 4, FindLookIn = 0, Article = 6 };
                excelProcessingRequest.SheetsNumberToSearch.Add(1);
                excelProcessingRequest.Quantity.Add(12);
                excelProcessingRequest.Price.Add(8);

                var findProductByArticles = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
                if (findProductByArticles.Any())
                {
                    foreach (var findProductByArticle in findProductByArticles)
                    {
                        if (findProductByArticle.FindArticle != null && findProductByArticle.FindArticle.Equals(findProductByArticle.Article))
                        {
                            var name = findProductByArticle.Name as string;
                            name = name?.Trim();

                            var manufacturer = findProductByArticle.Manufacturer as string;
                            manufacturer = manufacturer?.Trim();

                            var quantity = 0;

                            foreach (var quantityCellValue in findProductByArticle.Quantity)
                            {
                                var quantityString = quantityCellValue?.ToString().Trim();
                                if (!string.IsNullOrEmpty(quantityString))
                                    quantity = quantityString
                                        .ToLower()
                                        .Replace(">", "")
                                        .Replace("резерв", "")
                                        .Replace("заказ", "")
                                        .Trim()
                                        .ToInt();
                            }

                            var price = 0.0;
                            if (findProductByArticle.Price.Any())
                            {
                                var priceString = findProductByArticle.Price[0].ToString();
                                if (!string.IsNullOrEmpty(priceString))
                                {
                                    price = priceString.ToDouble();
                                    if (price < 1.0)
                                        price = 0.0;
                                }
                            }

                            var logic = new ComPriceProduct(findProductByArticle.Article, name, manufacturer, quantity, price, Currency.USD);
                            SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(logic, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                            logics.Add(logic);
                        }
                        else
                        {
                            var logic = new ComPriceProduct(findProductByArticle.Article);
                            SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(logic, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                            logics.Add(logic);
                        }
                    }
                }
            }

            return logics;
        }

        private readonly string _urlPriceListRequest = "https://3logic.ru/personal/orders.php";

        public string DownloadPrice()
        {
            var priceFilePath = string.Empty;
            var priceFileUrl = string.Empty;
            var cookies = LoginOn3Logic();
            if (cookies.Any())
            {
                var requestPriceList = (HttpWebRequest)WebRequest.Create(_urlPriceListRequest);
                requestPriceList.Method = WebRequestMethods.Http.Post;

                requestPriceList.Host = "3logic.ru";
                requestPriceList.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/110.0";
                requestPriceList.Accept = "application/xml, text/xml, */*; q=0.01";

                requestPriceList.KeepAlive = true;
                requestPriceList.AllowAutoRedirect = false;
                requestPriceList.Referer = "http://3logic.ru/?";

                requestPriceList.CookieContainer = new CookieContainer();
                var uriBindGrid = new Uri(_urlPriceListRequest);
                requestPriceList.CookieContainer.Add(uriBindGrid, new Cookie("PHPSESSID", cookies["PHPSESSID"]));

                var postData = "xaction=spe";
                var data = Encoding.Default.GetBytes(postData);

                requestPriceList.ContentType = "application/x-www-form-urlencoded";
                requestPriceList.ContentLength = data.Length;
                using (var stream = requestPriceList.GetRequestStream())
                    stream.Write(data, 0, data.Length);

                var responsePriceList = (HttpWebResponse)requestPriceList.GetResponse();
                using (var streamResponseSearch = new StreamReader(responsePriceList.GetResponseStream(), Encoding.UTF8))
                {
                    var responseHtml = streamResponseSearch.ReadToEnd();
                    if (!string.IsNullOrEmpty(responseHtml))
                    {
                        var xml = new XmlDocument();
                        xml.LoadXml(responseHtml);
                        var fileNode = xml.SelectSingleNode("/result/it");
                        if (fileNode != null)
                            priceFileUrl = fileNode.InnerText;
                    }
                }
                responsePriceList.Close();


                if (!string.IsNullOrEmpty(priceFileUrl))
                {
                    var priceFileDownloadUrl = "https://3logic.ru" + priceFileUrl;
                    var webRequestDownloadFile = (HttpWebRequest)WebRequest.Create(priceFileDownloadUrl);
                    webRequestDownloadFile.Method = WebRequestMethods.Http.Get;

                    webRequestDownloadFile.CookieContainer = new CookieContainer();
                    var uriPriceDownload = new Uri(priceFileDownloadUrl);
                    requestPriceList.CookieContainer.Add(uriPriceDownload, new Cookie("PHPSESSID", cookies["PHPSESSID"]));
                    webRequestDownloadFile.AllowAutoRedirect = false;

                    priceFilePath = SupplierProviderService.PriceListDownloadService.DownloadPriceList(webRequestDownloadFile, DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
                }

                LogoutOn3Logic(cookies);
            }

            return priceFilePath;
        }

        protected override string LoginSupplierFieldName { get; } = "Logic3Login";

        protected override string PasswordSupplierFieldName { get; } = "Logic3Password";

        protected override string InStockDeliveryDaySupplierFieldName { get; } = "Logic3InStockDeliveryDay";

        protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "Logic3UnderOrderDeliveryDay";

        protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "Logic3SpecifyDeliveryDay";
    }
}