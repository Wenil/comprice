﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Logic3
{
	public class Logic3ViewModel : SupplierBaseViewModel
	{
		public Logic3ViewModel(ISystemConfigurationService systemConfigurationService)
		{
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "Logic3Login", "Logic3Password");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "Logic3InStockDeliveryDay", "Logic3UnderOrderDeliveryDay", "Logic3SpecifyDeliveryDay");
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}