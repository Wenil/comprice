﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Spbdevice
{
	public class SpbdeviceViewModel : SupplierBaseViewModel
	{
		public SpbdeviceViewModel(ISystemConfigurationService systemConfigurationService)
		{
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "SpbdeviceInStockDeliveryDay", "SpbdeviceUnderOrderDeliveryDay", "SpbdeviceSpecifyDeliveryDay");
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; } = null;

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}