﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;

namespace Spbdevice
{
    public class Spbdevice : SupplierBase
    {
        private readonly ISupplierProviderService _supplierProviderService;

        public Spbdevice(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
        {
            _supplierProviderService = supplierProviderService;
        }

        private const string UrlSearchTemplate = "https://spbdevice.ru/search?q={0}&lang=ru";

        private const string Domain = "https://spbdevice.ru";

        private List<string> ComposeProductUrl(string article)
        {
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(new HtmlWeb().Load(string.Format(UrlSearchTemplate, article)).Text);

            var urls = new List<string>();

            var productList = htmlDocument.DocumentNode.Descendants()
                .Where(x => x.Name == "form" && x.GetAttributeValue("class", string.Empty).Contains("product-preview"))
                .ToList();

            foreach (var product in productList)
            {
                var productNameAnchorList = product.Descendants()
                    .Where(node => node.Name == "div" && node.GetAttributeValue("class", string.Empty).Contains("img-ratio__inner"))
                    .ToList();

                if (productNameAnchorList.Any())
                {
                    var productNameAnchor = productNameAnchorList[0].ChildNodes[1];
                    var href = productNameAnchor.GetAttributeValue("href", string.Empty);

                    if (!string.IsNullOrEmpty(href))
                        urls.Add(Domain + href);
                }
            }

            return urls;
        }

        private ComPriceProduct ComposeProductData(Tuple<string, List<string>> url)
        {
            var data = new ComPriceProduct();

            foreach (var article in url.Item2)
            {
                var htmlDocument = new HtmlWeb().LoadFromWebAsync(article).Result;
                var dataScript = htmlDocument.DocumentNode.SelectSingleNode($"//script[contains(text(), '\"sku\": \"{url.Item1}\",')]");
                if (dataScript != null)
                {
                    var json = JObject.Parse(dataScript.InnerText);
                    if (json != null)
                    {
                        if (!url.Item1.Equals(json["sku"].ToString().Trim()))
                            continue;

                        data.Name = json["name"].ToString();
                        data.Brand = string.Empty;
                        var price = json["offers"]["lowPrice"].ToString().ToDouble();
                        if (price > 0.0)
                            price /= _supplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];

                        data.Price = price;
                    }
                }

                var productQuantity = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='quantity_at_warehouse0']");
                if (productQuantity != null && productQuantity.InnerText.Contains("Есть"))
                {
                    var warhouseQuantity = productQuantity.ChildNodes[3];
                    if (warhouseQuantity != null)
                    {
                        var productQuantityText = warhouseQuantity.InnerText
                            .Replace("шт.", string.Empty)
                            .Replace(" ", string.Empty);
                        var quantity = productQuantityText.ToInt();
                        data.Quantity = quantity;
                    }
                    else
                        data.Quantity = 0;
                }
                else
                    data.Quantity = 0;


                if (data.AnyWithoutArticle())
                {
                    data.Article = url.Item1;
                    break;
                }
            }

            return data;
        }

        public override string Name { get; } = "Spbdevice";

        public override string Header { get; } = "Spbdevice";

        protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
        {
            var spbDeviceResult = new List<ComPriceProduct>();

            foreach (var article in articles)
            {
                var url = ComposeProductUrl(article);
                if (url.Any())
                {
                    var data = ComposeProductData(new Tuple<string, List<string>>(article, url));
                    if (data.IsEmpty())
                        data.Article = article;

                    SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(data, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                    spbDeviceResult.Add(data);
                }
            }

            return spbDeviceResult;
        }

        protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

        protected override string LoginSupplierFieldName { get; } = string.Empty;

        protected override string PasswordSupplierFieldName { get; } = string.Empty;

        protected override string InStockDeliveryDaySupplierFieldName { get; } = "SpbdeviceInStockDeliveryDay";

        protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "SpbdeviceUnderOrderDeliveryDay";

        protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "SpbdeviceSpecifyDeliveryDay";
    }
}