﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;

namespace VTT
{
	class VTT : SupplierBase
	{
		public VTT(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
		}

		private const string UrlStartPage = "https://b2b.vtt.ru/";

		private const string UrlValidateLogin = "https://b2b.vtt.ru/validateLogin";

		private const string UrlLogout = "https://b2b.vtt.ru/logout";

		private const string UrlSearch = "https://b2b.vtt.ru/catalog?options=cleanSearch&word={0}";

		private void ComposeVttSearchResponse(string response, string article, List<ComPriceProduct> vttResult)
		{
			var itemAdd = false;
			var html = new HtmlDocument();
			html.LoadHtml(response);
			var catalogs = html.DocumentNode.Descendants().Where(x => x.Name == "div" && (x.HasAttributes && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("catalog_list "))).ToList();
			if (catalogs.Any())
			{
				var mainCatalog = catalogs[0];
				if (mainCatalog != null)
				{
					var rows = mainCatalog.Descendants().Where(x => x.Name == "div" && (x.HasAttributes && x.Attributes["class"] != null && x.Attributes["class"].Value.Equals("catalog_list_row"))).ToList();
					if (rows.Any())
					{
						foreach (var row in rows)
						{
							var parseArticle = string.Empty;
							var articles = mainCatalog.Descendants()
								.Where(x => x.Name == "span" && (x.HasAttributes && x.Attributes["class"] != null && x.Attributes["class"].Value.Equals("artnum")))
								.ToList();
							if (articles.Any())
								parseArticle = articles.First().InnerText.Trim();

							var parseName = string.Empty;
							var names = mainCatalog.Descendants()
								.Where(x => x.Name == "a")
								.ToList();
							if (names.Any())
								parseName = names.Last().InnerText.Trim();

							var parsePrice = 0.0;
							var pricies = mainCatalog.Descendants()
								.Where(x => x.Name == "span" && (x.HasAttributes && x.Attributes["class"] != null && x.Attributes["class"].Value.Equals("price_usd")))
								.ToList();
							if (pricies.Any())
								parsePrice = pricies.First().InnerText.Replace("$", string.Empty).Trim().ToDouble();

							var parseQuantity = 0;
							var quantitys = mainCatalog.Descendants()
								.Where(x => x.Name == "span" && (x.HasAttributes && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains(" item_local_num")))
								.ToList();
							if (quantitys.Any())
								parseQuantity = quantitys.First().InnerText.Trim().ToInt();

							var vttItem = new ComPriceProduct(article, parseName, string.Empty, parseQuantity, parsePrice) { ManufacturerArticle = parseArticle };

							if (IsEqualArticle)
							{
								var isGarbage = false;
								if (!article.Equals(parseArticle))
								{
									isGarbage = true;
									var hpnSplit = parseName.Split(' ');
									if (hpnSplit.Length > 0)
									{
										foreach (var split in hpnSplit)
										{
											if (article.Equals(split))
											{
												isGarbage = false;
												break;
											}
										}
									}
								}

								if (isGarbage)
									continue;
							}

							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(vttItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							vttResult.Add(vttItem);
						}
					}
				}
			}

			if (!itemAdd)
			{
				var vttItem = new ComPriceProduct(article) { ManufacturerArticle = article };
				SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(vttItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
				vttResult.Add(vttItem);
			}
		}

		private Dictionary<string, string> LoginOnVtt()
		{
			var cookies = new Dictionary<string, string>();

			var requestLogin = (HttpWebRequest)WebRequest.Create(UrlStartPage);
			requestLogin.Method = WebRequestMethods.Http.Get;

			requestLogin.Host = "b2b.vtt.ru";
			requestLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0";
			requestLogin.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";
			requestLogin.KeepAlive = true;
			requestLogin.AllowAutoRedirect = true;
			requestLogin.CookieContainer = new CookieContainer();

			var csrfToken = string.Empty;
			var responseLogin = (HttpWebResponse)requestLogin.GetResponse();
			using (var streamResponseLogin = new StreamReader(responseLogin.GetResponseStream(), Encoding.Default))
			{
				var response = streamResponseLogin.ReadToEnd();
				var html = new HtmlDocument();
				html.LoadHtml(response);

				var meta = html.DocumentNode.SelectSingleNode("//meta[@name='csrf-token']");
				if (meta != null)
					csrfToken = meta.Attributes["content"].Value;
			}

			cookies.Add("XSRF-TOKEN", responseLogin.Cookies["XSRF-TOKEN"].Value);
			cookies.Add("b2bvttru_session", responseLogin.Cookies["b2bvttru_session"].Value);
			responseLogin.Close();

			if (cookies.Count > 1)
			{
				var requestValidateLogin = (HttpWebRequest)WebRequest.Create(UrlValidateLogin);
				requestValidateLogin.Method = WebRequestMethods.Http.Post;
				requestValidateLogin.Headers.Add("X-Csrf-Token", csrfToken);

				requestValidateLogin.CookieContainer = new CookieContainer();
				var uri2 = new Uri(UrlValidateLogin);
				requestValidateLogin.CookieContainer.Add(uri2, new Cookie("XSRF-TOKEN", cookies["XSRF-TOKEN"]));
				requestValidateLogin.CookieContainer.Add(uri2, new Cookie("b2bvttru_session", cookies["b2bvttru_session"]));

				requestValidateLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.5845.686 YaBrowser/23.9.5.686 Yowser/2.5 Safari/537.36";
				requestValidateLogin.Accept = "application/json, text/javascript, */*; q=0.01";
				requestValidateLogin.KeepAlive = true;
				requestValidateLogin.Referer = "https://b2b.vtt.ru/login";
				requestValidateLogin.ServicePoint.Expect100Continue = false;

				var postData = "login=" + SupplierLogin + "&password=" + SupplierPassword;
				var data = Encoding.UTF8.GetBytes(postData);

				requestValidateLogin.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
				requestValidateLogin.ContentLength = data.Length;

				using (var stream = requestValidateLogin.GetRequestStream())
					stream.Write(data, 0, data.Length);

				var responseValidateLogin = (HttpWebResponse)requestValidateLogin.GetResponse();

				using (var streamResponseSearch = new StreamReader(responseValidateLogin.GetResponseStream(), Encoding.Default))
				{
					var response = streamResponseSearch.ReadToEnd();
					cookies["XSRF-TOKEN"] = responseValidateLogin.Cookies["XSRF-TOKEN"].Value;
					cookies["b2bvttru_session"] = responseValidateLogin.Cookies["b2bvttru_session"].Value;
				}

				responseValidateLogin.Close();
			}

			return cookies;
		}

		private void LogoutOnVtt(Dictionary<string, string> session)
		{
			var requestLogout = (HttpWebRequest)WebRequest.Create(UrlLogout);
			requestLogout.Method = WebRequestMethods.Http.Get;

			requestLogout.CookieContainer = new CookieContainer();
			var uri2 = new Uri(UrlLogout);
			requestLogout.CookieContainer.Add(uri2, new Cookie("XSRF-TOKEN", session["XSRF-TOKEN"]));
			requestLogout.CookieContainer.Add(uri2, new Cookie("b2bvttru_session", session["b2bvttru_session"]));

			requestLogout.Host = "b2b.vtt.ru";
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0";
			requestLogout.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";
			requestLogout.KeepAlive = true;
			requestLogout.Referer = "http://b2b.vtt.ru/";


			var responseLogout = (HttpWebResponse)requestLogout.GetResponse();
			responseLogout.Close();
		}

		public override string Name { get; } = "vtt";

		public override string Header { get; } = "VTT";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var vttResult = new List<ComPriceProduct>();
			var loginCookie = LoginOnVtt();
			if (loginCookie != null && loginCookie.Count > 0)
			{
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					var requestSearch = (HttpWebRequest)WebRequest.Create(string.Format(UrlSearch, article));
					requestSearch.Method = WebRequestMethods.Http.Get;

					requestSearch.Host = "b2b.vtt.ru";
					requestSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0";
					requestSearch.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
					requestSearch.KeepAlive = true;
					requestSearch.AllowAutoRedirect = false;

					requestSearch.CookieContainer = new CookieContainer();
					var uri2 = new Uri(string.Format(UrlSearch, article));
					requestSearch.CookieContainer.Add(uri2, new Cookie("XSRF-TOKEN", loginCookie["XSRF-TOKEN"]));
					requestSearch.CookieContainer.Add(uri2, new Cookie("b2bvttru_session", loginCookie["b2bvttru_session"]));

					var responseSearch = (HttpWebResponse)requestSearch.GetResponse();

					using (var streamResponseSearch = new StreamReader(responseSearch.GetResponseStream(), Encoding.UTF8))
					{
						var response = streamResponseSearch.ReadToEnd();
						if (response.Contains("Выйти"))
						{
							try
							{
								ComposeVttSearchResponse(response, article, vttResult);
							}
							catch (Exception exception)
							{
								SupplierProviderService.LoggerService.Error($"Не удалось обработать ответ от VTT для товара {article} ", exception);
							}
						}
					}

					responseSearch.Close();
				}

				LogoutOnVtt(loginCookie);
			}

			return vttResult;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

		protected override string LoginSupplierFieldName { get; } = "VttLogin";

		protected override string PasswordSupplierFieldName { get; } = "VttPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "VttInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "VttUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "VttSpecifyDeliveryDay";
	}
}