﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using Com.Price.Data.Extensions;
using Com.Price.Data.ProcessFiles.Xml;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;

namespace RRC
{
	class RRC : SupplierBase
	{
		private readonly ISupplierProviderService _supplierProviderService;

		public RRC(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			_supplierProviderService = supplierProviderService;

			PriceListFileExtension = ".xml";
		}

		private const string UrlAuth = "https://b2b.rrc.ru/login";

		private const string UrlSearch = "https://b2b.rrc.ru/user/search";

		private const string UrlOut = "https://b2b.rrc.ru/logout ";

		private const string RrcHost = "b2b.rrc.ru";

		private const string RrcDefaultReferer = @"https://b2b.rrc.ru/";

		private readonly Dictionary<int, int[]> _parseColumns = new Dictionary<int, int[]>
		{
			{33, new[] {1, 8, 5, 13, 11, 14, 14}},
			{35, new[] {1, 8, 5, 13, 13, 16, 16}},
			{38, new[] {1, 10, 7, 13, 13, 16, 18}},
			{41, new[] {1, 6, 4, 9, 9, 14, 16}},
			{43, new[] {1, 6, 4, 9, 9, 14, 16}},
			{48, new[] {1, 6, 4, 17, 17, 26, 28}},
			{53, new[] {1, 7, 4, 22, 22, 29, 31}},
			{54, new[] {1, 6, 4, 23, 23, 32, 34}},
			{56, new[] {1, 11, 8, 17, 17, 26, 28}},
			{60, new[] {1, 8, 5, 14, 14, 23, 23}},
			{62, new[] {1, 11, 8, 17, 17, 26, 28}},
			{63, new[] {1, 11, 8, 17, 17, 26, 26}},
			{64, new[] {1, 11, 8, 17, 17, 30, 28}},
			{65, new[] {1, 11, 8, 17, 19, 31, 28}},
			{83, new[] {1, 6, 4, 19, 21, 41, 43}},
			{91, new[] {1, 7, 4, 23, 23, 43, 45}},
			{92, new[] {1, 7, 4, 24, 24, 44, 46}},
			{95, new[] {1, 6, 4, 25, 27, 47, 49}}
		};

		private const string UrlPriceListGeneration = "https://b2b.rrc.ru/personal/xml/?code={0}&id={1}&login={2}&pass={3}";

		private const string UrlPriceList = "https://b2b.rrc.ru/personal/xml/{0}.xml";

		private string DownloadPrice()
		{
			var directory = DirectoryPathForDownloadFile;

			var filePath = _supplierProviderService.PriceListDownloadService.PriceListPathIfExist(directory, PriceListFileName, PriceListFileExtension);
			if (!string.IsNullOrEmpty(filePath))
				return filePath;

			var startСatalogGeneration =
				(HttpWebRequest) WebRequest.Create(string.Format(UrlPriceListGeneration, "jmSI978NkezCKVsBiDL0gQa4ARqYlx5o", "rmpsL3C", HttpUtility.UrlEncode(SupplierLogin, Encoding.UTF8),
					SupplierPassword));
			startСatalogGeneration.Method = WebRequestMethods.Http.Get;
			startСatalogGeneration.Host = RrcHost;
			startСatalogGeneration.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
			startСatalogGeneration.KeepAlive = true;
			startСatalogGeneration.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
			startСatalogGeneration.Headers.Add("Accept-Encoding", "gzip, deflate, br");

			var startСatalogGenerationResponse = startСatalogGeneration.GetResponse();
			startСatalogGenerationResponse.Close();

			Thread.Sleep(10000);

			var webRequestDownloadFile = (HttpWebRequest) WebRequest.Create(string.Format(UrlPriceList, "jmSI978NkezCKVsBiDL0gQa4ARqYlx5o"));
			webRequestDownloadFile.Method = WebRequestMethods.Http.Get;
			webRequestDownloadFile.Host = RrcHost;
			webRequestDownloadFile.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
			webRequestDownloadFile.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			webRequestDownloadFile.KeepAlive = true;
			webRequestDownloadFile.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
			webRequestDownloadFile.Headers.Add("Accept-Encoding", "gzip, deflate, br");
			webRequestDownloadFile.AllowAutoRedirect = false;
			webRequestDownloadFile.AutomaticDecompression = DecompressionMethods.GZip;

			return _supplierProviderService.PriceListDownloadService.DownloadPriceList(webRequestDownloadFile, directory, PriceListFileName, PriceListFileExtension, encoding: Encoding.UTF8);
		}

		private Dictionary<string, string> LoginOnRrc()
		{
			var cookies = new Dictionary<string, string>();

			var preAuthenticationRequest = (HttpWebRequest) WebRequest.Create(UrlAuth);
			preAuthenticationRequest.Method = WebRequestMethods.Http.Get;
			preAuthenticationRequest.Host = RrcHost;
			preAuthenticationRequest.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0";
			preAuthenticationRequest.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			preAuthenticationRequest.KeepAlive = true;
			preAuthenticationRequest.AllowAutoRedirect = false;
			preAuthenticationRequest.Referer = RrcDefaultReferer;
			preAuthenticationRequest.ServicePoint.Expect100Continue = false;

			preAuthenticationRequest.CookieContainer = new CookieContainer();

			var preAuthenticationResponse = (HttpWebResponse) preAuthenticationRequest.GetResponse();

			if (preAuthenticationResponse.Cookies != null)
			{
				cookies["b2brrcru_session"] = preAuthenticationResponse.Cookies["b2brrcru_session"]?.Value;
				cookies["XSRF-TOKEN"] = preAuthenticationResponse.Cookies["XSRF-TOKEN"]?.Value;
			}

			var token = string.Empty;
			using (var streamPreAuthenticationResponse = new StreamReader(preAuthenticationResponse.GetResponseStream(), Encoding.UTF8))
			{
				var responseHtml = streamPreAuthenticationResponse.ReadToEnd();
				if (!string.IsNullOrEmpty(responseHtml))
				{
					var html = new HtmlDocument();
					html.LoadHtml(responseHtml);

					var hiddenInputElements = html.DocumentNode.Descendants().Where(element =>
						element.Name == "input" && element.HasAttributes &&
						element.Attributes["name"].Value == "_token").ToList();

					if (hiddenInputElements.Any())
					{
						var hiddenInputElement = hiddenInputElements[0];
						if (hiddenInputElement.HasAttributes && hiddenInputElement.Attributes["value"] != null)
							token = hiddenInputElement.Attributes["value"].Value;
					}
				}
			}

			preAuthenticationResponse.Close();

			var requestAuthentication = (HttpWebRequest) WebRequest.Create(UrlAuth);
			requestAuthentication.Method = WebRequestMethods.Http.Post;

			requestAuthentication.Host = RrcHost;
			requestAuthentication.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0";
			requestAuthentication.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

			requestAuthentication.KeepAlive = true;
			requestAuthentication.AllowAutoRedirect = false;
			requestAuthentication.Referer = RrcDefaultReferer;
			requestAuthentication.ServicePoint.Expect100Continue = false;

			requestAuthentication.CookieContainer = new CookieContainer();

			var uriSearch = new Uri(UrlAuth);
			requestAuthentication.CookieContainer.Add(uriSearch, new Cookie("b2brrcru_session", cookies["b2brrcru_session"]));
			requestAuthentication.CookieContainer.Add(uriSearch, new Cookie("XSRF-TOKEN", cookies["XSRF-TOKEN"]));

			var postData = @"_token=" + token;
			postData += @"&login=" + HttpUtility.UrlEncode(SupplierLogin, Encoding.UTF8);
			postData += @"&password=" + SupplierPassword;
			var data = Encoding.Default.GetBytes(postData);

			requestAuthentication.ContentType = "application/x-www-form-urlencoded";
			requestAuthentication.ContentLength = data.Length;

			using (var stream = requestAuthentication.GetRequestStream())
				stream.Write(data, 0, data.Length);

			var authenticationResponse = (HttpWebResponse) requestAuthentication.GetResponse();
			if (authenticationResponse.Cookies != null)
			{
				cookies["b2brrcru_session"] = authenticationResponse.Cookies["b2brrcru_session"]?.Value;
				cookies["XSRF-TOKEN"] = authenticationResponse.Cookies["XSRF-TOKEN"]?.Value;
			}

			return cookies;
		}

		private void LogoutOnRrc(Dictionary<string, string> cookies)
		{
			var requestLogout = (HttpWebRequest) WebRequest.Create(UrlOut);
			requestLogout.Method = WebRequestMethods.Http.Get;
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = false;
			requestLogout.CookieContainer = new CookieContainer();
			var uriSearch = new Uri(UrlOut);
			requestLogout.CookieContainer.Add(uriSearch, new Cookie("b2brrcru_session", cookies["b2brrcru_session"]));
			requestLogout.CookieContainer.Add(uriSearch, new Cookie("XSRF-TOKEN", cookies["XSRF-TOKEN"]));

			requestLogout.Host = RrcHost;
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0";
			requestLogout.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = true;

			var responseLogout = (HttpWebResponse) requestLogout.GetResponse();
			responseLogout.Close();
		}

		public override string Name { get; } = "rrc";

		public override string Header { get; } = "RRC";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var resultRrc = new List<ComPriceProduct>();
			var cookiesDictionary = LoginOnRrc();
			if (cookiesDictionary.Any())
			{
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					var preSearchRequest = (HttpWebRequest) WebRequest.Create(UrlSearch);
					preSearchRequest.Method = WebRequestMethods.Http.Get;
					preSearchRequest.Host = RrcHost;
					preSearchRequest.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0";
					preSearchRequest.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
					preSearchRequest.KeepAlive = true;
					preSearchRequest.AllowAutoRedirect = false;
					preSearchRequest.Referer = RrcDefaultReferer;
					preSearchRequest.ServicePoint.Expect100Continue = false;

					preSearchRequest.CookieContainer = new CookieContainer();
					var preUriSearch = new Uri(UrlSearch);
					preSearchRequest.CookieContainer.Add(preUriSearch, new Cookie("b2brrcru_session", cookiesDictionary["b2brrcru_session"]));
					preSearchRequest.CookieContainer.Add(preUriSearch, new Cookie("XSRF-TOKEN", cookiesDictionary["XSRF-TOKEN"]));

					var preSearchResponse = (HttpWebResponse) preSearchRequest.GetResponse();

					var token = string.Empty;
					using (var streamPreAuthenticationResponse = new StreamReader(preSearchResponse.GetResponseStream(), Encoding.UTF8))
					{
						var responseHtml = streamPreAuthenticationResponse.ReadToEnd();
						if (!string.IsNullOrEmpty(responseHtml))
						{
							var html = new HtmlDocument();
							html.LoadHtml(responseHtml);

							var hiddenInputElements = html.DocumentNode.Descendants().Where(element =>
								element.Name == "input" && element.HasAttributes && element.Attributes["name"] != null &&
								element.Attributes["name"].Value == "_token").ToList();

							if (hiddenInputElements.Any())
							{
								var hiddenInputElement = hiddenInputElements[0];
								if (hiddenInputElement.HasAttributes && hiddenInputElement.Attributes["value"] != null)
									token = hiddenInputElement.Attributes["value"].Value;
							}
						}
					}

					preSearchResponse.Close();

					var requestSearch = (HttpWebRequest) WebRequest.Create(UrlSearch);
					requestSearch.Method = WebRequestMethods.Http.Post;

					requestSearch.CookieContainer = new CookieContainer();
					var uriSearch = new Uri(UrlSearch);
					requestSearch.CookieContainer.Add(uriSearch, new Cookie("b2brrcru_session", cookiesDictionary["b2brrcru_session"]));
					requestSearch.CookieContainer.Add(uriSearch, new Cookie("XSRF-TOKEN", cookiesDictionary["XSRF-TOKEN"]));

					requestSearch.Host = RrcHost;
					requestSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0";
					requestSearch.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
					requestSearch.KeepAlive = true;
					requestSearch.AllowAutoRedirect = false;

					var postData = "inDesc=1";
					postData += "&_token=" + token;
					postData += "&search=" + article;
					var data = Encoding.Default.GetBytes(postData);

					requestSearch.ContentType = "application/x-www-form-urlencoded";
					requestSearch.ContentLength = data.Length;

					using (var stream = requestSearch.GetRequestStream())
						stream.Write(data, 0, data.Length);

					var responseSearch = (HttpWebResponse) requestSearch.GetResponse();
					using (var streamResponseSearch = new StreamReader(responseSearch.GetResponseStream(), Encoding.UTF8))
					{
						var itemsFind = new List<Dictionary<string, object>>();
						var responseHtml = streamResponseSearch.ReadToEnd();
						if (!string.IsNullOrEmpty(responseHtml))
						{
							var addProduction = false;
							var html = new HtmlDocument();
							html.LoadHtml(responseHtml);
							foreach (var node in html.DocumentNode.Descendants().Where(x => x.Name == "tr"))
							{
								var item = new ComPriceProduct();
								var td = node.Descendants().Where(x => x.Name == "#text")
									.Where(x => !x.InnerHtml.Contains("\\r\\n") && !x.InnerHtml.Equals(string.Empty)).ToList();
								var count = td.Count();
								var articleOnSite = td[1].InnerText.Replace("\t", string.Empty).Replace("\n", string.Empty).Trim();
								if (articleOnSite.Equals(article))
								{
									if (_parseColumns.ContainsKey(count))
									{
										var parseColumn = _parseColumns[count];
										item.Article = td[parseColumn[0]].InnerText.Replace("\t", string.Empty).Replace("\n", string.Empty).Trim();
										item.Name = td[parseColumn[1]].InnerText.Trim();
										item.Brand = td[parseColumn[2]].InnerText.Trim();

										var qty = td[parseColumn[3]].InnerText
											.Replace("\t", string.Empty)
											.Replace("\n", string.Empty)
											.Trim();

										if (!string.IsNullOrEmpty(qty))
										{
											qty = td[parseColumn[4]].InnerText
												.Replace("\t", string.Empty)
												.Replace("\n", string.Empty)
												.Trim();

											if (qty.Contains("Товар доступен к заказу"))
												item.Quantity = 1;
											else
												item.Quantity = td[parseColumn[4]].InnerText
													.Replace("\t", string.Empty)
													.Replace("\n", string.Empty)
													.Trim()
													.ToInt();
										}
										else
											item.Quantity = 0;

										if (td[parseColumn[5]].InnerText.Contains("$"))
										{
											item.Price = td[parseColumn[5]].InnerText
												.Replace("$", string.Empty)
												.Replace(" ", string.Empty)
												.Replace("&euro;", string.Empty)
												.Trim()
												.ToDouble();
										}
										else
										{
											item.Price = td[parseColumn[6]].InnerText
												.Replace("$", string.Empty)
												.Replace(" ", string.Empty)
												.Replace("&euro;", string.Empty)
												.Trim()
												.ToDouble();
										}
									}
								}

								if (!item.IsEmpty())
								{
									var itemAdd = false;

									for (var j = 0; j < resultRrc.Count; j++)
									{
										if (resultRrc[j].Article.Equals(item.Article))
										{
											if (item.Price < resultRrc[j].Price)
											{
												if (item.Quantity == resultRrc[j].Quantity)
													resultRrc[j].Price = item.Price;
												else if (item.Quantity > 0)
												{
													resultRrc[j].Quantity = item.Quantity;
													resultRrc[j].Price = item.Price;
												}
											}
											else
											{
												if (item.Quantity > 0 && resultRrc[j].Quantity < 1)
												{
													resultRrc[j].Quantity = item.Quantity;
													resultRrc[j].Price = item.Price;
												}
											}

											itemAdd = true;
										}
									}

									if (!itemAdd)
									{
										SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
										resultRrc.Add(item);
										addProduction = true;
									}
								}
							}

							if (!addProduction)
							{
								var item = new ComPriceProduct(article);
								SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
								resultRrc.Add(item);
							}
						}
					}

					responseSearch.Close();
				}

				LogoutOnRrc(cookiesDictionary);
			}

			return resultRrc;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = DownloadPrice();
			if (!string.IsNullOrEmpty(filePath))
			{
				var xmlProcessingRequest = new XmlProcessingRequest
				{
					FilePath = filePath,
					Item = "item",
					Article = "partnumber",
					Name = "name",
					Manufacturer = "vendor",
					Price = new List<string> {"price_usd"},
					Quantity = new List<string> {"quantity"}
				};

				var findProductByArticles = _supplierProviderService.XmlProcessingService.FindProductByArticles(articles, xmlProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						if (findProductByArticle.Name != null)
						{
							var name = findProductByArticle.Name.ToString().Trim();

							var manufacturer = findProductByArticle.Manufacturer.ToString().Trim();

							var quantity = 0;
							foreach (var quantityFindValue in findProductByArticle.Quantity)
							{
								var quantityString = quantityFindValue.ToString();

								if (!string.IsNullOrEmpty(quantityString))
								{
									int.TryParse(quantityString, out var quantityInt);
									quantity += quantityInt;
								}
							}

							var price = 0.0;

							if (findProductByArticle.Price.Any())
							{
								if (findProductByArticle.Price[0] != null)
								{
									var priceString = findProductByArticle.Price[0].ToString().Replace(" ", string.Empty).Trim();
									price = priceString.ToDouble();
								}
							}

							var item = new ComPriceProduct(findProductByArticle.Article, name, manufacturer, quantity, price, Currency.USD);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(item);
						}
						else
						{
							var item = new ComPriceProduct(findProductByArticle.Article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(item);
						}
					}
				}
			}

			return result;
		}

		protected override string LoginSupplierFieldName { get; } = "RrcLogin";

		protected override string PasswordSupplierFieldName { get; } = "RrcPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "RrcInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "RrcUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "RrcSpecifyDeliveryDay";
	}
}