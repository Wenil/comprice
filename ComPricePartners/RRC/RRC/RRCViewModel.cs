﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace RRC
{
	public class RRCViewModel : SupplierBaseViewModel
	{
		public RRCViewModel(ISystemConfigurationService systemConfigurationService)
		{
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "RrcLogin", "RrcPassword");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "RrcInStockDeliveryDay", "RrcUnderOrderDeliveryDay", "RrcSpecifyDeliveryDay");
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}