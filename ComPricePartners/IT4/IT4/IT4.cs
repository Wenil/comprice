﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;

namespace IT4
{
	class IT4 : SupplierBase
	{
		public IT4(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
		}

        private const string UrlPriceAvail =  "https://services.it4profit.com/product/ru/368/PriceAvail.xml?USERNAME={0}&PASSWORD={1}&SEARCH_CODE={2}";

		public override string Name { get; } = "it4";

		public override string Header { get; } = "IT4";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var it4Items = new List<ComPriceProduct>();

			foreach (var article in articles)
			{
				if (cancellationToken.IsCancellationRequested)
					throw new Exception("TerminateSearch");

				var searchArticle = article;
				if (article.StartsWith("CSE-"))
					searchArticle = searchArticle.Replace("CSE-", "SC");

				var reqPriceAvail = (HttpWebRequest) WebRequest.Create(string.Format(UrlPriceAvail, SupplierLogin, SupplierPassword, searchArticle));
				reqPriceAvail.Method = WebRequestMethods.Http.Get;

				reqPriceAvail.Host = "services.it4profit.com";
				reqPriceAvail.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0";
				reqPriceAvail.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
				reqPriceAvail.KeepAlive = true;
				reqPriceAvail.AllowAutoRedirect = true;

				var respPriceAvail = (HttpWebResponse) reqPriceAvail.GetResponse();
				using (var streamPriceAvail = new StreamReader(respPriceAvail.GetResponseStream(), Encoding.UTF8))
				{
					var responseXml = streamPriceAvail.ReadToEnd();
					var items = new List<ComPriceProduct>();
					if (!string.IsNullOrEmpty(responseXml) && !responseXml.Contains("Authorization failed"))
					{
						var doc = new XmlDocument();
						doc.LoadXml(responseXml);
						var prices = doc.GetElementsByTagName("PRICE");
						foreach (XmlElement price in prices)
						{
							var item = new ComPriceProduct();
							var realPart = price.SelectSingleNode("WIC").InnerText;
							if (article.StartsWith("CSE-"))
								realPart = article;

							if (article.StartsWith("MBD-"))
							{
								if (article.Equals("MBD-" + realPart))
									realPart = "MBD-" + realPart;
								else if (article.Equals("MBD-" + realPart + "-O"))
									realPart = "MBD-" + realPart + "-O";
								else if (article.Equals("MBD-" + realPart + "-B"))
									realPart = "MBD-" + realPart + "-B";
							}

							item.Article = article;
							item.Name = price.SelectSingleNode("DESCRIPTION").InnerText;
							item.Brand = price.SelectSingleNode("VENDOR_NAME").InnerText;
							var availible = price.SelectSingleNode("AVAIL").InnerText;
							if (!string.IsNullOrEmpty(availible))
							{
								availible = availible.ToLower();
								if (availible.Contains("да") || availible.Contains("достаточно"))
									item.Quantity = 2;
								else if (availible.Contains("-") || availible.Contains("звоните"))
									item.Quantity = 0;
								else
									item.Quantity = 1;
							}

							if (price.SelectSingleNode("MY_PRICE") != null)
							{
								var coast = price.SelectSingleNode("MY_PRICE").InnerText.ToDouble();
								if (price.SelectSingleNode("CURRENCY_CODE").InnerText == "USD")
									item.Price = coast;
								else
								{
									coast /= SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
									item.Price = coast;
								}
							}

							if (IsEqualArticle)
							{
								var isGarbage = false;
								item.Article = realPart;
								if (!article.Equals(item.Article))
								{
									isGarbage = true;
									if (price.SelectSingleNode("GROUP_ID").InnerText.Equals("185"))
									{
										isGarbage = false;
										item.Article = article;
									}

									if (item.Name.Contains("SKU: " + article))
									{
										isGarbage = false;
										item.Article = article;
									}

									var nameSplit = item.Name.Split(' ');
									if (nameSplit.Length > 0)
									{
										isGarbage = true;
										foreach (var split in nameSplit)
										{
											if (article.Equals(split))
											{
												isGarbage = false;
												item.Article = split;
												break;
											}
										}
									}
								}

								if (isGarbage)
									continue;
							}

							var itemAdd = false;

							for (var j = 0; j < items.Count; j++)
							{
								if (items[j].Article.Equals(item.Article))
								{
									if (item.Price < items[j].Price)
									{
										if (item.Quantity.Equals(items[j].Quantity))
										{
											items[j].Name = item.Name;
											items[j].Brand = item.Brand;
											items[j].Price = item.Price;
										}
										else if (item.Quantity != 0)
										{
											items[j].Name = item.Name;
											items[j].Brand = item.Brand;
											items[j].Quantity = item.Quantity;
											items[j].Price = item.Price;
										}
									}
									else
									{
										if (item.Quantity != 0 && items[j].Quantity == 0)
										{
											items[j].Name = item.Name;
											items[j].Brand = item.Brand;
											items[j].Quantity = item.Quantity;
											items[j].Price = item.Price;
										}
									}

									itemAdd = true;
								}
							}

							if (!itemAdd)
							{
								SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
								items.Add(item);
							}
						}
					}

					if (items.Count == 0) {
						var item = new ComPriceProduct(article);
						SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
						items.Add(item);
					}

					it4Items.AddRange(items);
				}

				respPriceAvail.Close();
			}

			return it4Items;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

		protected override string LoginSupplierFieldName { get; } = "IT4Login";

		protected override string PasswordSupplierFieldName { get; } = "IT4Password";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "IT4InStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "IT4UnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "IT4SpecifyDeliveryDay";
	}
}