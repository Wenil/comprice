﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Sunvel
{
	public class SunvelViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public SunvelViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "SunvelInStockDeliveryDay", "SunvelUnderOrderDeliveryDay", "SunvelSpecifyDeliveryDay");
		}

		public string SunvelCmo
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("SunvelaDiscountCmo");
			set => _systemConfigurationService.SetSystemConfigurationParameter("SunvelaDiscountCmo", value);
		}

		public string SunvelItk
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("SunvelDiscountItk");
			set => _systemConfigurationService.SetSystemConfigurationParameter("SunvelDiscountItk", value);
		}

		public string SunvelLanUnion
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("SunvelDiscountLanUnion");
			set => _systemConfigurationService.SetSystemConfigurationParameter("SunvelDiscountLanUnion", value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; } = null;

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}