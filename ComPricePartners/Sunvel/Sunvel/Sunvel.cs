﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;

namespace Sunvel
{
	public class Sunvel : SupplierBase
	{
		private readonly ISupplierProviderService _supplierProviderService;

		public Sunvel(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			_supplierProviderService = supplierProviderService;
		}

		/// <summary>
		/// Скидка ЦМО.
		/// </summary>
		private const string SunvelDiscountCmo = "SunvelaDiscountCmo";

		/// <summary>
		/// Скидка ИТК.
		/// </summary>
		private const string SunvelDiscountItk = "SunvelDiscountItk";

		/// <summary>
		/// Скидка Lan Union.
		/// </summary>
		private const string SunvelDiscountLanUnion = "SunvelDiscountLanUnion";

		/// <summary>
		/// Скидки.
		/// </summary>
		private readonly Dictionary<string, double> _discount = new Dictionary<string, double>
		{
			{"ЦМО", 0.0},
			{"Lan Union", 0.0},
			{"ITK", 0.0},
			{"Elbox", 0.0}
		};

		private const string SearchUrl = "https://www.sunvel.ru/search/?q=";

		private ComPriceProduct ComposeProductData(string article)
		{
			var htmlDocument = new HtmlDocument();
			htmlDocument.LoadHtml(new HtmlWeb().Load(SearchUrl + article).Text);

			return ParseHtmlProductData(htmlDocument, article);
		}

		private ComPriceProduct ParseHtmlProductData(HtmlDocument htmlDocument, string article)
		{
			var productListHtmlNodes = htmlDocument.DocumentNode.Descendants()
				.Where(x => x.Name == "li" && x.GetAttributeValue("class", string.Empty).Contains("ff_prod campaign"))
				.ToList();

			foreach (var productHtmlNode in productListHtmlNodes)
			{
				var product = ParseProductHtmlLiElement(productHtmlNode, article);
				if (!product.IsEmpty())
					return product;
			}

			return new ComPriceProduct();
		}

		private ComPriceProduct ParseProductHtmlLiElement(HtmlNode htmlNode, string article)
		{
			var data = new ComPriceProduct();

			var productAnchorHtmlNode = htmlNode.SelectSingleNode("//div[@class='product list_prod']//a[@class='product_url']");
			if (productAnchorHtmlNode != null)
			{
				var productInfoHtmlNode = productAnchorHtmlNode.SelectSingleNode("//div[@class='product-info']//div[@class='place_text']");
				if (productInfoHtmlNode != null)
				{
					var additionalInfoHtmlNode = productInfoHtmlNode.SelectSingleNode("//div[@class='additional-info']");
					if (additionalInfoHtmlNode != null)
					{
						var skuHtmlNode = productInfoHtmlNode.SelectSingleNode("//div[@class='sku']");
						if (skuHtmlNode != null)
						{
							if (article.Equals(skuHtmlNode.InnerText.Trim()))
								data.Article = article;
							else
								return new ComPriceProduct();
						}

						var productAvailableHtmlNode = productInfoHtmlNode.SelectSingleNode("//div[@class='product-available']");
						if (productAvailableHtmlNode != null)
							data.Quantity = productAvailableHtmlNode.InnerText.Contains("в наличии") ? 2 : 0;
					}

					var brandHtmlNode = productInfoHtmlNode.SelectSingleNode("//div[@class='brand']");
					if (brandHtmlNode != null)
						data.Brand = brandHtmlNode.InnerText.Trim();

					var nameHtmlNode = productInfoHtmlNode.SelectSingleNode("//div[@class='name']");
					if (nameHtmlNode != null)
						data.Name = nameHtmlNode.InnerText.Trim();
				}

				var tablePricesHtmlNode = productAnchorHtmlNode.SelectSingleNode("//div[@class='table-prices']");
				if (tablePricesHtmlNode != null)
				{
					var comparePriceStrikeHtmlNode = tablePricesHtmlNode.SelectSingleNode("//span[@class='compare-price strike']");
					if (comparePriceStrikeHtmlNode != null)
						data.Price = ComposePrice(comparePriceStrikeHtmlNode, data.Brand);
					else
					{
						var currentPriceStrikeHtmlNode = tablePricesHtmlNode.SelectSingleNode("//span[@class='current_price']");
						if (currentPriceStrikeHtmlNode != null)
							data.Price = ComposePrice(currentPriceStrikeHtmlNode, data.Brand);
					}
				}
			}

			if (!data.IsEmpty())
			{
				SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(data, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
				return data;
			}

			return new ComPriceProduct();
		}

		private double ComposePrice(HtmlNode node, string brand)
		{
			var value = node.InnerText
				.Replace("руб", string.Empty)
				.Replace(" ", string.Empty)
				.Trim();
			var priceAsDouble = value.ToDouble();
			if (priceAsDouble > 0.0)
			{
				if (_discount.ContainsKey(brand) && _discount[brand] > 0)
					priceAsDouble *= 1 - (_discount[brand] / 100);

				priceAsDouble /= _supplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
			}

			return priceAsDouble;
		}

		public override string Name { get; } = "Sunvel";

		public override string Header { get; } = "Sunvel";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var sunvelResult = new List<ComPriceProduct>();
			foreach (var article in articles)
			{
				var data = ComposeProductData(article);
				if (data.IsEmpty())
					data.Article = article;

				sunvelResult.Add(data);
			}

			return sunvelResult;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

		protected override string LoginSupplierFieldName { get; } = string.Empty;

		protected override string PasswordSupplierFieldName { get; } = string.Empty;

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "SunvelInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "SunvelUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "SunvelSpecifyDeliveryDay";

		public override void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
		{
			if (systemConfigurationParameters == null || !systemConfigurationParameters.Any())
				return;

			if (systemConfigurationParameters.ContainsKey(SunvelDiscountCmo) && systemConfigurationParameters[SunvelDiscountCmo] != null)
			{
				_discount["ЦМО"] = systemConfigurationParameters[SunvelDiscountCmo].ToDouble();
				_discount["Elbox"] = systemConfigurationParameters[SunvelDiscountCmo].ToDouble();
			}

			if (systemConfigurationParameters.ContainsKey(SunvelDiscountItk) && systemConfigurationParameters[SunvelDiscountItk] != null)
				_discount["ITK"] = systemConfigurationParameters[SunvelDiscountItk].ToDouble();

			if (systemConfigurationParameters.ContainsKey(SunvelDiscountLanUnion) && systemConfigurationParameters[SunvelDiscountLanUnion] != null)
				_discount["Lan Union"] = systemConfigurationParameters[SunvelDiscountLanUnion].ToDouble();

			base.ApplySupplierConfigurationParameters(systemConfigurationParameters);
		}
	}
}