﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace SoftTronik
{
	public class SoftTronikViewModel : SupplierBaseViewModel
	{
		public SoftTronikViewModel(ISystemConfigurationService systemConfigurationService)
		{
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "SoftTronikLoginKey", "SoftTronikPasswordKey");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "SoftTronikInStockDeliveryDay", "SoftTronikUnderOrderDeliveryDay", "SoftTronikSpecifyDeliveryDay");
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}