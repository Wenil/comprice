﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json.Linq;

namespace SoftTronik
{
	public class SoftTronik : SupplierBase
	{
		private readonly ISupplierProviderService _supplierProviderService;

		public SoftTronik(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			_supplierProviderService = supplierProviderService;

			PriceListFileExtension = ".xlsx";
		}

		private const string SearchUrl = "https://b2b.soft-tronik.ru/api/products?exact=false&free=&limit=20&offset=0&op=and&product_group=&remainder=&sale=&sort=%2Btitle&text=";

		private readonly Dictionary<string, string> _additionalLinesForSuppliers
			= new Dictionary<string, string>
			{
				{"molex", "MT"}, {"digi", "DG"}, {"hp networks", "HP-"}, {"huawei", "HW-"}, {"rittal", "MG-RT"},
				{"qnap", "QN-"}, {"uniview", "UN-"}, {"yealink", "YL-"}, {"d-link", "DL-"}
			};

		private bool HasAdditionalLinesForSuppliers(string searchArticle, string findArticle, string supplier)
		{
			var supplierLower = supplier.ToLower();
			if (_additionalLinesForSuppliers.ContainsKey(supplierLower))
			{
				var supplierLowerAdditionalLine = _additionalLinesForSuppliers[supplierLower];
				if (findArticle.StartsWith(supplierLowerAdditionalLine))
				{
					var splitArticle = findArticle.Split(new[] {supplierLowerAdditionalLine}, StringSplitOptions.None);
					if (splitArticle.Length > 1)
						return searchArticle.Equals(splitArticle[1]);
				}
			}

			return false;
		}

		private const string LoginUrl = "https://b2b.soft-tronik.ru/api/login";

		private Dictionary<string, string> LoginOnSoftTronik()
		{
			var cookies = new Dictionary<string, string>();

			var requestAuthentication = (HttpWebRequest) WebRequest.Create(LoginUrl);
			requestAuthentication.Method = WebRequestMethods.Http.Post;

			requestAuthentication.Host = "b2b.soft-tronik.ru";
			requestAuthentication.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0";
			requestAuthentication.Accept = @"application/json, text/plain, */*";

			requestAuthentication.KeepAlive = true;
			requestAuthentication.AllowAutoRedirect = false;
			requestAuthentication.Referer = "https://b2b.soft-tronik.ru/index.html";
			requestAuthentication.ServicePoint.Expect100Continue = false;

			requestAuthentication.CookieContainer = new CookieContainer();

			var postData = new JObject {{"login_code", HttpUtility.UrlEncode(SupplierLogin, Encoding.UTF8)}, {"login_password", HttpUtility.UrlEncode(SupplierPassword, Encoding.UTF8)}}.ToString();
			var data = Encoding.Default.GetBytes(postData);
			requestAuthentication.ContentType = "application/json;charset=utf-8";
			using (var stream = requestAuthentication.GetRequestStream())
				stream.Write(data, 0, data.Length);

			var authenticationResponse = (HttpWebResponse) requestAuthentication.GetResponse();

			if (authenticationResponse.Cookies != null)
			{
				cookies.Add("contact_person_code", authenticationResponse.Cookies["contact_person_code"]?.Value);
				cookies.Add("st", authenticationResponse.Cookies["st"]?.Value);
			}

			return cookies;
		}

		private const string LogoutUrl = "https://b2b.soft-tronik.ru/api/logout";

		private void LogoutOnSoftTronik(IReadOnlyDictionary<string, string> cookies)
		{
			var requestLogout = (HttpWebRequest) WebRequest.Create(LogoutUrl);
			requestLogout.Method = WebRequestMethods.Http.Get;
			requestLogout.CookieContainer = new CookieContainer();

			requestLogout.Host = "b2b.soft-tronik.ru";
			requestLogout.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0";
			requestLogout.Accept = @"application/json, text/plain, */*";

			var uri = new Uri(LogoutUrl);
			requestLogout.CookieContainer.Add(uri, new Cookie("st", cookies["st"]));
			requestLogout.CookieContainer.Add(uri, new Cookie("contact_person_code", cookies["contact_person_code"]));

			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = false;

			var responseLogout = (HttpWebResponse) requestLogout.GetResponse();
			using (var stream = new StreamReader(responseLogout.GetResponseStream(), Encoding.Default))
			{
				var response = stream.ReadToEnd();
			}

			responseLogout.Close();
		}

		private const string UrlPriceList = "https://b2b.soft-tronik.ru/api/products.csv?exact=&free=&limit=20&offset=0&op=and&product_group=&remainder=&sale=&sort=%2Btitle";

		private string DownloadPrice()
		{
			var priceFilePath = string.Empty;
			var cookies = LoginOnSoftTronik();
			if (cookies.Any())
			{
				var webRequestDownloadFile = (HttpWebRequest) WebRequest.Create(UrlPriceList);
				webRequestDownloadFile.Method = WebRequestMethods.Http.Get;
				webRequestDownloadFile.Host = "b2b.soft-tronik.ru";
				webRequestDownloadFile.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
				webRequestDownloadFile.Accept = "application/json, text/plain, */*";
				webRequestDownloadFile.KeepAlive = true;
				webRequestDownloadFile.AllowAutoRedirect = false;
				webRequestDownloadFile.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
				webRequestDownloadFile.Headers.Add("Accept-Encoding", "gzip, deflate, br");

				webRequestDownloadFile.CookieContainer = new CookieContainer();
				var uriPriceDownload = new Uri(UrlPriceList);

				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("st", cookies["st"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("contact_person_code", cookies["contact_person_code"]));

				webRequestDownloadFile.AllowAutoRedirect = false;

				var directory = DirectoryPathForDownloadFile;

				foreach (var file in Directory.GetFiles(directory))
					File.Delete(file);

				priceFilePath = _supplierProviderService.PriceListDownloadService.DownloadPriceList(webRequestDownloadFile, directory, PriceListFileName, PriceListFileExtension);

				LogoutOnSoftTronik(cookies);

				return priceFilePath;
			}

			return priceFilePath;
		}

		public override string Name { get; } = "SoftT";

		public override string Header { get; } = "SoftT";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			if (articles.Any())
			{
				var cookies = LoginOnSoftTronik();
				if (cookies.Any())
				{
					foreach (var article in articles)
					{
						var url = SearchUrl + HttpUtility.UrlEncode(article, Encoding.UTF8);
						var requestLogout = (HttpWebRequest) WebRequest.Create(url);
						requestLogout.Method = WebRequestMethods.Http.Get;
						requestLogout.CookieContainer = new CookieContainer();

						var uri = new Uri(url);
						requestLogout.CookieContainer.Add(uri, new Cookie("st", cookies["st"]));
						requestLogout.CookieContainer.Add(uri, new Cookie("contact_person_code", cookies["contact_person_code"]));

						requestLogout.Host = "b2b.soft-tronik.ru";
						requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
						requestLogout.Accept = "application/json, text/plain, */*";
						requestLogout.KeepAlive = true;
						requestLogout.AllowAutoRedirect = false;

						var responseLogout = (HttpWebResponse) requestLogout.GetResponse();
						using (var stream = new StreamReader(responseLogout.GetResponseStream(), Encoding.UTF8))
						{
							var jsonBody = stream.ReadToEnd();
							var responseJson = JObject.Parse(jsonBody);
							if (responseJson != null)
							{
								var item = new ComPriceProduct();
								var responseProducts = responseJson["data"]?["products"];

								if (responseProducts != null)
								{
									foreach (var responseProduct in responseProducts)
									{
										if (responseProduct != null)
										{
											var partNumber = responseProduct["title"].Value<string>();
											var brand = responseProduct["product_group_title"].Value<string>();

											if (partNumber.Equals(article) || HasAdditionalLinesForSuppliers(article, partNumber, brand))
											{
												var name = responseProduct["title_full"].Value<string>();

												var price = 0.0;
												if (responseProduct["price_final_usd"] != null)
													price = responseProduct["price_final_usd"].Value<float>();

												var quantity = "0";
												if (responseProduct["stock_free"] != null)
												{
													quantity = responseProduct["stock_free"].Value<string>();
													quantity = string.IsNullOrEmpty(quantity) ? "0" : quantity.Replace("+", string.Empty).Replace("*", string.Empty);
												}

												item.Article = article;
												item.Name = name;
												item.Brand = brand;
												item.Quantity = quantity.ToInt();
												item.Price = price;
												break;
											}
										}
									}
								}

								if (item.IsEmpty())
									item.Article = article;

								SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
								result.Add(item);
							}
						}

						responseLogout.Close();
					}

					LogoutOnSoftTronik(cookies);
				}
			}

			return result;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = _supplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest
				{
					FilePath = filePath,
					Article = 3,
					Name = 4,
					Manufacturer = 1,
					Currency = 8,
					FindLookIn = 2,
					FindLookAt = 1,
					Price = new List<int> {6},
					Quantity = new List<int> {9},
					SheetsNumberToSearch = new List<int> {1}
				};

				var findProductByArticles = _supplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						var isAddItem = false;

						if (findProductByArticle.Name != null)
						{
							var name = findProductByArticle.Name.ToString();
							name = name.Trim();

							var findArticle = findProductByArticle.FindArticle.ToString();
							findArticle = findArticle.Trim();

							var manufacturer = findProductByArticle.Manufacturer.ToString();
							manufacturer = manufacturer.Trim();

							if (findProductByArticle.Article.Equals(findArticle) || HasAdditionalLinesForSuppliers(findProductByArticle.Article, findArticle, manufacturer))
							{
								var currency = findProductByArticle.Currency.ToString();
								currency = currency.Trim();

								var quantity = 0;
								foreach (var quantityFindValue in findProductByArticle.Quantity)
								{
									if (quantityFindValue != null)
									{
										var quantityString = quantityFindValue.ToString();

										quantityString = quantityString
											.Replace("*", string.Empty)
											.Replace("+", string.Empty)
											.Trim();

										if (!string.IsNullOrEmpty(quantityString))
										{
											int.TryParse(quantityString, out var quantityInt);
											quantity += quantityInt;
										}
									}
								}

								var price = 0.0;

								if (findProductByArticle.Price.Any())
								{
									if (findProductByArticle.Price[0] != null)
									{
										var priceString = findProductByArticle.Price[0].ToString().Replace(" ", string.Empty).Trim();
										price = priceString.ReplaceCommaToPoint().ToDouble();

										if (currency == "RUB")
											price /= _supplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
									}
								}

								var item = new ComPriceProduct(findProductByArticle.Article, name, manufacturer, quantity, price, Currency.USD);
								SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
								result.Add(item);
								isAddItem = true;
							}
						}

						if (!isAddItem)
						{
							var item = new ComPriceProduct(findProductByArticle.Article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(item);
						}
					}
				}
			}

			return result;
		}

		protected override string LoginSupplierFieldName { get; } = "SoftTronikLoginKey";

		protected override string PasswordSupplierFieldName { get; } = "SoftTronikPasswordKey";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "SoftTronikInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "SoftTronikUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "SoftTronikSpecifyDeliveryDay";
	}
}