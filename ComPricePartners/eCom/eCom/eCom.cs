﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace eCom
{
    // ReSharper disable once InconsistentNaming
    class eCom : SupplierBase
	{
		public eCom(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
		
		}

		private List<ComPriceProduct> ParseResponse(JArray productArray, string article)
		{
			var eComItems = new List<ComPriceProduct>();
			var itemAdd = false;
			foreach (var product in productArray)
			{
				itemAdd = false;
				var eComItem = new ComPriceProduct();

				if (IsEqualArticle)
				{
					var isGarbage = false;
					if (product["manufacturerCode"] != null && !string.Equals(article, product["manufacturerCode"].ToString(), StringComparison.CurrentCultureIgnoreCase))
					{
						isGarbage = true;
						var nameSplit = product["productName"].ToString().Split(' ');
						if (nameSplit.Length > 0)
						{
							foreach (var split in nameSplit)
							{
								if (article.Equals(split) || article.ToUpper().Equals(split.ToUpper()))
								{
									isGarbage = false;
									break;
								}
							}
						}

						if (product["vendorCode"].ToString().Equals("INTEL") && (product["productName"].ToString().Contains("Процессор") || product["productName"].ToString().Contains("жесткий диск")) && !isGarbage)
							isGarbage = false;

						if (product["vendorCode"].ToString().Equals("ASUS") && product["productName"].ToString().Contains("Процессор Intel"))
							isGarbage = true;

						if (product["vendorCode"].ToString().Equals("HUAWEI") && product["productName"].ToString().Contains("Процессор Intel"))
							isGarbage = true;
					}

					if (article.StartsWith("MBD-"))
					{
						if (product["manufacturerCode"] != null)
						{
							var eComPart = product["manufacturerCode"].ToString();
							if (string.Equals(eComPart, article, StringComparison.CurrentCultureIgnoreCase)
								|| string.Equals(eComPart, article + "-O", StringComparison.CurrentCultureIgnoreCase)
								|| string.Equals(eComPart, article + "-B", StringComparison.CurrentCultureIgnoreCase))
							{
								isGarbage = false;
							}
						}
					}

					if (isGarbage)
						continue;
				}

				eComItem.Article = article;
				eComItem.Name = product["productName"].ToString();
				eComItem.Brand = product["vendorCode"].ToString();
				eComItem.ManufacturerArticle = product["manufacturerCode"].ToString();
				eComItem.Price = product["productPrice"].ToString().ToDouble() / SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
                if(product["inStock"] is JArray inStocks)
                {
					foreach(var inStock in inStocks)
                    {
                        if ("MSK".Equals(inStock["stock"].ToString()))
                        {
							eComItem.Quantity += inStock["quantity"]
								.ToString()
								.Replace("<", "")
								.Replace(">", "")
								.Trim()
								.ToInt();
						}
                    }
                }

				if (SupplierProviderService.CheckConditionService.IsGarbageName(eComItem.Name))
					continue;

				if (article.Equals(eComItem.ManufacturerArticle))
				{
					if (eComItems.Count > 0)
						eComItems.RemoveAll(find => !article.Equals(eComItem.ManufacturerArticle));
				}
				else
				{
					if (eComItems.Count > 0)
					{
						var goodProduct = eComItems.FindAll(find => article.Equals(eComItem.ManufacturerArticle));
						if (goodProduct.Any())
							continue;
					}
				}

				for (var j = 0; j < eComItems.Count; j++)
				{
					if (eComItems[j].Article.Equals(eComItem.Article))
					{
						if (eComItem.Price < eComItems[j].Price)
						{
							if (eComItem.Quantity == eComItems[j].Quantity)
								eComItems[j].Price = eComItem.Price;
							else if (eComItem.Quantity > 0)
							{
								eComItems[j].Quantity = eComItem.Quantity;
								eComItems[j].Price = eComItem.Price;
							}
						}
						else
						{
							if (eComItem.Quantity > 0 && eComItems[j].Quantity < 1)
							{
								eComItems[j].Quantity = eComItem.Quantity;
								eComItems[j].Price = eComItem.Price;
							}
						}

						itemAdd = true;
					}
				}

				if (!itemAdd)
				{
					SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(eComItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
					eComItems.Add(eComItem);
				}


				itemAdd = true;
			}
			

			if (!itemAdd)
			{
				var eComItem = new ComPriceProduct(article);
				SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(eComItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
				eComItems.Add(eComItem);
			}
			

			return eComItems;
		}

		public override string Name { get; } = "eCom";
		public override string Header { get; } = "eCom";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var eComItems = new List<ComPriceProduct>();
			var token = getEcomApiToken();
			if (!string.IsNullOrEmpty(token))
			{
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					eComItems.AddRange(ProductSearch(article, token));
				}
			}

			return eComItems;
		}

		private List<ComPriceProduct> ProductSearch(string article, string token)
        {
			var client = new HttpClient
			{
				BaseAddress = new Uri(BaseEcomApiAddress)
			};
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
			var response = client.GetAsync("api/Catalogs/ProductSearch?pattern=" + WebUtility.UrlEncode(article)).Result;
			client.Dispose();
			if (response.IsSuccessStatusCode)
			{
				var responseBody = JArray.Parse(response.Content.ReadAsStringAsync().Result);
				if(responseBody != null)
                {
					return ParseResponse(responseBody, article);
                }
			}
			else
			{
				SupplierProviderService.LoggerService.Error(string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
			}

			return new List<ComPriceProduct>() { new ComPriceProduct(article)};
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			return ManualProductSearch(articles, cancellationToken);
		}

		private const string BaseEcomApiAddress = "https://api.elko.ru/";

		private string getEcomApiToken()
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(BaseEcomApiAddress)
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			var body = new { username = SupplierLogin, password = SupplierPassword };
			var response = client.PostAsync("api/Token/CreateToken", new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json")).Result;
			client.Dispose();
			if (response.IsSuccessStatusCode)
			{
				return JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
			}
			else
			{
				SupplierProviderService.LoggerService.Error(string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
			}

			return string.Empty;
		}

		protected override string LoginSupplierFieldName { get; } = "eComLogin";

		protected override string PasswordSupplierFieldName { get; } = "eComPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "eComInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "eComUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "eComSpecifyDeliveryDay";
	}
}