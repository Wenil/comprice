﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace eCom
{
	// ReSharper disable once InconsistentNaming
	public class eComViewModel : SupplierBaseViewModel
	{
		public eComViewModel(ISystemConfigurationService systemConfigurationService)
		{
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "eComLogin", "eComPassword");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "eComInStockDeliveryDay", "eComUnderOrderDeliveryDay"
				, "eComSpecifyDeliveryDay");
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}