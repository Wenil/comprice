﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json.Linq;

namespace Eleprod
{
	public class Eleprod : SupplierBase
	{
		private const string ApiUrl = "https://www.eleprod.ru/csv/api.php";

		private const string Key = "Nbft4gfd1fDs";

		private const string UrlParamsTemplate = "?key={0}&mode=getinfopartnumber&partnomer={1}";

		private readonly ISupplierProviderService _supplierProviderService;

		public Eleprod(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			_supplierProviderService = supplierProviderService;
		}

		public override string Name { get; } = "Eleprod";

		public override string Header { get; } = "Eleprod";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var eleprodProducts = new List<ComPriceProduct>();
			var client = new HttpClient {BaseAddress = new Uri(ApiUrl)};
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			foreach (var article in articles)
			{
				var isNeedAddEmptyProduct = true;
				var response = client.GetAsync(string.Format(UrlParamsTemplate, Key, WebUtility.UrlEncode(article)), cancellationToken).Result;
				if (response.IsSuccessStatusCode)
				{
					var responseString = response.Content.ReadAsStringAsync().Result;
					if (!string.IsNullOrEmpty(responseString))
					{
						var json = JObject.Parse(responseString);

						if (json["tovars"] is JArray tovars)
						{
							foreach (var tovar in tovars)
							{
								var eleprodItem = new ComPriceProduct(article);

								eleprodItem.Name = tovar["name"]?.ToString();
								eleprodItem.Brand = tovar["brand"]?.ToString();
								eleprodItem.Quantity = (tovar["quantity"]?.ToString()).ToInt();
								eleprodItem.Price = (tovar["price"]?.ToString()).ToDouble();

								if (Enum.TryParse(tovar["currency"]?.ToString(), out Currency currency))
									eleprodItem.Currency = currency;

								eleprodProducts.Add(eleprodItem);
								isNeedAddEmptyProduct = false;
							}
						}
					}
				}

				if (isNeedAddEmptyProduct)
					eleprodProducts.Add(new ComPriceProduct(article));
			}

			client.Dispose();
			return eleprodProducts;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

		protected override string LoginSupplierFieldName { get; } = string.Empty;

		protected override string PasswordSupplierFieldName { get; } = string.Empty;

		protected override string InStockDeliveryDaySupplierFieldName { get; } = string.Empty;

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = string.Empty;

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = string.Empty;

		public override void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
		{
		}
	}
}