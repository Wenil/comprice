﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Mege
{
	public class MegeViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public MegeViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "MegeLogin", "MegePassword");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "MegeInStockDeliveryDay", "MegeUnderOrderDeliveryDay", "MegeSpecifyDeliveryDay");
		}

		public bool IsMegePfannenbergInEuro
		{
			get => _systemConfigurationService.IsSystemConfigurationParameter("MegePfannenbergInEuro");
			set => _systemConfigurationService.SetSystemConfigurationParameter("MegePfannenbergInEuro", value);
		}

		public bool IsMegeInRub
		{
			get => _systemConfigurationService.IsSystemConfigurationParameter("MegeInRub");
			set => _systemConfigurationService.SetSystemConfigurationParameter("MegeInRub", value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}