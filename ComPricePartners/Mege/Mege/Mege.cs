﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;

namespace Mege
{
    class Mege : SupplierBase
    {
        public Mege(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
        {
        }

        private bool _pfannenbergInEuro;

        private bool _inRub;

        private const string UrlLogin = "https://www.mege.ru/?login=yes";

        private const string UrlSearch = "https://www.mege.ru/search/?q={0}";

        private const string UrlLogout = "https://www.mege.ru/?logout=yes";

        private const string MegePfannenbergInEuro = "MegePfannenbergInEuro";

        private const string MegeInRub = "MegeInRub";

        private Dictionary<string, string> LoginOnMege()
        {
            var cookies = new Dictionary<string, string>();

            var requestLogin = (HttpWebRequest)WebRequest.Create(UrlLogin);
            requestLogin.Method = WebRequestMethods.Http.Post;

            requestLogin.Host = "www.mege.ru";
            requestLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0";
            requestLogin.Accept = "*/*";
            requestLogin.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            requestLogin.Headers.Add("Accept-Encoding", "gzip, deflate");
            requestLogin.Headers.Add("X-Requested-With", "XMLHttpRequest");
            requestLogin.Headers.Add("X-Compress", "1");
            requestLogin.KeepAlive = true;
            requestLogin.AllowAutoRedirect = false;
            requestLogin.Referer = "http://www.mege.ru/";
            requestLogin.ServicePoint.Expect100Continue = false;
            requestLogin.CookieContainer = new CookieContainer();

            var postData = $"backurl=%2F&AUTH_FORM=Y&TYPE=AUTH&auth_ajax=y&USER_LOGIN={WebUtility.UrlEncode(SupplierLogin)}&USER_PASSWORD={WebUtility.UrlEncode(SupplierPassword)}";
            var data = Encoding.Default.GetBytes(postData);

            requestLogin.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            requestLogin.ContentLength = data.Length;

            using (var stream = requestLogin.GetRequestStream())
                stream.Write(data, 0, data.Length);

            var response = (HttpWebResponse)requestLogin.GetResponse();

            if (response.Cookies.Count > 0)
                cookies.Add("PHPSESSID", response.Cookies["PHPSESSID"].Value);

            response.Close();

            return cookies;
        }

        private void LogoutOnMege(Dictionary<string, string> sessionParams)
        {
            var requestLogout = (HttpWebRequest)WebRequest.Create(UrlLogout);
            requestLogout.Method = WebRequestMethods.Http.Get;

            requestLogout.Host = "www.mege.ru";
            requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0";
            requestLogout.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";
            requestLogout.KeepAlive = true;
            requestLogout.AllowAutoRedirect = true;
            requestLogout.Headers.Add("Upgrade-Insecure-Requests", "1");

            requestLogout.CookieContainer = new CookieContainer();
            var uri2 = new Uri(UrlLogout);
            requestLogout.CookieContainer.Add(uri2, new Cookie("PHPSESSID", sessionParams["PHPSESSID"]));

            var responseSearch = (HttpWebResponse)requestLogout.GetResponse();
            responseSearch.Close();
        }

        public override string Name { get; } = "Mege";

        public override string Header { get; } = "Mege";

        protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
        {
            var megeResult = new List<ComPriceProduct>();
            var sessionParam = LoginOnMege();
            if (sessionParam != null && sessionParam.Any())
            {
                foreach (var article in articles)
                {
                    var isItemAdd = false;
                    var productUrls = new List<string>();
                    var reqSearch = (HttpWebRequest)WebRequest.Create(string.Format(UrlSearch, article));
                    reqSearch.Method = WebRequestMethods.Http.Get;

                    reqSearch.Host = "www.mege.ru";
                    reqSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0";
                    reqSearch.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";
                    reqSearch.KeepAlive = true;
                    reqSearch.AllowAutoRedirect = true;
                    reqSearch.Headers.Add("Upgrade-Insecure-Requests", "1");

                    reqSearch.CookieContainer = new CookieContainer();
                    var uri2 = new Uri(string.Format(UrlSearch, article));
                    reqSearch.CookieContainer.Add(uri2, new Cookie("PHPSESSID", sessionParam["PHPSESSID"]));
                    //reqSearch.CookieContainer.Add(uri2, new Cookie("BITRIX_SM_CURRENCY_ID", "EUR"));

                    var respSearch = (HttpWebResponse)reqSearch.GetResponse();
                    using (var streamResponseSearch =
                        new StreamReader(respSearch.GetResponseStream(), Encoding.UTF8))
                    {
                        var response = streamResponseSearch.ReadToEnd();
                        if (!string.IsNullOrEmpty(response))
                        {
                            var html = new HtmlDocument();
                            html.LoadHtml(response);
                            var elementsList = html.DocumentNode.Descendants()
                                .Where(x => x.Name == "div" &&
                                            x.GetAttributeValue("class", string.Empty).Contains("catalog-tab-item")).ToList();
                            if (elementsList.Any())
                            {
                                foreach (var elementHtmlNode in elementsList)
                                {
                                    var elementArticleHtml = elementHtmlNode.Descendants()
                                        .Where(x => x.Name == "div" &&
                                                    x.GetAttributeValue("class", string.Empty).Equals("article"))
                                        .ToList();
                                    if (elementArticleHtml.Any())
                                    {
                                        var elementArticle = elementArticleHtml[0].InnerText
                                            .Replace("Артикул", string.Empty)
                                            .Replace("&nbsp;", string.Empty)
                                            .Replace("\n", string.Empty)
                                            .Replace("\t", string.Empty)
                                            .Trim();
                                        if (
                                            elementArticle.Equals(article)
                                            ||
                                            (elementArticle.EndsWith(article) && string.IsNullOrEmpty(elementArticle
                                                .Replace(article, string.Empty).Replace("0", string.Empty)))
                                        )
                                        {
                                            try
                                            {
                                                var urlDiv = elementHtmlNode.Descendants()
                                                    .Where(x => x.Name == "div" &&
                                                                x.GetAttributeValue("class", string.Empty)
                                                                    .Contains("img"))
                                                    .ToList();

                                                if (urlDiv != null && urlDiv.Any())
                                                {
                                                    var divContent = urlDiv[0];
                                                    if (divContent != null && divContent.ChildNodes.Count > 1)
                                                    {
                                                        var anhorTag = divContent.ChildNodes[1];
                                                        if (anhorTag != null)
                                                        {
                                                            var href = "https://www.mege.ru" +
                                                                       anhorTag.GetAttributeValue("href", string.Empty);
                                                            if (!string.IsNullOrEmpty(href))
                                                                productUrls.Add(href);
                                                        }
                                                    }
                                                }
                                            }
                                            catch
                                            {
                                                // ignored
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    respSearch.Close();

                    if (productUrls.Any())
                    {
                        foreach (var variableProductUrl in productUrls)
                        {
                            var reqProduct = (HttpWebRequest)WebRequest.Create(variableProductUrl);
                            reqProduct.Method = WebRequestMethods.Http.Get;

                            reqProduct.Host = "www.mege.ru";
                            reqProduct.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0";
                            reqProduct.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";
                            reqProduct.KeepAlive = true;
                            reqProduct.AllowAutoRedirect = true;
                            reqProduct.Headers.Add("Upgrade-Insecure-Requests", "1");

                            reqProduct.CookieContainer = new CookieContainer();
                            var uriProduct = new Uri(variableProductUrl);
                            reqProduct.CookieContainer.Add(uriProduct, new Cookie("PHPSESSID", sessionParam["PHPSESSID"]));
                            //reqProduct.CookieContainer.Add(uriProduct, new Cookie("CURRENCY_ID", "EUR"));

                            var respProduct = (HttpWebResponse)reqProduct.GetResponse();
                            using (var streamResponseProduct = new StreamReader(respProduct.GetResponseStream(), Encoding.UTF8))
                            {
                                var responseProduct = streamResponseProduct.ReadToEnd();
                                if (!string.IsNullOrEmpty(responseProduct))
                                {
                                    var html = new HtmlDocument();
                                    html.LoadHtml(responseProduct);

                                    try
                                    {
                                        var megeItem = new ComPriceProduct();

                                        var detailRight = html.DocumentNode.Descendants()
                                            .Where(x => x.Name == "div" && x.GetAttributeValue("class", string.Empty).Contains("text-detail"))
                                            .ToList();

                                        if (detailRight.Any())
                                        {
                                            var detailInfo = detailRight[0];
                                            if (detailInfo != null && detailInfo.ChildNodes.Count > 3)
                                            {
                                                var detailInfoLiChildNodes = detailInfo?.ChildNodes;
                                                if (detailInfoLiChildNodes != null && detailInfoLiChildNodes.Count > 0)
                                                {
                                                    try
                                                    {
                                                        var manufacturerArticle = detailInfoLiChildNodes[1].ChildNodes[2].InnerText
                                                            .Replace("Артикул", string.Empty)
                                                            .Replace("&nbsp;", string.Empty)
                                                            .Replace("\n", string.Empty)
                                                            .Replace("\t", string.Empty)
                                                            .Trim();
                                                        ;
                                                        megeItem.ManufacturerArticle = manufacturerArticle.Trim();
                                                    }
                                                    catch
                                                    {
                                                        megeItem.ManufacturerArticle = string.Empty;
                                                    }

                                                    try
                                                    {
                                                        var name = detailInfoLiChildNodes[3].ChildNodes[2].InnerText;
                                                        megeItem.Name = name.Trim();
                                                    }
                                                    catch
                                                    {
                                                        megeItem.Name = string.Empty;
                                                    }

                                                    try
                                                    {
                                                        var brand = detailInfoLiChildNodes[5].ChildNodes[1].ChildNodes[0].InnerText;
                                                        megeItem.Brand = brand.Trim();
                                                    }
                                                    catch
                                                    {
                                                        megeItem.Brand = string.Empty;
                                                    }
                                                }
                                            }
                                        }

                                        var priceDetails = html.DocumentNode.Descendants()
                                            .Where(x => x.Name == "div" && x.GetAttributeValue("class", string.Empty).Contains("price-detail"))
                                            .ToList();

                                        if (priceDetails.Any())
                                        {
                                            var detailPrice = priceDetails[0];
                                            if (detailPrice != null && detailPrice.ChildNodes.Count > 7)
                                            {
                                                var divPrice = detailPrice.ChildNodes[7];
                                                if (divPrice != null && divPrice.ChildNodes.Count > 5)
                                                {

                                                    var price = divPrice.ChildNodes[1].InnerText
                                                    .Replace("\r", string.Empty)
                                                    .Replace("\n", string.Empty)
                                                    .Replace("\t", string.Empty)
                                                    .Replace(" ", string.Empty)
                                                    .Trim();
                                                    if (!string.IsNullOrEmpty(price))
                                                    {
                                                        var priceAsDouble = price.ToDouble();
                                                        if (divPrice.ChildNodes[3].ChildNodes[1].InnerText.Contains("EUR") && priceAsDouble > 0)
                                                        {
                                                            if (megeItem.Brand.Equals("PFANNENBERG") && _pfannenbergInEuro)
                                                            {
                                                                // оставляем 
                                                                megeItem.Currency = Currency.EUR;
                                                            }
                                                            else if (megeItem.Brand.ToLower().Equals("eaton"))
                                                                priceAsDouble *= SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.EUR];
                                                            else if (megeItem.Brand.ToLower().Equals("finder"))
                                                                priceAsDouble *= SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
                                                            else
                                                                priceAsDouble = (SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.EUR] * priceAsDouble) /
                                                                                SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
                                                        }
                                                        else if (divPrice.ChildNodes[3].ChildNodes[1].InnerText.Contains("USD") && _inRub)
                                                        {
                                                            if (megeItem.Brand.Equals("Finder") || megeItem.Brand.Equals("Eaton"))
                                                                priceAsDouble *= SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
                                                        }
                                                        else
                                                        {
                                                            megeItem.Currency = Currency.RUB;
                                                        }

                                                        megeItem.Price = priceAsDouble;
                                                    }

                                                }
                                            }
                                        }

                                        var elementQtyHtml = html.DocumentNode.Descendants()
                                            .Where(x => x.Name == "table" && x.GetAttributeValue("class", string.Empty).Contains(" price-detail-table"))
                                            .ToList();

                                        if (elementQtyHtml.Any())
                                        {
                                            try
                                            {
                                                var table = elementQtyHtml[0];
                                                var quantityTr = table.ChildNodes[3];
                                                var quantityTd = quantityTr.ChildNodes[1];
                                                var quantity = quantityTd.InnerText.Replace("Москва: ", string.Empty)
                                                    .Replace("шт", string.Empty).Trim();
                                                megeItem.Quantity = quantity.ToInt();
                                            }
                                            catch (Exception exception)
                                            {
                                                megeItem.Quantity = 0;
                                                SupplierProviderService.LoggerService.Error("Ошибка при попытке получения количества MEGE " + megeItem.ManufacturerArticle, exception);
                                            }
                                        }

                                        if (!megeItem.IsEmpty())
                                        {
                                            SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(megeItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                                            megeItem.Article = article;
                                            isItemAdd = true;
                                            megeResult.Add(megeItem);
                                        }
                                    }
                                    catch
                                    {
                                        // ignored
                                    }
                                }
                            }

                            respProduct.Close();
                        }
                    }

                    if (!isItemAdd)
                        megeResult.Add(new ComPriceProduct(article));
                }

                LogoutOnMege(sessionParam);
            }

            return megeResult;
        }

        protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

        protected override string LoginSupplierFieldName { get; } = "MegeLogin";

        protected override string PasswordSupplierFieldName { get; } = "MegePassword";

        protected override string InStockDeliveryDaySupplierFieldName { get; } = "MegeInStockDeliveryDay";

        protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "MegeUnderOrderDeliveryDay";

        protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "MegeSpecifyDeliveryDay";

        public override void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
        {
            if (systemConfigurationParameters != null && systemConfigurationParameters.Any())
            {
                if (systemConfigurationParameters.ContainsKey(MegePfannenbergInEuro) && systemConfigurationParameters[MegePfannenbergInEuro] != null)
                    _pfannenbergInEuro = systemConfigurationParameters[MegePfannenbergInEuro].ToBoolean();

                if (systemConfigurationParameters.ContainsKey(MegeInRub) && systemConfigurationParameters[MegeInRub] != null)
                    _inRub = systemConfigurationParameters[MegeInRub].ToBoolean();
            }

            base.ApplySupplierConfigurationParameters(systemConfigurationParameters);
        }
    }
}