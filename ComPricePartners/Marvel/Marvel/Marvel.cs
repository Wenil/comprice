﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json.Linq;

namespace Marvel
{
	class Marvel : SupplierBase
	{
		public Marvel(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
		}

		private const int PackStatus = 1;

		private const int ResponseFormat = 1;

		private const int GetExtendedItemInfo = 0;

		private const string UrlGetItems = "https://b2b.marvel.ru/Api/GetItems";

		private const int MaxArticleForSearchRequest = 500;

		private WareItem GetWareItem(List<string> articles)
		{
			var retVAl = new WareItem {wareItems = new List<Item>()};
			foreach (var article in articles)
			{
				var item = new Item {item = WebUtility.UrlEncode(article)};
				retVAl.wareItems.Add(item);
			}

			return retVAl;
		}

		public override string Name { get; } = "marv";

		public override string Header { get; } = "Marv";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var marvels = new List<ComPriceProduct>();

            foreach(var articleBatch in articles.Batch(MaxArticleForSearchRequest))
                marvels.AddRange(ProductSearch(articleBatch.ToList(), cancellationToken));
            
			return marvels;
		}

		private List<ComPriceProduct> ProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
            var marvels = new List<ComPriceProduct>();

            List<JToken> responseArray = null;
            var postDataReqGetItems = "?user=" + SupplierLogin;
            postDataReqGetItems += "&password=" + SupplierPassword;
            postDataReqGetItems += "&secretKey=";
            postDataReqGetItems += "&packStatus=" + PackStatus;
            postDataReqGetItems += "&responseFormat=" + ResponseFormat;
            postDataReqGetItems += "&getExtendedItemInfo=" + GetExtendedItemInfo;

            var wareItem = GetWareItem(articles);
            var json = new DataContractJsonSerializer(typeof(WareItem)).ToJson(wareItem);
            postDataReqGetItems += "&Items=" + json;

            var reqGetItems = (HttpWebRequest)WebRequest.Create(UrlGetItems + postDataReqGetItems);
            reqGetItems.Method = WebRequestMethods.Http.Post;

            reqGetItems.Host = "b2b.marvel.ru";
            reqGetItems.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0";
            reqGetItems.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            reqGetItems.ContentLength = 0;
            var respGetItems = (HttpWebResponse)reqGetItems.GetResponse();
            using (var streamResponse = new StreamReader(respGetItems.GetResponseStream(), Encoding.UTF8))
            {
                var response = streamResponse.ReadToEnd();
                if (!string.IsNullOrEmpty(response) && !response.Contains("Произошла ошибка в обработке запроса") &&
                    !response.Contains("Запрос должен содержать не более чем 100 артикулов"))
                {
                    var jsonResponse = JObject.Parse(response);
                    if (jsonResponse.ContainsKey("Body") && jsonResponse["Body"] != null)
                        responseArray = jsonResponse["Body"]["CategoryItem"].ToList();
                }
            }

            if (responseArray != null)
            {
                foreach (var article in articles)
                {
                    if (cancellationToken.IsCancellationRequested)
                        throw new Exception("TerminateSearch");

                    var item = new ComPriceProduct(article);
                    var findArticle = responseArray.FindAll(r => string.Equals(r["WareArticle"].ToString(), article, StringComparison.CurrentCultureIgnoreCase));
                    if (findArticle.Count > 0)
                    {
                        foreach (var find in findArticle)
                        {
                            if (find["Dimension"] != null && find["Dimension"].ToString().ToLower().Equals("осн"))
                            {
                                item.Name = find["WareFullName"].ToString();
                                item.Brand = find["WareVendor"].ToString();
                                item.Quantity = find["AvailableForB2BOrderQty"]
                                    .ToString()
                                    .Replace("+", "")
                                    .ToInt();

                                if (find["WarePriceCurrency"].ToString() == "USD")
                                    item.Price = find["WarePrice"].ToString().ToDouble();

                                else if (find["WarePriceUSD"] != null && !string.IsNullOrEmpty(find["WarePriceUSD"].ToString()) && !find["WarePriceUSD"].ToString().StartsWith("0"))
                                {
                                    var price = find["WarePriceRUB"].ToString().ToDouble();
                                    item.Price = price / SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
                                }
                                else if (find["WarePriceRUB"] != null && !string.IsNullOrEmpty(find["WarePriceRUB"].ToString()) && !find["WarePriceRUB"].ToString().StartsWith("0"))
                                {
                                    var price = find["WarePriceRUB"].ToString().ToDouble();
                                    item.Price = price / SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
                                }
                                else
                                {
                                    var price = find["WarePrice"].ToString().ToDouble();
                                    item.Price = price / SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
                                }

                                break;
                            }
                            else if (find["ExternalItemIdError"] != null && find["ExternalItemIdError"].ToString().Equals("Артикул недоступен в B2B"))
                                item.Name = find["ExternalItemIdError"].ToString();
                        }
                    }

                    SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                    marvels.Add(item);
                }
            }

            return marvels;
        }


        protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

		protected override string LoginSupplierFieldName { get; } = "MarvelLogin";

		protected override string PasswordSupplierFieldName { get; } = "MarvelPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "MarvelInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "MarvelUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "MarvelSpecifyDeliveryDay";
	}


	[DataContract]
	internal class WareItem
	{
		[DataMember(Name = "WareItem")] internal List<Item> wareItems;
	}

	[DataContract]
	internal class Item
	{
		[DataMember(Name = "ItemId")] internal string item;
	}
}