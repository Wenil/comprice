﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;

namespace Comptek
{
	class Comptek : SupplierBase
	{
		public Comptek(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
		}

		private const string UrlMain = "https://comptek.ru/personal/";
		private const string UrlAuthentication = "https://comptek.ru/personal/auth.xhtml ";
		private const string UrlSearch = "https://comptek.ru/search/search_body.xhtml";

		private const string PostSearchDataFormat = "q={0}&mode=2&param_[a]=1&param_[z]=0&param_[d]=0&param_[s]=0&filtr[dir_sort]=down&filtr[curTypeSort]=ts_1&filtr[avail_goods]=0";

		private string LoginOnComptek()
		{
			var sessionId = string.Empty;

			using (var client = new WebClient())
			{
				client.OpenRead(UrlMain);
				var cookie = client.ResponseHeaders.Get("Set-Cookie");
				if (!string.IsNullOrEmpty(cookie))
				{
					sessionId = cookie.Substring(cookie.IndexOf("PHPSESSID="), cookie.IndexOf(";"));
					sessionId = sessionId.Replace("PHPSESSID=", string.Empty);
				}
			}

			if (!string.IsNullOrEmpty(sessionId))
			{
				var requestAuthentication = (HttpWebRequest) WebRequest.Create(UrlAuthentication);
				requestAuthentication.Method = WebRequestMethods.Http.Post;

				requestAuthentication.Host = "comptek.ru";
				requestAuthentication.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0";
				requestAuthentication.Accept = "*/*";
				requestAuthentication.KeepAlive = true;
				requestAuthentication.AllowAutoRedirect = false;
				requestAuthentication.Referer = "http://www.comptek.ru/personal/auth.xhtml";

				requestAuthentication.CookieContainer = new CookieContainer();
				var uriAuth = new Uri(UrlAuthentication);
				requestAuthentication.CookieContainer.Add(uriAuth, new Cookie("PHPSESSID", sessionId));

				var postData = "login=" + SupplierLogin;
				postData += "&password=" + SupplierPassword;
				var data = Encoding.Default.GetBytes(postData);

				requestAuthentication.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
				requestAuthentication.ContentLength = data.Length;

				using (var stream = requestAuthentication.GetRequestStream())
					stream.Write(data, 0, data.Length);

				var responseAuthentication = (HttpWebResponse) requestAuthentication.GetResponse();
				responseAuthentication.Close();

				if (responseAuthentication.StatusCode == HttpStatusCode.OK)
					return sessionId;
			}

			return string.Empty;
		}

		private void LogoutOnComptek(string sessionId)
		{
			var requestLogout = (HttpWebRequest) WebRequest.Create(UrlMain + "?action=logout");
			requestLogout.Method = WebRequestMethods.Http.Get;

			requestLogout.CookieContainer = new CookieContainer();
			var uriSearch = new Uri(UrlSearch);
			requestLogout.CookieContainer.Add(uriSearch, new Cookie("PHPSESSID", sessionId));

			requestLogout.Host = "comptek.ru";
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0";
			requestLogout.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = false;

			var responseLogout = (HttpWebResponse) requestLogout.GetResponse();
			responseLogout.Close();
		}

		public override string Name { get; } = "comp";

		public override string Header { get; } = "Comp";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var comptekResult = new List<ComPriceProduct>();
			var sessionId = LoginOnComptek();
			if (!string.IsNullOrEmpty(sessionId))
			{
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					var reqSearch = (HttpWebRequest) WebRequest.Create(UrlSearch);
					reqSearch.Method = WebRequestMethods.Http.Post;

					reqSearch.CookieContainer = new CookieContainer();
					var uriSearch = new Uri(UrlSearch);
					reqSearch.CookieContainer.Add(uriSearch, new Cookie("PHPSESSID", sessionId));

					reqSearch.Host = "comptek.ru";
					reqSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0";
					reqSearch.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
					reqSearch.KeepAlive = true;
					reqSearch.AllowAutoRedirect = false;

					var data = Encoding.Default.GetBytes(string.Format(PostSearchDataFormat, article));

					reqSearch.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
					reqSearch.ContentLength = data.Length;

					using (var stream = reqSearch.GetRequestStream())
						stream.Write(data, 0, data.Length);

					var respSearch = (HttpWebResponse) reqSearch.GetResponse();
					using (var streamResponseSearch = new StreamReader(respSearch.GetResponseStream(), Encoding.Default))
					{
						var vendor = string.Empty;
						var responseHtml = streamResponseSearch.ReadToEnd();
						if (!string.IsNullOrEmpty(responseHtml))
						{
							var addProduction = false;
							var html = new HtmlDocument();
							html.LoadHtml(responseHtml);
							foreach (var node in html.DocumentNode.Descendants().Where(x => x.Name == "tr"))
							{
								var item = new ComPriceProduct();
								var td = node.Descendants().Where(x => x.Name == "#text")
									.Where(x => !x.InnerHtml.Contains("\\r\\n")).ToList();
								var count = td.Count;
								if ((td[1].InnerText + td[2].InnerText.Trim()).Equals(article))
								{
									if (count == 19)
									{
										item.Article = td[1].InnerText;
										item.Name = td[3].InnerText.Trim();
										item.Brand = vendor;
										item.Quantity = td[5].InnerText
											.Replace("\r", string.Empty)
											.Replace("\n", string.Empty)
											.Replace("\t", string.Empty)
											.Trim()
											.ToInt();
										var cost = td[10].InnerText.Replace(" руб", string.Empty).ToDouble() / SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
										item.Price = cost;
										if (td[11].InnerText.Contains("USD"))
										{
											var costUsd = td[10].InnerText.Replace("USD", string.Empty).Trim().ToDouble();
											item.Price = costUsd;
										}
									}
									else if (count == 20)
									{
										item.Article = td[1].InnerText;
										item.Name = td[3].InnerText.Trim();
										item.Brand = vendor;
										item.Quantity = td[5].InnerText
											.Replace("\r", string.Empty)
											.Replace("\n", string.Empty)
											.Replace("\t", string.Empty)
											.Trim()
											.ToInt();
										var cost = td[11].InnerText.Replace(" руб", string.Empty).ToDouble() / SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
										item.Price = cost;
										if (td[11].InnerText.Contains("USD"))
										{
											var costUsd = td[11].InnerText.Replace("USD", string.Empty).Trim().ToDouble();
											item.Price = costUsd;
										}
									}
								}

								if (count == 133 || count == 137 || count == 146 || count == 148 || count == 10 || count == 9)
								{
									if (!string.IsNullOrEmpty(td[4].InnerText.Replace("\r\n", string.Empty)))
										vendor = td[4].InnerText;
								}

								if (!item.IsEmpty() && !string.IsNullOrEmpty(item.Name))
								{
									foreach (var comptekResultItem in comptekResult)
									{
										if (comptekResultItem.Article.Equals(item.Article))
										{
											if (item.Price < comptekResultItem.Price)
											{
												if (item.Quantity == comptekResultItem.Quantity)
													comptekResultItem.Price = item.Price;
												else if (item.Quantity > 0)
												{
													comptekResultItem.Quantity = item.Quantity;
													comptekResultItem.Price = item.Price;
												}
											}
											else
											{
												if (item.Quantity > 0 && comptekResultItem.Quantity < 1)
												{
													comptekResultItem.Quantity = item.Quantity;
													comptekResultItem.Price = item.Price;
												}
											}

											addProduction = true;
										}
									}

									if (!addProduction)
									{
										SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
										comptekResult.Add(item);
									}

									addProduction = true;
								}
							}

							if (!addProduction)
							{
								var comptekItem = new ComPriceProduct(article);
								SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(comptekItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
								comptekResult.Add(comptekItem);
							}
						}
					}

					respSearch.Close();
				}
			}

			LogoutOnComptek(sessionId);
			return comptekResult;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

		protected override string LoginSupplierFieldName { get; } = "ComptekLogin";

		protected override string PasswordSupplierFieldName { get; } = "ComptekPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "ComptekInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "ComptekUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "ComptekSpecifyDeliveryDay";
	}
}