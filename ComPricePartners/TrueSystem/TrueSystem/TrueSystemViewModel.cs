﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace TrueSystem
{
	public class TrueSystemViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public TrueSystemViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "TrueSystemInStockDeliveryDay", "TrueSystemUnderOrderDeliveryDay", "TrueSystemSpecifyDeliveryDay");
		}

		public string Markup
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("TrueSystemMarkup");
			set => _systemConfigurationService.SetSystemConfigurationParameter("TrueSystemMarkup", value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; } = null;

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}