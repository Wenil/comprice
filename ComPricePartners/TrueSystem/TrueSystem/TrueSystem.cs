﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Suppliers;

namespace TrueSystem
{
	public class TrueSystem : SupplierBase
	{
		public TrueSystem(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
		}

		private const string UrlFileDownload = "https://www.truesystem.ru/upload/price/TrueSystem-price.xls";

		private double _markup = 0.0;

		private const string TrueSystemMarkup = "TrueSystemMarkup";

		private string DownloadPrice()
		{
			return SupplierProviderService.PriceListDownloadService
				.DownloadPriceList((HttpWebRequest) WebRequest.Create(UrlFileDownload), DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
		}

		public override string Name { get; } = "TrueSystem";

		public override string Header { get; } = "TrueSystem";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken) => AutomaticProductSearch(articles, cancellationToken);

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest {FilePath = filePath, Name = 3, Price = new List<int> {5}, Quantity = new List<int> {4}, Article = 2};
				var findProductByArticles = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						if (findProductByArticle.FindArticle != null && findProductByArticle.FindArticle.Equals(findProductByArticle.Article))
						{
							var name = findProductByArticle.Name.ToString();
							name = name.Trim();

							var quantity = 0;
							foreach (var quantityFindValue in findProductByArticle.Quantity)
							{
								if (quantityFindValue != null)
								{
									var quantityString = quantityFindValue.ToString();
									if (quantityString.Contains("+"))
									{
										var count = quantityString.Count(symbol => symbol.Equals('+'));
										quantity += 2 * count;
									}
								}
							}

							var price = 0.0;

							if (findProductByArticle.Price.Any())
							{
								if (findProductByArticle.Price[0] != null)
								{
									var priceString = findProductByArticle.Price[0].ToString().Replace(" ", string.Empty).Replace("USD", string.Empty).Replace("уе", string.Empty).Trim();
									price = priceString.ToDouble();

									if (_markup > 0)
										price *= 1 + (_markup / 100);
								}
							}


							var trueSystem = new ComPriceProduct(findProductByArticle.Article, name, string.Empty, quantity, price);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(trueSystem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(trueSystem);
						}
						else
						{
							var trueSystem = new ComPriceProduct(findProductByArticle.Article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(trueSystem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(trueSystem);
						}
					}
				}
			}

			return result;
		}

		protected override string LoginSupplierFieldName { get; } = string.Empty;

		protected override string PasswordSupplierFieldName { get; } = string.Empty;

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "TrueSystemInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "TrueSystemUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "TrueSystemSpecifyDeliveryDay";

		public override void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
		{
			if (systemConfigurationParameters != null && systemConfigurationParameters.ContainsKey(TrueSystemMarkup) && systemConfigurationParameters[TrueSystemMarkup] != null)
				_markup = systemConfigurationParameters[TrueSystemMarkup].ToDouble();

			base.ApplySupplierConfigurationParameters(systemConfigurationParameters);
		}
	}
}