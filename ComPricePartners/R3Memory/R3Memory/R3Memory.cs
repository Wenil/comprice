﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Com.Price.Partners;

namespace R3Memory
{
    public class R3Memory : PartnerAbstract
    {
        private string _login = "";
        private string _password = "";

        private const string R3MemoryLoginKey = "3RMemoryLogin";
        private const string R3MemoryPasswordKey = "3RMemoryPassword";


        private const string UrlLogin = "http://3rmemory.ru/partners/auth/";
        private const string UrlSearch = "http://3rmemory.ru/cat/search/?product={0}";
        private const string UrlLogout = "http://3rmemory.ru/logout.php";

        public override string GetColunHeader()
        {
            return "3RMem";
        }

        public override string GetColunName()
        {
            return "3RMem";
        }

        public override List<Dictionary<string, object>> GetItem(string article)
        {
            throw new NotImplementedException();
        }

        public override List<Dictionary<string, object>> GetItems(List<string> articles)
        {
            var r3MemoryResults = new List<Dictionary<string, object>>();

            var sessionParams = LoginOnR3Memory();

            if (sessionParams.Any())
            {
                foreach (var article in articles)
                {
                    try
                    {
                        var reqProduct = (HttpWebRequest)WebRequest.Create(string.Format(UrlSearch, WebUtility.UrlEncode(article)));
                        reqProduct.Method = WebRequestMethods.Http.Get;

                        reqProduct.Host = "3rmemory.ru";
                        reqProduct.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0";
                        reqProduct.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                        reqProduct.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
                        reqProduct.Headers.Add("Accept-Encoding", "gzip, deflate");
                        reqProduct.KeepAlive = true;
                        //reqProduct.AllowAutoRedirect = true;
                        reqProduct.Headers.Add("Upgrade-Insecure-Requests", "1");

                        //reqProduct.Headers.Add("Proxy-Authorization", "88b0b458b0fcf79172e5e9aac10faf9abcf500e4c7f53573e20cd218df89b9ae6e0891fe09bc549d");

                        reqProduct.Credentials = new NetworkCredential(WebUtility.UrlEncode(_login), _password);
                        reqProduct.PreAuthenticate = true;

                        reqProduct.CookieContainer = new CookieContainer();
                        var uriProduct = new Uri(string.Format(UrlSearch, WebUtility.UrlEncode(article)));
                        reqProduct.CookieContainer.Add(uriProduct, new Cookie("PHPSESSID", sessionParams["PHPSESSID"]));

                        var respProduct = (HttpWebResponse)reqProduct.GetResponse();
                        using (var streamResponseProduct = new StreamReader(respProduct.GetResponseStream(),
                                    Encoding.GetEncoding("Windows-1251")))
                        {
                            var responseProduct = streamResponseProduct.ReadToEnd();
                            if (!string.IsNullOrEmpty(responseProduct))
                            {
                                var html = new HtmlDocument();
                                html.LoadHtml(responseProduct);

                                var r3Item = new Dictionary<string, object> { { "search", article } };

                                var elementsList = html.DocumentNode.Descendants().Where(x => x.Name == "h3" && x.GetAttributeValue("class", "").Contains("font-family-system")).ToList();
                                if (elementsList.Any())
                                {
                                    r3Item.Add("Название", elementsList[0].InnerText.Trim());
                                }
                                if (!r3Item.ContainsKey("Название"))
                                {
                                    r3Item.Add("Название", "");
                                }

                                var tableList = html.DocumentNode.Descendants().Where(x => x.Name == "table" && x.GetAttributeValue("class", "").Contains(" table-striped")).ToList();
                                if (tableList.Count > 1)
                                {
                                    var firstTable = tableList[0].Descendants().Where(x => x.Name == "tr").ToList();
                                    var secondTable = tableList[1].Descendants().Where(x => x.Name == "tr").ToList();

                                    if (firstTable.Any())
                                    {
                                        foreach (var tableTr in firstTable)
                                        {
                                            if (tableTr.FirstChild.InnerText.Equals("Производитель"))
                                            {
                                                r3Item.Add("Производитель", tableTr.LastChild.InnerText.Trim());
                                            }
                                        }
                                    }

                                    if (secondTable.Any())
                                    {
                                        foreach (var tableTr in secondTable)
                                        {
                                            if (tableTr.FirstChild.InnerText.Equals("Стоимость"))
                                            {
                                                r3Item.Add("Цена", tableTr.LastChild.InnerText.Replace("$", "").Trim());
                                            }

                                            if (tableTr.FirstChild.InnerText.Equals("В наличии"))
                                            {
                                                r3Item.Add("Кол-во", tableTr.LastChild.InnerText.Replace("шт", "").Trim());
                                            }
                                        }
                                    }
                                }
                                if (!r3Item.ContainsKey("Производитель"))
                                {
                                    r3Item.Add("Производитель", "");
                                }

                                if (!r3Item.ContainsKey("Цена"))
                                {
                                    r3Item.Add("Цена", "");
                                }
                                if (!r3Item.ContainsKey("Кол-во"))
                                {
                                    r3Item.Add("Кол-во", "");
                                }
                                r3MemoryResults.Add(r3Item);
                            }
                        }

                        respProduct.Close();
                    }
                    catch
                    {
                        var r3Item =
                            new Dictionary<string, object>
                                {
                                    {"search", article},
                                    {"Название", ""},
                                    {"Производитель", ""},
                                    {"Кол-во", ""},
                                    {"Цена", ""}
                                };
                        r3MemoryResults.Add(r3Item);
                    }

                }
                LogoutOnR3Memory(sessionParams);
            }

            return r3MemoryResults;
        }

        public override List<Dictionary<string, object>> GetItemsFromFile(List<string> articles)
        {
            throw new NotImplementedException();
        }

        public override void SetPartnersParam(Dictionary<string, object> configList)
        {
            if (configList != null && configList.Any())
            {
                if (configList.ContainsKey(R3MemoryLoginKey) && configList[R3MemoryLoginKey] != null)
                {
                    _login = configList[R3MemoryLoginKey].ToString();
                }
                if (configList.ContainsKey(R3MemoryPasswordKey) && configList[R3MemoryPasswordKey] != null)
                {
                    _password = configList[R3MemoryPasswordKey].ToString();
                }
            }
        }

        private Dictionary<string, string> LoginOnR3Memory()
        {
            var cookies = new Dictionary<string, string>();

            var reqAuth = (HttpWebRequest)WebRequest.Create(UrlLogin);
            reqAuth.Method = WebRequestMethods.Http.Post;

            reqAuth.Host = "3rmemory.ru";
            reqAuth.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0";
            reqAuth.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            reqAuth.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            reqAuth.Headers.Add("Accept-Encoding", "gzip, deflate");
            //reqAuth.Headers.Add("X-Requested-With", "XMLHttpRequest");
            //reqAuth.Headers.Add("Proxy-Authorization", "88b0b458b0fcf79172e5e9aac10faf9abcf500e4c7f53573e20cd218df89b9ae6e0891fe09bc549d");
            reqAuth.Headers.Add("X-Compress", "1");
            reqAuth.KeepAlive = true;
            reqAuth.AllowAutoRedirect = false;
            reqAuth.Referer = "http://3rmemory.ru/partners/auth/";
            reqAuth.ServicePoint.Expect100Continue = false;

            reqAuth.Headers.Add("Upgrade-Insecure-Requests", "1");

            reqAuth.Credentials = new NetworkCredential(WebUtility.UrlEncode(_login), _password);
            reqAuth.PreAuthenticate = true;

            var postData = string.Format("login={0}&password={1}&loginform=", WebUtility.UrlEncode(_login), _password);
            var data = Encoding.Default.GetBytes(postData);

            reqAuth.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            reqAuth.ContentLength = data.Length;

            using (var stream = reqAuth.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var resp = (HttpWebResponse)reqAuth.GetResponse();

            var responseCookies = resp.Headers["Set-Cookie"];
            if (responseCookies != null)
            {
                var sessionId = responseCookies.Substring(responseCookies.IndexOf("PHPSESSID=", StringComparison.Ordinal), responseCookies.IndexOf(";", StringComparison.Ordinal));
                sessionId = sessionId.Replace("PHPSESSID=", "");
                cookies.Add("PHPSESSID", sessionId);
            }
            resp.Close();

            return cookies;
        }

        private void LogoutOnR3Memory(Dictionary<string, string> sessionParams)
        {
            var reqSearch = (HttpWebRequest)WebRequest.Create(UrlLogout);
            reqSearch.Method = WebRequestMethods.Http.Get;

            reqSearch.Host = "3rmemory.ru";
            reqSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0";
            reqSearch.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            reqSearch.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            reqSearch.Headers.Add("Accept-Encoding", "gzip, deflate");
            reqSearch.KeepAlive = true;
            reqSearch.AllowAutoRedirect = false;
            reqSearch.Headers.Add("Upgrade-Insecure-Requests", "1");

            reqSearch.Credentials = new NetworkCredential(WebUtility.UrlEncode(_login), _password);
            reqSearch.PreAuthenticate = true;

            reqSearch.CookieContainer = new CookieContainer();
            var uri2 = new Uri(UrlLogout);
            reqSearch.CookieContainer.Add(uri2, new Cookie("PHPSESSID", sessionParams["PHPSESSID"]));

            var respSearch = (HttpWebResponse)reqSearch.GetResponse();
            respSearch.Close();
        }
    }
}
