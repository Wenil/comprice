﻿namespace R3Memory
{
    partial class tbR3Memory
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbR3MemoryPassword = new System.Windows.Forms.TextBox();
            this.tbR3MemoryLogin = new System.Windows.Forms.TextBox();
            this.lbR3MemoryPassword = new System.Windows.Forms.Label();
            this.lbR3MemoryLogin = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbR3MemoryPassword
            // 
            this.tbR3MemoryPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbR3MemoryPassword.Location = new System.Drawing.Point(66, 43);
            this.tbR3MemoryPassword.MaxLength = 100;
            this.tbR3MemoryPassword.Name = "tbR3MemoryPassword";
            this.tbR3MemoryPassword.PasswordChar = '*';
            this.tbR3MemoryPassword.Size = new System.Drawing.Size(431, 34);
            this.tbR3MemoryPassword.TabIndex = 35;
            // 
            // tbR3MemoryLogin
            // 
            this.tbR3MemoryLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbR3MemoryLogin.Location = new System.Drawing.Point(66, 3);
            this.tbR3MemoryLogin.MaxLength = 100;
            this.tbR3MemoryLogin.Name = "tbR3MemoryLogin";
            this.tbR3MemoryLogin.Size = new System.Drawing.Size(431, 34);
            this.tbR3MemoryLogin.TabIndex = 34;
            // 
            // lbR3MemoryPassword
            // 
            this.lbR3MemoryPassword.AutoSize = true;
            this.lbR3MemoryPassword.Location = new System.Drawing.Point(3, 51);
            this.lbR3MemoryPassword.Name = "lbR3MemoryPassword";
            this.lbR3MemoryPassword.Size = new System.Drawing.Size(57, 17);
            this.lbR3MemoryPassword.TabIndex = 33;
            this.lbR3MemoryPassword.Text = "Пароль";
            // 
            // lbR3MemoryLogin
            // 
            this.lbR3MemoryLogin.AutoSize = true;
            this.lbR3MemoryLogin.Location = new System.Drawing.Point(3, 10);
            this.lbR3MemoryLogin.Name = "lbR3MemoryLogin";
            this.lbR3MemoryLogin.Size = new System.Drawing.Size(47, 17);
            this.lbR3MemoryLogin.TabIndex = 32;
            this.lbR3MemoryLogin.Text = "Логин";
            // 
            // tbR3Memory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tbR3MemoryPassword);
            this.Controls.Add(this.tbR3MemoryLogin);
            this.Controls.Add(this.lbR3MemoryPassword);
            this.Controls.Add(this.lbR3MemoryLogin);
            this.Name = "tbR3Memory";
            this.Size = new System.Drawing.Size(500, 150);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbR3MemoryPassword;
        private System.Windows.Forms.TextBox tbR3MemoryLogin;
        private System.Windows.Forms.Label lbR3MemoryPassword;
        private System.Windows.Forms.Label lbR3MemoryLogin;
    }
}
