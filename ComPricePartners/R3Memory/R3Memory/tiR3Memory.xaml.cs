﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Com.Price.Partners;

namespace R3Memory
{
    /// <summary>
    /// Логика взаимодействия для tiR3Memory.xaml
    /// </summary>
    public partial class tiR3Memory : UserControl, IPartnerConfig
    {
        private const string R3MemoryLoginKey = "3RMemoryLogin";
        private const string R3MemoryPasswordKey = "3RMemoryPassword";

        public tiR3Memory()
        {
            InitializeComponent();
        }

        public Dictionary<string, object> GetPartnerConfigParametrs()
        {
            var megeConfig = new Dictionary<string, object>
            {
                {R3MemoryLoginKey, tbR3MemoryLogin.Text},
                {R3MemoryPasswordKey, pbR3MemoryPassword.Password}
            };
            return megeConfig;
        }

        public void SetPartnerConfigParametrs(Dictionary<string, object> systemConfig)
        {
            if (systemConfig != null && systemConfig.Any())
            {
                if (systemConfig.ContainsKey(R3MemoryLoginKey) && systemConfig[R3MemoryLoginKey] != null)
                {
                    tbR3MemoryLogin.Text = systemConfig[R3MemoryLoginKey].ToString();
                }
                if (systemConfig.ContainsKey(R3MemoryPasswordKey) && systemConfig[R3MemoryPasswordKey] != null)
                {
                    pbR3MemoryPassword.Password = systemConfig[R3MemoryPasswordKey].ToString();
                }
            }
        }
    }
}
