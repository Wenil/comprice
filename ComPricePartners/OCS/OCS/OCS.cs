﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json.Linq;

namespace OCS
{
	class OCS : SupplierBase
	{
		public OCS(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			PriceListFileExtension = ".xlsm";
		}

		private const string UrlGetProducts = "https://b2b.ocs.ru/api/productCatalog/getProducts";

		private const string UrlPriceDownload = "https://b2b.ocs.ru/api/reports/stock/file?currency=0";

		private string DownloadPrice()
		{
			var priceFilePath = string.Empty;
			var cookies = LoginOnOcs();
			if (cookies.Any())
			{
				var webRequestDownloadFile = (HttpWebRequest) WebRequest.Create(UrlPriceDownload);
				webRequestDownloadFile.Method = WebRequestMethods.Http.Get;
				webRequestDownloadFile.Host = "b2b.ocs.ru";
				webRequestDownloadFile.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
				webRequestDownloadFile.Accept = "application/json, text/plain, */*";
				webRequestDownloadFile.KeepAlive = true;
				webRequestDownloadFile.AllowAutoRedirect = false;
				webRequestDownloadFile.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
				webRequestDownloadFile.Headers.Add("Accept-Encoding", "gzip, deflate, br");

				webRequestDownloadFile.CookieContainer = new CookieContainer();
				var uriPriceDownload = new Uri(UrlPriceDownload);

				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("token", cookies["token"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("machineId", cookies["machineId"]));

				webRequestDownloadFile.Headers.Add("Authorization", WebUtility.UrlEncode("Bearer " + cookies["token"]));
				webRequestDownloadFile.AllowAutoRedirect = false;

				var directory = DirectoryPathForDownloadFile;

				foreach (var file in Directory.GetFiles(directory))
					File.Delete(file);

				priceFilePath = SupplierProviderService.PriceListDownloadService.DownloadPriceList(webRequestDownloadFile, directory, PriceListFileName, PriceListFileExtension, true);

				LogoutOnOCS(cookies);

				return priceFilePath;
			}

			return priceFilePath;
		}

		private const string UrlLogin = "https://b2b.ocs.ru/api/auth/login";

		private Dictionary<string, string> LoginOnOcs()
		{
			var sessionParam = new Dictionary<string, string>();

			var requestLogin = (HttpWebRequest) WebRequest.Create(UrlLogin);
			requestLogin.Method = WebRequestMethods.Http.Post;

			requestLogin.Host = "b2b.ocs.ru";
			requestLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
			requestLogin.Accept = "application/json, text/plain, */*";
			requestLogin.KeepAlive = true;
			requestLogin.AllowAutoRedirect = false;
			requestLogin.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

			var jObject = new JObject
			{
				{"rememberMe", "true"},
				{"username", SupplierLogin},
				{"password", SupplierPassword}
			}.ToString();

			var postDataBytes = Encoding.UTF8.GetBytes(jObject);

			requestLogin.ContentType = "application/json;charset=utf-8";
			requestLogin.ContentLength = postDataBytes.Length;

			using (var streamBindGrid = requestLogin.GetRequestStream())
				streamBindGrid.Write(postDataBytes, 0, postDataBytes.Length);

			var responseLogin = (HttpWebResponse) requestLogin.GetResponse();

			using (var streamResponseSearch = new StreamReader(responseLogin.GetResponseStream(), Encoding.UTF8))
			{
				var responseJson = streamResponseSearch.ReadToEnd();
				var responseJObject = JObject.Parse(responseJson);
				if (responseJObject != null)
				{
					sessionParam.Add("token", responseJObject["token"].ToString());
					sessionParam.Add("machineId", responseJObject["machineId"].ToString());
				}
			}

			return sessionParam;
		}

		private const string UrlLogout = "https://b2b.ocs.ru/api/auth/logout";

		private void LogoutOnOCS(Dictionary<string, string> sessionParam)
		{
			var requestLogout = (HttpWebRequest) WebRequest.Create(UrlLogout);
			requestLogout.Method = WebRequestMethods.Http.Get;
			requestLogout.CookieContainer = new CookieContainer();

			var uriRequestLogout = new Uri(UrlLogout);
			requestLogout.CookieContainer.Add(uriRequestLogout, new Cookie("token", sessionParam["token"]));
			requestLogout.CookieContainer.Add(uriRequestLogout, new Cookie("machineId", sessionParam["machineId"]));

			requestLogout.Headers.Add("Authorization", WebUtility.UrlEncode("Bearer " + sessionParam["token"]));
			requestLogout.Host = "b2b.ocs.ru";
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
			requestLogout.Accept = "application/json, text/plain, */*";
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = false;
			requestLogout.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

			var respLogout = (HttpWebResponse) requestLogout.GetResponse();

			respLogout.Close();
		}

		public override string Name { get; } = "ocs";

		public override string Header { get; } = "OCS";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var threadName = Thread.CurrentThread.Name;
			var ocsItems = new List<ComPriceProduct>();
			var authStart = DateTime.Now;
			var session = LoginOnOcs();

			Console.WriteLine(@"Поток " + threadName + @" Время авторизации " + (DateTime.Now - authStart));
			if (session.Count == 2)
			{
				var countArticles = 0;
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					var searchStart = DateTime.Now;
					var requestGetProducts = (HttpWebRequest) WebRequest.Create(UrlGetProducts);
					requestGetProducts.Method = WebRequestMethods.Http.Post;
					requestGetProducts.Host = "b2b.ocs.ru";
					requestGetProducts.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
					requestGetProducts.Accept = "application/json, text/plain, */*";
					requestGetProducts.KeepAlive = true;
					requestGetProducts.AllowAutoRedirect = false;
					requestGetProducts.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
					requestGetProducts.Headers.Add("Accept-Encoding", "gzip, deflate, br");
					requestGetProducts.Headers.Add("Authorization", WebUtility.UrlEncode("Bearer " + session["token"]));
					requestGetProducts.CookieContainer = new CookieContainer();
					requestGetProducts.AutomaticDecompression = DecompressionMethods.GZip;

					var uriGetProducts = new Uri(UrlGetProducts);
					requestGetProducts.CookieContainer.Add(uriGetProducts, new Cookie("token", session["token"]));
					requestGetProducts.CookieContainer.Add(uriGetProducts, new Cookie("machineId", session["machineId"]));

					requestGetProducts.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

					var postDataBytes = Encoding.UTF8.GetBytes(ComposeSearchRequestBody(article));

					requestGetProducts.ContentType = "application/json;charset=utf-8";
					requestGetProducts.ContentLength = postDataBytes.Length;

					using (var streamGetProducts = requestGetProducts.GetRequestStream())
						streamGetProducts.Write(postDataBytes, 0, postDataBytes.Length);

					var responseGetProducts = (HttpWebResponse) requestGetProducts.GetResponse();
					using (var streamGetProducts = new StreamReader(responseGetProducts.GetResponseStream()))
					{
						var responseString = streamGetProducts.ReadToEnd();
						var responseJson = JObject.Parse(responseString);
						if (responseJson != null)
						{
							var item = new ComPriceProduct();

							foreach (var responseJsonItem in responseJson["items"])
							{
								if (responseJsonItem != null)
								{
									var partNumber = responseJsonItem["catalogInfo"]["partNumber"].Value<string>();

									if (partNumber.Equals(article) || partNumber.Equals(article + "#ACB"))
									{
										var name = responseJsonItem["catalogInfo"]["itemNameDisplay"].Value<string>();
										var brand = responseJsonItem["catalogInfo"]["producer"].Value<string>();

										var price = 0.0;
										if (responseJsonItem["priceInfo"].HasValues)
										{
											price = responseJsonItem["priceInfo"]["pricelistPrice"]["baseValue"]
												.Value<float>();

											if (responseJsonItem["priceInfo"]["pricelistPrice"]["currencyCode"]
												.Value<string>().Equals("RUR"))
												price /= SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
										}

										var quantity = responseJsonItem["stockInfo"]["groups"][0]["availableQuantity"]["quantityDisplay"].Value<int>();
										quantity += responseJsonItem["stockInfo"]["groups"][1]["availableQuantity"]["quantityDisplay"].Value<int>();

										item.Article = article;
										item.Name = name;
										item.Brand = brand;
										item.Quantity = quantity;
										item.Price = price;

										break;
									}
								}
							}

							if (item.IsEmpty())
								item.Article = article;

							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							ocsItems.Add(item);
						}
					}

					responseGetProducts.Close();
					Console.WriteLine(@"Поток " + threadName + @" Артикул № " + (countArticles++) + @" Время поиска " + (DateTime.Now - searchStart));
				}

				LogoutOnOCS(session);
			}

			return ocsItems;
		}

		private string ComposeSearchRequestBody(string article)
		{
			return new JObject
			{
				{"getCount", "true"},
				{"applyDiscountB2b", "true"},
				{"equipmentCodeList", string.Empty},
				{"goodsExistence", "false"},
				{
					"goodsMode",
					new JObject
					{
						{"catalog", "false"}, {"regular", "true"}, {"sale", "true"}, {"uncondition", "false"}
					}
				},
				{"groupByEquipment", "false"},
				{"itemAccess", "1"},
				{"mustKeepEndUserPrice", "false"},
				{"pageNumber", "1"},
				{"pageSize", "100"},
				{"priceCurrency", "USD"},
				{"priceFrom", string.Empty},
				{"priceTo", string.Empty},
				{"producerList", new JArray()},
				{"promoCode", string.Empty},
				{"reservePlace", "4"},
				{"searchString", article},
				{"shipmentCity", "Москва"},
				{"sortColumn", "ITEMNAME"},
				{"sortDirection", "asc"},
				{
					"stockLocationIds",
					new JArray
					{
						"Санкт-Петербург", "Москва", "INTERNALMOVEMENT;Санкт-Петербург",
						"INTERNALMOVEMENT;Москва", "FREENEARTR;FREELOCALPURCH"
					}
				},
				{"substandardCategoryList", new JArray()}
			}.ToString();
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest {FilePath = filePath, Name = 6, Manufacturer = 3, Currency = 9, FindLookIn = 2, FindLookAt = 1, Article = 5};

				excelProcessingRequest.SheetsNumberToSearch.Add(2);
				excelProcessingRequest.Quantity.Add(10);
				excelProcessingRequest.Quantity.Add(11);
				excelProcessingRequest.Price.Add(8);

				var findProductByArticles = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						var findArticle = findProductByArticle.FindArticle?.ToString();
						if (findProductByArticle.Name != null && findArticle != null &&
							(findProductByArticle.Article.Equals(findArticle) || findProductByArticle.Article.Equals(findArticle.Split('#')[0])))
						{
							var name = findProductByArticle.Name.ToString();
							name = name.Trim();

							var manufacturer = findProductByArticle.Manufacturer.ToString();
							manufacturer = manufacturer.Trim();

							var quantity = 0;
							foreach (var quantityFindValue in findProductByArticle.Quantity)
							{
								if (quantityFindValue == null)
									continue;

								var quantityString = quantityFindValue.ToString();
								quantityString = quantityString
									.Replace(">", string.Empty)
									.Replace("<", string.Empty)
									.Trim();
								if (!string.IsNullOrEmpty(quantityString) && !quantityString.Contains("Склад"))
								{
									int.TryParse(quantityString, out var quantityInt);
									quantity += quantityInt;
								}
								else if (quantityString.Contains("Склад"))
								{
									quantity += 2;
								}
							}

							var currency = findProductByArticle.Currency?.ToString().Trim();

							var price = 0.0;
							if (findProductByArticle.Price.Any())
							{
								var priceString = findProductByArticle.Price[0].ToString();

								if (!string.IsNullOrEmpty(priceString))
								{
									priceString = priceString.Replace(" ", string.Empty).Trim();
									price = priceString.ToDouble();

									if (currency != null && currency == "RUR")
										price /= SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
								}
							}

							var ocsItem = new ComPriceProduct(findProductByArticle.Article, name, manufacturer, quantity, price, Currency.USD);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(ocsItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(ocsItem);
						}
						else
						{
							var ocsItem = new ComPriceProduct(findProductByArticle.Article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(ocsItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(ocsItem);
						}
					}
				}
			}

			return result;
		}

		protected override string LoginSupplierFieldName { get; } = "OCSLogin";

		protected override string PasswordSupplierFieldName { get; } = "OCSPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "OCSInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "OCSUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "OCSSpecifyDeliveryDay";
	}
}