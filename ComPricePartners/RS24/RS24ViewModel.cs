﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace RS24
{
	public class RS24ViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public RS24ViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "RS24LoginKey", "RS24PasswordKey");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "RS24InStockDeliveryDay", "RS24UnderOrderDeliveryDay", "RS24SpecifyDeliveryDay");
		}

		public string RS24FtpLogin
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("Rs24FtpLoginKey");
			set => _systemConfigurationService.SetSystemConfigurationParameter("Rs24FtpLoginKey", value);
		}

		public string RS24FtpPassword
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("RS24FtpPasswordKey");
			set => _systemConfigurationService.SetSystemConfigurationParameter("RS24FtpPasswordKey", value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}