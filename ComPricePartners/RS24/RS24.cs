﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using Com.Price.Data.Extensions;
using Com.Price.Data.ProcessFiles.Xml;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json.Linq;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;

namespace RS24
{
	public class RS24 : SupplierBase
	{
		public RS24(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			PriceListFileExtension = ".xml";
		}

		private const string UrlSearch = "https://rs24.ru/jsonsearch.htm?Ntt={0}&Nrpp=16";

		private string ComposeBrandField(JToken jToken)
		{
			foreach (var token in jToken)
			{
				var value = token.Value<string>();
				if (value.Contains("Бренд: "))
					return value.Replace("Бренд:", string.Empty).Trim();
			}

			return string.Empty;
		}

		private const string UrlSpecialPrice = "https://rs24.ru/jsonpricesale.htm?id={0}&uom={1}&_={2}";

		private double ComposePriceField(string productId, string uom, Dictionary<string, string> cookies)
		{
			var price = 0.0;
			var urlRequest = string.Format(UrlSpecialPrice, productId, uom, DateTimeOffset.Now.ToUnixTimeMilliseconds());

			var requestSpecialPrice = (HttpWebRequest)WebRequest.Create(urlRequest);
			requestSpecialPrice.Method = WebRequestMethods.Http.Get;
			requestSpecialPrice.Host = "rs24.ru";
			requestSpecialPrice.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0";
			requestSpecialPrice.Accept = @"*/*";
			requestSpecialPrice.KeepAlive = true;
			requestSpecialPrice.AllowAutoRedirect = false;
			requestSpecialPrice.ServicePoint.Expect100Continue = false;
			requestSpecialPrice.Headers.Add("X-Requested-With", "XMLHttpRequest");
			requestSpecialPrice.Headers.Add("Accept-Encoding", "gzip, deflate, br");
			requestSpecialPrice.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
			requestSpecialPrice.CookieContainer = new CookieContainer();

			var uriSpecialPrice = new Uri(urlRequest);
			requestSpecialPrice.CookieContainer.Add(uriSpecialPrice, new Cookie("JSESSIONID", cookies["JSESSIONID"]));

			requestSpecialPrice.AutomaticDecompression = DecompressionMethods.GZip;

			var responseSpecialPrice = (HttpWebResponse)requestSpecialPrice.GetResponse();

			using (var streamResponseSearch = new StreamReader(responseSpecialPrice.GetResponseStream(), Encoding.UTF8, true, 1000))
			{
				var htmlContent = streamResponseSearch.ReadToEnd();
				if (!htmlContent.Contains("null"))
				{
					var htmlContentJObject = JObject.Parse(htmlContent);
					if (htmlContentJObject?["specialPrice"] != null)
						price = htmlContentJObject["specialPrice"].ToString().Replace(" ", string.Empty).Trim().ToDouble();
				}
			}

			responseSpecialPrice.Close();

			return price;
		}

		private const string UrlQuantity = "https://rs24.ru/jsonwhlist.htm?id={0}&_={1}";

		private ProductQuantity ComposeQuantityField(string productId, Dictionary<string, string> cookies, out int isTula)
		{
			var productQuantity = new ProductQuantity();
			isTula = 0;

			var urlRequest = string.Format(UrlQuantity, productId, DateTimeOffset.Now.ToUnixTimeMilliseconds());

			var requestQuantity = (HttpWebRequest)WebRequest.Create(urlRequest);
			requestQuantity.Method = WebRequestMethods.Http.Get;
			requestQuantity.Host = "rs24.ru";
			requestQuantity.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0";
			requestQuantity.Accept = @"*/*";
			requestQuantity.KeepAlive = true;
			requestQuantity.AllowAutoRedirect = false;
			requestQuantity.ServicePoint.Expect100Continue = false;
			requestQuantity.Headers.Add("X-Requested-With", "XMLHttpRequest");
			requestQuantity.Headers.Add("Accept-Encoding", "gzip, deflate, br");
			requestQuantity.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
			requestQuantity.CookieContainer = new CookieContainer();

			var uriQuantity = new Uri(urlRequest);
			requestQuantity.CookieContainer.Add(uriQuantity, new Cookie("JSESSIONID", cookies["JSESSIONID"]));

			requestQuantity.AutomaticDecompression = DecompressionMethods.GZip;

			var responseQuantity = (HttpWebResponse)requestQuantity.GetResponse();

			using (var streamResponseSearch = new StreamReader(responseQuantity.GetResponseStream(), Encoding.UTF8, true, 1000))
			{
				var htmlContent = streamResponseSearch.ReadToEnd();
				if (!htmlContent.Contains("null"))
				{
					var htmlContentJObject = JArray.Parse(htmlContent);
					if (htmlContentJObject != null && htmlContentJObject.Any())
					{

						var moskow = htmlContentJObject.Where(item => item["orgName"].ToString().Contains("Москва")).First();
						if (moskow != null)
							productQuantity = ComposeQuantity(moskow);

						var tula = htmlContentJObject.Where(item => item["orgName"].ToString().Contains("Тула")).First();
						if (tula != null && productQuantity.Quantity == 0)
						{
							productQuantity = ComposeQuantity(tula);
							if (productQuantity.Quantity > 0)
								isTula = 2;
						}

						var tver = htmlContentJObject.Where(item => item["orgName"].ToString().Contains("Тверь")).First();
						if (tver != null && productQuantity.Quantity == 0)
						{
							productQuantity = ComposeQuantity(tver);
							if (productQuantity.Quantity > 0)
								isTula = 3;
						}
					}
				}
			}

			responseQuantity.Close();

			return productQuantity;
		}

		private ProductQuantity ComposeQuantity(JToken item)
		{
			var productQuantity = new ProductQuantity();
			if (item["quantity"] != null && item["quantity"].ConvertValueToInt() > 0)
				productQuantity.Quantity = item["quantity"].ConvertValueToInt();
			else if (item["supplierQty"] != null && item["supplierQty"].ToString() != "-" && item["supplierQty"].ConvertValueToInt() > 0)
				productQuantity.PartnerQuantity = item["supplierQty"].ConvertValueToInt();

			return productQuantity;
		}

		private const string UrlLogin = "https://rs24.ru/authorize";

		private Dictionary<string, string> Login()
		{
			var cookies = new Dictionary<string, string>();
			var requestAuthentication = (HttpWebRequest)WebRequest.Create(UrlLogin);
			requestAuthentication.Method = WebRequestMethods.Http.Post;
			requestAuthentication.Host = "rs24.ru";
			requestAuthentication.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 YaBrowser/23.5.4.674 Yowser/2.5 Safari/537.36";
			requestAuthentication.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
			requestAuthentication.Headers.Add("Accept-Encoding", "gzip, deflate, br");
			requestAuthentication.Headers.Add("Accept-Language", "ru,en;q=0.9");
			requestAuthentication.KeepAlive = true;
			requestAuthentication.AllowAutoRedirect = false;
			requestAuthentication.Referer = "https://rs24.ru/login";
			requestAuthentication.ServicePoint.Expect100Continue = false;
			requestAuthentication.Headers.Add("Origin", "https://rs24.ru");
			requestAuthentication.Headers.Add("Upgrade-Insecure-Requests", "1");
			requestAuthentication.CookieContainer = new CookieContainer();

			requestAuthentication.ContentType = "application/x-www-form-urlencoded";
			var postData = @"event=&isFirstEntry=true&targetUrlParameter=%2Fhome.htm%3FNr%3Dproduct.not_export%253AY&userType=ORGANIZATION";
			postData += @"&username=" + HttpUtility.UrlEncode(SupplierLogin, Encoding.UTF8);
			postData += @"&password=" + HttpUtility.UrlEncode(SupplierPassword);
			var data = Encoding.Default.GetBytes(postData);
			using (var stream = requestAuthentication.GetRequestStream())
				stream.Write(data, 0, data.Length);

			var authenticationResponse = (HttpWebResponse)requestAuthentication.GetResponse();
			if (authenticationResponse.Cookies != null)
				cookies["JSESSIONID"] = authenticationResponse.Cookies["JSESSIONID"]?.Value;

			authenticationResponse.Close();

			return cookies;
		}

		private const string UrlLogout = "https://rs24.ru/logout";

		private void Logout(Dictionary<string, string> cookies)
		{
			var requestLogout = (HttpWebRequest)WebRequest.Create(UrlLogout);
			requestLogout.Method = WebRequestMethods.Http.Get;
			requestLogout.Host = "rs24.ru";
			requestLogout.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0";
			requestLogout.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = false;
			requestLogout.ServicePoint.Expect100Continue = false;
			requestLogout.CookieContainer = new CookieContainer();

			var uriLogout = new Uri(UrlLogout);
			requestLogout.CookieContainer.Add(uriLogout, new Cookie("JSESSIONID", cookies["JSESSIONID"]));

			var responseLogout = (HttpWebResponse)requestLogout.GetResponse();
			responseLogout.Close();
		}

		private string DownloadPrice()
		{
			var priceFilePath = ComposeFileUrl();
			if (!string.IsNullOrEmpty(priceFilePath))
			{
				var downloadFileRequest = (FtpWebRequest)WebRequest.Create(priceFilePath);
				downloadFileRequest.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);
				downloadFileRequest.Method = WebRequestMethods.Ftp.DownloadFile;

				return SupplierProviderService.PriceListDownloadService.DownloadPriceList(downloadFileRequest, DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension, false);
			}

			return priceFilePath;
		}

		private const string FtpServerUrl = "ftp://service.russvet.ru:21021/pricat/TVER/";

		private string ComposeFileUrl()
		{
			var url = string.Empty;
			var fileListRequest = (FtpWebRequest)WebRequest.Create(FtpServerUrl);
			fileListRequest.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);
			fileListRequest.Method = WebRequestMethods.Ftp.ListDirectory;

			var fileListLogout = fileListRequest.GetResponse();
			using (var stream = new StreamReader(fileListLogout.GetResponseStream(), Encoding.Default))
			{
				var response = stream.ReadToEnd();
				if (!string.IsNullOrEmpty(response))
				{
					var fileNames = response.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

					url = FtpServerUrl + fileNames.OrderBy(s => s).Last();
				}
			}

			fileListLogout.Close();
			return url;
		}

		private string _ftpLogin;

		private string _ftpPassword;

		private const string Rs24FtpLoginKey = "Rs24FtpLoginKey";

		private const string Rs24FtpPasswordKey = "RS24FtpPasswordKey";

		public override string Name { get; } = "RS24";

		public override string Header { get; } = "RS24";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var cookies = Login();
			if (cookies.Any())
			{
				foreach (var article in articles)
				{
					Thread.Sleep(1000);

					var requestSearch = (HttpWebRequest)WebRequest.Create(string.Format(UrlSearch, article));
					requestSearch.Method = WebRequestMethods.Http.Get;
					requestSearch.Host = "rs24.ru";
					requestSearch.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0";
					requestSearch.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
					requestSearch.KeepAlive = true;
					requestSearch.AllowAutoRedirect = false;
					requestSearch.ServicePoint.Expect100Continue = false;
					requestSearch.CookieContainer = new CookieContainer();
					requestSearch.AutomaticDecompression = DecompressionMethods.GZip;

					var uriLogout = new Uri(string.Format(UrlSearch, article));
					requestSearch.CookieContainer.Add(uriLogout, new Cookie("JSESSIONID", cookies["JSESSIONID"]));

					var responseSearch = (HttpWebResponse)requestSearch.GetResponse();

					using (var streamResponseSearch = new StreamReader(responseSearch.GetResponseStream(), Encoding.UTF8))
					{
						var htmlContent = streamResponseSearch.ReadToEnd();
						var htmlContentJObject = JObject.Parse(htmlContent);
						if (htmlContentJObject?["records"] != null)
						{
							foreach (var record in htmlContentJObject["records"])
							{
								var name = record[5].ToString();

								if (name.EndsWith(article))
								{
									var productQuantity = ComposeQuantityField(record[0].ToString(), cookies, out var isTula);
									var quantity = productQuantity.Quantity == 0 ? productQuantity.PartnerQuantity : productQuantity.Quantity;
									var quantityMessage = productQuantity.Quantity == 0 && productQuantity.PartnerQuantity > 0 ? "Под заказ до 10 дней" : string.Empty;

									var rs24Item = new ComPriceProduct(article, record[5].ToString(), ComposeBrandField(record[10]), quantity, ComposePriceField(record[0].ToString(), record[1].ToString(), cookies))
									{ QuantityMessage = quantityMessage };

									SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(rs24Item, SupplierInStockDeliveryDay + isTula, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
									rs24Item.Currency = Currency.RUB;
									result.Add(rs24Item);
								}
							}
						}
					}

					responseSearch.Close();
				}

				Logout(cookies);
			}

			return result;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var xmlProcessingRequest = new XmlProcessingRequest
				{
					FilePath = filePath,
					ArticleManufacturer = "SenderPrdCode",
					Name = "ProductName",
					Manufacturer = "Brand",
					Article = "VendorProdNum",
					Price = new List<string> { "Price2" },
					Quantity = new List<string> { "QTY", "SumQTY" },
					Item = "DocDetail"
				};

				var findProductByArticles = SupplierProviderService.XmlProcessingService.FindProductByArticles(articles, xmlProcessingRequest);
				var findInExcel = new List<string>(articles);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						findInExcel.Remove(findProductByArticle.Article);
						if (findProductByArticle.Name != null && findProductByArticle.FindArticle != null && articles.Contains(findProductByArticle.FindArticle.ToString()))
						{
							var name = findProductByArticle.Name.ToString();
							name = name.Trim();

							var manufacturer = findProductByArticle.Manufacturer.ToString();
							manufacturer = manufacturer.Trim();

							var articleManufacturer = findProductByArticle.ArticleManufacturer.ToString();
							Thread.Sleep(500);

							var productQuantity = ComposeQuantityFromApi(articleManufacturer, "96");
							var inStockDeliveryDayAdditional = 0;
							if (productQuantity.Quantity == 0)
							{
								Thread.Sleep(200);
								productQuantity = ComposeQuantityFromApi(articleManufacturer, "97");
								if (productQuantity.Quantity > 0)
									inStockDeliveryDayAdditional = 2;
							}

							if (productQuantity.Quantity == 0)
							{
								Thread.Sleep(200);
								productQuantity = ComposeQuantityFromApi(articleManufacturer, "14030");
								if (productQuantity.Quantity > 0)
									inStockDeliveryDayAdditional = 3;
							}

							var price = ComposePriceFromApi(articleManufacturer);

							var quantity = productQuantity.Quantity == 0 ? productQuantity.PartnerQuantity : productQuantity.Quantity;
							var quantityMessage = productQuantity.Quantity == 0 && productQuantity.PartnerQuantity > 0 ? "Под заказ до 10 дней" : string.Empty;

							var comPriceProduct = new ComPriceProduct(
									findProductByArticle.Article, name, manufacturer, quantity, price, Currency.RUB)
							{ QuantityMessage = quantityMessage };

							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(comPriceProduct, SupplierInStockDeliveryDay + inStockDeliveryDayAdditional, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(comPriceProduct);
						}
					}
				}

				var excelFilePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, "Коды РС", ".xlsx");
				if (!string.IsNullOrEmpty(excelFilePath) && findInExcel.Any())
				{
					var excelProcessingRequest = new ExcelProcessingRequest { FilePath = excelFilePath, Name = 2, Manufacturer = 3, FindLookIn = 0, FindLookAt = 0, Article = 4, ArticleManufacturer = 1 };
					excelProcessingRequest.SheetsNumberToSearch.Add(1);
					var findProductByArticlesInExcel = SupplierProviderService.ExcelProcessingService.FindProductByArticles(findInExcel, excelProcessingRequest);

					foreach (var findProductByArticleInExcel in findProductByArticlesInExcel)
					{
						if (findProductByArticleInExcel.Name != null && findProductByArticleInExcel.FindArticle != null && findInExcel.Contains(findProductByArticleInExcel.FindArticle.ToString()))
						{
							var name = findProductByArticleInExcel.Name.ToString();
							name = name.Trim();

							var manufacturer = findProductByArticleInExcel.Manufacturer.ToString();
							manufacturer = manufacturer.Trim();

							var articleManufacturer = findProductByArticleInExcel.FindArticleManufacturer.ToString();
							Thread.Sleep(500);
							var productQuantity = ComposeQuantityFromApi(articleManufacturer, "96");
							var inStockDeliveryDayAdditional = 0;
							if (productQuantity.Quantity == 0)
							{
								Thread.Sleep(200);
								productQuantity = ComposeQuantityFromApi(articleManufacturer, "97");
								if (productQuantity.Quantity > 0)
									inStockDeliveryDayAdditional = 2;
							}

							if (productQuantity.Quantity == 0)
							{
								Thread.Sleep(200);
								productQuantity = ComposeQuantityFromApi(articleManufacturer, "14030");
								if (productQuantity.Quantity > 0)
									inStockDeliveryDayAdditional = 3;
							}

							var price = ComposePriceFromApi(articleManufacturer);

							var quantity = productQuantity.Quantity == 0 ? productQuantity.PartnerQuantity : productQuantity.Quantity;
							var quantityMessage = productQuantity.Quantity == 0 && productQuantity.PartnerQuantity > 0 ? "Под заказ до 10 дней" : string.Empty;

							var comPriceProduct = new ComPriceProduct(
									findProductByArticleInExcel.Article, name, manufacturer, quantity, price, Currency.RUB)
							{ QuantityMessage = quantityMessage };

							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(comPriceProduct, SupplierInStockDeliveryDay + inStockDeliveryDayAdditional, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(comPriceProduct);
						}
						else
							result.Add(new ComPriceProduct(findProductByArticleInExcel.Article));

					}
				}
			}

			return result;
		}

		private const string UrlQuantityApi = "https://cdis.russvet.ru/rs/residue/{0}/{1}";

		private readonly HttpClient _httpClient = new HttpClient();

		private ProductQuantity ComposeQuantityFromApi(string productCode, string wareHouseId)
		{
			var productQuantity = new ProductQuantity();

			_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{SupplierLogin}:{SupplierPassword}")));
			var response = _httpClient.GetAsync(string.Format(UrlQuantityApi, wareHouseId, productCode)).Result;
			if (response.IsSuccessStatusCode)
			{
				var responseResult = response.Content.ReadAsStringAsync().Result;
				if (!string.IsNullOrEmpty(responseResult))
				{
					var json = JObject.Parse(responseResult);
					productQuantity.Quantity = json["Residue"].ToString().ToInt();

					if (json["partnerQuantityInfo"] != null)
						productQuantity.PartnerQuantity = json["partnerQuantityInfo"]["partnerQuantity"].ToString().ToInt();
				}
			}
			else
			{
				SupplierProviderService.LoggerService.ErrorFormat("Ошибка выполнения запроса {0} Код ответа {1}", string.Format(UrlQuantityApi, wareHouseId, productCode), response.StatusCode);
			}


			return productQuantity;
		}

		private const string UrlPriceApi = "https://cdis.russvet.ru/rs/price/{0}";

		private double ComposePriceFromApi(string productCode)
		{
			_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{SupplierLogin}:{SupplierPassword}")));
			var response = _httpClient.GetAsync(string.Format(UrlPriceApi, productCode)).Result;
			if (response.IsSuccessStatusCode)
			{
				var responseResult = response.Content.ReadAsStringAsync().Result;
				if (!string.IsNullOrEmpty(responseResult))
				{
					var json = JObject.Parse(responseResult);
					if (json["Price"] != null && json["Price"]["Personal_w_VAT"] != null)
						return json["Price"]["Personal_w_VAT"].ToString().ToDouble();
				}
			}

			return 0.0;
		}

		protected override string LoginSupplierFieldName { get; } = "RS24LoginKey";

		protected override string PasswordSupplierFieldName { get; } = "RS24PasswordKey";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "RS24InStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "RS24UnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "RS24SpecifyDeliveryDay";

		public override void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
		{
			if (systemConfigurationParameters != null && systemConfigurationParameters.Any())
			{
				if (systemConfigurationParameters.ContainsKey(Rs24FtpLoginKey) && systemConfigurationParameters[Rs24FtpLoginKey] != null)
					_ftpLogin = systemConfigurationParameters[Rs24FtpLoginKey];

				if (systemConfigurationParameters.ContainsKey(Rs24FtpPasswordKey) && systemConfigurationParameters[Rs24FtpPasswordKey] != null)
					_ftpPassword = systemConfigurationParameters[Rs24FtpPasswordKey];
			}

			base.ApplySupplierConfigurationParameters(systemConfigurationParameters);
		}

		private class ProductQuantity
		{
			public int Quantity { get; set; } = 0;

			public int PartnerQuantity { get; set; } = 0;
		}

	}
}