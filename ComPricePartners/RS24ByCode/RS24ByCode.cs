﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Data.Suppliers.Enums;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json.Linq;

namespace RS24ByCode
{
	public class RS24ByCode : SupplierBase
	{
		private readonly ISupplierProviderService _supplierProviderService;

		public RS24ByCode(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			_supplierProviderService = supplierProviderService;

			SearchField = MainSearchField.ArticleManufacturer;

			_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
		}

		public override string Name { get; } = "RS24ByCode";

		public override string Header { get; } = "RS24ByCode";

		private const string UrlProductApi = "https://cdis.russvet.ru/rs/specs/{0}";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();
			foreach (var article in articles)
			{
				Thread.Sleep(200);
				_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{SupplierLogin}:{SupplierPassword}")));
				var response = _httpClient.GetAsync(string.Format(UrlProductApi, article)).Result;
				if (response.IsSuccessStatusCode)
				{
					var responseResult = response.Content.ReadAsStringAsync().Result;
					if (!string.IsNullOrEmpty(responseResult))
					{
						var product = JObject.Parse(responseResult);
						var info = product["INFO"].First;
						var productQuantity = ComposeQuantityFromApi(article, "14030");
						var inStockDeliveryDayAdditional = 0;
						if (productQuantity.Quantity == 0)
						{
							productQuantity = ComposeQuantityFromApi(article, "97");
							if (productQuantity.Quantity > 0)
								inStockDeliveryDayAdditional = 2;
						}
						if (productQuantity.Quantity == 0)
						{
							productQuantity = ComposeQuantityFromApi(article, "96");
							if (productQuantity.Quantity > 0)
								inStockDeliveryDayAdditional = 3;
						}

						var quantity = productQuantity.Quantity == 0 ? productQuantity.PartnerQuantity : productQuantity.Quantity;
						var quantityMessage = productQuantity.Quantity == 0 && productQuantity.PartnerQuantity > 0 ? "Под заказ до 10 дней" : string.Empty;

						var rs24Item = new ComPriceProduct(article, info["DESCRIPTION"].ToString(), info["BRAND"]?.ToString(), quantity, ComposePriceFromApi(article)) { QuantityMessage = quantityMessage }; ;

						_supplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(rs24Item, SupplierInStockDeliveryDay + inStockDeliveryDayAdditional, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
						rs24Item.Currency = Currency.RUB;

						result.Add(rs24Item);
					}
				}
			}

			return result;
		}

		private const string UrlQuantityApi = "https://cdis.russvet.ru/rs/residue/{0}/{1}";

		private readonly HttpClient _httpClient = new HttpClient();

		private ProductQuantity ComposeQuantityFromApi(string productCode, string wareHouseId)
		{
			var productQuantity = new ProductQuantity();

			_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{SupplierLogin}:{SupplierPassword}")));
			var response = _httpClient.GetAsync(string.Format(UrlQuantityApi, wareHouseId, productCode)).Result;
			if (response.IsSuccessStatusCode)
			{
				var responseResult = response.Content.ReadAsStringAsync().Result;
				if (!string.IsNullOrEmpty(responseResult))
				{
					var json = JObject.Parse(responseResult);
					productQuantity.Quantity = json["Residue"].ToString().ToInt();

					if (json["partnerQuantityInfo"] != null)
						productQuantity.PartnerQuantity = json["partnerQuantityInfo"]["partnerQuantity"].ToString().ToInt();
				}
			}

			return productQuantity;
		}

		private const string UrlPriceApi = "https://cdis.russvet.ru/rs/price/{0}";

		private double ComposePriceFromApi(string productCode)
		{
			_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{SupplierLogin}:{SupplierPassword}")));
			var response = _httpClient.GetAsync(string.Format(UrlPriceApi, productCode)).Result;
			if (response.IsSuccessStatusCode)
			{
				var responseResult = response.Content.ReadAsStringAsync().Result;
				if (!string.IsNullOrEmpty(responseResult))
				{
					var json = JObject.Parse(responseResult);
					if (json["Price"] != null && json["Price"]["Personal_w_VAT"] != null)
						return json["Price"]["Personal_w_VAT"].ToString().ToDouble();
				}
			}

			return 0.0;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

		protected override string LoginSupplierFieldName { get; } = "RS24ByCodeLoginKey";

		protected override string PasswordSupplierFieldName { get; } = "RS24ByCodePasswordKey";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "RS24ByCodeInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "RS24ByCodeUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "RS24ByCodeSpecifyDeliveryDay";

		private class ProductQuantity
		{
			public int Quantity { get; set; } = 0;

			public int PartnerQuantity { get; set; } = 0;
		}
	}
}