﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace RS24ByCode
{
	public class RS24ByCodeViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public RS24ByCodeViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "RS24ByCodeLoginKey", "RS24ByCodePasswordKey");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "RS24ByCodeInStockDeliveryDay", "RS24ByCodeUnderOrderDeliveryDay", "RS24ByCodeSpecifyDeliveryDay");
		}

		public string RS24ByCodeFtpLogin
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("RS24ByCodeFtpLoginKey");
			set => _systemConfigurationService.SetSystemConfigurationParameter("RS24ByCodeFtpLoginKey", value);
		}

		public string RS24ByCodeFtpPassword
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("RS24ByCodeFtpPasswordKey");
			set => _systemConfigurationService.SetSystemConfigurationParameter("RS24ByCodeFtpPasswordKey", value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }


		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}