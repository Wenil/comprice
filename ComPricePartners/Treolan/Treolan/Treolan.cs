﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Xml;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Treolan.ApiTreolan;

namespace Treolan
{
	class Treolan : SupplierBase
	{
		private readonly ISupplierProviderService _supplierProviderService;

		public Treolan(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			_supplierProviderService = supplierProviderService;
		}

		private const string UrlPriceList =
			"https://b2b.treolan.ru/Catalog/SearchToExcel?Template=&Commodity=true&IncludeFullPriceList=false&OrderBy=0&Groups=&Vendors=&IncludeSubGroups=false&Condition=0&PriceMin=&PriceMax=&Currency=RUB&AvailableAtStockOnly=false&AdditionalParamsStr=&AddParamsShow=&GetExcel=true&UseExcelGrouping=false&GetForFastSearchWidget=false&FromLeftCol=false&CatalogProductsOnly=true&RusDescription=false&skip=0&take=50&LoadResults=false&DemoOnly=false&SparesOnly=false&ShowHpCarePack=false&MpTypes=-1&ShowConvertedPrices=false&SearchByLink=false&showActualGoods=false";

		private const string UrlEmpty = "https://b2b.treolan.ru/";

		private const string UrlLogin = "https://b2b.treolan.ru/Account/Login?ReturnUrl=/";

		private Dictionary<string, string> LoginOnTreolan()
		{
			var cookies = new Dictionary<string, string>();

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls |
			                                       SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

			ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
			var requestEmpty = (HttpWebRequest) WebRequest.Create(UrlEmpty);
			requestEmpty.Method = WebRequestMethods.Http.Get;

			requestEmpty.Host = "b2b.treolan.ru";
			requestEmpty.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0";
			requestEmpty.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			requestEmpty.Headers.Add(HttpRequestHeader.AcceptLanguage, "ru-RU");
			requestEmpty.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate, br");
			requestEmpty.KeepAlive = true;
			requestEmpty.AllowAutoRedirect = true;
			requestEmpty.CookieContainer = new CookieContainer();

			var respEmpty = (HttpWebResponse) requestEmpty.GetResponse();
			var cookieContainer = requestEmpty.CookieContainer.GetCookies(new Uri(UrlEmpty));
			respEmpty.Close();

			if (cookieContainer.Count > 0 && cookieContainer["B2BSession"] != null)
			{
				cookies.Add("B2BSession", cookieContainer["B2BSession"].Value);

				var requestLogin = (HttpWebRequest) WebRequest.Create(UrlLogin);
				requestLogin.Method = WebRequestMethods.Http.Post;

				requestLogin.CookieContainer = new CookieContainer();
				var uriRequestLogin = new Uri(UrlLogin);
				requestLogin.CookieContainer.Add(uriRequestLogin, new Cookie("B2BSession", cookies["B2BSession"]));

				requestLogin.Host = "b2b.treolan.ru";
				requestLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0";
				requestLogin.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";
				requestLogin.KeepAlive = true;
				requestLogin.AllowAutoRedirect = true;
				requestLogin.Referer = "https://b2b.treolan.ru/Account/Login?ReturnUrl=%2f&AspxAutoDetectCookieSupport=1";

				var postData1 = "UserName=" + SupplierLogin;
				postData1 += "&Password=" + SupplierPassword;
				var data1 = Encoding.UTF8.GetBytes(postData1);

				requestLogin.ContentType = "application/x-www-form-urlencoded";
				requestLogin.ContentLength = data1.Length;
				requestLogin.AllowAutoRedirect = true;

				using (var streamDataLogin = requestLogin.GetRequestStream())
					streamDataLogin.Write(data1, 0, data1.Length);

				var responseLogin = (HttpWebResponse) requestLogin.GetResponse();
				var cookieLogin = requestLogin.Headers["Cookie"];

				if (!string.IsNullOrEmpty(cookieLogin) && cookieLogin.Contains("B2BAuth"))
					cookies.Add("B2BAuth", cookieLogin.Replace("B2BAuth=", string.Empty));

				responseLogin.Close();
			}

			return cookies;
		}

		private const string UrlLogout = "https://b2b.treolan.ru/Account/LogOut";

		private void LogoutOnTreolan(IReadOnlyDictionary<string, string> cookies)
		{
			var requestLogout = (HttpWebRequest) WebRequest.Create(UrlLogout);
			requestLogout.Method = WebRequestMethods.Http.Get;

			requestLogout.CookieContainer = new CookieContainer();
			var uriLogout = new Uri(UrlLogout);

			requestLogout.CookieContainer.Add(uriLogout, new Cookie("B2BSession", cookies["B2BSession"]));
			requestLogout.CookieContainer.Add(uriLogout, new Cookie("B2BAuth", cookies["B2BAuth"]));

			requestLogout.Host = "b2b.treolan.ru";
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0";
			requestLogout.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8";
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = true;

			var responseLogout = (HttpWebResponse) requestLogout.GetResponse();
			responseLogout.Close();
		}

		private string DownloadPrice()
		{
			var priceFilePath = string.Empty;
			var cookies = LoginOnTreolan();
			if (cookies.Any())
			{
				var webRequestDownloadFile = (HttpWebRequest) WebRequest.Create(UrlPriceList);
				webRequestDownloadFile.Method = WebRequestMethods.Http.Get;
				webRequestDownloadFile.Host = "b2b.treolan.ru";
				webRequestDownloadFile.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
				webRequestDownloadFile.Accept = "application/json, text/plain, */*";
				webRequestDownloadFile.KeepAlive = true;
				webRequestDownloadFile.AllowAutoRedirect = false;
				webRequestDownloadFile.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
				webRequestDownloadFile.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                webRequestDownloadFile.Timeout = 300000;


                webRequestDownloadFile.CookieContainer = new CookieContainer();
				var uriPriceDownload = new Uri(UrlPriceList);

				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("B2BSession", cookies["B2BSession"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("B2BAuth", cookies["B2BAuth"]));

				webRequestDownloadFile.AllowAutoRedirect = false;

				var directory = DirectoryPathForDownloadFile;

				foreach (var file in Directory.GetFiles(directory))
					File.Delete(file);

				priceFilePath = _supplierProviderService.PriceListDownloadService.DownloadPriceList(webRequestDownloadFile, directory, PriceListFileName, PriceListFileExtension);

				LogoutOnTreolan(cookies);

				return priceFilePath;
			}

			return priceFilePath;
		}

		public override string Name { get; } = "treo";

		public override string Header { get; } = "Treo";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var treolan = new List<ComPriceProduct>();
			var timeout = Math.Max(60, articles.Count * 5);
			var basicHttpsBinding = new BasicHttpsBinding {SendTimeout = TimeSpan.FromSeconds(timeout) };
			var webServiceClient = new WebServiceSoapPortClient(basicHttpsBinding, new EndpointAddress("https://api.treolan.ru/webservices/treolan.ASP"));

			//  фильтр по разделу каталога. Указывается идентификатор раздела, “” - весь склад. Идентификатор раздела можнополучить из полного склада в формате xml.
			var category = string.Empty;
			// критерий поиска. 0 - начинается с, 1 – содержит, 2 - заканчивается на
			var criterion = 1;
			// поиск по артикулу. 0 - не искать в артикуле, 1 - искать в артикуле.
			var inArticle = true;
			// поиск по наименованию. 0 - не искать в наименовании, 1 - искать в наименовании.
			var inName = false;
			// фильтр по участию в маркетинговых программах. 0 - нет фильтра по участию в маркетинговых программах, 1 -участвует в маркетинговых программах.
			var inMark = false;
			// фильтр по некондиции. 0 –показывать некондицию, 1 – не показывать некондицию, 2 – показывать тольконекондицию.
			var showSubstandard = 1;

			// ключевые слова для поиска. Если несколько, то используется "или". Если пустая строка - нет фильтра понаименованию или артикулу.

			foreach (var articleBatch in articles.Batch(15))
			{
				var keywords = string.Join(" или ", articleBatch);
				var webServiceResponse = webServiceClient.GenCatalog(ref SupplierLogin, ref SupplierPassword, ref category, ref keywords, ref criterion, ref inArticle, ref inName, ref inMark,
					ref showSubstandard);

				if (!string.IsNullOrEmpty(webServiceResponse))
				{
					var xmlDocument = new XmlDocument();
					xmlDocument.LoadXml(webServiceResponse);

					var positionNodes = xmlDocument.GetElementsByTagName("position");
					foreach (var positionNode in positionNodes)
					{
						if (positionNode is XmlElement positionElement)
						{
							if (positionElement.HasAttributes)
							{
								var partNumber = positionElement.Attributes["articul"]?.Value.ToUpper();
								if (
									partNumber != null && (
										articles.Contains(partNumber)
										|| articles.Contains(partNumber.Split('#')[0])
										|| partNumber.EndsWith("B") && articles.Contains(partNumber.Remove(partNumber.Length - 1))
									)
								)
								{
									var article = partNumber;

									if (partNumber.Contains('#'))
										article = partNumber.Split('#')[0];
									else if (partNumber.EndsWith("B"))
										article = partNumber.Remove(partNumber.Length - 1);

									var price = positionElement.Attributes["dprice"].Value;

									var cost = price.ToDouble();
									if (positionElement.Attributes["currency"].Value.Equals("RUB"))
										cost /= _supplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];

									var quantity = positionElement.Attributes["freenom"].Value
										.ToLower()
										.Replace("*", string.Empty)
										.Replace("<", string.Empty)
										.Replace(">", string.Empty)
										.Replace("много", "4")
										.Trim()
										.ToInt();

									var treo = new ComPriceProduct(article, positionElement.Attributes["name"].Value, positionElement.Attributes["vendor"].Value, quantity, cost);
									SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(treo, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
									treolan.Add(treo);
								}
							}
						}
					}
				}


				articles.ForEach(article =>
				{
					if (!treolan.Any(item => item.Article.Equals(article)))
						treolan.Add(new ComPriceProduct(article));
				});
			}

			webServiceClient.Close();

			return treolan.OrderBy(e => articles.IndexOf(e.Article)).ToList();
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = _supplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest {FilePath = filePath, Name = 2, Manufacturer = 3, FindLookIn = 2, FindLookAt = 1, Article = 1};
				excelProcessingRequest.SheetsNumberToSearch.Add(1);
				excelProcessingRequest.Quantity.Add(4);
				excelProcessingRequest.Price.Add(7);
				excelProcessingRequest.Price.Add(8);

				var findProductByArticles = _supplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						var findArticle = findProductByArticle.FindArticle?.ToString();

						if (findArticle != null && findProductByArticle.Name != null && findProductByArticle.FindArticle != null &&
						    (
							    findArticle.Split('#')[0].Equals(findProductByArticle.Article)
							    || (findArticle.EndsWith("B") && findArticle.Remove(findArticle.Length - 1).Equals(findProductByArticle.Article))
						    )
							&& !SupplierProviderService.CheckConditionService.IsGarbageName(findProductByArticle.Name.ToString())
						)
						{
							var name = findProductByArticle.Name.ToString();
							name = name.Trim();

							var manufacturer = findProductByArticle.Manufacturer.ToString();
							manufacturer = manufacturer.Trim();

							var quantity = 0;
							foreach (var quantityFindValue in findProductByArticle.Quantity)
							{
								var quantityString = quantityFindValue.ToString();

								quantityString = quantityString
									.Replace("*", string.Empty)
									.Replace("<", string.Empty)
									.Replace(">", string.Empty)
									.Trim();

								if (!string.IsNullOrEmpty(quantityString) && quantityString.ToLower().Contains("много"))
								{
									quantity += 2;
								}
								else if (!string.IsNullOrEmpty(quantityString) && !quantityString.Contains("Склад"))
								{
									int.TryParse(quantityString, out var quantityInt);
									quantity += quantityInt;
								}
							}

							var price = 0.0;

							if (findProductByArticle.Price.Any())
							{
								if (findProductByArticle.Price[0] != null)
								{
									var priceString = findProductByArticle.Price[0].ToString().Replace(" ", string.Empty).Trim();
									price = priceString.ToDouble();
								}
								else if (findProductByArticle.Price[1] != null)
								{
									var priceString = findProductByArticle.Price[1].ToString().Replace(" ", string.Empty).Trim();
									price = priceString.ToDouble();
									price /= _supplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
								}
							}

							var treolan = new ComPriceProduct(findProductByArticle.Article, name, manufacturer, quantity, price, Currency.USD);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(treolan, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(treolan);
						}
					}
				}
			}

			return result;
		}

		protected override string LoginSupplierFieldName { get; } = "TreolanLogin";

		protected override string PasswordSupplierFieldName { get; } = "TreolanPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "TreolanInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "TreolanUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "TreolanSpecifyDeliveryDay";
	}
}