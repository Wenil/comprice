﻿using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Ipro.Data.Api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;

namespace Ipro
{
	class Ipro : SupplierBase
	{
		public Ipro(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			PriceListFileExtension = ".csv";
		}

		public override string Name => "Ipro";

		public override string Header => "Ipro";

		protected override string LoginSupplierFieldName { get; } = "IproLogin";

		protected override string PasswordSupplierFieldName { get; } = "IproPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "IproInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "IproUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "IproSpecifyDeliveryDay";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var iproResult = new List<ComPriceProduct>();
			var sesion = getSession();
			if (!string.IsNullOrEmpty(sesion))
			{
				var client = new HttpClient
				{
					BaseAddress = new Uri("https://ipro.etm.ru")
				};
				client.DefaultRequestHeaders.Clear();
				client.DefaultRequestHeaders.ConnectionClose = true;

				var goods = string.Join("%2C", articles);
				var body = SendRequest(client, string.Format("/api/v1/goods/{0}?type=mnf&session-id={1}", goods, sesion), HttpMethod.Get);

				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					var goodsResponseBody = SendRequest(client, string.Format("/api/v1/goods/{0}?type=mnf&session-id={1}", WebUtility.UrlEncode(article), sesion), HttpMethod.Get);
					var goodsResponse = JsonConvert.DeserializeObject<GoodsResponse>(goodsResponseBody);
					if (goodsResponse != null && goodsResponse.Data != null)
					{
						var product = new ComPriceProduct()
						{
							Article = article,
							Name = goodsResponse.Data.Name,
							Brand = goodsResponse.Data.Brand,
							Currency = Currency.RUB
						};

						var priceResponseBody = SendRequest(client, string.Format("/api/v1/goods/{0}/price?type=etm&session-id={1}", WebUtility.UrlEncode(goodsResponse.Data.Code), sesion), HttpMethod.Get);
						var priceResponse = JsonConvert.DeserializeObject<PriceResponse>(priceResponseBody);
						if (priceResponse != null && priceResponse.Data != null && priceResponse.Data.Rows.Any())
						{
							var fiirstPrice = priceResponse.Data.Rows.First();
							if (fiirstPrice != null && fiirstPrice.PriceWithDns != null)
								product.Price = fiirstPrice.PriceWithDns.ToDouble();
						}

						var inStockDeliveryDayAdditional = 0;
						var remainsResponseBody = SendRequest(client, string.Format("/api/v1/goods/{0}/remains?type=etm&session-id={1}", WebUtility.UrlEncode(goodsResponse.Data.Code), sesion), HttpMethod.Get);
						var remainsResponse = JsonConvert.DeserializeObject<RemainsResponse>(remainsResponseBody);
						if (remainsResponse != null && remainsResponse.Data != null && remainsResponse.Data.InfoStores.Any())
						{
							var rc = remainsResponse.Data.InfoStores.Where(infoStore => infoStore.Type.Equals("rc")).FirstOrDefault();
							if (rc != null)
							{
								product.Quantity = rc.Quantity.ToInt();
							}

							if (product.Quantity <= 0)
							{
								var crs = remainsResponse.Data.InfoStores.Where(infoStore => infoStore.Type.Equals("crs")).FirstOrDefault();
								if (crs != null)
								{
									product.Quantity = crs.Quantity.ToInt();
									if (product.Quantity > 0)
										inStockDeliveryDayAdditional = 1;
								}
							}

							if (product.Quantity == 0)
								continue;
						}

						SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(product, SupplierInStockDeliveryDay + inStockDeliveryDayAdditional, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);

						iproResult.Add(product);
					}
				}
			}

			return iproResult;
		}

		private string SendRequest(HttpClient httpClient, string requestUri, HttpMethod method)
		{
			Thread.Sleep(1200);

			var requestMessage = new HttpRequestMessage(method, requestUri);
			var task = httpClient.SendAsync(requestMessage);
			var response = task.Result;
			response.EnsureSuccessStatusCode();

			return response.Content.ReadAsStringAsync().Result;
		}

		private string getSession()
		{
			var client = new HttpClient
			{
				BaseAddress = new Uri("https://ipro.etm.ru")
			};
			client.DefaultRequestHeaders.Clear();
			client.DefaultRequestHeaders.ConnectionClose = true;

			var loginResponseBody = SendRequest(client, string.Format("/api/v1/user/login?log={0}&pwd={1}", WebUtility.UrlEncode(SupplierLogin), WebUtility.UrlEncode(SupplierPassword)), HttpMethod.Post);
			var loginResponse = JsonConvert.DeserializeObject<LoginResponse>(loginResponseBody);

			if (loginResponse != null && loginResponse.Data != null && !string.IsNullOrEmpty(loginResponse.Data.Session))
				return loginResponse.Data.Session;

			return null;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var mainPricePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, $"{GetType().Name}_main_{DateTime.Now:yy-MM-dd}", PriceListFileExtension);
			if (string.IsNullOrEmpty(mainPricePath))
				mainPricePath = DownloadPrice("ftp://92.53.96.128/etm/14/price.csv", $"{GetType().Name}_main_{DateTime.Now:yy-MM-dd}");

			var additionalPricePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, $"{GetType().Name}_additional_{DateTime.Now:yy-MM-dd}", PriceListFileExtension);
			if (string.IsNullOrEmpty(additionalPricePath))
				additionalPricePath = DownloadPrice("ftp://92.53.96.128/etm/19/price.csv", $"{GetType().Name}_additional_{DateTime.Now:yy-MM-dd}", false);

			var excelProcessingRequest = new ExcelProcessingRequest
			{
				FilePath = mainPricePath,
				Name = 2,
				Manufacturer = 6,
				FindLookIn = 0,
				Article = 5,
				ArticleManufacturer = 1
			};
			excelProcessingRequest.Price.Add(3);
			excelProcessingRequest.Quantity.Add(4);
			excelProcessingRequest.SheetsNumberToSearch.Add(1);

			var findInExcel = new List<string>(articles);
			if (!string.IsNullOrEmpty(mainPricePath))
			{
				var findProductByArticlesInExcel = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				foreach (var findProductByArticleInExcel in findProductByArticlesInExcel)
				{
					if (findProductByArticleInExcel.Name != null && findProductByArticleInExcel.FindArticle != null && articles.Contains(findProductByArticleInExcel.FindArticle.ToString()))
					{
						findInExcel.Remove(findProductByArticleInExcel.Article);
						var name = findProductByArticleInExcel.Name.ToString();
						name = name.Trim();

						var manufacturer = findProductByArticleInExcel.Manufacturer.ToString();
						manufacturer = manufacturer.Trim();

						var quantity = 0;

						foreach (var quantityCellValue in findProductByArticleInExcel.Quantity)
						{
							var quantityString = quantityCellValue?.ToString().Trim();
							if (!string.IsNullOrEmpty(quantityString))
								quantity += quantityString.ToInt();
						}

						var price = 0.0;
						if (findProductByArticleInExcel.Price.Any())
						{
							var priceString = findProductByArticleInExcel.Price[0].ToString();

							if (!string.IsNullOrEmpty(priceString))
								price = priceString.ToDouble();
						}

						var comPriceProduct = new ComPriceProduct(findProductByArticleInExcel.Article, name, manufacturer, quantity, price, Currency.RUB);

						SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(comPriceProduct, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
						result.Add(comPriceProduct);
					}
				}
			}

			if (!string.IsNullOrEmpty(additionalPricePath))
			{
				excelProcessingRequest.FilePath = additionalPricePath;
				var findProductByArticlesInExcel = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				foreach (var findProductByArticleInExcel in findProductByArticlesInExcel)
				{
					if (findProductByArticleInExcel.Name != null && findProductByArticleInExcel.FindArticle != null && articles.Contains(findProductByArticleInExcel.FindArticle.ToString()))
					{
						findInExcel.Remove(findProductByArticleInExcel.Article);
						var name = findProductByArticleInExcel.Name.ToString();
						name = name.Trim();

						var manufacturer = findProductByArticleInExcel.Manufacturer.ToString();
						manufacturer = manufacturer.Trim();

						var quantity = 0;

						foreach (var quantityCellValue in findProductByArticleInExcel.Quantity)
						{
							var quantityString = quantityCellValue?.ToString().Trim();
							if (!string.IsNullOrEmpty(quantityString))
								quantity += quantityString.ToInt();
						}

						var price = 0.0;
						if (findProductByArticleInExcel.Price.Any())
						{
							var priceString = findProductByArticleInExcel.Price[0].ToString();

							if (!string.IsNullOrEmpty(priceString))
								price = priceString.ToDouble();
						}

						var comPriceProduct = new ComPriceProduct(findProductByArticleInExcel.Article, name, manufacturer, quantity, price, Currency.RUB);
						var product = result.Find(item => item.Article.Equals(findProductByArticleInExcel.Article));
						if (product != null && product.Quantity == 0.0)
						{
							product.Quantity = quantity;
							product.Price = price;
						}
						else if (product == null)
						{
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(comPriceProduct, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(comPriceProduct);
						}
					}
				}
			}

			findInExcel.ForEach(arcticle => { result.Add(new ComPriceProduct(arcticle)); });

			return result;
		}

		private string DownloadPrice(string priceFilePath, string priceFileName, bool canCleanDirectory = true)
		{
			var downloadFileRequest = (FtpWebRequest)WebRequest.Create(priceFilePath);
			downloadFileRequest.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);
			downloadFileRequest.Method = WebRequestMethods.Ftp.DownloadFile;

			return SupplierProviderService.PriceListDownloadService.DownloadPriceList(downloadFileRequest, DirectoryPathForDownloadFile, priceFileName, PriceListFileExtension, false, null, canCleanDirectory);
		}

		private string _ftpLogin;

		private string _ftpPassword;

		private const string IproFtpLoginKey = "IproFtpLoginKey";

		private const string IproFtpPasswordKey = "IproFtpPasswordKey";

		public override void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
		{
			if (systemConfigurationParameters != null && systemConfigurationParameters.Any())
			{
				if (systemConfigurationParameters.ContainsKey(IproFtpLoginKey) && systemConfigurationParameters[IproFtpLoginKey] != null)
					_ftpLogin = systemConfigurationParameters[IproFtpLoginKey];

				if (systemConfigurationParameters.ContainsKey(IproFtpPasswordKey) && systemConfigurationParameters[IproFtpPasswordKey] != null)
					_ftpPassword = systemConfigurationParameters[IproFtpPasswordKey];
			}

			base.ApplySupplierConfigurationParameters(systemConfigurationParameters);
		}
	}
}