﻿using Newtonsoft.Json;

namespace Ipro.Data.Api
{
    public class GoodsResponse
    {
        [JsonProperty("data")]
        public GoodsResponseData Data { get; set; }
    }
}