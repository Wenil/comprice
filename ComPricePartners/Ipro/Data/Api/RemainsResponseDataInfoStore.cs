﻿using Newtonsoft.Json;

namespace Ipro.Data.Api
{
    public class RemainsResponseDataInfoStore
    {
        [JsonProperty("StoreCode")]
        public string Code { get; set; }

        [JsonProperty("StoreType")]
        public string Type { get; set; }

        [JsonProperty("StoreQuantRem")]
        public string Quantity { get; set; }
    }
}