﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ipro.Data.Api
{
    public class RemainsResponseData
    {
        [JsonProperty("InfoStores")]
        public List<RemainsResponseDataInfoStore> InfoStores { get; set; }
    }
}