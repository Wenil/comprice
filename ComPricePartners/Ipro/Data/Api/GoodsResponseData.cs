﻿using Newtonsoft.Json;

namespace Ipro.Data.Api
{
    public class GoodsResponseData
    {
        [JsonProperty("gdsNameTitle")]
        public string Name { get; set; }

        [JsonProperty("gdsCode")]
        public string Code { get; set; }

        [JsonProperty("gdsMnfName")]
        public string Brand { get; set; }
    }
}