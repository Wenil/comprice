﻿using Newtonsoft.Json;

namespace Ipro.Data.Api
{
    public class LoginResponse
    {
        [JsonProperty("data")]
        public LoginResponseData Data { get; set; }
    }
}