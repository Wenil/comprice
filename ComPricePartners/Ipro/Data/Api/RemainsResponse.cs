﻿using Newtonsoft.Json;

namespace Ipro.Data.Api
{
    public class RemainsResponse
    {
        [JsonProperty("data")]
        public RemainsResponseData Data { get; set; }
    }
}