﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ipro.Data.Api
{
    public class PriceResponseData
    {
        [JsonProperty("rows")]
        public List<PriceResponseDataRow> Rows { get; set; }
    }
}