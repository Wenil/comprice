﻿using Newtonsoft.Json;

namespace Ipro.Data.Api
{
    public class PriceResponse
    {
        [JsonProperty("data")]
        public PriceResponseData Data { get; set; }
    }
}