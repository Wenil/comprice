﻿using Newtonsoft.Json;

namespace Ipro.Data.Api
{
    public class LoginResponseData
    {
        [JsonProperty("session")]
        public string Session { get; set; }        
    }
}