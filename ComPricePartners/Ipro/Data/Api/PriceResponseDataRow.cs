﻿using Newtonsoft.Json;

namespace Ipro.Data.Api
{
    public class PriceResponseDataRow
    {
        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("pricewnds")]
        public string PriceWithDns { get; set; }

        [JsonProperty("price_tarif")]
        public string PriceTarif { get; set; }

        [JsonProperty("price_retail")]
        public string PriceRetail { get; set; }
    }
}