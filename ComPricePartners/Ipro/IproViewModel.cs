﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Inline
{
	public class IproViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public IproViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "IproLogin", "IproPassword");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "IproInStockDeliveryDay", "IproUnderOrderDeliveryDay", "IproSpecifyDeliveryDay");
		}

		public string IproFtpLoginKey
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("IproFtpLoginKey");
			set => _systemConfigurationService.SetSystemConfigurationParameter("IproFtpLoginKey", value);
		}

		public string IproFtpPasswordKey
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("IproFtpPasswordKey");
			set => _systemConfigurationService.SetSystemConfigurationParameter("IproFtpPasswordKey", value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}