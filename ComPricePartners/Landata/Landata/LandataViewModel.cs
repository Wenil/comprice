﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Landata
{
	public class LandataViewModel : SupplierBaseViewModel
	{
		public LandataViewModel(ISystemConfigurationService systemConfigurationService)
		{
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "LandataLogin", "LandataPassword");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "LandataInStockDeliveryDay", "LandataUnderOrderDeliveryDay", "LandataSpecifyDeliveryDay");
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}