﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;

namespace Landata
{
	class Landata : SupplierBase
	{
		public Landata(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
		}

		private const string UrlLogin = "https://www.landata.ru/";

		private const string UrlFileDownload = "http://www.landata.ru/tranzit/download/index.php";

		private CookieContainer _cookieContainer;

		private string DownloadPrice()
		{
			var priceFilePath = string.Empty;
			var sessionCookie = LoginOnLandata();
			if (sessionCookie.Any())
			{
				var downloadFileRequest = (HttpWebRequest) WebRequest.Create(UrlFileDownload);
				downloadFileRequest.CookieContainer = _cookieContainer;

				return SupplierProviderService.PriceListDownloadService.DownloadPriceList(downloadFileRequest, DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			}

			return priceFilePath;
		}

		private const string LoginPostDataFormat = "backurl=%2Findex.php&AUTH_FORM=Y&TYPE=AUTH&USER_LOGIN={0}&USER_PASSWORD={1}&Login=%C2%EE%E9%F2%E8";

		private Dictionary<string, string> LoginOnLandata()
		{
			var response = new Dictionary<string, string>();

			var requestAuthentication = (HttpWebRequest) WebRequest.Create(UrlLogin);
			requestAuthentication.Method = WebRequestMethods.Http.Post;

			requestAuthentication.Host = @"www.landata.ru";
			requestAuthentication.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0";
			requestAuthentication.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";

			requestAuthentication.KeepAlive = true;
			requestAuthentication.AllowAutoRedirect = false;
			requestAuthentication.Referer = @"http://www.landata.ru/";
			requestAuthentication.ServicePoint.Expect100Continue = false;

			var data = Encoding.Default.GetBytes(string.Format(LoginPostDataFormat, SupplierLogin, SupplierPassword));

			requestAuthentication.ContentType = "application/x-www-form-urlencoded";
			requestAuthentication.ContentLength = data.Length;

			using (var stream = requestAuthentication.GetRequestStream())
				stream.Write(data, 0, data.Length);

			_cookieContainer = requestAuthentication.CookieContainer = new CookieContainer();

			var responseAuthentication = (HttpWebResponse) requestAuthentication.GetResponse();

			if (responseAuthentication.Cookies?["PHPSESSID"] != null)
				response.Add("PHPSESSID", responseAuthentication.Cookies["PHPSESSID"].Value);

			responseAuthentication.Close();

			return response;
		}

		public override string Name { get; } = "LanD";

		public override string Header { get; } = "LanD";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken) => AutomaticProductSearch(articles, cancellationToken);

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest
				{
					FilePath = filePath,
					Name = 3,
					Manufacturer = 1,
					Currency = 8,
					FindLookIn = 2,
					Price = new List<int> {7},
					Quantity = new List<int> {6},
					Article = 2
				};

				var findProductByArticles = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						if (findProductByArticle.FindArticle != null && findProductByArticle.FindArticle.ToString().ToUpper().Equals(findProductByArticle.Article))
						{
							var name = findProductByArticle.Name.ToString();
							name = name.Trim();

							var quantity = 0;
							foreach (var quantityFindValue in findProductByArticle.Quantity)
							{
								if (quantityFindValue != null)
								{
									var quantityString = quantityFindValue.ToString().Trim();
									if (quantityString.ToLower().Equals("есть"))
										quantity += 1;
									else if (!string.IsNullOrEmpty(quantityString))
									{
										int.TryParse(quantityString, out var quantityInt);
										quantity += quantityInt;
									}
								}
							}

							var currency = findProductByArticle.Currency as string;
							currency = currency?.Trim();

							var price = 0.0;

							if (findProductByArticle.Price.Any())
							{
								if (findProductByArticle.Price[0] != null)
								{
									var priceString = findProductByArticle.Price[0].ToString().Replace(" ", string.Empty).Trim();
									price = priceString.ToDouble();
								}
							}

							if (currency != null && currency.Equals("EUR"))
							{
								price *= SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.EUR];
								price /= SupplierProviderService.ExchangeRateService.ExchangeRates[Currency.USD];
							}

							var landataProduct = new ComPriceProduct(findProductByArticle.Article, name, findProductByArticle.SheetName, quantity, price, Currency.USD);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(landataProduct, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(landataProduct);
						}
						else
						{
							var landataProduct = new ComPriceProduct(findProductByArticle.Article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(landataProduct, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(new ComPriceProduct(findProductByArticle.Article));
						}
					}
				}
			}

			return result;
		}

		protected override string LoginSupplierFieldName { get; } = "LandataLogin";

		protected override string PasswordSupplierFieldName { get; } = "LandataPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "LandataInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "LandataUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "LandataSpecifyDeliveryDay";
	}
}