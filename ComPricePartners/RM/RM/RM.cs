﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;

namespace RM
{
	class RM : SupplierBase
	{
		public RM(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			PriceListFileExtension = ".xlsx";
		}

		private const string LoginUrl = "https://b2b.resurs-media.ru/netcat/modules/auth/";

		private const string LogoutUrl = "https://b2b.resurs-media.ru/netcat/modules/auth/?&logoff=1&REQUESTED_FROM=/&REQUESTED_BY";

		private const string SearchUrl = "https://b2b.resurs-media.ru/netcat/modules/netshop/post.php";

		private const string UrlPriceList = "https://b2b.resurs-media.ru/excelPrice/price.xlsx";

		private string DownloadPrice()
		{
			var priceFilePath = string.Empty;
			var directory = DirectoryPathForDownloadFile;

			var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(directory, PriceListFileName, PriceListFileExtension);
			if (!string.IsNullOrEmpty(filePath))
				return filePath;

			var cookies = LoginOnRm();
			if (cookies.Any())
			{
				var webRequestDownloadFile = (HttpWebRequest) WebRequest.Create(UrlPriceList);
				webRequestDownloadFile.Method = WebRequestMethods.Http.Get;
				webRequestDownloadFile.Host = "b2b.resurs-media.ru";
				webRequestDownloadFile.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0";
				webRequestDownloadFile.Accept = "application/json, text/plain, */*";
				webRequestDownloadFile.KeepAlive = true;
				webRequestDownloadFile.AllowAutoRedirect = false;
				webRequestDownloadFile.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
				webRequestDownloadFile.Headers.Add("Accept-Encoding", "gzip, deflate, br");

				webRequestDownloadFile.CookieContainer = new CookieContainer();
				var uriPriceDownload = new Uri(UrlPriceList);

				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("ced", cookies["ced"]));

				webRequestDownloadFile.AllowAutoRedirect = false;


				foreach (var file in Directory.GetFiles(directory))
					File.Delete(file);

				priceFilePath = SupplierProviderService.PriceListDownloadService.DownloadPriceList(webRequestDownloadFile, directory, PriceListFileName, PriceListFileExtension, false);

				LogoutOnRm(cookies);

				return priceFilePath;
			}

			return priceFilePath;
		}

		private Dictionary<string, string> LoginOnRm()
		{
			var cookies = new Dictionary<string, string>();
			var requestLogin = (HttpWebRequest) WebRequest.Create(LoginUrl);
			requestLogin.Method = WebRequestMethods.Http.Post;

			ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

			requestLogin.Host = "b2b.resurs-media.ru";
			requestLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0";
			requestLogin.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			requestLogin.KeepAlive = true;
			requestLogin.AllowAutoRedirect = true;
			requestLogin.Referer = "https://b2b.resurs-media.ru/";
			requestLogin.CookieContainer = new CookieContainer();

			var postData1 = "AuthPhase=1&REQUESTED_FROM=%2F+&REQUESTED_BY=GET&catalogue=1&sub=334&cc=202";
			postData1 += "&AUTH_USER=" + SupplierLogin;
			postData1 += "&AUTH_PW=" + SupplierPassword;
			postData1 += "&submit=%D0%90%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D1%8C%D1%81%D1%8F";

			var data1 = Encoding.UTF8.GetBytes(postData1);

			requestLogin.ContentType = "application/x-www-form-urlencoded";
			requestLogin.ContentLength = data1.Length;
			requestLogin.AllowAutoRedirect = false;

			using (var streamDataLogin = requestLogin.GetRequestStream())
				streamDataLogin.Write(data1, 0, data1.Length);

			var responseLogin = (HttpWebResponse) requestLogin.GetResponse();
			if (responseLogin.Cookies?["ced"] != null)
				cookies.Add("ced", responseLogin.Cookies["ced"].Value);

			responseLogin.Close();

			return cookies;
		}

		private void LogoutOnRm(Dictionary<string, string> cookies)
		{
			var requestLogout = (HttpWebRequest) WebRequest.Create(LogoutUrl);
			requestLogout.Method = WebRequestMethods.Http.Get;

			requestLogout.CookieContainer = new CookieContainer();
			var uriLogout = new Uri(LogoutUrl);
			requestLogout.CookieContainer.Add(uriLogout, new Cookie("ced", cookies["ced"]));

			requestLogout.Host = "beta.resurs-media.ru";
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0";
			requestLogout.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = true;

			var responseLogout = (HttpWebResponse) requestLogout.GetResponse();

			responseLogout.Close();
		}

		public override string Name { get; } = "RM";

		public override string Header { get; } = "RM";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var rm = new List<ComPriceProduct>();
			var cookie = LoginOnRm();
			if (cookie.Any())
			{
				foreach (var article in articles)
				{
					var requestSearch = (HttpWebRequest) WebRequest.Create(SearchUrl);
					requestSearch.Method = WebRequestMethods.Http.Post;

					requestSearch.Host = "b2b.resurs-media.ru";
					requestSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0";
					requestSearch.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
					requestSearch.KeepAlive = true;
					requestSearch.AllowAutoRedirect = false;
					requestSearch.Referer = "https://b2b.resurs-media.ru/";

					requestSearch.CookieContainer = new CookieContainer();
					var uriSearch = new Uri(SearchUrl);
					requestSearch.CookieContainer.Add(uriSearch, new Cookie("ced", cookie["ced"]));

					var postData1 = "REQUEST_URI=%2Fnetshop%2F%3Fname%3D";
					postData1 += article;
					postData1 += "&controller=CContentLoader";

					var data1 = Encoding.UTF8.GetBytes(postData1);

					requestSearch.ContentType = "application/x-www-form-urlencoded";
					requestSearch.ContentLength = data1.Length;
					requestSearch.AllowAutoRedirect = false;

					using (var streamDataLogin = requestSearch.GetRequestStream())
						streamDataLogin.Write(data1, 0, data1.Length);

					var responseSearch = (HttpWebResponse) requestSearch.GetResponse();
					using (var streamResponseSearch = new StreamReader(responseSearch.GetResponseStream(), Encoding.UTF8))
					{
						var responseHtml = streamResponseSearch.ReadToEnd();
						if (!string.IsNullOrEmpty(responseHtml))
						{
							var jsonResponse = JObject.Parse(responseHtml);
							var dataItems = jsonResponse["content"]["dataitems"];
							if (dataItems?["items"] != null && dataItems["items"].Any())
							{
								foreach (var dataItem in dataItems["items"])
								{
									if (dataItem.First != null)
									{
										var productData = dataItem.First;
										if (
											(productData["Nomencl_Articul_Proizvod"].ToString().Equals(article) || productData["ItemID"].ToString().Equals(article))
											&& productData["NotDiscPrice_Value"].ToString().Equals("0")
										)
										{
											var item = new ComPriceProduct
											{
												Article = article,
												Name = productData["Name"].ToString(),
												Brand = productData["manufacturer_Name"].ToString()
											};

											var quantityString = productData["Svobod_Nalichie"].ToString();
											var quantity = 0;
											if (quantityString.Contains("Нет"))
												quantity = 0;
											else if (quantityString.Contains("Мало"))
												quantity = 1;
											else if (quantityString.Contains("Средне"))
												quantity = 2;
											else if (quantityString.Contains("Много"))
												quantity = 3;

											item.Quantity = quantity;

											var coast = productData["Price_Value"].ToString().ToDouble();
											item.Price = coast;

											SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
											rm.Add(item);
										}
									}
								}
							}
						}
					}

					responseSearch.Close();
				}

				LogoutOnRm(cookie);
			}

			return rm;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = DownloadPrice();
			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest {FilePath = filePath, Name = 6, Manufacturer = 3, FindLookIn = 3, Article = 5};
				excelProcessingRequest.SheetsNumberToSearch.Add(1);
				excelProcessingRequest.Quantity.Add(11);
				excelProcessingRequest.Price.Add(9);

				var findProductByArticles = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						if (findProductByArticle.FindArticle != null && findProductByArticle.FindArticle.Equals(findProductByArticle.Article))
						{
							var name = findProductByArticle.Name.ToString();
							name = name.Trim();

							var manufacturer = findProductByArticle.Manufacturer.ToString();
							manufacturer = manufacturer.Trim();

							var quantity = 0;
							foreach (var quantityFindValue in findProductByArticle.Quantity)
							{
								var quantityString = quantityFindValue.ToString().Trim();

								if (!string.IsNullOrEmpty(quantityString))
								{
									if (quantityString.Contains("Нет"))
										quantity += 0;
									else if (quantityString.Contains("Мало"))
										quantity += 1;
									else if (quantityString.Contains("Средне"))
										quantity += 2;
									else if (quantityString.Contains("Много"))
										quantity += 3;
									else
									{
										quantity += quantityString.ToInt();
									}
								}
							}

							var price = 0.0;

							if (findProductByArticle.Price.Any())
							{
								if (findProductByArticle.Price[0] != null)
								{
									var priceString = findProductByArticle.Price[0].ToString().Replace(" ", string.Empty).Trim();
									price = priceString.ToDouble();
								}
							}

							var item = new ComPriceProduct(findProductByArticle.Article, name, manufacturer, quantity, price, Currency.USD);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(item);
						}
						else
						{
							var item = new ComPriceProduct(findProductByArticle.Article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(item, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							result.Add(item);
						}
					}
				}
			}

			return result;
		}

		protected override string LoginSupplierFieldName { get; } = "RMLogin";

		protected override string PasswordSupplierFieldName { get; } = "RMPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "RMInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "RMUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "RMSpecifyDeliveryDay";
	}
}