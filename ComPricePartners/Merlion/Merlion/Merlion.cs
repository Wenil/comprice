﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Merlion
{
	class Merlion : SupplierBase
	{
		public Merlion(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			PriceListFileExtension = ".xlsm";
		}

		private string _clientCode = string.Empty;

		private const string MerlionClientKey = "MerlionClientKey";

		private const string UrlLogin = "https://b2b.merlion.com/api/login";

		private const string UrlSearchTemplate = "https://b2b.merlion.com/api/v1/sphinx/search/main?text={0}&onlyItems=1";

		private const string UrlLogout = "https://b2b.merlion.com/api/logout/b2e24e0348e8453f0b2e17437ddb0e08";

		private Dictionary<string, string> LoginOnMerlion()
		{
			var cookies = new Dictionary<string, string>();

			var requestLogin = (HttpWebRequest) WebRequest.Create(UrlLogin);
			requestLogin.Method = WebRequestMethods.Http.Post;
			requestLogin.AllowAutoRedirect = false;
			requestLogin.Host = "b2b.merlion.com";
			requestLogin.ContentType = "application/json";
			requestLogin.Referer = "https://b2b.merlion.com/";
			requestLogin.Accept = "*/*";
			requestLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0";
			requestLogin.KeepAlive = true;
			requestLogin.CookieContainer = new CookieContainer();
			requestLogin.Headers.Add("authorization", "Bearer initial");

			var loginRequest = new LoginRequest
			{
				clientLogin = SupplierLogin,
				clientNo = _clientCode,
				password = SupplierPassword
			};
			var loginRequestJson = JsonConvert.SerializeObject(loginRequest);
			requestLogin.ContentLength = Encoding.Default.GetBytes(loginRequestJson).Length;

			using (var streamWriter = new StreamWriter(requestLogin.GetRequestStream()))
				streamWriter.Write(loginRequestJson);

			var responseLogin = (HttpWebResponse) requestLogin.GetResponse();
			using (var stream = new StreamReader(responseLogin.GetResponseStream(), Encoding.Default))
			{
				if (responseLogin.Cookies?["_csrf"] != null && responseLogin.Cookies["refresh"] != null &&
				    responseLogin.Cookies["token"] != null && responseLogin.Cookies["uid"] != null)
				{
					cookies.Add("_csrf", responseLogin.Cookies["_csrf"].Value);
					cookies.Add("refresh", responseLogin.Cookies["refresh"].Value);
					cookies.Add("token", responseLogin.Cookies["token"].Value);
					cookies.Add("uid", responseLogin.Cookies["uid"].Value);
				}

				var result = stream.ReadToEnd();
				if (!string.IsNullOrEmpty(result))
				{
					var requestBody = JObject.Parse(result);
					if (requestBody != null)
						cookies.Add("csrf_token", requestBody["csrf_token"].ToString(Formatting.None));
				}
			}

			responseLogin.Close();

			return cookies;
		}

		private string SearchOnMerlion(Dictionary<string, string> cookies, string article)
		{
			var searchUrl = string.Format(UrlSearchTemplate, article);
			var requestSearch = (HttpWebRequest) WebRequest.Create(searchUrl);
			requestSearch.Method = WebRequestMethods.Http.Post;
			requestSearch.Headers.Add("Authorization", "Bearer " + cookies["csrf_token"]);

			requestSearch.CookieContainer = new CookieContainer();
			var uri = new Uri(searchUrl);
			requestSearch.CookieContainer.Add(uri, new Cookie("_csrf", cookies["_csrf"]));
			requestSearch.CookieContainer.Add(uri, new Cookie("refresh", cookies["refresh"]));
			requestSearch.CookieContainer.Add(uri, new Cookie("token", cookies["token"]));
			requestSearch.CookieContainer.Add(uri, new Cookie("uid", cookies["uid"]));

			requestSearch.ContentType = "application/json";
            requestSearch.Referer = "https://b2b.merlion.com/?action=YB28EC01&action1=YE4D2083";
			requestSearch.Accept = "application/json, text/javascript, */*; q=0.01";
			requestSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0";
			requestSearch.KeepAlive = true;
			requestSearch.AllowAutoRedirect = false;

			var dataJson = JsonConvert.SerializeObject(new SearchRequest());
			var data = Encoding.UTF8.GetBytes(dataJson);
			requestSearch.ContentLength = data.Length;
            using (var stream = requestSearch.GetRequestStream())
                stream.Write(data, 0, data.Length);

            var responseSearch = (HttpWebResponse) requestSearch.GetResponse();

			string searchResult;
			using (var stream = new StreamReader(responseSearch.GetResponseStream(), Encoding.UTF8))
				searchResult = stream.ReadToEnd();

			responseSearch.Close();

			return searchResult;
		}

		private void LogoutOnMerlion(Dictionary<string, string> cookies)
		{
			var requestLogout = (HttpWebRequest) WebRequest.Create(UrlLogout);
			requestLogout.Method = WebRequestMethods.Http.Get;
			requestLogout.AllowAutoRedirect = false;
			requestLogout.Host = "b2b.merlion.com";
			requestLogout.ContentType = "application/json";
			requestLogout.Referer = "https://b2b.merlion.com/";
			requestLogout.Accept = "*/*";
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0";
			requestLogout.KeepAlive = true;
			requestLogout.CookieContainer = new CookieContainer();

			var uri = new Uri(UrlLogout);
			requestLogout.CookieContainer.Add(uri, new Cookie("_csrf", cookies["_csrf"]));
			requestLogout.CookieContainer.Add(uri, new Cookie("refresh", cookies["refresh"]));
			requestLogout.CookieContainer.Add(uri, new Cookie("token", cookies["token"]));
			requestLogout.CookieContainer.Add(uri, new Cookie("uid", cookies["uid"]));

			requestLogout.AllowAutoRedirect = true;

			var responseLogout = (HttpWebResponse) requestLogout.GetResponse();
			using (var stream = new StreamReader(responseLogout.GetResponseStream(), Encoding.Default))
				stream.ReadToEnd();

			responseLogout.Close();
		}

		private const string UrlPriceDownload = "https://b2b.merlion.com/api/v1/pricelists/get?lol=29b80af6324cfbe929bf798f14829bbe&type=xlsm";

		public string DownloadPrice()
		{
			var priceFilePath = string.Empty;
			var cookies = LoginOnMerlion();
			if (cookies.Any())
			{
				var webRequestDownloadFile = (HttpWebRequest) WebRequest.Create(UrlPriceDownload);
				webRequestDownloadFile.Method = WebRequestMethods.Http.Get;

				webRequestDownloadFile.CookieContainer = new CookieContainer();
				var uriPriceDownload = new Uri(UrlPriceDownload);

				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("_csrf", cookies["_csrf"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("refresh", cookies["refresh"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("token", cookies["token"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("uid", cookies["uid"]));

				webRequestDownloadFile.Headers.Add("authorization", "Bearer " + cookies["csrf_token"]);
				webRequestDownloadFile.AllowAutoRedirect = false;

				return SupplierProviderService.PriceListDownloadService.DownloadPriceList(webRequestDownloadFile, DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension, true);
			}

			return priceFilePath;
		}

		public override string Name { get; } = "Merl";

		public override string Header { get; } = "Merl";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var merlionItems = new List<ComPriceProduct>();

			var cookies = LoginOnMerlion();
			if (cookies.Any())
			{
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					var articleTemp = article;
					var realArticle = articleTemp;
					articleTemp = articleTemp.ToUpper();

					var response = SearchOnMerlion(cookies, articleTemp);
					if (!string.IsNullOrEmpty(response))
					{
						var itemAdd = false;
						try
						{
							var jsonResponse = JObject.Parse(response);
							if (jsonResponse != null && jsonResponse["data"] != null && jsonResponse["data"]["data"] != null)
							{
								var listProduct = jsonResponse["data"]["data"];
								foreach (var item in listProduct)
								{
									var itemName = item["name"].ToString();
									if (!string.IsNullOrEmpty(itemName))
									{
										try
										{
											var vendorPart = item["vendorPart"].ToString().ToUpper();

											if (IsEqualArticle)
											{
												var isGarbage = false;
												if (!articleTemp.Equals(vendorPart))
												{
													isGarbage = true;
													var vendorPartSplit = vendorPart.Split(' ');
													var vendorPartSplit2 = vendorPart.Split('#');
													if (vendorPartSplit.Length > 0)
													{
														if (vendorPartSplit.Any(split => articleTemp.Equals(split.ToUpper())))
															isGarbage = false;
													}

													if (vendorPartSplit2.Length > 0 && vendorPart.Contains('#'))
													{
														if (vendorPartSplit2.Any(articles.Contains))
															isGarbage = false;
													}
												}

												if (isGarbage)
													continue;
											}

											var quantity = item["inventoryMSK1"].ToString().Replace("+", string.Empty);
											var priceClient = item["priceClient"].ToString().Replace(".", ",");

											if (priceClient.Equals(",00"))
												priceClient = "0";

											var brand = item["brand"].ToString();

											var merlionItem = new ComPriceProduct(realArticle, itemName, brand, quantity.ToInt(), priceClient.ToDouble());

											if (SupplierProviderService.CheckConditionService.IsGarbageName(merlionItem.Name))
												continue;

											for (var j = 0; j < merlionItems.Count; j++)
											{
												if (merlionItems[j].Article.Equals(merlionItem.Article))
												{
													if (merlionItem.Price < merlionItems[j].Price)
													{
														if (merlionItem.Quantity == merlionItems[j].Quantity)
															merlionItems[j].Price = merlionItem.Price;
														else if (merlionItem.Quantity > 0)
														{
															merlionItems[j].Quantity = merlionItem.Quantity;
															merlionItems[j].Price = merlionItem.Price;
														}
													}
													else
													{
														if (merlionItem.Quantity > 0 && merlionItems[j].Quantity < 1)
														{
															merlionItems[j].Quantity = merlionItem.Quantity;
															merlionItems[j].Price = merlionItem.Price;
														}
													}

													itemAdd = true;
												}
											}

											if (!itemAdd)
											{
												SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(merlionItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
												merlionItems.Add(merlionItem);
											}

											itemAdd = true;
										}
										catch
										{
											//
										}
									}
								}
							}
						}
						catch (Exception)
						{
							Console.WriteLine(@"Неудалось получить данные в Merlion");
						}

						if (!itemAdd)
						{
							var merlionItem = new ComPriceProduct(article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(merlionItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							merlionItems.Add(merlionItem);
						}
					}
				}

				LogoutOnMerlion(cookies);
			}

			return merlionItems;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var merlionResults = new List<ComPriceProduct>();

			var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest {FilePath = filePath, Name = 8, Manufacturer = 4, FindLookIn = 2, Article = 7};
				excelProcessingRequest.SheetsNumberToSearch.Add(1);
				excelProcessingRequest.Quantity.Add(12);
				excelProcessingRequest.Price.Add(10);

				var findProductByArticles = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						if (findProductByArticle.FindArticle != null && findProductByArticle.FindArticle.Equals(findProductByArticle.Article))
						{
							var name = findProductByArticle.Name as string;
							name = name?.Trim();

							var manufacturer = findProductByArticle.Manufacturer as string;
							manufacturer = manufacturer?.Trim();

							var quantity = 0;

							foreach (var quantityCellValue in findProductByArticle.Quantity)
							{
								var quantityString = quantityCellValue?.ToString().Trim();
								if (!string.IsNullOrEmpty(quantityString) && quantityString.Contains("+"))
									quantity += 2;
							}

							var price = 0.0;
							if (findProductByArticle.Price.Any())
							{
								var priceString = findProductByArticle.Price[0].ToString();

								if (!string.IsNullOrEmpty(priceString))
									price = priceString.ToDouble();
							}

							var merlionItem = new ComPriceProduct(findProductByArticle.Article, name, manufacturer, quantity, price, Currency.USD);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(merlionItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							merlionResults.Add(merlionItem);
						}
						else
						{
							var merlionItem = new ComPriceProduct(findProductByArticle.Article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(merlionItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							merlionResults.Add(merlionItem);
						}
					}
				}
			}

			return merlionResults;
		}

		protected override string LoginSupplierFieldName { get; } = "MerlionLogin";

		protected override string PasswordSupplierFieldName { get; } = "MerlionPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "MerlionInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "MerlionUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "MerlionSpecifyDeliveryDay";

		public override void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
		{
			if (systemConfigurationParameters != null && systemConfigurationParameters.Any())
			{
				if (systemConfigurationParameters.ContainsKey(MerlionClientKey) && systemConfigurationParameters[MerlionClientKey] != null)
					_clientCode = systemConfigurationParameters[MerlionClientKey];
			}

			base.ApplySupplierConfigurationParameters(systemConfigurationParameters);
		}
	}

	class LoginRequest
	{
		// ReSharper disable once InconsistentNaming

		public string clientLogin { get; set; }

		// ReSharper disable once InconsistentNaming

		public string clientNo { get; set; }

		// ReSharper disable once InconsistentNaming
		public string password { get; set; }
	}


	class SearchRequest
	{
		public ActiveFilter activeFilters = new ActiveFilter();

		public List<string> brand = new List<string>();
	}

	class ActiveFilter
    {

    }
}