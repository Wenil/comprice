﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Merlion
{
	public class MerlionViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public MerlionViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "MerlionLogin", "MerlionPassword");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "MerlionInStockDeliveryDay", "MerlionUnderOrderDeliveryDay", "MerlionSpecifyDeliveryDay");
		}

		public string MerlionClientKey
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("MerlionClientKey");
			set => _systemConfigurationService.SetSystemConfigurationParameter("MerlionClientKey", value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}