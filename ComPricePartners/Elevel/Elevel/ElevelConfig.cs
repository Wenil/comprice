﻿namespace Elevel
{
	static class ElevelConfig
	{
		public const string ElevelLoginKey = "ElevelLoginKey";
		public const string ElevelPasswordKey = "ElevelPasswordKey";
		public const string ElevelApiLogin = "ElevelApiLogin";
		public const string ElevelApiPassword = "ElevelApiPassword";
		public const string ElevelInStockDeliveryDay = "ElevelInStockDeliveryDay";
		public const string ElevelUnderOrderDeliveryDay = "ElevelUnderOrderDeliveryDay";
		public const string ElevelSpecifyDeliveryDay = "ElevelSpecifyDeliveryDay";
	}
}