﻿using Newtonsoft.Json;

namespace Elevel.Data.Api.Request
{
	public class StocksOfGoodsRequestItem
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("amount")]
		public int Amount { get; set; } = 0;
	}
}