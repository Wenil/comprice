﻿using Elevel.Data.Converter;
using Newtonsoft.Json;
using System;

namespace Elevel.Data.Api.Request
{
	public class NomenclatureRequestUpdateItem
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("modified")]
		[JsonConverter(typeof(DateFormatConverter), "yyyy-MM-dd")]
		public DateTime Modified { get; set; } = DateTime.Now;
	}
}