﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Elevel.Data.Api.Request
{
	public class NomenclatureRequest
	{
		[JsonProperty("update")]
		public List<NomenclatureRequestUpdateItem> Update { get; set; } = new List<NomenclatureRequestUpdateItem>();
	}
}