﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Elevel.Data.Api.Request
{
	public class StocksOfGoodsRequest
	{
		[JsonProperty("goods")]
		public List<StocksOfGoodsRequestItem> Goods { get; set; } = new List<StocksOfGoodsRequestItem>();
	}
}