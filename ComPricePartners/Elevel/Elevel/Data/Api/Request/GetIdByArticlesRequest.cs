﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Elevel.Data.Api.Request
{
	public class GetIdByArticlesRequest
	{
		[JsonProperty("articles")]
		public List<string> Articles { get; set; } = new List<string>();

		[JsonProperty("typeOfSearch")]
		public string TypeOfSearch { get; set; } = "Артикул";
	}
}