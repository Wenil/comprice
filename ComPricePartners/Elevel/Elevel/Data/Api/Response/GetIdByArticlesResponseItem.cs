﻿using Newtonsoft.Json;

namespace Elevel.Data.Api.Response
{
	public class GetIdByArticlesResponseItem
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("article")]
		public string Article { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("categoryId")]
		public string CategoryId { get; set; }

		[JsonProperty("brand")]
		public string Brand { get; set; }

		[JsonProperty("brandId")]
		public string BrandId { get; set; }
	}
}