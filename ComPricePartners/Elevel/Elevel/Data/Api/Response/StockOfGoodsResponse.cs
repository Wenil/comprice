﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Elevel.Data.Api.Response
{
	public class StockOfGoodsResponse
	{
		[JsonProperty("stockOfGoods")]
		public List<StockOfGoodsResponseItem> StockOfGoods { get; set; }

		[JsonProperty("message")]
		public string Message { get; set; }
	}
}