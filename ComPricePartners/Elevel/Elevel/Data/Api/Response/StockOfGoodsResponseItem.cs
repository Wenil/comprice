﻿using Newtonsoft.Json;

namespace Elevel.Data.Api.Response
{
	///
	public class StockOfGoodsResponseItem
	{
		[JsonProperty("nomenclaturaid")]
		/// <summary>
		/// Внутренний идентификатор номенклатуры Элевел.
		/// </summary>
		public string NomenclaturaId { get; set; }

		[JsonProperty("articul")]
		/// <summary>
		/// Артикул производителя.
		/// </summary>
		public string Articul { get; set; }

		[JsonProperty("id")]
		/// <summary>
		/// Идентификатор – число, уникальный номер карточки номенклатуры.
		/// </summary>
		public string Id { get; set; }

		[JsonProperty("price")]
		/// <summary>
		/// Цена клиента.
		/// </summary>
		public string Price { get; set; }

		[JsonProperty("priceBasic")]
		/// <summary>
		/// Цена по тарифу.
		/// </summary>
		public string PriceBasic { get; set; }

		[JsonProperty("name")]
		/// <summary>
		/// Название карточки номенклатуры.
		/// </summary>
		public string Name { get; set; }

		[JsonProperty("stock")]
		/// <summary>
		/// Остаток номенклатуры на основных складах текстом.
		/// </summary>
		public string Stock { get; set; }

		[JsonProperty("stockamount")]
		/// <summary>
		/// Остаток номенклатуры на основных складах.
		/// </summary>
		public string StockAmount { get; set; }

		[JsonProperty("stockamountAdd")]
		/// <summary>
		/// Остаток номенклатуры на дополнительных складах.
		/// </summary>
		public string StockAmountAdd { get; set; }

		[JsonProperty("shippingDate")]
		/// <summary>
		/// Дата возможности отгрузки со склада Элевел.
		/// </summary>
		public string shippingDate { get; set; }
	}
}
