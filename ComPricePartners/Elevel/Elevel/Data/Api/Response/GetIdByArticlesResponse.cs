﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Elevel.Data.Api.Response
{
	public class GetIdByArticlesResponse
	{
		[JsonProperty("result")]
		public List<GetIdByArticlesResponseItem> Result { get; set; }
	}
}