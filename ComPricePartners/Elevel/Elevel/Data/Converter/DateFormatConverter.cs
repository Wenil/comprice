﻿using Newtonsoft.Json.Converters;

namespace Elevel.Data.Converter
{
	public class DateFormatConverter : IsoDateTimeConverter
	{
		public DateFormatConverter(string format)
		{
			DateTimeFormat = format;
		}
	}
}