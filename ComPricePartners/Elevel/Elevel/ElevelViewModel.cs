﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Elevel
{
	public class ElevelViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public ElevelViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, ElevelConfig.ElevelLoginKey, ElevelConfig.ElevelPasswordKey);
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, ElevelConfig.ElevelInStockDeliveryDay, ElevelConfig.ElevelUnderOrderDeliveryDay, ElevelConfig.ElevelSpecifyDeliveryDay);
		}

		public string ElevelApiLogin
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter(ElevelConfig.ElevelApiLogin);
			set => _systemConfigurationService.SetSystemConfigurationParameter(ElevelConfig.ElevelApiLogin, value);
		}

		public string ElevelApiPassword
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter(ElevelConfig.ElevelApiPassword);
			set => _systemConfigurationService.SetSystemConfigurationParameter(ElevelConfig.ElevelApiPassword, value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}