﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Elevel.Data.Api.Request;
using Elevel.Data.Api.Response;
using Newtonsoft.Json;

namespace Elevel
{
    class Elevel : SupplierBase
    {
        public Elevel(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
        {
        }

        private string _apiLogin = string.Empty;

        private string _apiPassword = string.Empty;

        private const string HostElevelApi = "http://swop.krokus.ru";

        public override string Name { get; } = "elvl";

        public override string Header { get; } = "Elvl";

        protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
        {
            var elevelResult = new List<ComPriceProduct>();

            if (cancellationToken.IsCancellationRequested)
                throw new Exception("TerminateSearch");

            var client = new HttpClient
            {
                BaseAddress = new Uri(HostElevelApi)
            };
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.ConnectionClose = true;


            var authenticationString = $"{_apiLogin}:{_apiPassword}";
            var base64EncodedAuthenticationString =
                Convert.ToBase64String(Encoding.UTF8.GetBytes(authenticationString));
            var authenticationHeaderValue = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);


            var getIdByArticlesRequest = new GetIdByArticlesRequest() { Articles = articles };
            var getIdByArticlesRequestJson = JsonConvert.SerializeObject(getIdByArticlesRequest);

            var getIdByArticlesRequestResponseBody = SendRequest(client, "/ExchangeBase/hs/catalog/getidbyarticles", new StringContent(getIdByArticlesRequestJson, Encoding.UTF8, "application/json"), authenticationHeaderValue);

            var getIdByArticlesResponse =
                JsonConvert.DeserializeObject<GetIdByArticlesResponse>(getIdByArticlesRequestResponseBody);

            if (getIdByArticlesResponse != null && getIdByArticlesResponse.Result != null &&
                getIdByArticlesResponse.Result.Any())
            {
                var nomenclatureRequest = new NomenclatureRequest();
                var stocksOfGoodsRequest = new StocksOfGoodsRequest();
                getIdByArticlesResponse.Result
                    .ForEach(item =>
                        {
                            nomenclatureRequest.Update.Add(new NomenclatureRequestUpdateItem() { Id = item.Id });
                            stocksOfGoodsRequest.Goods.Add(new StocksOfGoodsRequestItem() { Id = item.Id });
                        }
                    );

                var nomenclatureRequestJson = JsonConvert.SerializeObject(nomenclatureRequest);
                SendRequest(client, "/ExchangeBase/hs/catalog/nomenclature?fieldSet=min", new StringContent(nomenclatureRequestJson, Encoding.UTF8, "application/json"), authenticationHeaderValue);

                var threadSleep = nomenclatureRequest.Update.Count * 700;
                Thread.Sleep(threadSleep);

                var stocksOfGoodsRequestJson = JsonConvert.SerializeObject(stocksOfGoodsRequest);
                var stocksOfGoodsRequestResponseBody = SendRequest(client, "/ExchangeBase/hs/catalog/stockOfGoods",
                    new StringContent(stocksOfGoodsRequestJson, Encoding.UTF8, "application/json"),
                    authenticationHeaderValue);

                var stockOfGoodsResponse =
                    JsonConvert.DeserializeObject<StockOfGoodsResponse>(stocksOfGoodsRequestResponseBody);

                if (stockOfGoodsResponse.StockOfGoods != null)
                    elevelResult.AddRange(ComposeElevelResult(stockOfGoodsResponse.StockOfGoods, articles,
                        getIdByArticlesResponse.Result));
            }

            return elevelResult;
        }

        private string SendRequest(HttpClient httpClient, string requestUri, HttpContent requestContent, AuthenticationHeaderValue authenticationHeaderValue)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUri);
            requestMessage.Headers.Authorization = authenticationHeaderValue;

            requestMessage.Content = requestContent;

            var task = httpClient.SendAsync(requestMessage);
            var response = task.Result;
            response.EnsureSuccessStatusCode();

            return response.Content.ReadAsStringAsync().Result;
        }

        private List<ComPriceProduct> ComposeElevelResult(List<StockOfGoodsResponseItem> responseItems, List<string> articles, List<GetIdByArticlesResponseItem> getIdByArticlesResponseItems)
        {
            var elevelResult = new List<ComPriceProduct>();

            foreach (var article in articles)
            {
                var findItems = responseItems.Where(item => item.Articul.Equals(article));
                var itemAdd = false;
                foreach (var findItem in findItems)
                {
                    if (itemAdd)
                        break;

                    var name = findItem.Name;
                    var brand = string.Empty;
                    var productData = getIdByArticlesResponseItems.FirstOrDefault(item => item.Article.Equals(article));
                    if (productData != null)
                        brand = productData.Brand;

                    var price = findItem.Price.ToDouble();
                    int quantity;
                    var mainQuantity = findItem.StockAmount.ToInt();
                    var additionalQuantity = findItem.StockAmountAdd.ToInt();
                    string quantityMessage;

                    if (mainQuantity == 0 && additionalQuantity > 0)
                    {
                        quantityMessage = "Под заказ до 10 дней";
                        quantity = additionalQuantity;
                    }
                    else if (mainQuantity == 0 && additionalQuantity == 0)
                        continue;  
                    else
                    {
                        quantity = mainQuantity;
                        quantityMessage = string.Empty;
                    }

                    var product = new ComPriceProduct(article, name, brand, quantity, price) { QuantityMessage = quantityMessage };
                    SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(product, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                    product.Currency = Currency.RUB;

                    var isGarbage = false;
                    if (IsEqualArticle)
                    {
                        if (product.Name.ToLower(CultureInfo.CurrentCulture).Contains("(упаковка)"))
                            isGarbage = true;
                    }

                    if (!isGarbage)
                    {
                        elevelResult.Add(product);
                        itemAdd = true;
                    }
                }
            }

            return elevelResult;
        }

        protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken) => ManualProductSearch(articles, cancellationToken);

        protected override string LoginSupplierFieldName { get; } = ElevelConfig.ElevelLoginKey;

        protected override string PasswordSupplierFieldName { get; } = ElevelConfig.ElevelPasswordKey;

        protected override string InStockDeliveryDaySupplierFieldName { get; } = ElevelConfig.ElevelInStockDeliveryDay;

        protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = ElevelConfig.ElevelUnderOrderDeliveryDay;

        protected override string SpecifyDeliveryDaySupplierFieldName { get; } = ElevelConfig.ElevelSpecifyDeliveryDay;

        public override void ApplySupplierConfigurationParameters(
            Dictionary<string, string> systemConfigurationParameters)
        {
            if (systemConfigurationParameters != null && systemConfigurationParameters.Any())
            {
                if (systemConfigurationParameters.ContainsKey(ElevelConfig.ElevelApiLogin) && systemConfigurationParameters[ElevelConfig.ElevelApiLogin] != null)
                    _apiLogin = systemConfigurationParameters[ElevelConfig.ElevelApiLogin];

                if (systemConfigurationParameters.ContainsKey(ElevelConfig.ElevelApiPassword) && systemConfigurationParameters[ElevelConfig.ElevelApiPassword] != null)
                    _apiPassword = systemConfigurationParameters[ElevelConfig.ElevelApiPassword];

                base.ApplySupplierConfigurationParameters(systemConfigurationParameters);
            }
        }
    }
}