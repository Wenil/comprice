﻿using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;


namespace AVS
{
    // ReSharper disable once InconsistentNaming
    class AVS : SupplierBase
    {
        public AVS(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
        {

        }

        private const string BaseAvsAddress = "https://avs.express/";

        public override string Name { get; } = "AVS";
        public override string Header { get; } = "AVS";

        protected override string LoginSupplierFieldName { get; } = "AVSLogin";

        protected override string PasswordSupplierFieldName { get; } = "AVSPassword";

        protected override string InStockDeliveryDaySupplierFieldName { get; } = "AVSInStockDeliveryDay";

        protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "AVSUnderOrderDeliveryDay";

        protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "AVSSpecifyDeliveryDay";

        protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
        {
            return ManualProductSearch(articles, cancellationToken);
        }

        protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
        {
            var avsItems = new List<ComPriceProduct>();
            var token = getAvsToken();
            if (!string.IsNullOrEmpty(token))
            {
                foreach (var article in articles)
                {
                    if (cancellationToken.IsCancellationRequested)
                        throw new Exception("TerminateSearch");

                    avsItems.AddRange(ProductSearch(article, token));
                    Thread.Sleep(500);
                }
            }

            return avsItems;
        }

        private List<ComPriceProduct> ProductSearch(string article, string token)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(BaseAvsAddress)
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var requestArticle = WebUtility.UrlEncode("\"" + article + "\"");
            var response = client.GetAsync($"api/v1/product?filters[keyword]={requestArticle}&page=1&per-page=20").Result;
            client.Dispose();
            if (response.IsSuccessStatusCode)
            {
                var responseBody = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                if (responseBody != null)
                {
                    return ParseResponse(responseBody.GetValue("items") as JArray, article);
                }
            }
            else
            {
                SupplierProviderService.LoggerService.Error(string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
            }

            return new List<ComPriceProduct>() { new ComPriceProduct(article) };
        }

        private List<ComPriceProduct> ParseResponse(JArray productArray, string article)
        {
            var avsItems = new List<ComPriceProduct>();
            var itemAdd = false;
            foreach (var product in productArray)
            {
                itemAdd = false;
                var avsItem = new ComPriceProduct();

                if (IsEqualArticle)
                {
                    var isGarbage = false;
                    if (product["manufacturerCode"] != null && !string.Equals(article, product["manufacturerCode"].ToString(), StringComparison.CurrentCultureIgnoreCase))
                    {
                        isGarbage = true;
                    }

                    if (isGarbage)
                        continue;
                }

                avsItem.Article = article;
                avsItem.Name = product["name"].ToString();
                if (product["brand"] != null && product["brand"].SelectToken("name") != null)
                    avsItem.Brand = product["brand"]["name"].ToString();
                avsItem.ManufacturerArticle = product["manufacturerCode"].ToString();
                avsItem.Price = product["price"]["value"].ToString().ToDouble();
                if (product["stocks"] is JArray stocks)
                {
                    foreach (var stock in stocks)
                    {
                        if (!stock["isVirtual"].Value<bool>())
                        {
                            avsItem.Quantity += stock["amount"].ToString().ToInt();
                        }
                    }
                }

                if (SupplierProviderService.CheckConditionService.IsGarbageName(avsItem.Name) || avsItem.Quantity == 0)
                    continue;

                if (article.Equals(avsItem.ManufacturerArticle))
                {
                    if (avsItems.Count > 0)
                        avsItems.RemoveAll(find => !article.Equals(avsItem.ManufacturerArticle));
                }
                else
                {
                    if (avsItems.Count > 0)
                    {
                        var goodProduct = avsItems.FindAll(find => article.Equals(avsItem.ManufacturerArticle));
                        if (goodProduct.Any())
                            continue;
                    }
                }

                for (var j = 0; j < avsItems.Count; j++)
                {
                    if (avsItems[j].Article.Equals(avsItem.Article))
                    {
                        if (avsItem.Price < avsItems[j].Price)
                        {
                            if (avsItem.Quantity == avsItems[j].Quantity)
                                avsItems[j].Price = avsItem.Price;
                            else if (avsItem.Quantity > 0)
                            {
                                avsItems[j].Quantity = avsItem.Quantity;
                                avsItems[j].Price = avsItem.Price;
                            }
                        }
                        else
                        {
                            if (avsItem.Quantity > 0 && avsItems[j].Quantity < 1)
                            {
                                avsItems[j].Quantity = avsItem.Quantity;
                                avsItems[j].Price = avsItem.Price;
                            }
                        }

                        itemAdd = true;
                    }
                }

                if (!itemAdd)
                {
                    SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(avsItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                    avsItem.Currency = Currency.RUB;

                    avsItems.Add(avsItem);
                }

                itemAdd = true;
            }


            if (!itemAdd)
            {
                var avsItem = new ComPriceProduct(article);
                SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(avsItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
                avsItems.Add(avsItem);
            }


            return avsItems;
        }

        private string getAvsToken()
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(BaseAvsAddress)
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var body = new { phone = SupplierLogin, password = SupplierPassword };
            var response = client.PostAsync("api/v1/user/login", new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json")).Result;
            client.Dispose();
            if (response.IsSuccessStatusCode)
            {
                return JObject.Parse(response.Content.ReadAsStringAsync().Result).GetValue("token").Value<string>();
            }
            else
            {
                SupplierProviderService.LoggerService.Error(string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
            }

            return string.Empty;
        }

    }
}