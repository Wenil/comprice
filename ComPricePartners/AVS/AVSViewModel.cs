﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace eCom
{
	// ReSharper disable once InconsistentNaming
	public class AVSViewModel : SupplierBaseViewModel
	{
		public AVSViewModel(ISystemConfigurationService systemConfigurationService)
		{
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "AVSLogin", "AVSPassword");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "AVSInStockDeliveryDay", "AVSUnderOrderDeliveryDay"
				, "AVSSpecifyDeliveryDay");
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}