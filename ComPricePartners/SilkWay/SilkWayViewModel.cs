﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace SilkWay
{
	internal class SilkWayViewModel : SupplierBaseViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public SilkWayViewModel(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
			SupplierParameterViewModel = new SupplierParameterViewModel(systemConfigurationService, "SilkWayLogin", "SilkWayPassword");
			SupplierDeliveryDayViewModel = new SupplierDeliveryDayViewModel(
				systemConfigurationService, "SilkWayInStockDeliveryDay", "SilkWayUnderOrderDeliveryDay", "SilkWaySpecifyDeliveryDay");
		}

		public string SilkWayClientKey
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter("SilkWayClientKey");
			set => _systemConfigurationService.SetSystemConfigurationParameter("SilkWayClientKey", value);
		}

		public override SupplierParameterViewModel SupplierParameterViewModel { get; }

		public override SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}