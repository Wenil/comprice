﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Suppliers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Infrastructure.Services.Exchange.Data;

namespace SilkWay
{
	public class SilkWay : SupplierBase
	{
		public SilkWay(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			PriceListFileExtension = ".xlsx";
		}

		public override string Name { get; } = "SilkWay";

		public override string Header { get; } = "SilkWay";

		protected override string LoginSupplierFieldName { get; } = "SilkWayLogin";

		protected override string PasswordSupplierFieldName { get; } = "SilkWayPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "SilkWayInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "SilkWayUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "SilkWaySpecifyDeliveryDay";

		private const string UrlLogin = "https://b2b.silkway.org.ru/api/login";

		private const string UrlSearchTemplate = "https://b2b.silkway.org.ru/api/v1/sphinx/search/main?text={0}&onlyItems=1";

		private const string UrlLogout = "https://b2b.silkway.org.ru/api/logout/b2e24e0348e8453f0b2e17437ddb0e08";

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var silkWayResults = new List<ComPriceProduct>();

			var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest { FilePath = filePath, Name = 2, Manufacturer = 3, FindLookIn = 2, Article = 4 };
				excelProcessingRequest.SheetsNumberToSearch.Add(1);
				excelProcessingRequest.Quantity.Add(8);
				excelProcessingRequest.Price.Add(12);

				var findProductByArticles = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						if (findProductByArticle.FindArticle != null && findProductByArticle.FindArticle.Equals(findProductByArticle.Article))
						{
							var name = findProductByArticle.Name as string;
							name = name?.Trim();

							var manufacturer = findProductByArticle.Manufacturer as string;
							manufacturer = manufacturer?.Trim();

							var quantity = 0;

							foreach (var quantityCellValue in findProductByArticle.Quantity)
							{
								var quantityString = quantityCellValue?.ToString().Replace("+", string.Empty).Trim();
								quantity = quantityString.ToInt();
							}

							var price = 0.0;
							if (findProductByArticle.Price.Any())
							{
								var priceString = findProductByArticle.Price[0].ToString();

								if (!string.IsNullOrEmpty(priceString))
									price = priceString.ToDouble();
							}

							var silkWayItem = new ComPriceProduct(findProductByArticle.Article, name, manufacturer, quantity, price, Currency.USD);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(silkWayItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							silkWayResults.Add(silkWayItem);
						}
						else
						{
							var silkWayItem = new ComPriceProduct(findProductByArticle.Article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(silkWayItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							silkWayResults.Add(silkWayItem);
						}
					}
				}
			}

			return silkWayResults;
		}

		private const string UrlPriceDownloadOrder = "https://b2b.silkway.org.ru/api/v1/file/get/export-items-all?type=xlsx";

		private const string UrlPriceDownload = "https://b2b.silkway.org.ru/api/v1/file/get/items";

		public string DownloadPrice()
		{
			var priceFilePath = string.Empty;
			var cookies = LoginOnSilkWay();
			if (cookies.Any())
			{
				var webRequestDownloadFileOrder = (HttpWebRequest)WebRequest.Create(UrlPriceDownloadOrder);
				webRequestDownloadFileOrder.Method = WebRequestMethods.Http.Post;
				webRequestDownloadFileOrder.KeepAlive = true;
				webRequestDownloadFileOrder.ContentType = "application/json";
				webRequestDownloadFileOrder.Referer = "https://b2b.silkway.org.ru/";
				webRequestDownloadFileOrder.Accept = "*/*";
				webRequestDownloadFileOrder.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 YaBrowser/23.7.4.971 Yowser/2.5 Safari/537.36";
				webRequestDownloadFileOrder.ContentLength = 0; ;

				webRequestDownloadFileOrder.CookieContainer = new CookieContainer();
				var uriPriceDownloadOrder = new Uri(UrlPriceDownloadOrder);

				webRequestDownloadFileOrder.CookieContainer.Add(uriPriceDownloadOrder, new Cookie("_csrf", cookies["_csrf"]));
				webRequestDownloadFileOrder.CookieContainer.Add(uriPriceDownloadOrder, new Cookie("refresh", cookies["refresh"]));
				webRequestDownloadFileOrder.CookieContainer.Add(uriPriceDownloadOrder, new Cookie("token", cookies["token"]));
				webRequestDownloadFileOrder.CookieContainer.Add(uriPriceDownloadOrder, new Cookie("uid", cookies["uid"]));

				webRequestDownloadFileOrder.Headers.Add("authorization", "Bearer " + cookies["csrf_token"]);
				webRequestDownloadFileOrder.AllowAutoRedirect = false;

				var responseDownloadFileOrder = (HttpWebResponse)webRequestDownloadFileOrder.GetResponse();

				Thread.Sleep(20000);

				var webRequestDownloadFile = (HttpWebRequest)WebRequest.Create(UrlPriceDownload);
				webRequestDownloadFile.Method = WebRequestMethods.Http.Post;

				webRequestDownloadFile.KeepAlive = true;
				webRequestDownloadFile.ContentType = "application/json";
				webRequestDownloadFile.Referer = "https://b2b.silkway.org.ru/";
				webRequestDownloadFile.Accept = "*/*";
				webRequestDownloadFile.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 YaBrowser/23.7.4.971 Yowser/2.5 Safari/537.36";

				webRequestDownloadFile.CookieContainer = new CookieContainer();
				var uriPriceDownload = new Uri(UrlPriceDownload);

				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("_csrf", cookies["_csrf"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("refresh", cookies["refresh"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("token", cookies["token"]));
				webRequestDownloadFile.CookieContainer.Add(uriPriceDownload, new Cookie("uid", cookies["uid"]));

				webRequestDownloadFile.Headers.Add("authorization", "Bearer " + cookies["csrf_token"]);
				webRequestDownloadFile.AllowAutoRedirect = false;

				var fileDownloadPayload = new FileDownloadPayload
				{
					path = $"Price_SW_{_clientCode}_{DateTime.Now:dd.MM.yyyy}.xlsx"
				};
				var dataJson = JsonConvert.SerializeObject(fileDownloadPayload);
				var data = Encoding.UTF8.GetBytes(dataJson);
				webRequestDownloadFile.ContentLength = data.Length;
				using (var stream = webRequestDownloadFile.GetRequestStream())
					stream.Write(data, 0, data.Length);

				return SupplierProviderService.PriceListDownloadService.DownloadPriceList(webRequestDownloadFile, DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension, false);
			}

			return priceFilePath;
		}

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var silkWayItems = new List<ComPriceProduct>();

			var cookies = LoginOnSilkWay();
			if (cookies.Any())
			{
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					var articleTemp = article;
					var realArticle = articleTemp;
					articleTemp = articleTemp.ToUpper();

					var response = SearchOnSilkWay(cookies, articleTemp);
					if (!string.IsNullOrEmpty(response))
					{
						var itemAdd = false;
						try
						{
							var jsonResponse = JObject.Parse(response);
							if (jsonResponse != null && jsonResponse["data"] != null && jsonResponse["data"]["data"] != null)
							{
								var listProduct = jsonResponse["data"]["data"];
								foreach (var item in listProduct)
								{
									var itemName = item["name"].ToString();
									if (!string.IsNullOrEmpty(itemName))
									{
										try
										{
											var vendorPart = item["vendorPart"].ToString().ToUpper();

											if (IsEqualArticle)
											{
												var isGarbage = false;
												if (!articleTemp.Equals(vendorPart))
												{
													isGarbage = true;
													var vendorPartSplit = vendorPart.Split(' ');
													var vendorPartSplit2 = vendorPart.Split('#');
													if (vendorPartSplit.Length > 0)
													{
														if (vendorPartSplit.Any(split => articleTemp.Equals(split.ToUpper())))
															isGarbage = false;
													}

													if (vendorPartSplit2.Length > 0 && vendorPart.Contains('#'))
													{
														if (vendorPartSplit2.Any(articles.Contains))
															isGarbage = false;
													}
												}

												if (isGarbage)
													continue;
											}

											var quantity = item["inventoryMSK1"]?.ToString().Replace("+", string.Empty);
											if(quantity == null)											
												quantity = item["availableMSK1"]?.ToString().Replace("+", string.Empty);
											
											var priceClient = item["priceClient"].ToString().Replace(".", ",");

											if (priceClient.Equals(",00"))
												priceClient = "0";

											var brand = item["brand"].ToString();

											var silkWayItem = new ComPriceProduct(realArticle, itemName, brand, quantity.ToInt(), priceClient.ToDouble());

											if (SupplierProviderService.CheckConditionService.IsGarbageName(silkWayItem.Name))
												continue;

											for (var j = 0; j < silkWayItems.Count; j++)
											{
												if (silkWayItems[j].Article.Equals(silkWayItem.Article))
												{
													if (silkWayItem.Price < silkWayItems[j].Price)
													{
														if (silkWayItem.Quantity == silkWayItems[j].Quantity)
															silkWayItems[j].Price = silkWayItem.Price;
														else if (silkWayItem.Quantity > 0)
														{
															silkWayItems[j].Quantity = silkWayItem.Quantity;
															silkWayItems[j].Price = silkWayItem.Price;
														}
													}
													else
													{
														if (silkWayItem.Quantity > 0 && silkWayItems[j].Quantity < 1)
														{
															silkWayItems[j].Quantity = silkWayItem.Quantity;
															silkWayItems[j].Price = silkWayItem.Price;
														}
													}

													itemAdd = true;
												}
											}

											if (!itemAdd)
											{
												SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(silkWayItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
												silkWayItems.Add(silkWayItem);
											}

											itemAdd = true;
										}
										catch
										{
											//
										}
									}
								}
							}
						}
						catch (Exception)
						{
							Console.WriteLine(@"Неудалось получить данные в SilkWay");
						}

						if (!itemAdd)
						{
							var silkWayItem = new ComPriceProduct(article);
							SupplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(silkWayItem, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
							silkWayItems.Add(silkWayItem);
						}
					}
				}

				LogoutOnSilkWay(cookies);
			}

			return silkWayItems;
		}

		private Dictionary<string, string> LoginOnSilkWay()
		{
			var cookies = new Dictionary<string, string>();

			var requestLogin = (HttpWebRequest)WebRequest.Create(UrlLogin);
			requestLogin.Method = WebRequestMethods.Http.Post;
			requestLogin.AllowAutoRedirect = false;
			requestLogin.Host = "b2b.silkway.org.ru";
			requestLogin.ContentType = "application/json";
			requestLogin.Referer = "https://b2b.silkway.org.ru/";
			requestLogin.Accept = "*/*";
			requestLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 YaBrowser/23.7.4.971 Yowser/2.5 Safari/537.36";
			requestLogin.KeepAlive = true;
			requestLogin.CookieContainer = new CookieContainer();
			requestLogin.Headers.Add("authorization", "Bearer initial");

			var loginRequest = new LoginRequest
			{
				clientLogin = SupplierLogin,
				clientNo = _clientCode,
				password = SupplierPassword
			};
			var loginRequestJson = JsonConvert.SerializeObject(loginRequest);
			requestLogin.ContentLength = Encoding.Default.GetBytes(loginRequestJson).Length;

			using (var streamWriter = new StreamWriter(requestLogin.GetRequestStream()))
				streamWriter.Write(loginRequestJson);

			var responseLogin = (HttpWebResponse)requestLogin.GetResponse();
			using (var stream = new StreamReader(responseLogin.GetResponseStream(), Encoding.Default))
			{
				if (responseLogin.Cookies?["_csrf"] != null && responseLogin.Cookies["refresh"] != null &&
					responseLogin.Cookies["token"] != null && responseLogin.Cookies["uid"] != null)
				{
					cookies.Add("_csrf", responseLogin.Cookies["_csrf"].Value);
					cookies.Add("refresh", responseLogin.Cookies["refresh"].Value);
					cookies.Add("token", responseLogin.Cookies["token"].Value);
					cookies.Add("uid", responseLogin.Cookies["uid"].Value);
				}

				var result = stream.ReadToEnd();
				if (!string.IsNullOrEmpty(result))
				{
					var requestBody = JObject.Parse(result);
					if (requestBody != null)
						cookies.Add("csrf_token", requestBody["csrf_token"].ToString(Formatting.None));
				}
			}

			responseLogin.Close();

			return cookies;
		}

		private string SearchOnSilkWay(Dictionary<string, string> cookies, string article)
		{
			var searchUrl = string.Format(UrlSearchTemplate, article);
			var requestSearch = (HttpWebRequest)WebRequest.Create(searchUrl);
			requestSearch.Method = WebRequestMethods.Http.Post;
			requestSearch.Headers.Add("Authorization", "Bearer " + cookies["csrf_token"]);

			requestSearch.CookieContainer = new CookieContainer();
			var uri = new Uri(searchUrl);
			requestSearch.CookieContainer.Add(uri, new Cookie("_csrf", cookies["_csrf"]));
			requestSearch.CookieContainer.Add(uri, new Cookie("refresh", cookies["refresh"]));
			requestSearch.CookieContainer.Add(uri, new Cookie("token", cookies["token"]));
			requestSearch.CookieContainer.Add(uri, new Cookie("uid", cookies["uid"]));

			requestSearch.ContentType = "application/json";
			requestSearch.Referer = "https://b2b.silkway.org.ru/?action=YB28EC01&action1=YE4D2083";
			requestSearch.Accept = "application/json, text/javascript, */*; q=0.01";
			requestSearch.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 YaBrowser/23.7.4.971 Yowser/2.5 Safari/537.36";
			requestSearch.KeepAlive = true;
			requestSearch.AllowAutoRedirect = false;

			var dataJson = JsonConvert.SerializeObject(new SearchRequest());
			var data = Encoding.UTF8.GetBytes(dataJson);
			requestSearch.ContentLength = data.Length;
			using (var stream = requestSearch.GetRequestStream())
				stream.Write(data, 0, data.Length);

			var responseSearch = (HttpWebResponse)requestSearch.GetResponse();

			string searchResult;
			using (var stream = new StreamReader(responseSearch.GetResponseStream(), Encoding.UTF8))
				searchResult = stream.ReadToEnd();

			responseSearch.Close();

			return searchResult;
		}

		private void LogoutOnSilkWay(Dictionary<string, string> cookies)
		{
			var requestLogout = (HttpWebRequest)WebRequest.Create(UrlLogout);
			requestLogout.Method = WebRequestMethods.Http.Get;
			requestLogout.AllowAutoRedirect = false;
			requestLogout.Host = "b2b.silkway.org.ru";
			requestLogout.ContentType = "application/json";
			requestLogout.Referer = "https://b2b.silkway.org.ru/";
			requestLogout.Accept = "*/*";
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 YaBrowser/23.7.4.971 Yowser/2.5 Safari/537.36";
			requestLogout.KeepAlive = true;
			requestLogout.CookieContainer = new CookieContainer();

			var uri = new Uri(UrlLogout);
			requestLogout.CookieContainer.Add(uri, new Cookie("_csrf", cookies["_csrf"]));
			requestLogout.CookieContainer.Add(uri, new Cookie("refresh", cookies["refresh"]));
			requestLogout.CookieContainer.Add(uri, new Cookie("token", cookies["token"]));
			requestLogout.CookieContainer.Add(uri, new Cookie("uid", cookies["uid"]));

			requestLogout.AllowAutoRedirect = true;

			var responseLogout = (HttpWebResponse)requestLogout.GetResponse();
			using (var stream = new StreamReader(responseLogout.GetResponseStream(), Encoding.Default))
				stream.ReadToEnd();

			responseLogout.Close();
		}

		private string _clientCode = string.Empty;

		private const string SilkWayClientKey = "SilkWayClientKey";

		public override void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
		{
			if (systemConfigurationParameters != null && systemConfigurationParameters.Any())
			{
				if (systemConfigurationParameters.ContainsKey(SilkWayClientKey) && systemConfigurationParameters[SilkWayClientKey] != null)
					_clientCode = systemConfigurationParameters[SilkWayClientKey];
			}

			base.ApplySupplierConfigurationParameters(systemConfigurationParameters);
		}
	}

	class LoginRequest
	{
		// ReSharper disable once InconsistentNaming

		public string clientLogin { get; set; }

		// ReSharper disable once InconsistentNaming

		public string clientNo { get; set; }

		// ReSharper disable once InconsistentNaming
		public string password { get; set; }
	}


	class SearchRequest
	{
		public ActiveFilter activeFilters = new ActiveFilter();

		public List<string> brand = new List<string>();
	}

	class ActiveFilter
	{

	}

	class FileDownloadPayload
	{
		public string path = string.Empty;

		public string uid = "37b4de360acb93bf6069dd907ffca443";
	}
}
