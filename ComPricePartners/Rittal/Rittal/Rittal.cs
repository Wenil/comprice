﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Suppliers;
using HtmlAgilityPack;

namespace Rittal
{
	class Rittal : SupplierBase
	{
		private readonly ISupplierProviderService _supplierProviderService;

		public Rittal(ISupplierProviderService supplierProviderService) : base(supplierProviderService)
		{
			_supplierProviderService = supplierProviderService;
		}

		private const string UrlStartPage = "https://webas02.loh-group.com/ext(bD1ydSZjPTQwMA==)/rittal/infosys/index.htm";

		private const string UrlSearchPage = "https://webas02.loh-group.com/ext(bD1ydSZjPTQwMA==)/rittal/infosys/artinfo/selection.htm";

		private const string UrlFileDownload = "https://176.53.182.242/index.php/s/9XxEJQEd6KbZ5X7/download/new_whinfo.xls";

		private string DownloadPrice()
		{
			var request = (HttpWebRequest)WebRequest.Create(UrlFileDownload);
			request.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

			return SupplierProviderService.PriceListDownloadService.DownloadPriceList(request, DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
		}

		private Dictionary<string, string> LoginOnRittal()
		{
			var sessionParam = new Dictionary<string, string>();

			var requestLogin = (HttpWebRequest) WebRequest.Create(UrlStartPage);
			requestLogin.Method = WebRequestMethods.Http.Get;
			ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
			requestLogin.Host = "webas02.loh-group.com";
			requestLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0";
			requestLogin.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			requestLogin.KeepAlive = true;
			requestLogin.AllowAutoRedirect = true;

			var responseLogin = (HttpWebResponse) requestLogin.GetResponse();

			var cookie = responseLogin.Headers.Get("Set-Cookie");
			var sapAppContext = cookie.Substring(cookie.IndexOf("sap-appcontext="), cookie.IndexOf(";"));
			sapAppContext = sapAppContext.Replace("sap-appcontext=", string.Empty);
			sessionParam.Add("sap-appcontext", sapAppContext);

			responseLogin.Close();

			if (sessionParam.Count > 0)
			{
				var subRequest = (HttpWebRequest) WebRequest.Create(UrlStartPage);
				subRequest.Method = WebRequestMethods.Http.Post;

				subRequest.CookieContainer = new CookieContainer();
				var uri2 = new Uri(UrlStartPage);
				subRequest.CookieContainer.Add(uri2, new Cookie("sap-appcontext", sessionParam["sap-appcontext"]));

				subRequest.Host = "webas02.loh-group.com";
				subRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0";
				subRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
				subRequest.KeepAlive = true;
				subRequest.AllowAutoRedirect = true;
				subRequest.Referer = "https://webas02.loh-group.com/ext(bD1ydSZjPTQwMA==)/rittal/infosys/index.htm";

				var postData = "htmlbevt_ty=htmlb:button:click:null";
				postData += "&htmlbevt_frm=htmlb_form_1";
				postData += "&htmlbevt_oid=login";
				postData += "&htmlbevt_id=LOGIN";
				postData += "&htmlbevt_cnt=0";
				postData += "&onInputProcessing=htmlb";
				postData += "&sap-htmlb-design=";
				postData += "&session_id=";
				postData += "&userid=" + SupplierLogin;
				postData += "&passwd=" + SupplierPassword;
				var data = Encoding.Default.GetBytes(postData);

				subRequest.ContentType = "application/x-www-form-urlencoded";
				subRequest.ContentLength = data.Length;
				subRequest.KeepAlive = true;
				subRequest.AllowAutoRedirect = true;

				using (var stream = subRequest.GetRequestStream())
					stream.Write(data, 0, data.Length);

				var subResponse = (HttpWebResponse) subRequest.GetResponse();

				using (var streamResponseSearch = new StreamReader(subResponse.GetResponseStream(), Encoding.Default))
				{
					var response = streamResponseSearch.ReadToEnd();
					if (!string.IsNullOrEmpty(response))
					{
						var searchTemplate = "name=\"session_id\" value=\"";
						var startIndex = response.IndexOf(searchTemplate);
						var endIndex = response.IndexOf("\">", startIndex);
						var sessionId = response.Substring(startIndex + searchTemplate.Length,
							endIndex - (startIndex + searchTemplate.Length));
						sessionParam.Add("session_id", sessionId);
					}
				}

				var cookie2 = subRequest.Headers.Get("Cookie");
				var flg = cookie2.Substring(cookie2.IndexOf("FLGInformationSystem_400="), cookie2.Length);
				flg = flg.Replace("FLGInformationSystem_400=", "");
				sessionParam.Add("FLGInformationSystem_400", flg);

				sessionParam.Add("Query", subResponse.ResponseUri.Query);

				subResponse.Close();
			}

			return sessionParam;
		}

		private void LogoutOnRittal(Dictionary<string, string> cookies)
		{
			var requestLogout = (HttpWebRequest) WebRequest.Create(UrlStartPage);
			requestLogout.Method = WebRequestMethods.Http.Post;
			ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
			requestLogout.CookieContainer = new CookieContainer();
			var uri2 = new Uri(UrlStartPage);
			requestLogout.CookieContainer.Add(uri2, new Cookie("sap-appcontext", cookies["sap-appcontext"]));
			requestLogout.CookieContainer.Add(uri2, new Cookie("FLGInformationSystem_400", cookies["FLGInformationSystem_400"]));
			requestLogout.CookieContainer.Add(uri2, new Cookie("sap-usercontext", "sap-language=R&sap-client=400"));

			requestLogout.Host = "webas02.loh-group.com";
			requestLogout.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0";
			requestLogout.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = true;
			requestLogout.Referer = "https://webas02.loh-group.com/ext(bD1ydSZjPTQwMA==)/rittal/infosys/index.htm";

			var postData = "htmlbevt_ty=htmlb:button:click:null";
			postData += "&htmlbevt_frm=htmlb_form_1";
			postData += "&htmlbevt_oid=deleteClientCookie";
			postData += "&htmlbevt_id=deleteClientCookie";
			postData += "&htmlbevt_cnt=0";
			postData += "&onInputProcessing=htmlb";
			postData += "&sap-htmlb-design=";
			postData += "&session_id=";
			postData += "&userid=" + SupplierLogin;
			postData += "&passwd=";
			var data = Encoding.Default.GetBytes(postData);

			requestLogout.ContentType = "application/x-www-form-urlencoded";
			requestLogout.ContentLength = data.Length;
			requestLogout.KeepAlive = true;
			requestLogout.AllowAutoRedirect = true;

			using (var stream = requestLogout.GetRequestStream())
				stream.Write(data, 0, data.Length);

			var responseLogout = (HttpWebResponse) requestLogout.GetResponse();
			responseLogout.Close();
		}

		public override string Name { get; } = "Rit";

		public override string Header { get; } = "Rit";

		protected override List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var rittalItems = new List<ComPriceProduct>();
			var sessionParam = LoginOnRittal();
			if (sessionParam != null && sessionParam.Count == 4)
			{
				foreach (var article in articles)
				{
					if (cancellationToken.IsCancellationRequested)
						throw new Exception("TerminateSearch");

					GC.Collect();
					var req = (HttpWebRequest) WebRequest.Create(UrlSearchPage + sessionParam["Query"]);
					req.Method = WebRequestMethods.Http.Post;

					req.CookieContainer = new CookieContainer();
					var uri2 = new Uri(UrlSearchPage + sessionParam["Query"]);
					req.CookieContainer.Add(uri2, new Cookie("sap-appcontext", sessionParam["sap-appcontext"]));
					req.CookieContainer.Add(uri2,
						new Cookie("FLGInformationSystem_400", sessionParam["FLGInformationSystem_400"]));

					ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

					req.Host = "webas02.loh-group.com";
					req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0";
					req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
					req.KeepAlive = true;
					req.AllowAutoRedirect = true;
					req.Referer = "https://webas02.loh-group.com/ext(bD1ydSZjPTQwMA==)/rittal/infosys/index.htm";

					var postData = "htmlbevt_ty=htmlb:button:click:null";
					postData += "&htmlbevt_frm=htmlb_form_1";
					postData += "&htmlbevt_oid=GO";
					postData += "&htmlbevt_id=GO";
					postData += "&htmlbevt_cnt=0";
					postData += "&onInputProcessing=htmlb";
					postData += "&sap-htmlb-design=";
					postData += "&session_id=" + sessionParam["session_id"];
					postData += "&p_matnr=" + article;
					postData += "&GROUP02=RB03";
					var data = Encoding.Default.GetBytes(postData);

					req.ContentType = "application/x-www-form-urlencoded";
					req.ContentLength = data.Length;
					req.KeepAlive = true;
					req.AllowAutoRedirect = true;

					using (var stream = req.GetRequestStream())
						stream.Write(data, 0, data.Length);

					var resp = (HttpWebResponse) req.GetResponse();

					using (var streamResponseSearch = new StreamReader(resp.GetResponseStream(), Encoding.UTF8))
					{
						var response = streamResponseSearch.ReadToEnd();
						if (!string.IsNullOrEmpty(response) && !response.Contains("нет или он не активирован"))
						{
							var html = new HtmlDocument();
							html.LoadHtml(response);
							var a = html.DocumentNode.Descendants();
							var table = html.DocumentNode.Descendants().Where(x => x.Name == "table").ToList();
							if (table.Count > 5)
							{
								var productInfoTable = table.ToList()[1];
								var productQuantityTable = table.ToList()[4];

								var productInfoTd = productInfoTable.Descendants().Where(x => x.Name == "td").ToList();

								var search = productInfoTd[1].InnerText.Replace("\r\n", string.Empty).Replace(" ", string.Empty);
								var name = productInfoTd[2].InnerText.Replace("\r\n", string.Empty).Replace(" ", string.Empty).Replace("&#x20;", " ").Replace("&#x22;", " ").Replace("&#x29;", " ");
								var brand = "Rittal";

								var cost = productInfoTd[9].InnerText.Replace("\r\n", string.Empty).Replace(" ", string.Empty).Replace("&#x20;", " ").Replace("&#x22;", " ").Replace("&#x29;", " ");
								if (cost.Contains("EUR"))
									cost = cost.Remove(cost.IndexOf("EUR")).Replace(" ", string.Empty).Replace(".", string.Empty);
								else
									cost = cost.Replace(" ", string.Empty).Replace(".", string.Empty);


								var costDouble = cost.ToDouble() * 0.78 * 1.2;

								var productQtyTd = productQuantityTable.Descendants().Where(x => x.Name == "td").ToList();

								var quantity = productQtyTd.Count >= 5 ? productQtyTd[4].InnerText.Replace(".", string.Empty).ToInt() : 0;

								if (IsEqualArticle)
								{
									if (!article.Equals(search))
										continue;
								}

								var rittalProduct = new ComPriceProduct(search, name, brand, quantity, costDouble);
								CalculateDeliveryDaysRittalEdition(rittalProduct);

								rittalItems.Add(rittalProduct);
							}
						}
					}
				}

				LogoutOnRittal(sessionParam);
			}

			return rittalItems;
		}

		protected override List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken)
		{
			var result = new List<ComPriceProduct>();

			var filePath = SupplierProviderService.PriceListDownloadService.PriceListPathIfExist(DirectoryPathForDownloadFile, PriceListFileName, PriceListFileExtension);
			if (string.IsNullOrEmpty(filePath))
				filePath = DownloadPrice();

			if (!string.IsNullOrEmpty(filePath))
			{
				var excelProcessingRequest = new ExcelProcessingRequest {FilePath = filePath, Name = 2, Article = 1};
				excelProcessingRequest.Quantity.Add(3);

				var findProductByArticles = SupplierProviderService.ExcelProcessingService.FindProductByArticles(articles, excelProcessingRequest);
				if (findProductByArticles.Any())
				{
					foreach (var findProductByArticle in findProductByArticles)
					{
						if (findProductByArticle.FindArticle != null && findProductByArticle.FindArticle.Equals(findProductByArticle.Article))
						{
							var name = findProductByArticle.Name as string;
							name = name?.Trim();

							var quantity = 0;
							foreach (var quantityCellValue in findProductByArticle.Quantity)
							{
								var quantityString = quantityCellValue?.ToString().Trim();
								if (!string.IsNullOrEmpty(quantityString))
									int.TryParse(quantityString, out quantity);
							}

							var rittalProduct = new ComPriceProduct(findProductByArticle.Article, name, string.Empty, quantity, 1.0, Currency.USD);
							CalculateDeliveryDaysRittalEdition(rittalProduct);

							result.Add(rittalProduct);
						}
						else
							result.Add(new ComPriceProduct(findProductByArticle.Article));
					}
				}
			}

			return result;
		}

		private void CalculateDeliveryDaysRittalEdition(ComPriceProduct rittalProduct)
        {
			_supplierProviderService.CalculateDeliveryService.CalculateDeliveryDays(rittalProduct, SupplierInStockDeliveryDay, SupplierUnderOrderDeliveryDay, SupplierSpecifyDeliveryDay);
			if (rittalProduct.DayCountOnDelivery != null)
			{
				var startDate = DateTime.Now;
				var endDate = startDate.AddDays((double)rittalProduct.DayCountOnDelivery);
				while (!endDate.DayOfWeek.Equals(DayOfWeek.Friday))
					endDate = endDate.AddDays(1D);

				rittalProduct.DayCountOnDelivery = (int)(endDate - startDate).TotalDays;
			}
		}

		protected override string LoginSupplierFieldName { get; } = "RittalLogin";

		protected override string PasswordSupplierFieldName { get; } = "RittalPassword";

		protected override string InStockDeliveryDaySupplierFieldName { get; } = "RittalInStockDeliveryDay";

		protected override string UnderOrderDeliveryDaySupplierFieldName { get; } = "RittalUnderOrderDeliveryDay";

		protected override string SpecifyDeliveryDaySupplierFieldName { get; } = "RittalSpecifyDeliveryDay";
	}
}