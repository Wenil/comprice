﻿namespace Com.Price.Data.Suppliers.Structs
{
	public struct MarkupRange
	{
		public int RangeStart { get; set; }
		public int RangeEnd { get; set; }
		public double Markup { get; set; }
	}
}