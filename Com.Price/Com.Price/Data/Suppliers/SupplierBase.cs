﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Com.Price.Data.Article;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers.Enums;
using Com.Price.Infrastructure.Suppliers;

namespace Com.Price.Data.Suppliers
{
	/// <summary>
	/// Поставщик.
	/// </summary>
	public abstract class SupplierBase
	{
		protected readonly ISupplierProviderService SupplierProviderService;

		protected SupplierBase(ISupplierProviderService supplierProviderService)
		{
			SupplierProviderService = supplierProviderService;
			PriceListFileName = $"{GetType().Name}_{DateTime.Now:yy-MM-dd}";
		}

		/// <summary>
		/// Наименование поставщика.
		/// </summary>
		public abstract string Name { get; }

		/// <summary>
		/// Заголовок в таблице результатов.
		/// </summary>
		public abstract string Header { get; }

		/// <summary>
		/// Наименование файла прайс листа.
		/// </summary>
		public string PriceListFileName { get; protected set; }

		/// <summary>
		/// Расширение файла прайс листа.
		/// </summary>
		public string PriceListFileExtension { get; protected set; } = ".xls";

		/// <summary>
		/// Директория для хранения прайс листа.
		/// </summary>
		public string DirectoryPathForDownloadFile
		{
			get
			{
				var directoryPath = "Uploads\\" + GetType().Name;

				if (!Directory.Exists(directoryPath))
					Directory.CreateDirectory(directoryPath);

				return directoryPath;
			}
		}

		/// <summary>
		/// Основеное поле для поиска.
		/// </summary>
		public MainSearchField SearchField = MainSearchField.Article;

		/// <summary>
		/// Search by exact match of part number.
		/// </summary>
		public bool IsEqualArticle { get; set; }

		/// <summary>
		/// Результаты поиска.
		/// </summary>
		public List<ComPriceProduct> SearchProductResult { get; set; } = new List<ComPriceProduct>();

		/// <summary>
		/// Ручной поиск товаров.
		/// </summary>
		/// <param name="articles">Список партномеров.</param>
		/// <param name="cancellationToken"></param>
		/// <returns>Коллекция товаров.</returns>
		protected abstract List<ComPriceProduct> ManualProductSearch(List<string> articles, CancellationToken cancellationToken);

		/// <summary>
		/// Автоматический поиск товаров.
		/// </summary>
		/// <param name="articles">Список партномеров.</param>
		/// <param name="cancellationToken"></param>
		/// <returns>Коллекция товаров.</returns>
		protected abstract List<ComPriceProduct> AutomaticProductSearch(List<string> articles, CancellationToken cancellationToken);

		/// <summary>
		/// Статус поиска продуктов.
		/// </summary>
		public bool IsRunningSearchProduct { get; set; }

		/// <summary>
		/// Поиск продуктов.
		/// </summary>
		/// <param name="comPriceArticles"></param>
		/// <param name="searchType"></param>
		/// <param name="cancellationToken"></param>
		public virtual async Task SearchProductAsync(List<ComPriceArticle> comPriceArticles, SearchType searchType, CancellationToken cancellationToken)
		{
			IsRunningSearchProduct = true;

			try
			{
				var articles = comPriceArticles
					.SelectMany(comPriceArticle => comPriceArticle.ToList())
					.Distinct()
					.ToList();

				var rawComPriceProducts =
					await Task.Run(() =>
							searchType == SearchType.Automatic ? AutomaticProductSearch(articles, cancellationToken) : ManualProductSearch(articles, cancellationToken), cancellationToken
					);

				foreach (var comPriceArticle in comPriceArticles)
				{
					var comPriceProductTemp = new ComPriceProduct(comPriceArticle.MainArticle);
					var comPriceProduct = rawComPriceProducts.Find(product => comPriceArticle.MainArticle.Equals(product.Article));
					if (comPriceProduct != null)
						comPriceProductTemp = comPriceProduct;

					if (comPriceArticle.AdditionalArticles.Any())
					{
						var additionalProducts = new List<ComPriceProduct>();
						comPriceArticle.AdditionalArticles.ForEach(additionalArticle =>
						{
							var additionalProduct = rawComPriceProducts.Find(product => additionalArticle.Equals(product.Article));
							if (additionalProduct != null)
								additionalProducts.Add(additionalProduct);
						});

						foreach (var additionalProduct in additionalProducts)
						{
							if (!additionalProduct.AnyWithoutArticle())
								continue;

							if (additionalProduct.Price < comPriceProductTemp.Price && additionalProduct.Quantity > 1)
								comPriceProductTemp = additionalProduct;
							else if (Math.Abs(additionalProduct.Price - comPriceProductTemp.Price) < 0.01 && additionalProduct.Quantity > comPriceProductTemp.Quantity)
								comPriceProductTemp = additionalProduct;
							else if (additionalProduct.Price >= 0 && comPriceProductTemp.Price == 0)
								comPriceProductTemp = additionalProduct;
							else if (additionalProduct.Quantity >= 0 && comPriceProductTemp.Quantity == 0)
								comPriceProductTemp = additionalProduct;
						}

						comPriceProductTemp.Article = comPriceArticle.MainArticle;
						SearchProductResult.Add(comPriceProductTemp);
					}
					else
						SearchProductResult.Add(comPriceProductTemp);
				}
			}
			catch (Exception ex)
			{
				SupplierProviderService.LoggerService.Error($"{GetType().Name} упал с ошибкой {ex.Message} {ex.StackTrace}", ex);
				foreach (var comPriceArticle in comPriceArticles)
					SearchProductResult.Add(new ComPriceProduct(comPriceArticle.MainArticle, "Ошибка во время поиска", "Ошибка", -1, -1));
			}
			finally
			{
				IsRunningSearchProduct = false;
			}
		}

		protected abstract string LoginSupplierFieldName { get; }

		protected abstract string PasswordSupplierFieldName { get; }

		protected abstract string InStockDeliveryDaySupplierFieldName { get; }

		protected abstract string UnderOrderDeliveryDaySupplierFieldName { get; }

		protected abstract string SpecifyDeliveryDaySupplierFieldName { get; }

		protected string SupplierLogin;

		protected string SupplierPassword;

		protected int SupplierInStockDeliveryDay;

		protected int SupplierUnderOrderDeliveryDay;

		protected int SupplierSpecifyDeliveryDay;

		/// <summary>
		/// Применить настройки поставщика.
		/// </summary>
		/// <param name="systemConfigurationParameters">Параметры системы.</param>
		public virtual void ApplySupplierConfigurationParameters(Dictionary<string, string> systemConfigurationParameters)
		{
			if (systemConfigurationParameters != null && systemConfigurationParameters.Any())
			{
				if (systemConfigurationParameters.ContainsKey(LoginSupplierFieldName) && systemConfigurationParameters[LoginSupplierFieldName] != null)
					SupplierLogin = systemConfigurationParameters[LoginSupplierFieldName];

				if (systemConfigurationParameters.ContainsKey(PasswordSupplierFieldName) && systemConfigurationParameters[PasswordSupplierFieldName] != null)
					SupplierPassword = systemConfigurationParameters[PasswordSupplierFieldName];

				if (systemConfigurationParameters.ContainsKey(InStockDeliveryDaySupplierFieldName) && systemConfigurationParameters[InStockDeliveryDaySupplierFieldName] != null)
					SupplierInStockDeliveryDay = systemConfigurationParameters[InStockDeliveryDaySupplierFieldName].ToInt();

				if (systemConfigurationParameters.ContainsKey(UnderOrderDeliveryDaySupplierFieldName) && systemConfigurationParameters[UnderOrderDeliveryDaySupplierFieldName] != null)
					SupplierUnderOrderDeliveryDay = systemConfigurationParameters[UnderOrderDeliveryDaySupplierFieldName].ToInt();

				if (systemConfigurationParameters.ContainsKey(SpecifyDeliveryDaySupplierFieldName) && systemConfigurationParameters[SpecifyDeliveryDaySupplierFieldName] != null)
					SupplierSpecifyDeliveryDay = systemConfigurationParameters[SpecifyDeliveryDaySupplierFieldName].ToInt();
			}
		}
	}
}