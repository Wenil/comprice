﻿using Catel.MVVM;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Com.Price.Data.Suppliers
{
	public class SupplierParameterViewModel : ViewModelBase
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public SupplierParameterViewModel(ISystemConfigurationService systemConfigurationService, string loginFieldName, string passwordFieldName)
		{
			_systemConfigurationService = systemConfigurationService;
			LoginFieldName = loginFieldName;
			PasswordFieldName = passwordFieldName;
		}

		public string Login
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter(LoginFieldName);
			set => _systemConfigurationService.SetSystemConfigurationParameter(LoginFieldName, value);
		}

		public string Password
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter(PasswordFieldName);
			set => _systemConfigurationService.SetSystemConfigurationParameter(PasswordFieldName, value);
		}

		public string LoginFieldName { get; }

		public string PasswordFieldName { get; }
	}
}