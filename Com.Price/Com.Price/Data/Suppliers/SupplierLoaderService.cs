﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Com.Price.Infrastructure.Services.SystemConfiguration;
using Com.Price.Infrastructure.Suppliers;

namespace Com.Price.Data.Suppliers
{
	public class SupplierLoaderService : ISupplierLoaderService
	{
		private readonly ISupplierProviderService _providerService;
		private readonly ISystemConfigurationService _systemConfigurationService;

		public SupplierLoaderService(ISupplierProviderService providerService, ISystemConfigurationService systemConfigurationService)
		{
			_providerService = providerService;
			_systemConfigurationService = systemConfigurationService;
		}

		public Dictionary<string, Type> Suppliers { get; } = new Dictionary<string, Type>();

		public HashSet<Assembly> AssemblySuppliers { get; } = new HashSet<Assembly>();

		/// <summary>
		/// Название директории с dll поставщиков.
		/// </summary>
		private const string SupplierDirectoryName = "Suppliers";

		public void LoadSuppliers()
		{
			if (Directory.Exists(SupplierDirectoryName))
			{
				foreach (var importFile in Directory.GetFiles(SupplierDirectoryName))
				{
					try
					{
						LoadSupplierFromDll(importFile);
					}
					catch (Exception exception)
					{
						_providerService.LoggerService.Error($"Ошибка при загрузке сборки поставщика {importFile}", exception);
					}
				}
			}
		}

		private void LoadSupplierFromDll(string filePath)
		{
			if (filePath.EndsWith(".dll"))
			{
				var assembly = Assembly.LoadFrom(filePath);
				AssemblySuppliers.Add(assembly);
				var types = assembly.GetTypes();
				var name = assembly.GetName().Name;
				foreach (var type in types)
				{
					if (type.Name.Equals(name) && !Suppliers.ContainsKey(name) && Activator.CreateInstance(type, _providerService) is SupplierBase)
						Suppliers.Add(name, type);
				}
			}
		}

		public List<SupplierBase> CreateInstanceSuppliers(List<string> supplierNames)
		{
			var partners = new List<SupplierBase>();
			if (supplierNames != null && supplierNames.Any())
			{
				foreach (var supplierName in supplierNames)
				{
					if (Suppliers != null && Suppliers.ContainsKey(supplierName))
					{
						var supplierClass = Activator.CreateInstance(Suppliers[supplierName], _providerService);
						if (supplierClass is SupplierBase supplier)
						{
							supplier.ApplySupplierConfigurationParameters(_systemConfigurationService.SystemConfiguration);
							supplier.IsEqualArticle = _systemConfigurationService.SearchOnlyForExactMatchArticle;

							partners.Add(supplier);
						}
					}
				}
			}

			return partners;
		}
	}
}