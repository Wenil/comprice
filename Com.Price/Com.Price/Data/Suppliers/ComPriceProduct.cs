﻿using Com.Price.Data.Suppliers.Enums;
using Com.Price.Infrastructure.Services.Exchange.Data;
using NetOffice.OfficeApi;

namespace Com.Price.Data.Suppliers
{
	public class ComPriceProduct
	{
		public ComPriceProduct()
		{
		}

		public ComPriceProduct(string article)
		{
			Article = article;
		}

		public ComPriceProduct(string article, string name, string brand, int quantity, double price)
		{
			Article = article;
			Name = name;
			Brand = brand;
			Quantity = quantity;
			Price = price;
		}

		public ComPriceProduct(string article, string name, string brand, int quantity, double price, Currency currency)
		{
			Article = article;
			Name = name;
			Brand = brand;
			Quantity = quantity;
			Price = price;
			Currency = currency;
		}

		public string Article { get; set; } = string.Empty;

		public string ManufacturerArticle { get; set; } = string.Empty;

		public string Name { get; set; } = string.Empty;

		public string Brand { get; set; } = string.Empty;

		public int Quantity { get; set; }

		public string QuantityMessage { get; set; }

		public double Price { get; set; }

		public Currency? Currency { get; set; } = null;

		public bool IsEmpty() => string.IsNullOrEmpty(Article) && string.IsNullOrEmpty(Name) && string.IsNullOrEmpty(Brand) && Quantity == 0 && Price == 0.0;

		public bool AnyWithoutArticle() => !string.IsNullOrEmpty(Name) || !string.IsNullOrEmpty(Brand) || Quantity != 0 || Price != 0.0;

		public string ResultKey { get; set; } = string.Empty;

		public Availability Availability { get; set; } = Availability.UnderOrder;

		public int? DayCountOnDelivery { get; set; } = null;
	}
}