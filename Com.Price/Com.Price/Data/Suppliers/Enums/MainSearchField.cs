﻿namespace Com.Price.Data.Suppliers.Enums
{
	/// <summary>
	/// Основное поле для поиска.
	/// </summary>
	public enum MainSearchField
	{
		/// <summary>
		/// Парт номер производителя товара. 
		/// </summary>
		Article,

		/// <summary>
		/// Парт номер поставщика товара.
		/// </summary>
		ArticleManufacturer
	}
}