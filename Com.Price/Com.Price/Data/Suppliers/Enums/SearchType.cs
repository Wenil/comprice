﻿namespace Com.Price.Data.Suppliers.Enums
{
	/// <summary>
	/// Тип поиска.
	/// </summary>
	public enum SearchType
	{
		/// <summary>
		/// Ручной поиск.
		/// </summary>
		Manual,

		/// <summary>
		/// Автоматический поиск.
		/// </summary>
		Automatic
	}
}