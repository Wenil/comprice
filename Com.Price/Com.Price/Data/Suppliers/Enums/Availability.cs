﻿namespace Com.Price.Data.Suppliers.Enums
{
	public enum Availability
	{
		Specify = 0,
		UnderOrder = 1,
		InStock = 2
	}
}