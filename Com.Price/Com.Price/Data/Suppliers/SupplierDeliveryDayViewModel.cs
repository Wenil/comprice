﻿using Com.Price.Data.Extensions;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Com.Price.Data.Suppliers
{
	public class SupplierDeliveryDayViewModel
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public SupplierDeliveryDayViewModel(ISystemConfigurationService systemConfigurationService, string inStockDeliveryDayFieldName, string underOrderDeliveryDayFieldName, string specifyDeliveryDayFieldName)
		{
			_systemConfigurationService = systemConfigurationService;
			InStockDeliveryDayFieldName = inStockDeliveryDayFieldName;
			UnderOrderDeliveryDayFieldName = underOrderDeliveryDayFieldName;
			SpecifyDeliveryDayFieldName = specifyDeliveryDayFieldName;
		}

		public int InStockDeliveryDay
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter(InStockDeliveryDayFieldName).ToInt();
			set => _systemConfigurationService.SetSystemConfigurationParameter(InStockDeliveryDayFieldName, value.ToString());
		}

		public int UnderOrderDeliveryDay
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter(UnderOrderDeliveryDayFieldName).ToInt();
			set => _systemConfigurationService.SetSystemConfigurationParameter(UnderOrderDeliveryDayFieldName, value.ToString());
		}

		public int SpecifyDeliveryDay
		{
			get => _systemConfigurationService.GetSystemConfigurationParameter(SpecifyDeliveryDayFieldName).ToInt();
			set => _systemConfigurationService.SetSystemConfigurationParameter(SpecifyDeliveryDayFieldName, value.ToString());
		}

		public string InStockDeliveryDayFieldName { get; }

		public string UnderOrderDeliveryDayFieldName { get; }

		public string SpecifyDeliveryDayFieldName { get; }
	}
}