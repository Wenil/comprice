﻿using Catel.MVVM;

namespace Com.Price.Data.Suppliers
{
	public abstract class SupplierBaseViewModel : ViewModelBase
	{
		public abstract SupplierParameterViewModel SupplierParameterViewModel { get; }

		public abstract SupplierDeliveryDayViewModel SupplierDeliveryDayViewModel { get; }
	}
}