﻿using Com.Price.Data.ProcessFiles.Xml;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Services.ProcessFiles.Excel.Response;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.Delivery;
using Com.Price.Infrastructure.Services.Download;
using Com.Price.Infrastructure.Services.Exchange;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel.Processing;
using Com.Price.Infrastructure.Services.ProcessFiles.Xml;
using Com.Price.Infrastructure.Suppliers;

namespace Com.Price.Data.Suppliers
{
	public class SupplierProviderService : ISupplierProviderService
	{
		public SupplierProviderService(IExchangeRateService exchangeRateService, ICheckConditionService checkConditionService, IPriceListDownloadService priceListDownloadService,
			IExcelProcessingService<ExcelProcessingRequest, ExcelProcessingResponse> excelProcessingService,
			IXmlProcessingService<XmlProcessingRequest, XmlProcessingResponse> xmlProcessingService, ILoggerService loggerService, 
			ICalculateDeliveryService calculateDeliveryService)
		{
			ExchangeRateService = exchangeRateService;
			CheckConditionService = checkConditionService;
			PriceListDownloadService = priceListDownloadService;
			ExcelProcessingService = excelProcessingService;
			XmlProcessingService = xmlProcessingService;
			LoggerService = loggerService;
			CalculateDeliveryService = calculateDeliveryService;
		}

		public IExchangeRateService ExchangeRateService { get; }

		public ICheckConditionService CheckConditionService { get; }

		public IPriceListDownloadService PriceListDownloadService { get; }

		public IExcelProcessingService<ExcelProcessingRequest, ExcelProcessingResponse> ExcelProcessingService { get; }

		public IXmlProcessingService<XmlProcessingRequest, XmlProcessingResponse> XmlProcessingService { get; }

		public ILoggerService LoggerService { get; }

		public ICalculateDeliveryService CalculateDeliveryService { get; }
	}
}