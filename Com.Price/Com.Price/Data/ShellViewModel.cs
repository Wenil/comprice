﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using Catel;
using Catel.IoC;
using Catel.MVVM;
using Catel.Services;
using Com.Price.Data.Menu.Administration;
using Com.Price.Data.TaskManager;
using Com.Price.Data.TaskManager.Enums;
using Com.Price.Infrastructure.Services.Exchange;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Services.SystemConfiguration;
using Com.Price.Infrastructure.Services.TaskManager;
using Com.Price.Infrastructure.Services.TaskManager.Model;
using Com.Price.Infrastructure.Suppliers;
using Com.Price.Properties;

namespace Com.Price.Data
{
	public class ShellViewModel : ViewModelBase
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		private readonly ISupplierLoaderService _supplierLoaderService;

		private readonly IUIVisualizerService _uiVisualizerService;

		private readonly IExchangeRateService _exchangeRateService;

		private readonly IComPriceTaskManagerService _comPriceTaskManagerService;

		private readonly IComPriceTaskConfigurationService _comPriceTaskConfigurationService;
		private readonly INavigationService _navigationService;
		private readonly IMessageService _messageService;
		private readonly IViewModelManager _viewModelManager;
		private readonly IComPriceTaskExecuterService _comPriceTaskExecuterService;

		private readonly System.Timers.Timer _tomorrowExchangeRateUpdateTimer = new System.Timers.Timer(1000);

		private readonly System.Timers.Timer _exchangeRateUpdateTimer = new System.Timers.Timer(86400000);

		public ShellViewModel(ISystemConfigurationService systemConfigurationService, ISupplierLoaderService supplierLoaderService, IUIVisualizerService uiVisualizerService,
			IExchangeRateService exchangeRateService, IComPriceTaskManagerService comPriceTaskManagerService, IComPriceTaskConfigurationService comPriceTaskConfigurationService,
			INavigationService navigationService, IMessageService messageService, IViewModelManager viewModelManager, IComPriceTaskExecuterService comPriceTaskExecuterService)
		{
			Argument.IsNotNull(() => systemConfigurationService);
			Argument.IsNotNull(() => supplierLoaderService);
			Argument.IsNotNull(() => uiVisualizerService);
			Argument.IsNotNull(() => exchangeRateService);
			Argument.IsNotNull(() => comPriceTaskManagerService);
			Argument.IsNotNull(() => comPriceTaskConfigurationService);

			_systemConfigurationService = systemConfigurationService;
			_supplierLoaderService = supplierLoaderService;
			_uiVisualizerService = uiVisualizerService;
			_exchangeRateService = exchangeRateService;
			_comPriceTaskManagerService = comPriceTaskManagerService;
			_comPriceTaskConfigurationService = comPriceTaskConfigurationService;
			_navigationService = navigationService;
			_messageService = messageService;
			_viewModelManager = viewModelManager;
			_comPriceTaskExecuterService = comPriceTaskExecuterService;

			_supplierLoaderService.LoadSuppliers();
			_systemConfigurationService.LoadSystemConfiguration();

			ShowSystemConfigurationCommand = new TaskCommand(ShowSystemConfigurationAsync);

			SaveComPriceTaskCommand = new Command(SaveComPriceTask);
			LoadComPriceTaskCommand = new Command(LoadComPriceTask);
			RunComPriceTaskCommand = new Command(RunComPriceTask);

			// navigationService.ApplicationClosing += NavigationServiceOnApplicationClosing;
			// Cannot set Visibility or call Show, ShowDialog, or WindowInteropHelper.EnsureHandle after a Window has closed
			this.InitializedAsync += OnInitializedAsync;
		}

		private async Task OnInitializedAsync(object sender, EventArgs e)
		{
			IsBusy = true;

			await Task.Run(() =>
				{
					_exchangeRateService.ExchangeRates = _exchangeRateService.LoadExchangeRates();

					UpdateExchangeRateView();
					InitializeExchangeRatesTimers();
				}
			);

			IsBusy = false;
		}

		public bool IsBusy { get; set; }

		public string WindowTitle { get; } = "com.Price";

		public string UsdExchange { get; private set; }

		public string EurExchange { get; private set; }

		/// <summary>
		/// Gets the AddFamily command.
		/// </summary>
		public TaskCommand ShowSystemConfigurationCommand { get; private set; }

		/// <summary>
		/// Method to invoke when the AddFamily command is executed.
		/// </summary>
		private async Task ShowSystemConfigurationAsync()
		{
			var typeFactory = this.GetTypeFactory();
			var systemConfigurationViewModel = typeFactory.CreateInstanceWithParametersAndAutoCompletion<SystemConfigurationViewModel>(_systemConfigurationService);

			await _uiVisualizerService.ShowDialogAsync(systemConfigurationViewModel);
		}

		private void InitializeExchangeRatesTimers()
		{
			_exchangeRateUpdateTimer.Elapsed += CurseUpdateTimeElapsed;
			_exchangeRateUpdateTimer.AutoReset = true;
			_exchangeRateUpdateTimer.Enabled = true;

			_tomorrowExchangeRateUpdateTimer.Elapsed += TomorrowExchangeRateUpdateTimeElapsed;
			_tomorrowExchangeRateUpdateTimer.AutoReset = true;
			_tomorrowExchangeRateUpdateTimer.Enabled = true;
		}

		private void UpdateExchangeRateView()
		{
			UsdExchange = $"Курс $: {_exchangeRateService.ExchangeRates[Currency.USD]:0.##}";
			EurExchange = $"Курс €: {_exchangeRateService.ExchangeRates[Currency.EUR]:0.##}";
		}

		private void CurseUpdateTimeElapsed(object sender, ElapsedEventArgs e)
		{
			_exchangeRateService.ExchangeRates = _exchangeRateService.LoadExchangeRates();

			UpdateExchangeRateView();
		}

		private void TomorrowExchangeRateUpdateTimeElapsed(object sender, ElapsedEventArgs e)
		{
			_tomorrowExchangeRateUpdateTimer.Stop();

			_exchangeRateService.ExchangeRatesForTomorrow = _exchangeRateService.LoadExchangeRates();

			var currentDate = DateTime.Now;
			var updateTomorrowExchangeRate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, 18, 0, 0);
			if (currentDate.Millisecond >= updateTomorrowExchangeRate.Millisecond)
			{
				updateTomorrowExchangeRate = updateTomorrowExchangeRate.AddDays(1);
				_tomorrowExchangeRateUpdateTimer.Interval = (updateTomorrowExchangeRate.Ticks - currentDate.Ticks) / TimeSpan.TicksPerMillisecond;
			}
			else
				_tomorrowExchangeRateUpdateTimer.Interval = (currentDate.Ticks - updateTomorrowExchangeRate.Ticks) / TimeSpan.TicksPerMillisecond;

			_tomorrowExchangeRateUpdateTimer.Start();
		}

		public ICommand SaveComPriceTaskCommand { get; }

		private void SaveComPriceTask()
		{
			if (!Directory.Exists("Tasks"))
				Directory.CreateDirectory("Tasks");
			else
			{
				var window = MessageBox.Show(@"Сохранение найдено. Перезаписать существующее сохранение?",
					@"Сохранение списка задач", MessageBoxButton.YesNo, MessageBoxImage.Warning);

				if (window == MessageBoxResult.No)
					return;
			}

			if (_comPriceTaskManagerService.ComPriceTasks.Any())
			{
				_comPriceTaskConfigurationService.Save("Tasks/Savies.lpxml", _comPriceTaskManagerService.ComPriceTasks);

				MessageBox.Show(@"Список задач сохранён", @"Системное сообщение");
			}
			else
			{
				if (File.Exists("Tasks/Savies.lpxml"))
				{
					File.Delete("Tasks/Savies.lpxml");

					MessageBox.Show(@"Файл с сохранениями удален", @"Системное сообщение");
				}
				else
					MessageBox.Show(@"Нечего сохранять", @"Системное сообщение");
			}
		}

		public ICommand LoadComPriceTaskCommand { get; }

		private void LoadComPriceTask()
		{
			if (Directory.Exists("Tasks"))
			{
				if (File.Exists("Tasks/Savies.lpxml"))
				{
					_comPriceTaskManagerService.ComPriceTasks = new List<ListPriceTask>(_comPriceTaskConfigurationService.Load("Tasks/Savies.lpxml"));

					MessageBox.Show(@"Список задач загружен", @"Системное сообщение");
				}
				else
					MessageBox.Show(@"Файл не найден", @"Системное сообщение");
			}
			else
				MessageBox.Show(@"Папка с сохранениями не найдена", @"Системное сообщение");
		}

		public ICommand RunComPriceTaskCommand { get; }

		private async void RunComPriceTask()
		{
			if (_viewModelManager.GetFirstOrDefaultInstance(typeof(TaskManagerViewModel)) is TaskManagerViewModel taskManagerViewModel)
			{
				if (MessageBox.Show(@"Внимание!! Используйте данный функционал на свой страх и риск :)", @"Осторожно",
					MessageBoxButton.OKCancel) == MessageBoxResult.OK)
				{
					if (taskManagerViewModel.ComPriceTaskSelected != null && taskManagerViewModel.ComPriceTaskSelected.Id != Guid.Empty)
					{
						var resultExecute = ListPriceTaskExecuterReturnValues.AllDone;
						if (taskManagerViewModel.ComPriceTaskSelected != null)
							await _comPriceTaskExecuterService.ExecuteAsync(taskManagerViewModel.ComPriceTaskSelected, CancellationToken.None);

						MessageBox.Show(resultExecute.ToString());
					}
				}
			}
		}

		// private void NavigationServiceOnApplicationClosing(object sender, ApplicationClosingEventArgs e)
		// {
		// 	if (!e.Cancel)
		// 	{
		// 		e.Cancel = true;
		// 	}
		//
		// 	var window = MessageBox.Show(@"Вы действительно хотите выйти?", @"Выход", MessageBoxButton.YesNo, MessageBoxImage.Warning);
		// 	// var window = _messageService.ShowAsync(@"Вы действительно хотите выйти?", @"Выход", MessageButton.YesNo, MessageImage.Warning).Result;
		// 	e.Cancel = window == MessageBoxResult.No;
		//
		// 	if (window == MessageBoxResult.Yes)
		// 		_comPriceTaskManagerService.StopTaskManager();
		// }
	}
}