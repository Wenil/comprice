﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Price.Data.Article
{
	public class ComPriceArticle
	{
		public ComPriceArticle(string article)
		{
			var articles = article
				.Split('|')
				.Distinct()
				.ToArray();

			for (var i = 0; i < articles.Length; i++)
			{
				if (i == 0)
					MainArticle = articles[i];
				else
					AdditionalArticles.Add(articles[i]);
			}
		}

		public ComPriceArticle(string mainArticle, List<string> additionalArticles)
		{
			MainArticle = mainArticle;
			AdditionalArticles = new List<string>(additionalArticles);
		}

		public string MainArticle { get; private set; }

		public List<string> AdditionalArticles { get; private set; } = new List<string>();

		public override string ToString()
		{
			var articleBuilder = new StringBuilder();
			articleBuilder.Append(MainArticle);
			if (AdditionalArticles.Any())
			{
				AdditionalArticles.ForEach(a =>
				{
					articleBuilder.Append("|");
					articleBuilder.Append(a);
				});
			}

			return base.ToString();
		}

		public List<string> ToList()
		{
			var toList = new List<string> {MainArticle};
			toList.AddRange(AdditionalArticles);

			return toList;
		}

		public bool Equals(string article)
		{
			var isEquals = MainArticle.Equals(article);
			if (!isEquals)
			{
				foreach (var additionalArticle in AdditionalArticles)
				{
					isEquals = additionalArticle.Equals(article);
					if(isEquals)
						break;
				}
			}

			return isEquals;
		}
	}
}