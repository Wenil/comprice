﻿using NetOffice.ExcelApi;

namespace Com.Price.Data.Extensions
{
	public static class RangeExtensions
	{
		public static int? ValueToInt(this Range cell)
		{
			if (cell != null && cell.Value2 != null)
			{
				var cellStringValue = cell.Value2.ToString();
				if (int.TryParse(cellStringValue, out var cellIntegerValue))
					return cellIntegerValue;
			}

			return null;
		}

		public static string ValueToString(this Range cell)
		{
			if (cell != null && cell.Value2 != null)
			{
				var cellStringValue = cell.Value2.ToString();
				if (!string.IsNullOrEmpty(cellStringValue))
					return cellStringValue;
			}

			return null;
		}
	}
}