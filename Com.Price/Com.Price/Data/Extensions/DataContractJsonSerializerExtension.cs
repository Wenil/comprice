﻿using System.IO;
using System.Runtime.Serialization.Json;

namespace Com.Price.Data.Extensions
{
	public static class DataContractJsonSerializerExtension
	{
		public static string ToJson(this DataContractJsonSerializer jsonSerializer, object serializeObject)
		{
			using var stream = new MemoryStream();
			jsonSerializer.WriteObject(stream, serializeObject);
			stream.Position = 0;
			using var sr = new StreamReader(stream);

			return sr.ReadToEnd();
		}
	}
}