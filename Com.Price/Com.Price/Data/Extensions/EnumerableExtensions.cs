﻿using System.Collections.Generic;
using System.Linq;

namespace Com.Price.Data.Extensions
{
	public static class EnumerableExtensions
	{
		public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> collection, int batchSize)
		{
			return collection
				.Select((item, inx) => new {item, inx})
				.GroupBy(x => x.inx / batchSize)
				.Select(g => g.Select(x => x.item));
		}
	}
}