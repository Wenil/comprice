﻿namespace Com.Price.Data.Extensions
{
	public static class StringExtension
	{
		public static double ToDouble(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return 0.0;

			var clearValue = value.Replace(",", ".");
			if (double.TryParse(clearValue, out var doubleResult))
				return doubleResult;

			clearValue = value.Replace(".", ",");

			return double.TryParse(clearValue, out doubleResult) ? doubleResult : 0.0;
		}

		public static string ReplaceCommaToPoint(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return string.Empty;

			return value.Replace(",", ".");
		}

		public static int ToInt(this string value, int defaultValue = 0)
		{
			if (int.TryParse(value, out var result))
				return result;

			return defaultValue;
		}

		public static bool ToBoolean(this string value)
		{
			if (bool.TryParse(value, out var result))
				return result;

			return false;
		}
	}
}