﻿using System;
using System.ComponentModel;

namespace Com.Price.Data.Extensions
{
	public static class EnumExtension
	{
		public static string Description(this Enum value)
		{
			return (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()),
				typeof(DescriptionAttribute)) as DescriptionAttribute)?.Description;
		}
	}
}