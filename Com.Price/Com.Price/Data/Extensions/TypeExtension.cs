﻿using Com.Price.Data.Converters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Com.Price.Data.Extensions
{
	public static class TypeExtension
	{
		public static IEnumerable<ValueDescription> GetAllValuesAndDescriptions(this Type type)
		{
			if (!type.IsEnum)
				throw new ArgumentException("t must be an enum type");

			return Enum.GetValues(type)
				.Cast<Enum>()
				.Select((e) => new ValueDescription() { Value = e, Description = e.Description() }).ToList();
		}
	}
}
