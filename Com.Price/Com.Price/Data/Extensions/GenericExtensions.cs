﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Com.Price.Data.Extensions
{
	public static class GenericExtensions
	{
		public static T DeepCopy<T>(this T other)
		{
			using var ms = new MemoryStream();
			var formatter = new BinaryFormatter();
			formatter.Serialize(ms, other);
			ms.Position = 0;

			return (T) formatter.Deserialize(ms);
		}
	}
}