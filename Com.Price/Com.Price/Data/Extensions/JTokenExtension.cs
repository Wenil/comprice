﻿using System;
using Newtonsoft.Json.Linq;

namespace Com.Price.Data.Extensions
{
	public static class JTokenExtension
	{
		public static int ConvertValueToInt(this JToken jToken)
		{
			if (jToken != null)
			{
				return jToken.Type switch
				{
					JTokenType.Integer => jToken.Value<int>(),
					JTokenType.Float => Convert.ToInt32(jToken.Value<double>()),
					_ => Convert.ToInt32(jToken.ToString().ToDouble())
				};
			}

			return 0;
		}
	}
}