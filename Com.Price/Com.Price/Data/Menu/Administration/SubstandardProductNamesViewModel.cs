﻿using System;
using Catel;
using Catel.MVVM;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Com.Price.Data.Menu.Administration
{
	public class SubstandardProductNamesViewModel : ViewModelBase
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public SubstandardProductNamesViewModel(ISystemConfigurationService systemConfigurationService)
		{
			Argument.IsNotNull(() => systemConfigurationService);

			_systemConfigurationService = systemConfigurationService;
		}

		public string GarbageNames
		{
			get => string.Join(Environment.NewLine, _systemConfigurationService.Garbages);
			set => _systemConfigurationService.Garbages = value.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
		}
	}
}