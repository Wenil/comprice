﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Catel;
using Catel.MVVM;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.SystemConfiguration;
using Com.Price.Infrastructure.Suppliers;
using Com.Price.Views.Menu.Administration;

namespace Com.Price.Data.Menu.Administration
{
	public class SystemConfigurationViewModel : ViewModelBase
	{
		private readonly ISystemConfigurationService _systemConfigurationService;
		private readonly ISupplierLoaderService _supplierLoaderService;
		private readonly IViewModelLocator _viewModelLocator;
		private readonly ILoggerService _loggerService;

		public SystemConfigurationViewModel(ISystemConfigurationService systemConfigurationService, ISupplierLoaderService supplierLoaderService, IViewModelLocator viewModelLocator, ILoggerService loggerService)
		{
			Argument.IsNotNull(() => systemConfigurationService);
			Argument.IsNotNull(() => supplierLoaderService);
			Argument.IsNotNull(() => viewModelLocator);

			_systemConfigurationService = systemConfigurationService;
			_supplierLoaderService = supplierLoaderService;
			_viewModelLocator = viewModelLocator;
			_loggerService = loggerService;

			SaveSystemConfigurationCommand = new Command(SaveSystemConfiguration);
			if (!Items.Any())
			{
				Items.Add(new TabItem {Header = "Общие", Content = new GeneralSettingsView {DataContext = new GeneralSettingsViewModel(_systemConfigurationService)}});
				Items.Add(new TabItem
					{Header = "Некондиционные названия", Content = new SubstandardProductNamesView {DataContext = new SubstandardProductNamesViewModel(_systemConfigurationService)}});
				InitializeSupplierConfigurations();
			}
		}

		public ObservableCollection<TabItem> Items { get; set; } = new ObservableCollection<TabItem>();

		private void InitializeSupplierConfigurations()
		{
			if (_supplierLoaderService.AssemblySuppliers != null && _supplierLoaderService.AssemblySuppliers.Any())
			{
				foreach (var supplierAssembly in _supplierLoaderService.AssemblySuppliers)
				{
					try
					{
						CreateTabItem(supplierAssembly);
					}
					catch (Exception exception)
					{
						_loggerService.Error("Ошибка при инициализации панели настроек поставщика", exception);
					}
				}
			}
		}

		private void CreateTabItem(Assembly supplierAssembly)
		{
			var types = supplierAssembly.GetTypes();
			var name = supplierAssembly.GetName().Name;
			Type view = null;
			Type viewModel = null;

			foreach (var type in types)
			{
				if (type.Name.EndsWith("View"))
					view = type;

				if (type.Name.EndsWith("ViewModel"))
					viewModel = type;

				if (view != null && viewModel != null)
					break;
			}

			if (view != null && viewModel != null)
			{
				_viewModelLocator.Register(view, viewModel);

				var tapItem = new TabItem
				{
					Name = "TabItem" + name,
					Header = name
				};

				if (Activator.CreateInstance(view) is UserControl userControl)
				{
					userControl.DataContext = Activator.CreateInstance(viewModel, _systemConfigurationService);
					tapItem.Content = userControl;
					Items.Add(tapItem);
				}
			}
		}

		public ICommand SaveSystemConfigurationCommand { get; set; }

		public void SaveSystemConfiguration()
		{
			if (!_systemConfigurationService.SaveSystemConfiguration())
				MessageBox.Show(@"Не удалось сохранить конфигурацию системы");
			else
				this.SaveAndCloseViewModelAsync();
		}
	}
}