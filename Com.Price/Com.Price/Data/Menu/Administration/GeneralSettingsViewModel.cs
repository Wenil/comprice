﻿using Catel;
using Catel.MVVM;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Com.Price.Data.Menu.Administration
{
	public class GeneralSettingsViewModel : ViewModelBase
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public GeneralSettingsViewModel(ISystemConfigurationService systemConfigurationService)
		{
			Argument.IsNotNull(() => systemConfigurationService);

			_systemConfigurationService = systemConfigurationService;
		}

		public bool SearchOnlyForExactMatchArticle
		{
			get => _systemConfigurationService.SearchOnlyForExactMatchArticle;
			set => _systemConfigurationService.SearchOnlyForExactMatchArticle = value;
		}

		public bool AvailabilityIfQuantityProductMoreThanOne
		{
			get => _systemConfigurationService.AvailabilityIfQuantityProductMoreThanOne;
			set => _systemConfigurationService.AvailabilityIfQuantityProductMoreThanOne = value;
		}

		public bool DisregardSubstandardGoods
		{
			get => _systemConfigurationService.DisregardSubstandardGoods;
			set => _systemConfigurationService.DisregardSubstandardGoods = value;
		}

		public bool UseAutomaticSearchInsteadOfManual
		{
			get => _systemConfigurationService.UseAutomaticSearchInsteadOfManual;
			set => _systemConfigurationService.UseAutomaticSearchInsteadOfManual = value;
		}

		public bool DisableFilteringProductsByNumberOfWalks
		{
			get => _systemConfigurationService.DisableFilteringProductsByNumberOfWalks;
			set => _systemConfigurationService.DisableFilteringProductsByNumberOfWalks = value;
		}
	}
}