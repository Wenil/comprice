﻿using System;

namespace Com.Price.Data.Converters
{
	public class ValueDescription
	{
		public Enum Value { get; set; }
		public string Description { get; set; }
	}
}
