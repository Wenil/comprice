﻿using Com.Price.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace Com.Price.Data.Converters
{
	[ValueConversion(typeof(Enum), typeof(IEnumerable<ValueDescription>))]
	public class EnumToCollectionConverter : MarkupExtension, IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value?.GetType().GetAllValuesAndDescriptions();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => null;

		public override object ProvideValue(IServiceProvider serviceProvider) => this;
	}
}