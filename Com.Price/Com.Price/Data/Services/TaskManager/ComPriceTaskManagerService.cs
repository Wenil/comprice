﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Com.Price.Data.Extensions;
using Com.Price.Data.TaskManager.Enums;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.TaskManager;
using Com.Price.Infrastructure.Services.TaskManager.Model;

namespace Com.Price.Data.Services.TaskManager
{
	public class ComPriceTaskManagerService : IComPriceTaskManagerService
	{
		private readonly ILoggerService _loggerService;
		private readonly IComPriceTaskExecuterService _comPriceTaskExecuterService;

		public ComPriceTaskManagerService(ILoggerService loggerService, IComPriceTaskExecuterService comPriceTaskExecuterService)
		{
			_loggerService = loggerService;
			_comPriceTaskExecuterService = comPriceTaskExecuterService;
		}

		private List<ListPriceTask> _comPriceTasks = new List<ListPriceTask>();

		private readonly object _comPriceTasksLock = new object();

		public List<ListPriceTask> ComPriceTasks
		{
			get
			{
				lock (_comPriceTasksLock)
				{
					return _comPriceTasks;
				}
			}
			set
			{
				lock (_comPriceTasksLock)
				{
					_comPriceTasks = value;
					ComPriceTaskCollectionChanged?.Invoke(null, new ComPriceTaskCollectionChangedEventArgs {Action = ComPriceTaskCollectionChangedAction.Refresh});
				}
			}
		}

		public event EventHandler<ComPriceTaskCollectionChangedEventArgs> ComPriceTaskCollectionChanged;

		private CancellationTokenSource _cancellationTokenSource;

		public void StartTaskManager()
		{
			if (IsTaskManagerRunning)
				return;

			_cancellationTokenSource = new CancellationTokenSource();
			IsTaskManagerRunning = true;

			Task.Run(() =>
			{

				while (IsTaskManagerRunning)
				{
					RunManagerIteration(_cancellationTokenSource.Token);

					if (IsTaskManagerRunning)
						Thread.Sleep(10000);
				}
			});
		}

		private void RunManagerIteration(CancellationToken cancellationToken)
		{
			if (ComPriceTasks.Count > 0)
			{
				var isAwaiting = ComPriceTasks.Count(select => select.Status == ListPriceTaskStatus.Awaiting);
				if (isAwaiting == ComPriceTasks.Count && !IsTaskManagerRunning)
				{
					RunExecuteManager = false;
					return;
				}
			}
			else
			{
				if (!IsTaskManagerRunning)
				{
					RunExecuteManager = false;
					return;
				}
			}

			foreach (var comPriceTask in ComPriceTasks)
			{
				if (comPriceTask.Status == ListPriceTaskStatus.Awaiting && IsTaskManagerRunning)
				{
					if (comPriceTask.StartDate <= DateTime.Now)
					{
						if (_runningTasks.ContainsKey(comPriceTask.Id))
							_runningTasks.Remove(comPriceTask.Id);
						
						var runningPartners = new List<string>();
						foreach (var runningTask in _runningTasks)
							runningPartners.AddRange(runningTask.Value.Suppliers);
						
						var taskCanRun = true;
						foreach (var taskPartner in comPriceTask.Partners)
						{
							if (runningPartners.Contains(taskPartner))
							{
								taskCanRun = false;
						
								break;
							}
						}
						
						if (taskCanRun)
						{
							comPriceTask.Status = ListPriceTaskStatus.Running;

							var task = ComposeExecuterTask(cancellationToken, comPriceTask);
							task.Start();

							_runningTasks.Add(comPriceTask.Id,
								new ComPriceTaskManagerRunItem
								{
									ExecuteTask = task,
									ComPriceTaskExecuterService = _comPriceTaskExecuterService,
									Suppliers = comPriceTask.Partners
								});
						}
					}
				}
				else if (comPriceTask.Status == ListPriceTaskStatus.Done)
				{
					comPriceTask.Status = ListPriceTaskStatus.Awaiting;
					if (comPriceTask.IntervalBetweenStarts > 0 && comPriceTask.StartDate <= DateTime.Now)
					{
						while (comPriceTask.StartDate <= DateTime.Now)
							comPriceTask.StartDate = comPriceTask.StartDate.AddSeconds(comPriceTask.IntervalBetweenStarts);
					}

					if (_runningTasks.ContainsKey(comPriceTask.Id))
						_runningTasks.Remove(comPriceTask.Id);
				}
			}
		}

		private readonly Dictionary<Guid, ComPriceTaskManagerRunItem> _runningTasks = new Dictionary<Guid, ComPriceTaskManagerRunItem>();

		private Task ComposeExecuterTask(CancellationToken cancellationToken, ListPriceTask comPriceTask)
		{
			return new Task(() =>
			{
				var startRunTask = DateTime.Now;
				var runningTask = ComPriceTasks.FirstOrDefault(priceTask => priceTask.Id == comPriceTask.Id);

				if (runningTask != null)
				{
					// run
					_loggerService.Information($"Начало выполнения задачи {runningTask.Name}.");
					var result = ListPriceTaskExecuterReturnValues.AllDone;
					try
					{
						result = _comPriceTaskExecuterService.ExecuteAsync(runningTask, cancellationToken).Result;
					}
					catch (Exception exception)
					{
						_loggerService.Error($"Ошибка при выполнении задачи {comPriceTask.Name}.! \n\r {exception.Message}", exception);
						result = ListPriceTaskExecuterReturnValues.UserTerminateExecute;
					}

					_loggerService.Information($"Результат выполнения задачи {result}.");
					_loggerService.Information($"Завершение выполнения задачи {runningTask.Name}.");
					// the end
					if (result != ListPriceTaskExecuterReturnValues.UserTerminateExecute)
					{
						runningTask.EndTimeLastRun = DateTime.Now;
						runningTask.FullTimeLastRun = new DateTime((runningTask.EndTimeLastRun - startRunTask).Ticks);
						runningTask.StartDate = runningTask.StartDate.AddSeconds(runningTask.IntervalBetweenStarts);
						runningTask.Status = ListPriceTaskStatus.Done;
						// UpdateTask(runningTask);
					}
					else
					{
						runningTask.Status = ListPriceTaskStatus.Awaiting;
						// UpdateTask(runningTask);
					}

					runningTask.MessageAfterRun = result.Description();
				}
			}, cancellationToken);
		}

		public bool RunExecuteManager { get; set; }

		public void StopTaskManager()
		{
			if (!IsTaskManagerRunning)
				return;

			_cancellationTokenSource!.Cancel();
			_cancellationTokenSource!.Dispose();

			IsTaskManagerRunning = false;
		}

		public bool IsTaskManagerRunning { get; private set; }

		public void AddComPriceTask(ListPriceTask listPriceTask)
		{
			if (IsTaskManagerRunning)
				return;

			ComPriceTasks.Add(listPriceTask);
			ComPriceTaskCollectionChanged?.Invoke(null, new ComPriceTaskCollectionChangedEventArgs {Action = ComPriceTaskCollectionChangedAction.Add, Task = listPriceTask});
		}

		public void UpdateComPriceTask(ListPriceTask listPriceTask)
		{
			if (IsTaskManagerRunning)
				return;

			var index = ComPriceTasks.FindIndex(task => task.Id == listPriceTask.Id);
			if (index >= 0)
			{
				ComPriceTasks[index] = listPriceTask;
				ComPriceTaskCollectionChanged?.Invoke(null, new ComPriceTaskCollectionChangedEventArgs {Action = ComPriceTaskCollectionChangedAction.Update, Task = listPriceTask });
			}
		}

		public void RemoveComPriceTask(ListPriceTask listPriceTask)
		{
			if (IsTaskManagerRunning)
				return;

			ComPriceTasks.Remove(listPriceTask);
			ComPriceTaskCollectionChanged?.Invoke(null, new ComPriceTaskCollectionChangedEventArgs {Action = ComPriceTaskCollectionChangedAction.Remove, Task = listPriceTask});
		}
	}
}