﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Com.Price.Infrastructure.Services.TaskManager;

namespace Com.Price.Data.Services.TaskManager
{
	public class ComPriceTaskManagerRunItem
	{
		public Task ExecuteTask { get; set; }

		public IComPriceTaskExecuterService ComPriceTaskExecuterService { get; set; }

		public List<string> Suppliers { get; set; } = new List<string>();
	}
}