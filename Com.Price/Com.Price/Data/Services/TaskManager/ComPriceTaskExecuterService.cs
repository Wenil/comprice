﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Com.Price.Data.Article;
using Com.Price.Data.Services.Api;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Services.ProcessFiles.Excel.Response;
using Com.Price.Data.Suppliers;
using Com.Price.Data.Suppliers.Enums;
using Com.Price.Data.TaskManager.Enums;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.Api;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel.Processing;
using Com.Price.Infrastructure.Services.SystemConfiguration;
using Com.Price.Infrastructure.Services.TaskManager;
using Com.Price.Infrastructure.Services.TaskManager.Model;
using Com.Price.Infrastructure.Suppliers;

namespace Com.Price.Data.Services.TaskManager
{
	public class ComPriceTaskExecuterService : IComPriceTaskExecuterService
	{
		private readonly ILoggerService _loggerService;
		private readonly ISupplierLoaderService _supplierLoaderService;
		private readonly IExcelCpumemProcessingService<ExcelCpumemProcessingRequest, ExcelCpumemProcessingResponse> _excelCpumemProcessingService;
		private readonly ISystemConfigurationService _configurationService;
		private readonly IEleprodApiService<EleprodProductInfo> _eleprodApiService;

		public ComPriceTaskExecuterService(ILoggerService loggerService, ISupplierLoaderService supplierLoaderService,
			IExcelCpumemProcessingService<ExcelCpumemProcessingRequest, ExcelCpumemProcessingResponse> excelCpumemProcessingService,
			ISystemConfigurationService configurationService, IEleprodApiService<EleprodProductInfo> eleprodApiService)
		{
			_loggerService = loggerService;
			_supplierLoaderService = supplierLoaderService;
			_excelCpumemProcessingService = excelCpumemProcessingService;
			_configurationService = configurationService;
			_eleprodApiService = eleprodApiService;
		}

		public async Task<ListPriceTaskExecuterReturnValues> ExecuteAsync(ListPriceTask task, CancellationToken cancellationToken)
		{
			if (task == null)
			{
				_loggerService.Information("Параметры задачи не заполнены.");
				return ListPriceTaskExecuterReturnValues.TaskParametersIsEmpty;
			}

			var files = ComposeComPriceTaskFiles(task.DirectoryPath, "*.xls?");
			if (!files.Any())
			{
				_loggerService.Information("Файлы не найдены.");
				return ListPriceTaskExecuterReturnValues.DirectoryIsEmpty;
			}

			_loggerService.Information($"В указанной директорие найдено {files.Count} файла/ов.");
			foreach (var file in files)
			{
				_loggerService.Information($"Обработка файла {file}");
				if (cancellationToken.IsCancellationRequested)
					return ListPriceTaskExecuterReturnValues.UserTerminateExecute;

				var articles = ReadArticleFromExcel(file);
				if (articles.Any())
				{
					_loggerService.Information($"В файле найдено {articles.Count} парт-номеров.");
					var suppliers = _supplierLoaderService.CreateInstanceSuppliers(task.Partners);
					if (suppliers.Any())
					{
						_loggerService.Information("Поиск...");
						await Task.WhenAll(ComposeSupplierTasks(articles, file, suppliers, cancellationToken));

						_loggerService.Information("Поиск завершен.");
						if (cancellationToken.IsCancellationRequested)
							return ListPriceTaskExecuterReturnValues.UserTerminateExecute;

						_loggerService.Information("Обновление данных в файле.");
						var updateFindArticles = _excelCpumemProcessingService.UpdateFindArticles(file, task, suppliers);

						_loggerService.Information("Файл обновлен.");
						if (updateFindArticles.Any())
						{
							_loggerService.Information("Обновление данных на сайте.");
							UpdateOnWebSite(updateFindArticles, task.Type);
						}
					}
				}
			}

			return ListPriceTaskExecuterReturnValues.AllDone;
		}

		private List<string> ComposeComPriceTaskFiles(string path, string searchPattern)
		{
			try
			{
				return Directory.GetFiles(path, searchPattern, SearchOption.AllDirectories).ToList();
			}
			catch (Exception)
			{
				// ignored
			}

			return new List<string>();
		}

		private const int MaxWalkCounterValue = 35;

		private const int NumberRoundsForUpdatedOldGoods = 10;

		private List<ReadArticleData> ReadArticleFromExcel(string filePath)
		{
			var readArticlesAndStatistics = _excelCpumemProcessingService.ReadArticlesAndStatistics(filePath);

			return readArticlesAndStatistics
				.Where(response =>
				{
					if (_configurationService.DisableFilteringProductsByNumberOfWalks)
						return true;

					return response.WalkCounter < MaxWalkCounterValue || (response.ProcessingFileCounter - response.WalkCounter) % NumberRoundsForUpdatedOldGoods == 0;
				})
				.Select(response => new ReadArticleData { Article = response.Article, ArticleManufacturer = response.ArticleManufacturer })
				.ToList();
		}

		private class ReadArticleData
		{
			public ComPriceArticle Article { get; set; }

			public ComPriceArticle ArticleManufacturer { get; set; }
		}

		private Task[] ComposeSupplierTasks(IReadOnlyCollection<ReadArticleData> articles, string filePath, List<SupplierBase> suppliers, CancellationToken cancellationToken)
		{
			var task = new List<Task>();

			foreach (var supplier in suppliers)
			{
				var systemConfigClone = new Dictionary<string, string>(_configurationService.SystemConfiguration) { { "ArticleSourceFilePath", filePath } };
				supplier.ApplySupplierConfigurationParameters(systemConfigClone);
				supplier.IsEqualArticle = true;

				var searchArticles = supplier.SearchField == MainSearchField.Article ? articles.Select(data => data.Article).ToList() : articles.Select(data => data.ArticleManufacturer).ToList();
				task.Add(supplier.SearchProductAsync(searchArticles, SearchType.Automatic, cancellationToken));
			}

			return task.ToArray();
		}

		private void UpdateOnWebSite(IEnumerable<ComPriceProduct> productForUpdate, ListPriceTaskType taskType)
		{
			foreach (var product in productForUpdate)
			{
				var webSiteProductInfo = _eleprodApiService.GetProductInfo(product.Article, product.Brand);
				if (webSiteProductInfo != null && !webSiteProductInfo.IsEmpty())
				{
					var productAvailability = ((int)product.Availability).ToString();
					switch (taskType)
					{
						case ListPriceTaskType.AvailabilityAndCostUpdate:
							{
								if (!productAvailability.Equals(webSiteProductInfo.Availability))
								{
									webSiteProductInfo.Availability = productAvailability;
									_eleprodApiService.UpdateAvailability(webSiteProductInfo);
								}

								var price = product.Price.ToString(CultureInfo.InvariantCulture);
								if (!price.Equals(webSiteProductInfo.Price))
								{
									webSiteProductInfo.Price = price;
									_eleprodApiService.UpdatePrice(webSiteProductInfo);
								}

								var quantity = product.Quantity.ToString();
								if (!quantity.Equals(webSiteProductInfo.Quantity))
								{
									webSiteProductInfo.Quantity = quantity;
									_eleprodApiService.UpdateQuantity(webSiteProductInfo);
								}

								_eleprodApiService.UpdateDeliveryDay(webSiteProductInfo, product.DayCountOnDelivery);

								break;
							}
						case ListPriceTaskType.AvailabilityUpdate:
							{
								if (!productAvailability.Equals(webSiteProductInfo.Availability))
								{
									webSiteProductInfo.Availability = productAvailability;
									_eleprodApiService.UpdateAvailability(webSiteProductInfo);
								}

								var quantity = product.Quantity.ToString();
								if (!quantity.Equals(webSiteProductInfo.Quantity))
								{
									webSiteProductInfo.Quantity = quantity;
									_eleprodApiService.UpdateQuantity(webSiteProductInfo);
								}

								_eleprodApiService.UpdateDeliveryDay(webSiteProductInfo, product.DayCountOnDelivery);

								break;
							}
						case ListPriceTaskType.CostUpdate:
							{
								var price = product.Price.ToString(CultureInfo.InvariantCulture);
								if (!price.Equals(webSiteProductInfo.Price))
								{
									webSiteProductInfo.Price = price;
									_eleprodApiService.UpdatePrice(webSiteProductInfo);
								}

								break;
							}
						default:
							throw new ArgumentOutOfRangeException(nameof(taskType), taskType, null);
					}
				}
				else
					_loggerService.Information($"Нет совпадения по паре парт \"{product.Article}\" название \"{product.Name}\".");
			}
		}
	}
}