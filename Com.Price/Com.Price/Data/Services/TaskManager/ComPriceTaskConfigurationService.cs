﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Com.Price.Data.TaskManager.Enums;
using Com.Price.Infrastructure.Services.TaskManager;
using Com.Price.Infrastructure.Services.TaskManager.Model;

namespace Com.Price.Data.Services.TaskManager
{
	public class ComPriceTaskConfigurationService : IComPriceTaskConfigurationService
	{
		public void Save(string path, IList<ListPriceTask> comPriceTasks)
		{
			var formatter = new XmlSerializer(typeof(List<ListPriceTask>));

			using var fs = new FileStream(path, FileMode.Create);
			formatter.Serialize(fs, comPriceTasks);
		}

		public IList<ListPriceTask> Load(string path)
		{
			var stream = File.Open(path, FileMode.Open);
			var formatter = new XmlSerializer(typeof(List<ListPriceTask>));
			var loadData = (List<ListPriceTask>) formatter.Deserialize(stream);
			stream.Close();

			var tasks = new List<ListPriceTask>(loadData);
			foreach (var comPriceTask in tasks.Where(task => task.Status != ListPriceTaskStatus.Awaiting))
				comPriceTask.Status = ListPriceTaskStatus.Awaiting;

			return tasks;
		}
	}
}