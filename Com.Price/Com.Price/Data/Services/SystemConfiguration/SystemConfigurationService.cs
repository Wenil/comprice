﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Com.Price.Data.Services.SystemConfiguration
{
	public class SystemConfigurationService : ISystemConfigurationService
	{
		public SystemConfigurationService()
		{
			if (!Directory.Exists("Uploads"))
				Directory.CreateDirectory("Uploads");

			if (!Directory.Exists("Settings"))
				Directory.CreateDirectory("Settings");

			LoadSystemConfiguration();
		}

		public Dictionary<string, string> SystemConfiguration { get; private set; } = new Dictionary<string, string>();

		public string GetSystemConfigurationParameter(string name) => GetConfigurationValue(name);

		public void SetSystemConfigurationParameter(string name, string value) => SetConfigurationValue(name, value);

		public bool IsSystemConfigurationParameter(string name) => GetBooleanConfigurationValue(name);

		public void SetSystemConfigurationParameter(string name, bool value) => SetConfigurationValue(name, value ? "1" : "0");

		public string[] Garbages { get; set; } = {string.Empty};

		private const string PathSystemConfiguration = @"Settings\SystemConfig.conf";

		private const string PathGarbageInProduct = @"Settings\GarbageInProductName.conf";

		/// <summary>
		/// Разделитель настроек приложения. 
		/// </summary>
		private const string SeparatorSystemConfigParameters = "::;;";

		public bool LoadSystemConfiguration()
		{
			if (IsExistSystemConfiguration())
			{
				var configLines = File.ReadAllLines(PathSystemConfiguration);
				if (configLines.Any())
				{
					SystemConfiguration = new Dictionary<string, string>();
					foreach (var line in configLines)
					{
						var split = Regex.Split(line, SeparatorSystemConfigParameters);
						if (split.Length == 2)
							SystemConfiguration.Add(split[0], split[1]);
					}
				}
			}

			if (IsExistGarbage())
				Garbages = File.ReadAllLines(PathGarbageInProduct);

			return true;
		}

		private bool IsExistSystemConfiguration() => File.Exists(PathSystemConfiguration);

		private bool IsExistGarbage() => File.Exists(PathGarbageInProduct);

		public bool SaveSystemConfiguration()
		{
			if (SystemConfiguration.Any())
			{
				var saveLines = SystemConfiguration
					.OrderBy(pair => pair.Key)
					.Select(systemConfigValue => systemConfigValue.Key + SeparatorSystemConfigParameters + systemConfigValue.Value)
					.ToList();

				File.WriteAllLines(PathSystemConfiguration, saveLines);
			}

			if (Garbages.Any())
				File.WriteAllLines(PathGarbageInProduct, Garbages.Where(garbage => !string.IsNullOrEmpty(garbage)).Distinct());

			return true;
		}

		public void AddRangeSystemConfigurationParameters(Dictionary<string, string> parameters)
		{
			if (parameters != null && parameters.Any())
			{
				foreach (var param in parameters)
				{
					if (SystemConfiguration.ContainsKey(param.Key))
						SystemConfiguration[param.Key] = param.Value;
					else
						SystemConfiguration.Add(param.Key, param.Value);
				}
			}
		}

		/// <summary>
		/// Искать точное вхождение артикула.
		/// </summary>
		public bool SearchOnlyForExactMatchArticle
		{
			get => GetBooleanConfigurationValue("SearchOnlyForExactMatchArticle");
			set => SetConfigurationValue("SearchOnlyForExactMatchArticle", value ? "1" : "0");
		}

		/// <summary>
		/// Выводить товар с минимальную цену
		/// </summary>
		public bool DisplayProductWithLowestPrice
		{
			get => GetBooleanConfigurationValue("DisplayProductWithLowestPrice");
			set => SetConfigurationValue("DisplayProductWithLowestPrice", value ? "1" : "0");
		}

		/// <summary>
		/// Ставить наличие "Есть", если количество товара больше 1
		/// </summary>
		public bool AvailabilityIfQuantityProductMoreThanOne
		{
			get => GetBooleanConfigurationValue("AvailabilityIfQuantityProductMoreThanOne");
			set => SetConfigurationValue("AvailabilityIfQuantityProductMoreThanOne", value ? "1" : "0");
		}

		public bool DisregardSubstandardGoods
		{
			get => GetBooleanConfigurationValue("DisregardSubstandardGoods");
			set => SetConfigurationValue("DisregardSubstandardGoods", value ? "1" : "0");
		}

		public bool UseAutomaticSearchInsteadOfManual
		{
			get => GetBooleanConfigurationValue("UseAutomaticSearchInsteadOfManual");
			set => SetConfigurationValue("UseAutomaticSearchInsteadOfManual", value ? "1" : "0");
		}

		public bool CalculateAtTheRate
		{
			get => GetBooleanConfigurationValue("CalculateAtTheRate");
			set => SetConfigurationValue("CalculateAtTheRate", value ? "1" : "0");
		}

		public bool DisableFilteringProductsByNumberOfWalks
		{
			get => GetBooleanConfigurationValue("DisableFilteringProductsByNumberOfWalks");
			set => SetConfigurationValue("DisableFilteringProductsByNumberOfWalks", value ? "1" : "0");
		}

		private string GetConfigurationValue(string configurationKey)
		{
			if (SystemConfiguration != null && !string.IsNullOrEmpty(configurationKey) && SystemConfiguration.ContainsKey(configurationKey))
				return SystemConfiguration[configurationKey];

			return string.Empty;
		}

		private void SetConfigurationValue(string configurationKey, string configurationValue)
		{
			if (SystemConfiguration != null && !string.IsNullOrEmpty(configurationKey) && configurationValue != null)
			{
				if (SystemConfiguration.ContainsKey(configurationKey))
					SystemConfiguration[configurationKey] = configurationValue;
				else
					SystemConfiguration.Add(configurationKey, configurationValue);
			}
		}

		private bool GetBooleanConfigurationValue(string configurationKey)
		{
			var configurationValue = GetConfigurationValue(configurationKey);

			return configurationValue != null && configurationValue.Equals("1");
		}
	}
}