﻿using System.Collections.Generic;

namespace Com.Price.Data.ProcessFiles.Xml
{
	public class XmlProcessingRequest
	{
		public string FilePath { get; set; }

		public string Item { get; set; } = string.Empty;

		public string Article { get; set; } = string.Empty;

        public string ArticleManufacturer { get; set; } = string.Empty;

        public string Name { get; set; } = string.Empty;

		public string Manufacturer { get; set; } = string.Empty;

		public IList<string> Price { get; set; } = new List<string>();

		public IList<string> Quantity { get; set; } = new List<string>();
	}
}