﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Com.Price.Data.ProcessFiles.Xml;
using Com.Price.Infrastructure.Services.ProcessFiles.Xml;

namespace Com.Price.Data.Services.ProcessFiles.Xml
{
	public class XmlProcessingService : IXmlProcessingService<XmlProcessingRequest, XmlProcessingResponse>
	{
		public IList<XmlProcessingResponse> FindProductByArticles(IList<string> articlesList, XmlProcessingRequest xmlProcessingRequest)
		{
			var findProducts = new List<XmlProcessingResponse>();

			var xmlCatalog = XElement.Load(xmlProcessingRequest.FilePath);
			var itemElements = xmlCatalog.Descendants(xmlProcessingRequest.Item).ToList();
			if (itemElements.Any())
			{
				foreach (var article in articlesList)
				{
					var item = itemElements
						.Where(itemElement => itemElement.Element(xmlProcessingRequest.Article).Value.Equals(article))
						.ToList();

					if (item.Any())
					{
						var xmlProcessingResponse = new XmlProcessingResponse
						{
							Article = article,
							
                            FindArticle = item[0].Element(xmlProcessingRequest.Article)?.Value,
							Name = item[0].Element(xmlProcessingRequest.Name)?.Value,
							Manufacturer = item[0].Element(xmlProcessingRequest.Manufacturer)?.Value,
						};

						if (!string.IsNullOrEmpty(xmlProcessingRequest.ArticleManufacturer))
                            xmlProcessingResponse.ArticleManufacturer = item[0].Element(xmlProcessingRequest.ArticleManufacturer)?.Value;			

						foreach (var quantity in xmlProcessingRequest.Quantity)
							xmlProcessingResponse.Quantity.Add(item[0].Element(quantity)?.Value);

						foreach (var price in xmlProcessingRequest.Price)
							xmlProcessingResponse.Price.Add(item[0].Element(price)?.Value);

						findProducts.Add(xmlProcessingResponse);
					}
				}
			}

			return findProducts;
		}
	}
}