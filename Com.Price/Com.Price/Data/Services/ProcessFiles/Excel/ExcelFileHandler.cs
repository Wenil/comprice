﻿using NetOffice.ExcelApi;

namespace Com.Price.Data.Services.ProcessFiles.Excel
{
	public class ExcelFileHandler
	{
		public uint ExcelProcessId { get; set; }

		public Application ExcelApplication { get; set; }

		public Workbooks ExcelWorkbooks { get; set; }

		public Workbook ExcelWorkbook { get; set; }

		public bool Save { get; set; }
	}
}