﻿using System.Collections.Generic;

namespace Com.Price.Data.Services.ProcessFiles.Excel.Request
{
	public class ExcelProcessingRequestBase
	{
		public int Article { get; set; } = -1;

		public int ArticleManufacturer { get; set; } = -1;

		public string FilePath { get; set; }

		public IList<int> SheetsNumberToSearch { get; set; } = new List<int>();

		public IList<int> SheetsNumberNotSearchable { get; set; } = new List<int>();

		/// <summary>
		///
		/// NOTE: xlValues = 0, xlComments = 1, xlFormulas = 2
		/// </summary>
		public byte FindLookIn { get; set; } = 0;

		/// <summary>
		///
		/// NOTE: xlWhole  = 0, xlPart = 1
		/// </summary>
		public byte FindLookAt { get; set; } = 0;
	}
}