﻿using System.Collections.Generic;

namespace Com.Price.Data.Services.ProcessFiles.Excel.Request
{
	public class ExcelProcessingRequest : ExcelProcessingRequestBase
	{
		public int Name { get; set; } = -1;

		public int Manufacturer { get; set; } = -1;

		public IList<int> Price { get; set; } = new List<int>();

		public IList<int> Quantity { get; set; } = new List<int>();

		public int Currency { get; set; } = -1;
	}
}