﻿using System.Collections.Generic;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.TaskManager.Model;

namespace Com.Price.Data.Services.ProcessFiles.Excel.Request
{
	public class ExcelCpumemProcessingRequest : ExcelProcessingRequestBase
	{
		public int WalkCounter { get; set; } = -1;

		public int ProcessingFileCounter { get; set; } = -1;

		public ListPriceTask Task { get; set; }

		public List<SupplierBase> Suppliers { get; set; }

		public int Brand { get; set; }
	}
}