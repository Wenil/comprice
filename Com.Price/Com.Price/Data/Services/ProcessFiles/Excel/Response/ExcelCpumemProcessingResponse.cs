﻿using Com.Price.Data.Article;

namespace Com.Price.Data.Services.ProcessFiles.Excel.Response
{
	public class ExcelCpumemProcessingResponse : ExcelProcessingResponseBase
	{
		public ComPriceArticle Article { get; set; }

		public ComPriceArticle ArticleManufacturer { get; set; }

		public int WalkCounter { get; set; }

		public int ProcessingFileCounter { get; set; }
	}
}