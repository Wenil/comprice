﻿namespace Com.Price.Data.Services.ProcessFiles.Excel.Response
{
	public class ExcelProcessingResponseBase
	{
		public string SheetName { get; set; }

		public int SheetNumber { get; set; }

		public int RowNumber { get; set; }
	}
}