﻿using System.Collections.Generic;

namespace Com.Price.Data.Services.ProcessFiles.Excel.Response
{
	public class ExcelProcessingResponse : ExcelProcessingResponseBase
	{
		public string Article { get; set; }

		public string ArticleManufacturer { get; set; }

		public object FindArticle { get; set; }

        public object FindArticleManufacturer { get; set; }

        public object Name { get; set; }

		public object Manufacturer { get; set; }

		public IList<object> Quantity { get; set; } = new List<object>(); 

		public IList<object> Price { get; set; } = new List<object>();

		public object Currency { get; set; }
	}
}