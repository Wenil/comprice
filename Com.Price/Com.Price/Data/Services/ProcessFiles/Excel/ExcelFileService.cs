﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;

namespace Com.Price.Data.Services.ProcessFiles.Excel
{
	public class ExcelFileService : IExcelFileService<ExcelFileHandler>
	{
		private readonly ILoggerService _loggerService;

		public ExcelFileService(ILoggerService loggerService)
		{
			_loggerService = loggerService;
		}

		[DllImport("user32.dll", SetLastError = true)]
		private static extern uint GetWindowThreadProcessId(IntPtr handleWindow, out uint processId);

		public ExcelFileHandler OpenFile(string filePath)
		{
			var excelFileHandler = new ExcelFileHandler { ExcelApplication = new Application() };

			System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

			GetWindowThreadProcessId(new IntPtr(excelFileHandler.ExcelApplication.Hwnd), out var processId);
			excelFileHandler.ExcelProcessId = processId;

			try
			{
				excelFileHandler.ExcelApplication.Visible = false;
				excelFileHandler.ExcelApplication.DisplayAlerts = false;
				excelFileHandler.ExcelWorkbooks = excelFileHandler.ExcelApplication.Workbooks;
				var fullFilePath = Path.Combine(System.Windows.Forms.Application.StartupPath, filePath);
				if (!fullFilePath.EndsWith(".csv"))
				{
					excelFileHandler.ExcelWorkbook = excelFileHandler.ExcelWorkbooks.Open(fullFilePath);
				}
				else
				{
					excelFileHandler.ExcelWorkbook = excelFileHandler.ExcelWorkbooks.Open(
						filename: fullFilePath,
						Type.Missing,
						Type.Missing,
						format: XlFileFormat.xlCSV,
						Type.Missing,
						Type.Missing,
						Type.Missing,
						Type.Missing,
						delimiter: ';',
						Type.Missing,
						Type.Missing,
						Type.Missing,
						Type.Missing,
						true,
						Type.Missing
					);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.WriteLine(ex.StackTrace);

				_loggerService.Error($"{GetType().Name} : Ошибка при работе с Excel {filePath} {ex.Message}", ex);

				return null;
			}

			return excelFileHandler;
		}

		public void CloseFile(ExcelFileHandler excelFileHandler)
		{
			excelFileHandler.ExcelWorkbook.Close(excelFileHandler.Save);

			excelFileHandler.ExcelWorkbooks.Close();
			if (excelFileHandler.ExcelWorkbooks != null)
				excelFileHandler.ExcelWorkbooks.Dispose();

			if (excelFileHandler.ExcelWorkbook != null)
				excelFileHandler.ExcelWorkbook.Dispose();

			excelFileHandler.ExcelApplication.Quit();
			excelFileHandler.ExcelApplication.Dispose();

			try
			{
				if (excelFileHandler.ExcelProcessId != 0)
				{
					var excelProcess = Process.GetProcessById((int)excelFileHandler.ExcelProcessId);
					excelProcess.CloseMainWindow();
					excelProcess.Refresh();
					excelProcess.Kill();
				}
			}
			catch (Exception ex)
			{
				_loggerService.Error($"{GetType().Name} : Ошибка при попытке закрыть процесс Excel {ex.Message} {ex.StackTrace}", ex);
			}
		}
	}
}