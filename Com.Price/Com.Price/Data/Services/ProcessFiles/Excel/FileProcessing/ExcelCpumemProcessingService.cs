﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Com.Price.Data.Article;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Services.ProcessFiles.Excel.Response;
using Com.Price.Data.Suppliers;
using Com.Price.Data.Suppliers.Enums;
using Com.Price.Data.Suppliers.Structs;
using Com.Price.Data.TaskManager.Enums;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel.Processing;
using Com.Price.Infrastructure.Services.TaskManager.Model;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;

namespace Com.Price.Data.Services.ProcessFiles.Excel.FileProcessing
{
	public class ExcelCpumemProcessingService : IExcelCpumemProcessingService<ExcelCpumemProcessingRequest, ExcelCpumemProcessingResponse>
	{
		private readonly IExcelFileService<ExcelFileHandler> _excelFileService;
		private readonly ILoggerService _loggerService;
		private readonly IListPriceOptimalArticleFindService _listPriceOptimalArticleFindService;

		public ExcelCpumemProcessingService(IExcelFileService<ExcelFileHandler> excelFileService, ILoggerService loggerService, IListPriceOptimalArticleFindService listPriceOptimalArticleFindService)
		{
			_excelFileService = excelFileService;
			_loggerService = loggerService;
			_listPriceOptimalArticleFindService = listPriceOptimalArticleFindService;
		}

		private const int ArticleColumnIndex = 19;

		private const int BrandColumnIndex = 18;

		private const int PriceColumnIndex = 23;

		private const int QuantityTypeColumnIndex = 25;

		private const int QuantityColumnIndex = 14;

		private const int WalkCounterColumnIndex = 5;

		private const int ProcessingFileCounterColumnIndex = 6;

		private const int ArticleManufacturerColumnIndex = 3;

		private const int StartAdditionalArticleColumnIndex = 7;

		private const int EndAdditionalArticleColumnIndex = 13;

		public List<ExcelCpumemProcessingResponse> ReadArticlesAndStatistics(string filePath)
		{
			return ReadArticlesAndStatistics(new ExcelCpumemProcessingRequest
			{
				Article = ArticleColumnIndex,
				FilePath = filePath,
				WalkCounter = WalkCounterColumnIndex,
				ProcessingFileCounter = ProcessingFileCounterColumnIndex,
				ArticleManufacturer = ArticleManufacturerColumnIndex
			});
		}

		public List<ExcelCpumemProcessingResponse> ReadArticlesAndStatistics(ExcelCpumemProcessingRequest excelProcessingRequest)
		{
			var readArticles = new List<ExcelCpumemProcessingResponse>();

			var excelFileHandler = _excelFileService.OpenFile(excelProcessingRequest.FilePath);
			if (excelFileHandler != null)
			{
				try
				{
					var fundProductList = ReadArticlesAndStatisticsFromFile(excelFileHandler, excelProcessingRequest);
					if (fundProductList != null)
						readArticles.AddRange(fundProductList);
				}
				catch (Exception exception)
				{
					_loggerService.Error($"{GetType().Name} : Ошибка при работе с Excel {exception.Message} {Environment.NewLine} {exception.StackTrace}", exception);
				}
				finally
				{
					_excelFileService.CloseFile(excelFileHandler);
				}
			}

			return readArticles;
		}

		private const int SkipRowCount = 5;

		private const int SheetNumber = 1;

		private IList<ExcelCpumemProcessingResponse> ReadArticlesAndStatisticsFromFile(ExcelFileHandler excelFileHandler, ExcelCpumemProcessingRequest excelProcessingRequest)
		{
			var readArticles = new List<ExcelCpumemProcessingResponse>();

			if (excelFileHandler.ExcelWorkbook == null)
				return readArticles;

			var excelSheet = (Worksheet) excelFileHandler.ExcelWorkbook.Sheets[SheetNumber];
			var lastRow = excelSheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell).Row;
			for (var i = SkipRowCount; i < lastRow + 1; i++)
			{
				var response = new ExcelCpumemProcessingResponse
				{
					RowNumber = i, SheetName = excelSheet.Name, SheetNumber = SheetNumber
				};

				if (excelProcessingRequest.Article > 0)
				{
					var cellValue = excelSheet.Cells[i, excelProcessingRequest.Article].ValueToString();
					if (string.IsNullOrEmpty(cellValue))
						continue;

					var additionalArticles = new List<string>();
					for (var j = StartAdditionalArticleColumnIndex; j <= EndAdditionalArticleColumnIndex; j++)
					{
						var additionalArticleValue = excelSheet.Cells[i, j].ValueToString();
						if (!string.IsNullOrEmpty(additionalArticleValue))
							additionalArticles.Add(additionalArticleValue);
					}


					response.Article = new ComPriceArticle(cellValue, additionalArticles);
				}

				if (excelProcessingRequest.ArticleManufacturer > 0)
				{
					var cellValue = excelSheet.Cells[i, excelProcessingRequest.ArticleManufacturer].ValueToString();
					response.ArticleManufacturer = new ComPriceArticle(cellValue ?? string.Empty);
				}

				if (excelProcessingRequest.WalkCounter > 0)
				{
					var cellValue = excelSheet.Cells[i, excelProcessingRequest.WalkCounter].ValueToInt();
					response.WalkCounter = cellValue ?? 0;
				}

				if (excelProcessingRequest.ProcessingFileCounter > 0)
				{
					var cellValue = excelSheet.Cells[i, excelProcessingRequest.ProcessingFileCounter].ValueToInt();
					response.ProcessingFileCounter = cellValue ?? 0;
				}

				readArticles.Add(response);
			}

			return readArticles;
		}

		public List<ComPriceProduct> UpdateFindArticles(string filePath, ListPriceTask task, List<SupplierBase> suppliers)
		{
			var products = new List<ComPriceProduct>();

			var excelProcessingRequest = new ExcelCpumemProcessingRequest
			{
				Article = ArticleColumnIndex,
				FilePath = filePath,
				ArticleManufacturer = ArticleManufacturerColumnIndex,
				Suppliers = suppliers,
				Task = task,
				Brand = BrandColumnIndex
			};

			var excelFileHandler = _excelFileService.OpenFile(excelProcessingRequest.FilePath);
			if (excelFileHandler != null)
			{
				try
				{
					var updateFindArticles = UpdateFindArticles(excelFileHandler, excelProcessingRequest);
					if (updateFindArticles != null)
						products.AddRange(updateFindArticles);
				}
				catch (Exception exception)
				{
					_loggerService.Error($"{GetType().Name} : Ошибка при работе с Excel {exception.Message} {Environment.NewLine} {exception.StackTrace}", exception);
				}
				finally
				{
					_excelFileService.CloseFile(excelFileHandler);
				}
			}

			return products;
		}

		private const int MaxWalkCounterValue = 35;

		private List<ComPriceProduct> UpdateFindArticles(ExcelFileHandler excelFileHandler, ExcelCpumemProcessingRequest excelProcessingRequest)
		{
			var products = new List<ComPriceProduct>();

			if (excelFileHandler.ExcelWorkbook == null)
				return products;

			var excelSheet = (Worksheet) excelFileHandler.ExcelWorkbook.Sheets[SheetNumber];
			var lastRow = excelSheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell).Row;
			for (var i = SkipRowCount; i < lastRow + 1; i++)
			{
				var cellRange = excelSheet.Cells[i, excelProcessingRequest.Article];
				if (cellRange != null && cellRange.Value2 != null)
				{
					var articleFromFile = cellRange.Value2.ToString();
					if (!string.IsNullOrEmpty(articleFromFile))
					{
						ComPriceProduct optimal;
						var isSearchByArticleManufacturer = excelProcessingRequest.Suppliers.Any(partner => partner.SearchField == MainSearchField.ArticleManufacturer);
						if (isSearchByArticleManufacturer)
						{
							Range articleManufacturerCellRange = excelSheet.Cells[i, ArticleManufacturerColumnIndex];
							if (articleManufacturerCellRange == null || articleManufacturerCellRange.Value2 == null)
								continue;

							optimal = OptimalArticle(articleManufacturerCellRange.Value2.ToString(), excelProcessingRequest.Suppliers, CreateMarkupRangeList(excelProcessingRequest.Task),
								excelProcessingRequest.Task.Type);
						}
						else
							optimal = OptimalArticle(articleFromFile, excelProcessingRequest.Suppliers, CreateMarkupRangeList(excelProcessingRequest.Task), excelProcessingRequest.Task.Type);

						if (optimal != null && !optimal.IsEmpty())
						{
							var product = new ComPriceProduct(articleFromFile);

							var brandCellRange = excelSheet.Cells[i, excelProcessingRequest.Brand];
							if (brandCellRange != null && brandCellRange.Value2 != null)
								product.Brand = brandCellRange.Value2.ToString();

							UpdateFieldOptimalProduct(excelProcessingRequest.Task.Type, optimal, product, excelSheet, i);

							// Обнуляем количество проходов без обновления товара.
							ResetCellCounter(excelSheet.Cells[i, WalkCounterColumnIndex]);

							product.DayCountOnDelivery = optimal.DayCountOnDelivery;
							products.Add(product);
						}
						else
						{
							// Если ничего не смогли найти, ставим Уточняйте по умолчанию
							var product = new ComPriceProduct {Article = articleFromFile, Availability = Availability.UnderOrder};
							var brandCellRange = excelSheet.Cells[i, excelProcessingRequest.Brand];

							if (brandCellRange != null && brandCellRange.Value2 != null)
								product.Brand = brandCellRange.Value2.ToString();

							// Увеличиваем количество проходов без обновления товара.
							var walkCounterValue = IncreaseCellCounter(excelSheet.Cells[i, WalkCounterColumnIndex], MaxWalkCounterValue);
							if (walkCounterValue >= MaxWalkCounterValue)
								product.Price = 0.0;

							UpdateFieldNotFoundProduct(excelProcessingRequest.Task.Type, optimal, product, excelSheet, i);

							products.Add(product);
						}
					}

					// Увеличиваем количество проходов файла.
					IncreaseCellCounter(excelSheet.Cells[i, ProcessingFileCounterColumnIndex]);
				}
			}

			excelFileHandler.ExcelWorkbook.Save();

			return products;
		}

		private void UpdateFieldOptimalProduct(ListPriceTaskType taskType, ComPriceProduct optimalComPriceProduct, ComPriceProduct composeComPriceProduct, Worksheet worksheet, int row)
		{
			switch (taskType)
			{
				case ListPriceTaskType.AvailabilityAndCostUpdate:
					UpdatePriceFieldOptimalProduct(optimalComPriceProduct, composeComPriceProduct, worksheet, row);
					UpdateQuantityFieldOptimalProduct(optimalComPriceProduct, composeComPriceProduct, worksheet, row);
					break;
				case ListPriceTaskType.AvailabilityUpdate:
					UpdateQuantityFieldOptimalProduct(optimalComPriceProduct, composeComPriceProduct, worksheet, row);
					break;
				case ListPriceTaskType.CostUpdate:
					UpdatePriceFieldOptimalProduct(optimalComPriceProduct, composeComPriceProduct, worksheet, row);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(taskType), taskType, null);
			}
		}

		private void UpdatePriceFieldOptimalProduct(ComPriceProduct optimalComPriceProduct, ComPriceProduct composeComPriceProduct, Worksheet worksheet, int row)
		{
			Range cellRangeAvail = worksheet.Cells[row, PriceColumnIndex];

			composeComPriceProduct.Price = optimalComPriceProduct.Price;
			cellRangeAvail.Value2 = Math.Round(composeComPriceProduct.Price, 2).ToString(CultureInfo.InvariantCulture);
		}

		private void UpdateQuantityFieldOptimalProduct(ComPriceProduct optimalComPriceProduct, ComPriceProduct composeComPriceProduct, Worksheet worksheet, int row)
		{
			Range cellRangeQuantityType = worksheet.Cells[row, QuantityTypeColumnIndex];
			Range cellRangeQuantity = worksheet.Cells[row, QuantityColumnIndex];

			if (optimalComPriceProduct.Availability == Availability.InStock)
			{
				composeComPriceProduct.Availability = optimalComPriceProduct.Availability;
				cellRangeQuantityType.Value2 = composeComPriceProduct.Availability;
			}
			else
			{
				composeComPriceProduct.Availability = Availability.UnderOrder;
				cellRangeQuantityType.Value2 = composeComPriceProduct.Availability;
			}

			composeComPriceProduct.Quantity = optimalComPriceProduct.Quantity;
			cellRangeQuantity.Value2 = composeComPriceProduct.Quantity;
		}

		private void UpdateFieldNotFoundProduct(ListPriceTaskType taskType, ComPriceProduct optimalComPriceProduct, ComPriceProduct composeComPriceProduct, Worksheet worksheet, int row)
		{
			switch (taskType)
			{
				case ListPriceTaskType.AvailabilityAndCostUpdate:
					UpdatePriceFieldNotFoundProduct(optimalComPriceProduct, composeComPriceProduct, worksheet, row);
					UpdateQuantityFieldNotFoundProduct(optimalComPriceProduct, composeComPriceProduct, worksheet, row);
					break;
				case ListPriceTaskType.AvailabilityUpdate:
					UpdateQuantityFieldNotFoundProduct(optimalComPriceProduct, composeComPriceProduct, worksheet, row);
					break;
				case ListPriceTaskType.CostUpdate:
					UpdatePriceFieldNotFoundProduct(optimalComPriceProduct, composeComPriceProduct, worksheet, row);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(taskType), taskType, null);
			}
		}

		private void UpdatePriceFieldNotFoundProduct(ComPriceProduct optimalComPriceProduct, ComPriceProduct composeComPriceProduct, Worksheet worksheet, int row)
		{
			Range cellRangeAvail = worksheet.Cells[row, PriceColumnIndex];

			cellRangeAvail.Value2 = Math.Round(optimalComPriceProduct.Price, 2).ToString(CultureInfo.InvariantCulture);
			composeComPriceProduct.Price = optimalComPriceProduct.Price;
		}

		private void UpdateQuantityFieldNotFoundProduct(ComPriceProduct optimalComPriceProduct, ComPriceProduct composeComPriceProduct, Worksheet worksheet, int row)
		{
			Range cellRangeQuantityType = worksheet.Cells[row, QuantityTypeColumnIndex];
			Range cellRangeQuantity = worksheet.Cells[row, QuantityColumnIndex];

			if (!string.IsNullOrEmpty(optimalComPriceProduct.QuantityMessage) && optimalComPriceProduct.QuantityMessage.Equals("Под заказ до 10 дней"))
			{
				composeComPriceProduct.Availability = Availability.UnderOrder;
				cellRangeQuantityType.Value2 = optimalComPriceProduct.Availability;
			}
			else
			{
				composeComPriceProduct.Availability = Availability.UnderOrder;
				cellRangeQuantityType.Value2 = Availability.UnderOrder;
			}

			composeComPriceProduct.Quantity = optimalComPriceProduct.Quantity;
			cellRangeQuantity.Value2 = composeComPriceProduct.Quantity;
		}

		/// <summary>
		/// Возвращает оптимальный товар.
		/// </summary>
		/// <param name="article">Парт номер.</param>
		/// <param name="suppliers">Поставщики.</param>
		/// <param name="markupRange">Наценки.</param>
		/// <param name="type">Тип задачи.</param>
		/// <returns>Оптимальный товар</returns>
		/// <exception cref="ArgumentOutOfRangeException">Выстреливает если тип задачи не обработан.</exception>
		private ComPriceProduct OptimalArticle(string article, List<SupplierBase> suppliers, List<MarkupRange> markupRange, ListPriceTaskType type)
		{
			return type switch
			{
				ListPriceTaskType.CostUpdate => _listPriceOptimalArticleFindService.FindOptimalArticleByPrice(article, suppliers, markupRange),
				ListPriceTaskType.AvailabilityUpdate => _listPriceOptimalArticleFindService.FindOptimalArticleByQuantity(article, suppliers, markupRange),
				ListPriceTaskType.AvailabilityAndCostUpdate => _listPriceOptimalArticleFindService.FindOptimalArticleCommon(article, suppliers, markupRange),
				_ => throw new ArgumentOutOfRangeException(nameof(type), type, null),
			};
		}

		private List<MarkupRange> CreateMarkupRangeList(ListPriceTask task)
		{
			var markupRange = new List<MarkupRange>();
			if (task.IsUseMarkupRange)
			{
				foreach (var iteMarkupRangeModel in task.MarkupRange.Where(item => !string.IsNullOrEmpty(item.RangeStart)).ToList())
				{
					markupRange.Add(new MarkupRange
					{
						RangeStart = iteMarkupRangeModel.RangeStart.ToInt(),
						Markup = iteMarkupRangeModel.Markup.ToDouble(),
						RangeEnd = iteMarkupRangeModel.RangeEnd.ToInt()
					});
				}
			}
			else
				markupRange.Add(new MarkupRange {RangeStart = 0, Markup = task.Markup, RangeEnd = 0});

			return markupRange;
		}

		private int IncreaseCellCounter(Range cellCounter, int? maxCounterValue = null)
		{
			if (cellCounter != null && cellCounter.Value2 != null)
			{
				if (int.TryParse(cellCounter.Value2.ToString(), out var walkCounterCellValue))
				{
					if (maxCounterValue == null || (walkCounterCellValue < maxCounterValue))
					{
						cellCounter.Value2 = walkCounterCellValue + 1;
						return walkCounterCellValue + 1;
					}

					return walkCounterCellValue;
				}
				else
				{
					cellCounter.Value2 = 0;
					return 0;
				}
			}
			else if (cellCounter != null)
			{
				cellCounter.Value2 = 1;
				return 1;
			}

			return 0;
		}

		private void ResetCellCounter(Range cellCounter)
		{
			if (cellCounter != null)
				cellCounter.Value2 = 0;
		}
	}
}