﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Services.ProcessFiles.Excel.Response;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel.Processing;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;

namespace Com.Price.Data.Services.ProcessFiles.Excel.FileProcessing
{
    public class ExcelProcessingService : IExcelProcessingService<ExcelProcessingRequest, ExcelProcessingResponse>
    {
        private readonly IExcelFileService<ExcelFileHandler> _excelFileService;
        private readonly ILoggerService _loggerService;

        public ExcelProcessingService(IExcelFileService<ExcelFileHandler> excelFileService, ILoggerService loggerService)
        {
            _excelFileService = excelFileService;
            _loggerService = loggerService;
        }

        public IList<ExcelProcessingResponse> FindProductByArticles(IList<string> articlesList, ExcelProcessingRequest excelProcessingRequest)
        {
            var findProducts = new List<ExcelProcessingResponse>();

            var excelFileHandler = _excelFileService.OpenFile(excelProcessingRequest.FilePath);
            if (excelFileHandler != null)
            {
                try
                {
                    var fundProductList = FindProducts(articlesList, excelFileHandler, excelProcessingRequest);
                    if (fundProductList != null)
                        findProducts.AddRange(fundProductList);
                }
                catch (Exception exception)
                {
                    _loggerService.Error($"{GetType().Name} : Ошибка при работе с Excel {exception.Message} {Environment.NewLine} {exception.StackTrace}", exception);
                }
                finally
                {
                    _excelFileService.CloseFile(excelFileHandler);
                }
            }

            return findProducts;
        }

        private IList<ExcelProcessingResponse> FindProducts(IList<string> articlesList, ExcelFileHandler excelFileHandler, ExcelProcessingRequest excelProcessingRequest)
        {
            var findProducts = new List<ExcelProcessingResponse>();

            foreach (var article in articlesList)
            {
                var addArticle = false;

                for (var sheetNumber = 1; sheetNumber <= excelFileHandler.ExcelWorkbook.Sheets.Count; sheetNumber++)
                {
                    var excelSheet = (Worksheet)excelFileHandler.ExcelWorkbook.Sheets[sheetNumber];

                    if (excelProcessingRequest.SheetsNumberToSearch.Any() && !excelProcessingRequest.SheetsNumberToSearch.Contains(sheetNumber))
                        continue;

                    if (excelProcessingRequest.SheetsNumberNotSearchable.Contains(sheetNumber))
                        continue;

                    var searchRange = excelSheet
                        .Columns[excelProcessingRequest.Article]
                        .Find(article, Missing.Value, ComposeXlFindLookIn(excelProcessingRequest.FindLookIn),
                            ComposeXlLookAt(excelProcessingRequest.FindLookAt), XlSearchOrder.xlByColumns, XlSearchDirection.xlNext);

                    if (searchRange != null)
                    {
                        var firstRangeAddress = searchRange.Address;

                        do
                        {
                            var rowNumber = searchRange.Row;

                            var excelProcessingResponse = new ExcelProcessingResponse { SheetName = excelSheet.Name, SheetNumber = sheetNumber, RowNumber = rowNumber, Article = article };

                            if (excelProcessingRequest.Article >= 0)
                                excelProcessingResponse.FindArticle = excelSheet.Cells[rowNumber, excelProcessingRequest.Article].Value2;

                            if (excelProcessingRequest.Name >= 0)
                                excelProcessingResponse.Name = excelSheet.Cells[rowNumber, excelProcessingRequest.Name].Value2;

                            if (excelProcessingRequest.Manufacturer >= 0)
                                excelProcessingResponse.Manufacturer = excelSheet.Cells[rowNumber, excelProcessingRequest.Manufacturer].Value2;

                            if (excelProcessingRequest.ArticleManufacturer >= 0)
                                excelProcessingResponse.FindArticleManufacturer = excelSheet.Cells[rowNumber, excelProcessingRequest.ArticleManufacturer].Value2;

                            foreach (var quantityCellNumber in excelProcessingRequest.Quantity)
                            {
                                if (quantityCellNumber >= 0)
                                    excelProcessingResponse.Quantity.Add(excelSheet.Cells[rowNumber, quantityCellNumber].Value2);
                            }

                            foreach (var priceCellNumber in excelProcessingRequest.Price)
                            {
                                if (priceCellNumber >= 0)
                                    excelProcessingResponse.Price.Add(excelSheet.Cells[rowNumber, priceCellNumber].Value2);
                            }

                            if (excelProcessingRequest.Currency >= 0)
                                excelProcessingResponse.Currency = excelSheet.Cells[rowNumber, excelProcessingRequest.Currency].Value2;

                            findProducts.Add(excelProcessingResponse);
                            addArticle = true;

                            searchRange = excelSheet.Columns[excelProcessingRequest.Article].FindNext(searchRange);
                        } while (searchRange != null && searchRange.Address != firstRangeAddress);

                        break;
                    }
                }

                if (!addArticle)
                    findProducts.Add(new ExcelProcessingResponse { Article = article });
            }

            return findProducts;
        }

        private XlFindLookIn ComposeXlFindLookIn(byte requestValue)
        {
            var findLookIn = XlFindLookIn.xlValues;
            if (requestValue == 1)
            {
                findLookIn = XlFindLookIn.xlComments;
            }
            else if (requestValue == 2)
            {
                findLookIn = XlFindLookIn.xlFormulas;
            }

            return findLookIn;
        }

        private XlLookAt ComposeXlLookAt(byte requestValue)
        {
            var findLookAt = XlLookAt.xlWhole;
            if (requestValue == 1)
                findLookAt = XlLookAt.xlPart;

            return findLookAt;
        }
    }
}