﻿using Com.Price.Data.Suppliers;
using Com.Price.Data.Suppliers.Enums;
using Com.Price.Infrastructure.Services.Delivery;

namespace Com.Price.Data.Services.Delivery
{
	public class CalculateDeliveryService : ICalculateDeliveryService
	{
		public void CalculateDeliveryDays(ComPriceProduct comPriceProduct, int inStockDeliveryDay, int underOrderDeliveryDay, int specifyDeliveryDay)
		{
			if (comPriceProduct.Quantity == 0)
			{
				comPriceProduct.DayCountOnDelivery = underOrderDeliveryDay;
				comPriceProduct.Availability = Availability.UnderOrder;

			}
			else if (!string.IsNullOrEmpty(comPriceProduct.QuantityMessage))
			{
				comPriceProduct.DayCountOnDelivery = specifyDeliveryDay;
				comPriceProduct.Availability = Availability.Specify;
			}
			else
			{
				comPriceProduct.DayCountOnDelivery = inStockDeliveryDay;
				comPriceProduct.Availability = Availability.InStock;
			}
		}
	}
}