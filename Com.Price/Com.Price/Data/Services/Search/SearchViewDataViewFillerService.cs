﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Com.Price.Data.Article;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.Exchange;
using Com.Price.Infrastructure.Services.Exchange.Data;
using Com.Price.Infrastructure.Services.Search;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Com.Price.Data.Services.Search
{
	public class SearchViewDataViewFillerService : ISearchViewDataViewFillerService<ComPriceArticle>
	{
		private readonly ICheckConditionService _checkConditionService;
		private readonly IExchangeRateService _exchangeRateService;
		private readonly ISystemConfigurationService _systemConfigurationService;

		public SearchViewDataViewFillerService(ICheckConditionService checkConditionService, IExchangeRateService exchangeRateService, ISystemConfigurationService systemConfigurationService)
		{
			_checkConditionService = checkConditionService;
			_exchangeRateService = exchangeRateService;
			_systemConfigurationService = systemConfigurationService;
		}

		private const string NameFieldColumnHeaderFormat = "Название - {0}";
		private const string BrandFieldColumnHeaderFormat = "Брэнд - {0}";
		private const string QuantityFieldColumnHeaderFormat = "Кол-во - {0}";
		private const string PriceFieldColumnHeaderFormat = "Цена - {0}";
		private const string DeliveryDaysFieldColumnHeaderFormat = "Достака (дней) - {0}";

		public DataTable ProcessFullResult(List<ComPriceArticle> articles, List<SupplierBase> suppliers)
		{
			var resultDataTable = new DataTable();
			resultDataTable.Columns.Add("Part");
			suppliers.ForEach(supplier => AddSupplierColumns(resultDataTable, supplier.Header));

			foreach (var article in articles)
			{
				var comPriceProductFindByArticle = suppliers
					.ToDictionary(
						supplier => supplier.Header,
						supplier => supplier.SearchProductResult.FindAll(product => article.Equals(product.Article) && !_checkConditionService.IsGarbageName(product.Name))
					);

				var countArticle = comPriceProductFindByArticle.Select(find => find.Value.Count).ToList();
				var maxCount = countArticle.Max();

				if (maxCount == 0)
				{
					var row = resultDataTable.NewRow();
					row["Part"] = article.MainArticle;
					resultDataTable.Rows.Add(row);
				}
				else if (countArticle.All(count => count == maxCount) || maxCount == 1)
				{
					for (var i = 0; i < maxCount; i++)
					{
						var rowData = comPriceProductFindByArticle
							.Where(supplierProduct => i < supplierProduct.Value.Count)
							.ToDictionary(supplierProduct => supplierProduct.Key, supplierProduct => supplierProduct.Value[i]);

						var row = resultDataTable.NewRow();
						row["Part"] = article.MainArticle;

						foreach (var supplier in suppliers)
						{
							if (rowData.ContainsKey(supplier.Header))
								AddSupplierDataToRow(row, supplier.Header, rowData[supplier.Header]);
						}

						resultDataTable.Rows.Add(row);
					}
				}
				else
				{
					foreach (var supplierProducts in comPriceProductFindByArticle)
					{
						supplierProducts.Value.ForEach(product =>
						{
							var row = resultDataTable.NewRow();
							row["Part"] = article.MainArticle;
							AddSupplierDataToRow(row, supplierProducts.Key, product);
							resultDataTable.Rows.Add(row);
						});
					}
				}
			}

			return resultDataTable;
		}

		private void AddSupplierColumns(DataTable dataTable, string header)
		{
			dataTable.Columns.Add(string.Format(NameFieldColumnHeaderFormat, header));
			dataTable.Columns.Add(string.Format(BrandFieldColumnHeaderFormat, header));
			dataTable.Columns.Add(string.Format(QuantityFieldColumnHeaderFormat, header));
			dataTable.Columns.Add(string.Format(PriceFieldColumnHeaderFormat, header));
			dataTable.Columns.Add(string.Format(DeliveryDaysFieldColumnHeaderFormat, header));
		}

		private void AddSupplierDataToRow(DataRow row, string supplier, ComPriceProduct supplierProduct)
		{
			row[string.Format(NameFieldColumnHeaderFormat, supplier)] = supplierProduct.Name;
			row[string.Format(BrandFieldColumnHeaderFormat, supplier)] = supplierProduct.Brand;
			row[string.Format(QuantityFieldColumnHeaderFormat, supplier)] = supplierProduct.Quantity;
			row[string.Format(PriceFieldColumnHeaderFormat, supplier)] = Math.Round(supplierProduct.Price, 2).ToString(CultureInfo.InvariantCulture);
			row[string.Format(DeliveryDaysFieldColumnHeaderFormat, supplier)] = supplierProduct.DayCountOnDelivery;
		}

		public DataTable ProcessShortResult(List<ComPriceArticle> articles, List<SupplierBase> suppliers, double markup)
		{
			var resultDataTable = new DataTable();
			resultDataTable.Columns.Add("Part");
			resultDataTable.Columns.Add("Название");
			resultDataTable.Columns.Add("Производитель");
			resultDataTable.Columns.Add("Поставщик");
			resultDataTable.Columns.Add("Кол-во");
			resultDataTable.Columns.Add("Цена");
			resultDataTable.Columns.Add("Доставка (дней)");
			resultDataTable.Columns.Add("Наличие");

			foreach (var article in articles)
			{
				var availability = string.Empty;

				var isAvailabilityIfQuantityProductMoreThanOne = _systemConfigurationService.AvailabilityIfQuantityProductMoreThanOne ? 1 : 0;

				var comPriceProductFindByArticle = suppliers
					.ToDictionary(
						supplier => supplier.Header,
						supplier => supplier.SearchProductResult
							.FindAll(product => article.Equals(product.Article)
							                    && !_checkConditionService.IsGarbageName(product.Name)
							                    && product.Price > 0.0
							                    && product.Quantity > isAvailabilityIfQuantityProductMoreThanOne
							)
					);

				var countArticle = comPriceProductFindByArticle.Select(find => find.Value.Count).ToList();
				var maxCount = countArticle.Max();

				if (maxCount == 0)
				{
					comPriceProductFindByArticle = suppliers
						.ToDictionary(supplier => supplier.Header,
							supplier => supplier.SearchProductResult
								.FindAll(product => article.Equals(product.Article) && !_checkConditionService.IsGarbageName(product.Name) && product.Price > 0.0)
						);

					countArticle = comPriceProductFindByArticle.Select(find => find.Value.Count).ToList();
					maxCount = countArticle.Max();
					if (maxCount == 0)
					{
						comPriceProductFindByArticle = suppliers
							.ToDictionary(supplier => supplier.Header,
								supplier => supplier.SearchProductResult
									.FindAll(product => article.Equals(product.Article) && !_checkConditionService.IsGarbageName(product.Name))
							);


						countArticle = comPriceProductFindByArticle.Select(find => find.Value.Count).ToList();
						maxCount = countArticle.Max();
					}
				}
				else
					availability = "Есть";

				if (maxCount == 0)
				{
					var row = resultDataTable.NewRow();
					row["Part"] = article.MainArticle;
					resultDataTable.Rows.Add(row);
				}
				else if (countArticle.All(count => count == maxCount) || maxCount == 1)
				{
					for (var i = 0; i < maxCount; i++)
					{
						var resultData = new ComPriceProduct();
						var resultKey = string.Empty;

						foreach (var comPriceProducts in comPriceProductFindByArticle.Where(comPriceProducts => i < comPriceProducts.Value.Count))
						{
							if (resultData == null || resultData.IsEmpty())
							{
								resultData = comPriceProducts.Value[i];
								resultKey = comPriceProducts.Key;
							}
							else
							{
								if (resultData.Price > comPriceProducts.Value[i].Price)
								{
									resultData = comPriceProducts.Value[i];
									resultKey = comPriceProducts.Key;
								}
							}
						}

						var name = resultData.Name ?? string.Empty;
						var brand = resultData.Brand ?? string.Empty;
						var price = resultData.Price;
						var dayCountOnDelivery = resultData.DayCountOnDelivery;

						if (markup > 0)
							price *= markup;

						if (_systemConfigurationService.CalculateAtTheRate)
						{
							if (resultData.Currency != null)
								price *= _exchangeRateService.ExchangeRates[resultData.Currency.Value];

							else
								price *= _exchangeRateService.ExchangeRates[Currency.USD];
						}

						resultDataTable.Rows.Add(ComposeShowOptimalResultRow(resultDataTable.NewRow(), article.MainArticle, name, brand, resultKey, resultData.Quantity, resultData.QuantityMessage, price, availability, dayCountOnDelivery));
					}
				}
				else
				{
					foreach (var findProducts in comPriceProductFindByArticle)
					{
						var products = findProducts.Value;
						foreach (var product in products)
						{
							var price = product.Price;
							if (markup > 0)
								price *= markup;

							if (_systemConfigurationService.CalculateAtTheRate)
							{
								if (product.Currency != null)
									price *= _exchangeRateService.ExchangeRates[product.Currency.Value];

								else
									price *= _exchangeRateService.ExchangeRates[Currency.USD];
							}

							resultDataTable.Rows.Add(ComposeShowOptimalResultRow(resultDataTable.NewRow(), product, price, findProducts.Key, string.Empty));
						}
					}
				}
			}

			return resultDataTable;
		}

		private DataRow ComposeShowOptimalResultRow(DataRow row, ComPriceProduct product, double price, string supplier, string availability)
		{
			return ComposeShowOptimalResultRow(row, product.Article, product.Name, product.Brand, supplier, product.Quantity, product.QuantityMessage, price, availability, product.DayCountOnDelivery);
		}

		private DataRow ComposeShowOptimalResultRow(DataRow row, string article, string name, string brand, string supplier, int quantity, string quantityMessage, double price, string availability, int? dayCountOnDelivery)
		{
			row["Part"] = article;
			row["Название"] = name;
			row["Производитель"] = brand;
			row["Поставщик"] = supplier;
			row["Кол-во"] = quantity;
			row["Цена"] = Math.Round(price, 2).ToString(CultureInfo.InvariantCulture);
			row["Доставка (дней)"] = dayCountOnDelivery;
			if (string.IsNullOrEmpty(quantityMessage))
				row["Наличие"] = availability;
			else
				row["Наличие"] = quantityMessage;

			return row;
		}
	}
}