﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using Com.Price.Infrastructure.Services.Download;

namespace Com.Price.Data.Services.Download
{
	public class PriceListDownloadService : IPriceListDownloadService
	{
		private const string ZipExtensions = ".zip";

		public string PriceListPathIfExist(string directoryPath, string fileName, string fileExtension)
		{
			var filePath = Path.Combine(directoryPath, fileName + fileExtension);
			if (!string.IsNullOrEmpty(directoryPath))
			{
				if (File.Exists(filePath))
					return filePath;
			}

			return null;
		}

		public string DownloadPriceList(WebRequest httpWebRequest, string directoryPath, string fileName, string fileExtension, bool isArchive = false, Encoding encoding = null, bool canCleanDirectory = true)
		{
			var filePath = Path.Combine(directoryPath, fileName + fileExtension);
			if (isArchive)
				filePath = Path.Combine(directoryPath, fileName + ZipExtensions);

			if (canCleanDirectory)
			{
				foreach (var file in Directory.GetFiles(directoryPath))
				{
					if (file.EndsWith(fileExtension))
						File.Delete(file);
				}
			}

			if (encoding != null)
				DownloadFile(httpWebRequest, filePath, encoding);
			else
				DownloadFileByChuck(httpWebRequest, filePath);

			if (!isArchive)
				return filePath;

			if (!directoryPath.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal))
				directoryPath += Path.DirectorySeparatorChar;

			using (var archive = ZipFile.OpenRead(filePath))
			{
				foreach (ZipArchiveEntry entry in archive.Entries)
				{
					if (entry.FullName.EndsWith(fileExtension, StringComparison.OrdinalIgnoreCase))
					{
						var destinationPath = Path.Combine(directoryPath, entry.FullName);
						if (destinationPath.StartsWith(directoryPath, StringComparison.Ordinal))
						{
							entry.ExtractToFile(destinationPath);
							var source = Path.GetFullPath(destinationPath);
							var destination = Path.GetFullPath(Path.Combine(directoryPath, fileName + fileExtension));
							File.Move(source, destination);
							break;
						}
					}
				}
			}

			File.Delete(filePath);
			filePath = Path.Combine(directoryPath, fileName + fileExtension);

			if (!File.Exists(filePath))
				throw new FileNotFoundException("Неудалось скачать файл " + fileName + fileExtension);

			return filePath;
		}

		private void DownloadFileByChuck(WebRequest httpWebRequest, string filePath)
		{
			using (var response = httpWebRequest.GetResponse())
			{
				using (var localStream = File.Create(filePath))
				{
					var buffer = new byte[1024];
					int bytesRead;
					do
					{
						bytesRead = response.GetResponseStream().Read(buffer, 0, buffer.Length);
						localStream.Write(buffer, 0, bytesRead);
					} while (bytesRead > 0);

					localStream.Flush();
				}
			}
		}

		private void DownloadFile(WebRequest httpWebRequest, string filePath, Encoding encoding)
		{
			using (var response = httpWebRequest.GetResponse())
			{
				using (var streamReader = new StreamReader(response.GetResponseStream(), encoding))
				{
					var fileContext = streamReader.ReadToEnd();
					File.WriteAllText(filePath, fileContext, encoding);
				}
			}
		}
	}
}