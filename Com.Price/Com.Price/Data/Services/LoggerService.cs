﻿using System;
using Com.Price.Infrastructure.Services;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace Com.Price.Data.Services
{
	public class LoggerService : ILoggerService
	{
		private ILog Log { get; } = LogManager.GetLogger("LOGGER");

		public LoggerService()
		{
			Setup();
			Information("Инициализация логера");
		}

		private void Setup()
		{
			var hierarchy = (Hierarchy) LogManager.GetRepository();

			var patternLayout = new PatternLayout {ConversionPattern = "%d [%thread] %-5level %logger - %message%newline"};
			patternLayout.ActivateOptions();

			var roller = new RollingFileAppender
			{
				AppendToFile = true,
				File = @"Logs\com.price_log_",
				DatePattern = "yyyyMMdd",
				Layout = patternLayout,
				MaxSizeRollBackups = 10,
				MaximumFileSize = "1GB",
				RollingStyle = RollingFileAppender.RollingMode.Date,
				StaticLogFileName = false,
				LockingModel = new FileAppender.MinimalLock(),
				
			};
			roller.ActivateOptions();
			hierarchy.Root.AddAppender(roller);

			var memory = new MemoryAppender();
			memory.ActivateOptions();
			hierarchy.Root.AddAppender(memory);

			hierarchy.Root.Level = Level.Trace;
			hierarchy.Configured = true;
		}

		public void Error(string message)
		{
			Log.Error(message);
		}

		public void ErrorFormat(string format, params object[] args)
		{
			Log.ErrorFormat(format, args);
		}

		public void Error(string message, Exception exception)
		{
			Log.Error(message, exception);
		}

		public void Information(string message)
		{
			Log.Info(message);
		}
	}
}