﻿using Newtonsoft.Json;

namespace Com.Price.Data.Services.Api
{
	public class EleprodProductInfo
	{
		[JsonProperty("id")] public string Id { get; set; }

		[JsonProperty("partnumber")] public string PartNumber { get; set; }

		[JsonProperty("name")] public string Name { get; set; }

		[JsonProperty("brand")] public string Brand { get; set; }

		[JsonProperty("price")] public string Price { get; set; }

		[JsonProperty("currency")] public string Currency { get; set; }

		[JsonProperty("quantity")] public string Quantity { get; set; }

		[JsonProperty("availability")] public string Availability { get; set; }

		public bool IsEmpty()
		{
			return string.IsNullOrEmpty(Id) && string.IsNullOrEmpty(PartNumber) 
				&& string.IsNullOrEmpty(Name) && string.IsNullOrEmpty(Brand) 
				&& string.IsNullOrEmpty(Price) && string.IsNullOrEmpty(Currency) 
				&& string.IsNullOrEmpty(Quantity) && string.IsNullOrEmpty(Availability);
		}
	}
}