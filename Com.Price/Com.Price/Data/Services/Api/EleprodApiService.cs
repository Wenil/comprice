﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.Api;
using Newtonsoft.Json.Linq;

namespace Com.Price.Data.Services.Api
{
	public class EleprodApiService : IEleprodApiService<EleprodProductInfo>
	{
		private const string ApiUrl = "https://www.eleprod.ru/csv/api.php";

		private const string Key = "Nbft4gfd1fDs";

		private readonly ILoggerService _loggerService;

		public EleprodApiService(ILoggerService loggerService)
		{
			_loggerService = loggerService;
		}

		private const string UrlParamsGetProductInfoTemplate = "?key={0}&mode=getinfobrand&brand={1}&partnomer={2}";

		public EleprodProductInfo GetProductInfo(string article, string brand)
		{
			var response = GetResponse(string.Format(UrlParamsGetProductInfoTemplate, Key, WebUtility.UrlEncode(brand), WebUtility.UrlEncode(article)));
			if (!string.IsNullOrEmpty(response))
			{
				var json = JObject.Parse(response);

				if (json["tovars"] is JArray tovars)
				{
					var jsonObject = JObject.Parse(tovars[0].ToString());
					var eleprodProductInfo = jsonObject.ToObject<EleprodProductInfo>();

					return eleprodProductInfo;
				}
			}

			return null;
		}

		private const string UrlParamsUpdateAvailabilityTemplate = "?key={0}&mode=updatenal&id={1}&availability={2}";

		public void UpdateAvailability(EleprodProductInfo productInfo)
		{
			GetResponse(string.Format(UrlParamsUpdateAvailabilityTemplate, Key, productInfo.Id, productInfo.Availability));
		}

		private const string UrlParamsUpdatePriceTemplate = "?key={0}&mode=updateprice&id={1}&price={2}";

		public void UpdatePrice(EleprodProductInfo productInfo)
		{
			GetResponse(string.Format(UrlParamsUpdatePriceTemplate, Key, productInfo.Id, productInfo.Price.Replace(",", ".")));
		}

		private const string UrlParamsUpdateQuantityTemplate = "?key={0}&mode=updatequantity&id={1}&quantity={2}";

		public void UpdateQuantity(EleprodProductInfo productInfo)
		{
			GetResponse(string.Format(UrlParamsUpdateQuantityTemplate, Key, productInfo.Id, productInfo.Quantity));
		}

		private const string UrlParamsUpdateDeliveryDayTemplate = "?key={0}&mode=updatesrok&id={1}&srok={2}";

		public void UpdateDeliveryDay(EleprodProductInfo productInfo, int? days)
		{
			GetResponse(string.Format(UrlParamsUpdateDeliveryDayTemplate, Key, productInfo.Id, days));
		}

		private string GetResponse(string url)
		{
			string responseResult = null;

			try
			{
				var request = (HttpWebRequest)WebRequest.Create(ApiUrl + url);
				request.Method = WebRequestMethods.Http.Get;
				request.UserAgent = @"Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0";
				request.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
				request.KeepAlive = true;
				request.AllowAutoRedirect = false;

				var response = (HttpWebResponse) request.GetResponse();
				using (var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
				{
					responseResult = streamReader.ReadToEnd();
					if (string.IsNullOrEmpty(responseResult))
						_loggerService.Information("Запрос вернул пустой ответ. Параметры запроса	API : " + url);

				}

				response.Close();

				return responseResult;
			}
			catch (Exception e)
			{
				_loggerService.Error($"Ошибка при выполнении запроса {url}", e);
			}

			return null;
		}
	}
}