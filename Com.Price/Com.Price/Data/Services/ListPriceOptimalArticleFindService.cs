﻿using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.SystemConfiguration;
using System.Collections.Generic;
using System.Linq;
using Com.Price.Data.Suppliers.Enums;
using Com.Price.Data.Suppliers.Structs;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.Exchange.Data;
using System;

namespace Com.Price.Data.Services
{
    public class ListPriceOptimalArticleFindService : IListPriceOptimalArticleFindService
    {
        private readonly ISystemConfigurationService _systemConfigurationService;
        private readonly ICheckConditionService _checkConditionService;

        private readonly List<Availability> _availabilities = new List<Availability> { Availability.InStock, Availability.UnderOrder, Availability.Specify };

        public ListPriceOptimalArticleFindService(ISystemConfigurationService systemConfigurationService, ICheckConditionService checkConditionService)
        {
            _systemConfigurationService = systemConfigurationService;
            _checkConditionService = checkConditionService;
        }

        /// <summary>
        /// Поиск оптимального товара.
        /// </summary>
        /// <param name="article">Артикул товара.</param>
        /// <param name="suppliers">Товары полученные от поставщиков.</param>
        /// <param name="markupRange">Наценки.</param>
        /// <param name="checkAvailability">Нужно проверять наличие.</param>
        /// <param name="checkPrice">Нужно проверять цену.</param>
        /// <returns>Возвращает найденный товар иначе пустой словарь.</returns>
        public ComPriceProduct FindOptimalArticleCommon(string article, List<SupplierBase> suppliers, List<MarkupRange> markupRange, bool checkAvailability = true, bool checkPrice = true)
        {
            var optimalArticle = new ComPriceProduct();
            var availability = Availability.UnderOrder;

            var findsArticle = FirstStepSelectRightProductByPartners(article, suppliers, checkAvailability, checkPrice);
            var maxCount = findsArticle.Select(find => find.Value.Count).Max();
            if (maxCount == 0)
            {
                findsArticle = SecondStepSelectRightProductByPartners(article, suppliers, checkAvailability, checkPrice);
                maxCount = findsArticle.Select(find => find.Value.Count).Max();
                if (maxCount == 0)
                {
                    findsArticle = ThirdStepSelectRightProductByPartners(article, suppliers, checkPrice);
                    maxCount = findsArticle.Select(find => find.Value.Count).Max();
                }
            }
            else
                availability = Availability.InStock;


            if (findsArticle.Any() && maxCount == 1)
                optimalArticle = ComposeFindOptimalArticle(findsArticle, markupRange, article, availability);

            return optimalArticle;
        }

        /// <summary>
        /// Выбор подходящих товаров у поставщиков(первая проверка).
        /// </summary>
        /// <param name="article">Артикул товара.</param>
        /// <param name="suppliers">Товары полученные от поставщиков.</param>
        /// <param name="checkAvailability">Проверка наличия.</param>
        /// <param name="checkPrice">Проверка цены.</param>
        /// <returns></returns>
        private Dictionary<string, List<ComPriceProduct>> FirstStepSelectRightProductByPartners(string article, IEnumerable<SupplierBase> suppliers, bool checkAvailability, bool checkPrice)
        {
            var isAvailabilityIfQuantityProductMoreThanOne = _systemConfigurationService.AvailabilityIfQuantityProductMoreThanOne ? 1 : 0;

            return suppliers.ToDictionary(
                p => p.Name,
                p => p.SearchProductResult.FindAll(find => IsConditionFirstStepSelectRightProductByPartners(find, isAvailabilityIfQuantityProductMoreThanOne, article, checkAvailability, checkPrice))
            );
        }

        private bool IsConditionFirstStepSelectRightProductByPartners(ComPriceProduct find, int isAvailabilityIfQuantityProductMoreThanOne, string article, bool checkAvailability, bool checkPrice)
        {
            var isRightAvailability = !checkAvailability || (find.Quantity > isAvailabilityIfQuantityProductMoreThanOne && string.IsNullOrEmpty(find.QuantityMessage));

            return IsRightArticleAndRightName(article, find.Article, find.Name) && IsRightPrice(find.Price, checkPrice) && isRightAvailability;
        }

        /// <summary>
        /// Выбор подходящих товаров у поставщиков(вторая проверка).
        /// </summary>
        /// <param name="article">Артикул товара.</param>
        /// <param name="suppliers">Товары полученные от поставщиков.</param>
        /// <param name="checkAvailability">Проверка наличия.</param>
        /// <param name="checkPrice">Проверка цены.</param>
        /// <returns></returns>
        private Dictionary<string, List<ComPriceProduct>> SecondStepSelectRightProductByPartners(string article, IEnumerable<SupplierBase> suppliers, bool checkAvailability, bool checkPrice)
        {
            return suppliers.ToDictionary(
                p => p.Name,
                p => p.SearchProductResult.FindAll(find => IsConditionSecondStepSelectRightProductByPartners(find, article, checkAvailability, checkPrice))
            );
        }

        private bool IsConditionSecondStepSelectRightProductByPartners(ComPriceProduct find, string article, bool checkAvailability, bool checkPrice)
        {
            var isRightAvailability = !checkAvailability || (find.Quantity == 1 && string.IsNullOrEmpty(find.QuantityMessage));

            return IsRightArticleAndRightName(article, find.Article, find.Name) && IsRightPrice(find.Price, checkPrice) && isRightAvailability;
        }

        /// <summary>
        /// Выбор подходящих товаров у поставщиков(третья проверка).
        /// </summary>
        /// <param name="article">Артикул товара.</param>
        /// <param name="suppliers">Товары полученные от поставщиков.</param>
        /// <param name="checkPrice">Проверка цены.</param>
        /// <returns></returns>
        private Dictionary<string, List<ComPriceProduct>> ThirdStepSelectRightProductByPartners(string article, IEnumerable<SupplierBase> suppliers, bool checkPrice)
        {
            return suppliers.ToDictionary(
                p => p.Name,
                p => p.SearchProductResult
                    .FindAll(find => IsRightArticleAndRightName(article, find.Article, find.Name) && IsRightPrice(find.Price, checkPrice))
            );
        }

        /// <summary>
        /// Проверка корректности найденного парт номера и названия товара.
        /// </summary>
        /// <param name="targetArticle">Искомый парт номер.</param>
        /// <param name="foundArticle">Найденный парт номер.</param>
        /// <param name="name">Название товара.</param>
        /// <returns>Вернёт true, если парт номера совпадают и название корректно.</returns>
        private bool IsRightArticleAndRightName(string targetArticle, string foundArticle, string name) =>
            foundArticle.Equals(targetArticle) && !_checkConditionService.IsGarbageName(name);

        /// <summary>
        /// Проверка корректности найденной цены.
        /// </summary>
        /// <param name="price">Найденная цена.</param>
        /// <param name="isCheckPrice">Нужно проверять цену.</param>
        /// <returns>Вернёт true, если цена больше 0 иначе false.</returns>
        private bool IsRightPrice(double price, bool isCheckPrice) => !isCheckPrice || price > 0.0;

        /// <summary>
        /// Обработка найденных значений.
        /// </summary>
        /// <param name="findProducts">Найденные товары.</param>
        /// <param name="markupRange">Наценки.</param>
        /// <param name="searchArticle">Искомый артикул.</param>
        /// <param name="availability">Наличие.</param>
        /// <returns>Возвращает оптимальный товар из найденных.</returns>
        private ComPriceProduct ComposeFindOptimalArticle(Dictionary<string, List<ComPriceProduct>> findProducts,
            IReadOnlyList<MarkupRange> markupRange, string searchArticle, Availability availability)
        {
            var optimal = new ComPriceProduct();
            var resultData = new ComPriceProduct();
            var resultKey = string.Empty;

            var findProductsFiltered = new List<KeyValuePair<string, List<ComPriceProduct>>>();
            foreach (var availabilityFilter in _availabilities)
            {
                var findProductsWithAvailabilityFilter = findProducts.Where(findProduct => findProduct.Value.Any(product => availabilityFilter.Equals(product.Availability))).ToList();
                if (findProductsWithAvailabilityFilter.Any())
                {
                    for (int i = 0; i < findProductsWithAvailabilityFilter.Count(); i++)
                    {
                        var newValue = findProductsWithAvailabilityFilter[i].Value.Where(product => availabilityFilter.Equals(product.Availability)).ToList();
                        findProductsWithAvailabilityFilter[i] = new KeyValuePair<string, List<ComPriceProduct>>(findProductsWithAvailabilityFilter[i].Key, newValue);
                    }

                    findProductsFiltered.AddRange(findProductsWithAvailabilityFilter);
                    break;
                }
            }

            if (!findProductsFiltered.Any())            
                findProductsFiltered.AddRange(findProducts.Where(findProduct => findProduct.Value.Any()).ToList());            

            foreach (var findProduct in findProductsFiltered)
            {
                if (resultData == null || resultData.IsEmpty())
                {
                    resultData = findProduct.Value[0];
                    resultKey = findProduct.Key;
                }
                else
                {
                    var currentPrice = resultData.Price;
                    var nextPrice = findProduct.Value[0].Price;

                    if (!(currentPrice > nextPrice))
                        continue;

                    resultData = findProduct.Value[0];
                    resultKey = findProduct.Key;
                }
            }

            return CreateOptimalComPriceProduct(resultData, searchArticle, resultKey, markupRange, availability);
        }

        private ComPriceProduct CreateOptimalComPriceProduct(ComPriceProduct searchResult, string searchArticle, string resultKey, IReadOnlyList<MarkupRange> markupRange, Availability availability)
        {
            var optimal = new ComPriceProduct
            {
                Article = searchArticle,
                Name = searchResult.Name ?? string.Empty,
                Brand = searchResult.Brand ?? string.Empty,
                ResultKey = resultKey,
                Quantity = searchResult.Quantity,
                Currency = searchResult.Currency
            };
            var price = MarkupCalculation(searchResult.Price, markupRange);
            if (Currency.RUB.Equals(optimal.Currency) && price > 0.0)
                price = Math.Ceiling(price);

            optimal.Price = price;
            optimal.Availability = availability;
            optimal.QuantityMessage = searchResult.QuantityMessage;
            optimal.DayCountOnDelivery = searchResult.DayCountOnDelivery;

            return optimal;
        }

        /// <summary>
        /// Рассчет цены с наценкой.
        /// </summary>
        /// <param name="value">Цена.</param>
        /// <param name="markupRange">Наценки.</param>
        /// <returns>Цена с добавленной наценкой иначе цена вернётся без изменений.</returns>
        private double MarkupCalculation(double value, IReadOnlyList<MarkupRange> markupRange)
        {
            if (!markupRange.Any())
                return value;

            var markup = 1.0;
            if (markupRange.Count == 1)
            {
                if (markupRange[0].Markup > 0)
                    markup = markupRange[0].Markup;
            }
            else
            {
                foreach (var markupRangeItem in markupRange)
                {
                    if (markupRangeItem.RangeStart <= value && markupRangeItem.RangeEnd > value)
                    {
                        if (markupRangeItem.Markup > 0)
                            markup = markupRangeItem.Markup;

                        break;
                    }
                }
            }

            if (value > 0.0)
                value *= markup;

            return value;
        }

        /// <summary>
        /// Поиск оптимального товара опираясь на присутствие цены.
        /// </summary>
        /// <param name="article">Артикул товара.</param>
        /// <param name="suppliers">Товары полученные от поставщиков.</param>
        /// <param name="markupRange">Наценки.</param>
        /// <returns>Возвращает найденный товар иначе пустой словарь.</returns>
        public ComPriceProduct FindOptimalArticleByPrice(string article, List<SupplierBase> suppliers, List<MarkupRange> markupRange) =>
            FindOptimalArticleCommon(article, suppliers, markupRange, false);

        /// <summary>
        /// Поиск оптимального товара опираясь на присутствие наличия.
        /// </summary>
        /// <param name="article">Артикул товара.</param>
        /// <param name="suppliers">Товары полученные от поставщиков.</param>
        /// <param name="markupRange">Наценки.</param>
        /// <returns>Возвращает найденный товар иначе пустой словарь.</returns>
        public ComPriceProduct FindOptimalArticleByQuantity(string article, List<SupplierBase> suppliers, List<MarkupRange> markupRange) =>
            FindOptimalArticleCommon(article, suppliers, markupRange, true, false);
    }
}