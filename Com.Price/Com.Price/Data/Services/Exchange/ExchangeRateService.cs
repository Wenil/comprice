﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using Com.Price.Data.Extensions;
using Com.Price.Data.Services.Exchange.Data;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.Exchange;
using Com.Price.Infrastructure.Services.Exchange.Data;

namespace Com.Price.Data.Services.Exchange
{
	public class ExchangeRateService : IExchangeRateService
	{
		private readonly ILoggerService _loggerService;

		public ExchangeRateService(ILoggerService loggerService)
		{
			_loggerService = loggerService;
		}

		public Dictionary<Currency, double> ExchangeRates { get; set; }

		public Dictionary<Currency, double> ExchangeRatesForTomorrow { get; set; }

		public Dictionary<Currency, double> LoadExchangeRates(int numberOfAttempts = 3)
		{
			var retVal = new Dictionary<Currency, double>();

			while (numberOfAttempts > 0 && !retVal.Any())
			{
				try
				{
					var currencyRates = GetExchangeRates();
					retVal.Add(Currency.USD, currencyRates.First(rate => rate.CurrencyStringCode == "USD").ExchangeRate);
					retVal.Add(Currency.EUR, currencyRates.First(rate => rate.CurrencyStringCode == "EUR").ExchangeRate);
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					_loggerService.ErrorFormat("Ошибка при получении курса валют.! \n\r {0} \n\r {1}", e.Message, e.StackTrace);
					Thread.Sleep(5000);
				}
				finally
				{
					--numberOfAttempts;
				}
			}

			return retVal;
		}

		/// <summary>
		/// Получить список котировок ЦБ ФР на данный момент.
		/// </summary>
		/// 
		/// <param name="date">Дата запроса.</param>
		/// <returns>Список котировок ЦБ РФ.</returns>
		private List<CurrencyRate> GetExchangeRates(DateTime date)
		{
			var xs = new XmlSerializer(typeof(ValCurs));
			var xr = new XmlTextReader(@"http://www.cbr.ru/scripts/XML_daily.asp" + $"?date_req={date:dd/MM/yyyy}");
			return ((ValCurs) xs
					.Deserialize(xr))
				.ValuteList
				.Select(
					valute => new CurrencyRate()
					{
						CurrencyName = valute.ValuteName,
						CurrencyStringCode = valute.ValuteStringCode,
						ExchangeRate = valute.ExchangeRate.ToDouble()
					}
				)
				.ToList();
		}

		/// <summary>
		/// Получить список котировок ЦБ ФР на данный момент.
		/// </summary>
		/// <returns>Список котировок ЦБ РФ.</returns>
		private List<CurrencyRate> GetExchangeRates()
		{
			return GetExchangeRates(DateTime.Now);
		}
	}
}