﻿namespace Com.Price.Data.Services.Exchange.Data
{
	/// <summary>
	/// 
	/// </summary>
	public class CurrencyRate
	{
		/// <summary>
		/// Закодированное строковое обозначение валюты
		/// Например: USD, EUR, AUD и т.д.
		/// </summary>
		public string CurrencyStringCode { get; set; }

		/// <summary>
		/// Наименование валюты
		/// Например: Доллар, Евро и т.д.
		/// </summary>
		public string CurrencyName { get; set; }

		/// <summary>
		/// Обменный курс
		/// </summary>
		public double ExchangeRate { get; set; }
	}
}