﻿using System.Xml.Serialization;

namespace Com.Price.Data.Services.Exchange.Data
{
	public class ValCurs
	{
		[XmlElement("Valute")] public ValCursValute[] ValuteList;
	}
}