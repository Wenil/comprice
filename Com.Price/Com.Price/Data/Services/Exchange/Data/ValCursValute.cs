﻿using System.Xml.Serialization;

namespace Com.Price.Data.Services.Exchange.Data
{
	/// <summary>
	/// 
	/// </summary>
	public class ValCursValute
	{
		/// <summary>
		/// 
		/// </summary>
		[XmlElement("CharCode")] public string ValuteStringCode;

		/// <summary>
		/// 
		/// </summary>
		[XmlElement("Name")] public string ValuteName;

		/// <summary>
		/// 
		/// </summary>
		[XmlElement("Value")] public string ExchangeRate;
	}
}