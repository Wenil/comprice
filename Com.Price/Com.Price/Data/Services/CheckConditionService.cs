﻿using System.Linq;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.SystemConfiguration;

namespace Com.Price.Data.Services
{
	public class CheckConditionService : ICheckConditionService
	{
		private readonly ISystemConfigurationService _systemConfigurationService;

		public CheckConditionService(ISystemConfigurationService systemConfigurationService)
		{
			_systemConfigurationService = systemConfigurationService;
		}

		private static readonly string[] WellKnownQuantity =
		{
			"На заказ", "пустое поле", "0",
			"0*", "нет", "пустое поле",
			"Резерв", "call", "звонить",
			"-", "Уточнить", "звоните", "Уточняйте", "Заказ", "заказ",
			"Под заказ 3-10 дней", "Под заказ до 10 дней"
		};

		public bool IsGarbageName(string productName)
		{
			if (_systemConfigurationService.DisregardSubstandardGoods)
			{
				if (string.IsNullOrEmpty(productName))
					return true;

				return _systemConfigurationService.Garbages
					.Any(garbage => productName.ToUpper().Contains(garbage.ToUpper()));
			}

			return false;
		}

		public bool IsEmptyQuantity(string quantity)
		{
			if (string.IsNullOrEmpty(quantity))
				return true;

			return WellKnownQuantity.Any(quantity.Equals);
		}
	}
}