﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Catel.IoC;
using Catel.MVVM;
using Catel.Services;
using Com.Price.Infrastructure.Services.TaskManager;
using Com.Price.Infrastructure.Services.TaskManager.Model;
using Com.Price.Infrastructure.Suppliers;

namespace Com.Price.Data.TaskManager
{
	public class TaskManagerViewModel : ViewModelBase
	{
		private readonly IComPriceTaskManagerService _comPriceTaskManagerService;
		private readonly IUIVisualizerService _uiVisualizerService;
		private readonly ISupplierLoaderService _supplierLoaderService;

		public TaskManagerViewModel(IComPriceTaskManagerService comPriceTaskManagerService, IUIVisualizerService uiVisualizerService, ISupplierLoaderService supplierLoaderService)
		{
			_comPriceTaskManagerService = comPriceTaskManagerService;
			_uiVisualizerService = uiVisualizerService;
			_supplierLoaderService = supplierLoaderService;

			_comPriceTaskManagerService.ComPriceTaskCollectionChanged += ComPriceTaskManagerServiceOnComPriceTasksCollectionChanged;

			StartTaskManagerCommand = new Command(StartTaskManager);
			AddComPriceTaskCommand = new TaskCommand(AddComPriceTask, () => !IsTaskRunning);
			RemoveComPriceTaskCommand = new Command(RemoveComPriceTask, () => !IsTaskRunning);
			OpenComPriceTaskEditorCommand = new TaskCommand(OpenComPriceTaskEditor, () => !IsTaskRunning);
		}

		private void ComPriceTaskManagerServiceOnComPriceTasksCollectionChanged(object sender, ComPriceTaskCollectionChangedEventArgs eventArgs)
		{
			switch (eventArgs.Action)
			{
				case ComPriceTaskCollectionChangedAction.Refresh:
					ComPriceTasks = new ObservableCollection<ListPriceTask>(_comPriceTaskManagerService.ComPriceTasks);
					break;
				case ComPriceTaskCollectionChangedAction.Add:
					ComPriceTasks.Add(eventArgs.Task);
					break;
				case ComPriceTaskCollectionChangedAction.Remove:
					ComPriceTasks.Remove(eventArgs.Task);
					break;
				case ComPriceTaskCollectionChangedAction.Update:
					for (var index = 0; index < ComPriceTasks.Count; index++)
					{
						if (ComPriceTasks[index].Id == eventArgs.Task.Id)
						{
							ComPriceTasks[index] = eventArgs.Task;
							break;
						}
					}

					break;
				default:
					break;
			}
		}

		public ObservableCollection<ListPriceTask> ComPriceTasks { get; set; } = new ObservableCollection<ListPriceTask>();

		public ListPriceTask ComPriceTaskSelected { get; set; }

		public bool IsTaskRunning { get; set; }

		public ICommand StartTaskManagerCommand { get; }

		private void StartTaskManager()
		{
			IsTaskRunning = !IsTaskRunning;
			if (IsTaskRunning)
			{
				_comPriceTaskManagerService.StartTaskManager();
			}
			else
			{
				_comPriceTaskManagerService.StopTaskManager();
			}
		}

		public ICommand AddComPriceTaskCommand { get; }

		public async Task AddComPriceTask()
		{
			await OpenAddComPriceTaskEditorWindow();
		}

		public ICommand RemoveComPriceTaskCommand { get; }

		public void RemoveComPriceTask()
		{
			if (ComPriceTaskSelected != null)
				_comPriceTaskManagerService.RemoveComPriceTask(ComPriceTaskSelected);
		}

		public ICommand OpenComPriceTaskEditorCommand { get; }

		public async Task OpenComPriceTaskEditor()
		{
			await OpenUpdateComPriceTaskEditorWindow();
		}

		private async Task OpenAddComPriceTaskEditorWindow()
		{
			var comPriceTask = new ListPriceTask();

			var typeFactory = this.GetTypeFactory();
			var comPriceTaskViewModel = typeFactory.CreateInstanceWithParametersAndAutoCompletion<ComPriceTaskViewModel>(_supplierLoaderService, comPriceTask);
			if (await _uiVisualizerService.ShowDialogAsync(comPriceTaskViewModel) ?? false)
			{
				_comPriceTaskManagerService.AddComPriceTask(comPriceTaskViewModel.ComPriceTask);
				ComPriceTaskSelected = comPriceTaskViewModel.ComPriceTask;
			}
		}

		private async Task OpenUpdateComPriceTaskEditorWindow()
		{
			var comPriceTask = ComPriceTaskSelected;

			var typeFactory = this.GetTypeFactory();
			var comPriceTaskViewModel = typeFactory.CreateInstanceWithParametersAndAutoCompletion<ComPriceTaskViewModel>(_supplierLoaderService, comPriceTask);
			if (await _uiVisualizerService.ShowDialogAsync(comPriceTaskViewModel) ?? false)
			{
				_comPriceTaskManagerService.UpdateComPriceTask(comPriceTaskViewModel.ComPriceTask);
				ComPriceTaskSelected = comPriceTaskViewModel.ComPriceTask;
			}
		}
	}
}