﻿using System.ComponentModel;

namespace Com.Price.Data.TaskManager.Enums
{
	public enum ListPriceTaskExecuterReturnValues
	{
		[Description(@"Обновление выполнено успешно")]
		AllDone,

		[Description(@"В указанной директории отсутствуют файлы")]
		DirectoryIsEmpty,

		[Description(@"Обновление выполнено успешно")]
		TaskParametersIsEmpty,

		[Description(@"Не удалось получить параметры задачи")]
		CanNotUpdateFiles,

		[Description(@"Выполнение прервано пользователем")]
		UserTerminateExecute,

		[Description(@"Не удалось получить значение курс")]
		CouldNotGetRateValue
	}
}