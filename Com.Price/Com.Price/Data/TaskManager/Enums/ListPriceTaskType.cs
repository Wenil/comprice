﻿namespace Com.Price.Data.TaskManager.Enums
{
	public enum ListPriceTaskType
	{
		[System.ComponentModel.Description("Наличие")]
		AvailabilityUpdate,

		[System.ComponentModel.Description("Цена")]
		CostUpdate,

		[System.ComponentModel.Description("Наличие + Цена")]
		AvailabilityAndCostUpdate
	}
}