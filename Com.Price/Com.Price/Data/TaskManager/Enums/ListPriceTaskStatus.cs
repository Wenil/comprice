﻿using System.ComponentModel;

namespace Com.Price.Data.TaskManager.Enums
{
	public enum ListPriceTaskStatus
	{
		[Description("Ожидает выполнения")] Awaiting,
		[Description("Выполняется")] Running,
		[Description("Выполнена")] Done
	}
}