﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using Catel.MVVM;
using Com.Price.Data.Extensions;
using Com.Price.Data.Search;
using Com.Price.Data.TaskManager.Enums;
using Com.Price.Infrastructure.Services.TaskManager.Model;
using Com.Price.Infrastructure.Suppliers;

namespace Com.Price.Data.TaskManager
{
	public class ComPriceTaskViewModel : ViewModelBase
	{
		public ComPriceTaskViewModel(ISupplierLoaderService supplierLoaderService, ListPriceTask comPriceTask)
		{
			SupplierCheckBoxItems = ComposeSupplierCheckBoxes(supplierLoaderService.Suppliers);

			foreach (var supplier in SupplierCheckBoxItems)
			{
				if (comPriceTask.Partners.Contains(supplier.Name))
					supplier.Checked = true;
			}

			comPriceTask.MarkupRange?.ForEach(range => PriceRange.Add(range));

			OpenFileSaveDialogCommand = new Command(OpenFileSaveDialog);

			AddPriceRangeCommand = new Command(AddPriceRange);
			RemovePriceRangeCommand = new Command(RemovePriceRange);

			SaveComPriceTaskCommand = new Command(SaveComPriceTask);

			ComPriceTask = comPriceTask.Clone();
		}

		private ObservableCollection<SupplierCheckBoxItem> ComposeSupplierCheckBoxes(Dictionary<string, Type> suppliers)
		{
			return new ObservableCollection<SupplierCheckBoxItem>(suppliers
				.Select(supplier => new SupplierCheckBoxItem {Name = supplier.Key, Checked = false})
				.ToArray()
			);
		}

		public ListPriceTask ComPriceTask { get; }

		public string Name
		{
			get => ComPriceTask.Name;
			set => ComPriceTask.Name = value;
		}

		public ListPriceTaskType Type
		{
			get => ComPriceTask.Type;
			set => ComPriceTask.Type = value;
		}

		public DateTime StartDate
		{
			get => ComPriceTask.StartDate;
			set => ComPriceTask.StartDate = value;
		}

		public string DirectoryPath
		{
			get => ComPriceTask.DirectoryPath;
			set => ComPriceTask.DirectoryPath = value;
		}

		public ICommand OpenFileSaveDialogCommand { get; }

		private void OpenFileSaveDialog()
		{
			using var dialog = new FolderBrowserDialog();

			if (!string.IsNullOrEmpty(DirectoryPath))
				dialog.SelectedPath = DirectoryPath;

			if (dialog.ShowDialog() == DialogResult.OK)
				DirectoryPath = dialog.SelectedPath;
		}

		public ObservableCollection<SupplierCheckBoxItem> SupplierCheckBoxItems { get; set; }

		public string IntervalBetweenStarts
		{
			get => ComPriceTask.IntervalBetweenStarts.ToString(CultureInfo.InvariantCulture);
			set => ComPriceTask.IntervalBetweenStarts = value.ToInt();
		}

		public string Markup
		{
			get => ComPriceTask.Markup.ToString(CultureInfo.InvariantCulture);
			set => ComPriceTask.Markup = value.ToDouble();
		}

		public ObservableCollection<MarkupRangeModel> PriceRange { get; set; } = new ObservableCollection<MarkupRangeModel>();

		public ICommand AddPriceRangeCommand { get; }

		private void AddPriceRange()
		{
			PriceRange.Add(new MarkupRangeModel());
		}

		public ICommand RemovePriceRangeCommand { get; }

		private void RemovePriceRange()
		{
			if (MarkupRangeItemIndexSelected >= 0 && PriceRange.Count > 0)
				PriceRange.RemoveAt(MarkupRangeItemIndexSelected);

			if (PriceRange.Count <= 0)
				IsUseMarkupRange = false;
		}

		public int MarkupRangeItemIndexSelected { get; set; }

		public bool IsUseMarkupRange
		{
			get => ComPriceTask.IsUseMarkupRange;
			set => ComPriceTask.IsUseMarkupRange = value;
		}

		public ICommand SaveComPriceTaskCommand { get; set; }

		private void SaveComPriceTask()
		{
			if (ComPriceTask.Id == Guid.Empty)
			{
				ComPriceTask.Id = Guid.NewGuid();
				ComPriceTask.Status = ListPriceTaskStatus.Awaiting;
			}

			var partnerList = (from supplier in SupplierCheckBoxItems where supplier.Checked select supplier.Name).ToList();
			ComPriceTask.Partners = partnerList;
			ComPriceTask.MarkupRange = PriceRange.ToList();

			this.SaveAndCloseViewModelAsync();
		}
	}
}