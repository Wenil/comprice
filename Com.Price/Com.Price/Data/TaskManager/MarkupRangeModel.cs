﻿using System;
using PropertyChanged;

namespace Com.Price.Data.TaskManager
{
	[Serializable]
	[AddINotifyPropertyChangedInterface]
	public class MarkupRangeModel
	{
		public string RangeStart { get; set; }

		public string RangeEnd { get; set; }

		public string Markup { get; set; }

		public bool IsEmpty()
		{
			return string.IsNullOrEmpty(RangeStart) && string.IsNullOrEmpty(RangeEnd) && string.IsNullOrEmpty(Markup);
		}
	}
}