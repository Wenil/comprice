﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Catel;
using Catel.MVVM;
using Com.Price.Data.Article;
using Com.Price.Data.Extensions;
using Com.Price.Data.Suppliers;
using Com.Price.Data.Suppliers.Enums;
using Com.Price.Infrastructure.Services.Search;
using Com.Price.Infrastructure.Services.SystemConfiguration;
using Com.Price.Infrastructure.Suppliers;

namespace Com.Price.Data.Search
{
	public class SearchViewModel : ViewModelBase
	{
		private readonly ISupplierLoaderService _supplierLoaderService;
		private readonly ISystemConfigurationService _systemConfigurationService;
		private readonly ISearchViewDataViewFillerService<ComPriceArticle> _searchViewDataViewFillerService;

		public SearchViewModel(ISupplierLoaderService supplierLoaderService, ISystemConfigurationService systemConfigurationService,
			ISearchViewDataViewFillerService<ComPriceArticle> searchViewDataViewFillerService)
		{
			Argument.IsNotNull(() => supplierLoaderService);
			Argument.IsNotNull(() => systemConfigurationService);
			Argument.IsNotNull(() => searchViewDataViewFillerService);

			_supplierLoaderService = supplierLoaderService;
			_systemConfigurationService = systemConfigurationService;
			_searchViewDataViewFillerService = searchViewDataViewFillerService;

			SupplierCheckBoxItems = ComposeSupplierCheckBoxes(_supplierLoaderService.Suppliers);

			SearchProductCommand = new Command(SearchProductAsync, () => !IsBusy);
			PasteArticleCommand = new Command<string>(PasteArticle);
			ClearArticlesCommand = new Command(ClearArticles, () => !IsBusy);
			RefreshResultViewCommand = new Command(RefreshResultView, () => !IsBusy);
		}

		public ObservableCollection<SupplierCheckBoxItem> SupplierCheckBoxItems { get; set; }

		public ObservableCollection<ArticleDataGridItem> ArticleDataGridItems { get; set; } = new ObservableCollection<ArticleDataGridItem> {new ArticleDataGridItem()};

		public bool IsBusy { get; set; }

		public bool IsDisplayProductWithLowestPrice
		{
			get => _systemConfigurationService.DisplayProductWithLowestPrice;
			set => _systemConfigurationService.DisplayProductWithLowestPrice = value;
		}

		public bool IsCalculateAtTheRate
		{
			get => _systemConfigurationService.CalculateAtTheRate;
			set => _systemConfigurationService.CalculateAtTheRate = value;
		}

		public string ResultLabelMessage { get; set; } = "Результаты";
		
		public DataView ResultView { get; set; } = new DataView();

		private List<SupplierBase> _suppliers;

		/// <summary>
		/// Команда поиска товаров.
		/// </summary>
		public ICommand SearchProductCommand { get; }

		public string Markup { get; set; }

		/// <summary>
		/// Поиск товаров.
		/// </summary>
		private async void SearchProductAsync()
		{
			IsBusy = true;

			var selectSuppliers = SupplierCheckBoxItems
				.Where(item => item.Checked)
				.Select(item => item.Name)
				.ToList();

			_suppliers = _supplierLoaderService.CreateInstanceSuppliers(selectSuppliers);
			var comPriceArticles = ArticleDataGridItems.Select(item => new ComPriceArticle(item.Article)).ToList();
			if (_suppliers.Any() && comPriceArticles.Any())
			{
				var searchType = _systemConfigurationService.UseAutomaticSearchInsteadOfManual ? SearchType.Automatic : SearchType.Manual;
				var tasks = _suppliers
					.Select(supplier => supplier.SearchProductAsync(comPriceArticles, searchType, CancellationToken.None))
					.ToArray();

				await Task.WhenAll(tasks);

				ShowResultView(comPriceArticles);

				ResultLabelMessage = $"Результаты : {comPriceArticles.Count}/{ResultView.Count}";
			}

			IsBusy = false;
		}

		private void ShowFullResult(List<ComPriceArticle> articles)
		{
			ResultView = _searchViewDataViewFillerService.ProcessFullResult(articles, _suppliers).DefaultView;
		}

		private void ShowOptimalResult(List<ComPriceArticle> articles)
		{
			ResultView = _searchViewDataViewFillerService.ProcessShortResult(articles, _suppliers, Markup.ToDouble()).DefaultView;
		}

		public Command<string> PasteArticleCommand { get; }

		private void PasteArticle(string articles)
		{
			try
			{
				var lines = articles
					.Replace("\r\n", "\n")
					.Split('\n');

				ArticleDataGridItems = new ObservableCollection<ArticleDataGridItem>();
				foreach (var line in lines)
				{
					if (!string.IsNullOrEmpty(line))
						ArticleDataGridItems.Add(new ArticleDataGridItem {Article = line.Trim()});
				}
			}
			catch (FormatException)
			{
				MessageBox.Show(@"Неудалось вставить");
			}
		}

		public ICommand ClearArticlesCommand { get; }

		public void ClearArticles()
		{
			ArticleDataGridItems?.Clear();
			ArticleDataGridItems?.Add(new ArticleDataGridItem());
		}

		private ObservableCollection<SupplierCheckBoxItem> ComposeSupplierCheckBoxes(Dictionary<string, Type> suppliers)
		{
			return new ObservableCollection<SupplierCheckBoxItem>(suppliers
				.Select(supplier => new SupplierCheckBoxItem {Name = supplier.Key, Checked = false})
				.ToArray()
			);
		}

		public ICommand RefreshResultViewCommand { get; }

		private void RefreshResultView()
		{
			var comPriceArticles = ArticleDataGridItems.Select(item => new ComPriceArticle(item.Article)).ToList();
			ShowResultView(comPriceArticles);
		}

		private void ShowResultView(List<ComPriceArticle> articles)
		{
			if (!_systemConfigurationService.DisplayProductWithLowestPrice)
				ShowFullResult(articles);
			else
				ShowOptimalResult(articles);
		}
	}
}