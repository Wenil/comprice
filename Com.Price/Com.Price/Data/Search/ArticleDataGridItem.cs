﻿using System;
using PropertyChanged;

namespace Com.Price.Data.Search
{
	[Serializable]
	[AddINotifyPropertyChangedInterface]
	public class ArticleDataGridItem
	{
		public string Article { get; set; }
	}
}