﻿ using System;
using PropertyChanged;

namespace Com.Price.Data.Search
{
	[Serializable]
	[AddINotifyPropertyChangedInterface]
	public class SupplierCheckBoxItem
	{
		public string Name { get; set; }

		public bool Checked { get; set; }
	}
}