﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Com.Price.Data.Suppliers;

namespace Com.Price.Infrastructure.Suppliers
{
	public interface ISupplierLoaderService
	{
		public Dictionary<string, Type> Suppliers { get; }

		public HashSet<Assembly> AssemblySuppliers { get; }

		public void LoadSuppliers();

		public List<SupplierBase> CreateInstanceSuppliers(List<string> supplierNames);
	}
}