﻿using System.Collections.Generic;

namespace Com.Price.Infrastructure.Suppliers
{
	/// <summary>
	/// Интерфейс заполнения параметров поставщика.
	/// </summary>
	public interface ISupplierConfiguration
	{
		/// <summary>
		/// Параметры поставщика.
		/// </summary>
		Dictionary<string, object> SupplierConfigurationParameters { get; set; }
	}
}