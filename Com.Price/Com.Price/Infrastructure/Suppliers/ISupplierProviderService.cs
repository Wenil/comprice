﻿using Com.Price.Data.ProcessFiles.Xml;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Services.ProcessFiles.Excel.Response;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.Delivery;
using Com.Price.Infrastructure.Services.Download;
using Com.Price.Infrastructure.Services.Exchange;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel.Processing;
using Com.Price.Infrastructure.Services.ProcessFiles.Xml;

namespace Com.Price.Infrastructure.Suppliers
{
	public interface ISupplierProviderService
	{
		public IExchangeRateService ExchangeRateService { get; }

		public ICheckConditionService CheckConditionService { get; }

		public IPriceListDownloadService PriceListDownloadService { get; }

		public IExcelProcessingService<ExcelProcessingRequest, ExcelProcessingResponse> ExcelProcessingService { get; }

		public IXmlProcessingService<XmlProcessingRequest, XmlProcessingResponse> XmlProcessingService { get; }

		public ILoggerService LoggerService { get; }

		public ICalculateDeliveryService CalculateDeliveryService { get; }
	}
}