﻿using Com.Price.Infrastructure.Services.TaskManager.Model;
using System;

namespace Com.Price.Infrastructure.Services.TaskManager
{
	public class ComPriceTaskCollectionChangedEventArgs : EventArgs
	{
		public ListPriceTask Task { get; set; }
		public ComPriceTaskCollectionChangedAction Action { get; set; }
	}
}
