﻿using System.Collections.Generic;
using Com.Price.Infrastructure.Services.TaskManager.Model;

namespace Com.Price.Infrastructure.Services.TaskManager
{
	public interface IComPriceTaskConfigurationService
	{
		public void Save(string path, IList<ListPriceTask> comPriceTasks);

		public IList<ListPriceTask> Load(string path);
	}
}