﻿namespace Com.Price.Infrastructure.Services.TaskManager
{
	public enum ComPriceTaskCollectionChangedAction
	{
		Add, 
		Update, 
		Remove,
		Refresh
	}
}
