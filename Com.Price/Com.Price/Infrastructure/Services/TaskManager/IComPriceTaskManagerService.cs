﻿using System;
using System.Collections.Generic;
using Com.Price.Infrastructure.Services.TaskManager.Model;

namespace Com.Price.Infrastructure.Services.TaskManager
{
	public interface IComPriceTaskManagerService
	{
		public List<ListPriceTask> ComPriceTasks { get; set; }

		public event EventHandler<ComPriceTaskCollectionChangedEventArgs> ComPriceTaskCollectionChanged;

		public void StartTaskManager();

		public void StopTaskManager();

		public bool IsTaskManagerRunning { get; }

		public void AddComPriceTask(ListPriceTask listPriceTask);

		public void UpdateComPriceTask(ListPriceTask listPriceTask);

		public void RemoveComPriceTask(ListPriceTask listPriceTask);
	}
}