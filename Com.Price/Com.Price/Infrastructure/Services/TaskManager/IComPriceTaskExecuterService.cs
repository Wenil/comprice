﻿using System.Threading;
using System.Threading.Tasks;
using Com.Price.Data.TaskManager.Enums;
using Com.Price.Infrastructure.Services.TaskManager.Model;

namespace Com.Price.Infrastructure.Services.TaskManager
{
	public interface IComPriceTaskExecuterService
	{
		public Task<ListPriceTaskExecuterReturnValues> ExecuteAsync(ListPriceTask task, CancellationToken cancellationToken);
	}
}