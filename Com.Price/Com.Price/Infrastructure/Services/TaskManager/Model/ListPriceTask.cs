﻿using System;
using System.Collections.Generic;
using Com.Price.Data.TaskManager;
using Com.Price.Data.TaskManager.Enums;
using PropertyChanged;

namespace Com.Price.Infrastructure.Services.TaskManager.Model
{
	[AddINotifyPropertyChangedInterface]
	[Serializable]
	public class ListPriceTask
	{
		public Guid Id { get; set; } = Guid.Empty;

		public string Name { get; set; }

		public ListPriceTaskType Type { get; set; }

		public DateTime StartDate { get; set; } = DateTime.Now;

		public string DirectoryPath { get; set; }

		public List<string> Partners { get; set; } = new List<string>();

		public int ThreadCount { get; set; }

		public int IntervalBetweenStarts { get; set; }

		public DateTime EndTimeLastRun { set; get; }

		public DateTime FullTimeLastRun { set; get; }

		public ListPriceTaskStatus Status { get; set; } = ListPriceTaskStatus.Awaiting;

		public double Markup { get; set; }

		public bool IsUseMarkupRange { get; set; }

		public List<MarkupRangeModel> MarkupRange { get; set; } = new List<MarkupRangeModel>();

		public string MessageAfterRun { get; set; }

		public ListPriceTask Clone()
		{
			return MemberwiseClone() as ListPriceTask;
		}
	}
}