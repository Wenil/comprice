﻿namespace Com.Price.Infrastructure.Services
{
	public interface ICheckConditionService
	{
		public bool IsGarbageName(string productName);

		public bool IsEmptyQuantity(string quantity);
	}
}