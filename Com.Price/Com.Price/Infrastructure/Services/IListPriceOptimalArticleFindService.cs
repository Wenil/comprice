﻿using System.Collections.Generic;
using Com.Price.Data.Suppliers;
using Com.Price.Data.Suppliers.Structs;

namespace Com.Price.Infrastructure.Services
{
	public interface IListPriceOptimalArticleFindService
	{
		public ComPriceProduct FindOptimalArticleCommon(string article, List<SupplierBase> suppliers, List<MarkupRange> markupRange, bool checkAvailability = true, bool checkPrice = true);

		/// <summary>
		/// Поиск оптимального товара опираясь на присутствие цены.
		/// </summary>
		/// <param name="article">Артикул товара.</param>
		/// <param name="suppliers">Товары полученные от поставщиков.</param>
		/// <param name="markupRange">Наценки.</param>
		/// <returns>Возвращает найденный товар иначе пустой словарь.</returns>
		public ComPriceProduct FindOptimalArticleByPrice(string article, List<SupplierBase> suppliers, List<MarkupRange> markupRange);

		/// <summary>
		/// Поиск оптимального товара опираясь на присутствие наличия.
		/// </summary>
		/// <param name="article">Артикул товара.</param>
		/// <param name="suppliers">Товары полученные от поставщиков.</param>
		/// <param name="markupRange">Наценки.</param>
		/// <returns>Возвращает найденный товар иначе пустой словарь.</returns>
		public ComPriceProduct FindOptimalArticleByQuantity(string article, List<SupplierBase> suppliers, List<MarkupRange> markupRange);
	}
}