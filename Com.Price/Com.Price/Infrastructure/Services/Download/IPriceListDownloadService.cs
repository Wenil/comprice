﻿using System.Net;
using System.Text;

namespace Com.Price.Infrastructure.Services.Download
{
	public interface IPriceListDownloadService
	{
		public string PriceListPathIfExist(string directoryPath, string fileName, string fileExtension);

		public string DownloadPriceList(WebRequest httpWebRequest, string directoryPath, string fileName, string fileExtension, bool isArchive = false, Encoding encoding = null, bool canCleanDirectory = true);
	}
}