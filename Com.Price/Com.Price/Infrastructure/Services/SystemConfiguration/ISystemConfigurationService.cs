﻿using System.Collections.Generic;

namespace Com.Price.Infrastructure.Services.SystemConfiguration
{
	public interface ISystemConfigurationService
	{
		public Dictionary<string, string> SystemConfiguration { get; }

		public string GetSystemConfigurationParameter(string name);

		public void SetSystemConfigurationParameter(string name, string value);

		public bool IsSystemConfigurationParameter(string name);

		public void SetSystemConfigurationParameter(string name, bool value);

		public string[] Garbages { get; set; }

		public bool LoadSystemConfiguration();

		public bool SaveSystemConfiguration();

		public void AddRangeSystemConfigurationParameters(Dictionary<string, string> parameters);

		/// <summary>
		/// Искать точное вхождение артикула.
		/// </summary>
		public bool SearchOnlyForExactMatchArticle { get; set; }

		/// <summary>
		/// Выводить товар с минимальную цену
		/// </summary>
		public bool DisplayProductWithLowestPrice { get; set; }

		/// <summary>
		/// Ставить наличие "Есть", если количество товара больше 1
		/// </summary>
		public bool AvailabilityIfQuantityProductMoreThanOne { get; set; }

		public bool DisregardSubstandardGoods { get; set; }

		public bool UseAutomaticSearchInsteadOfManual { get; set; }

		public bool CalculateAtTheRate { get; set; }

		public bool DisableFilteringProductsByNumberOfWalks { get; set; }
	}
}