﻿using System.Collections.Generic;

namespace Com.Price.Infrastructure.Services.ProcessFiles.Xml
{
	public interface IXmlProcessingService<in TRequest, TResponse>
	{
		public IList<TResponse> FindProductByArticles(IList<string> articlesList, TRequest xmlProcessingRequest);
	}
}