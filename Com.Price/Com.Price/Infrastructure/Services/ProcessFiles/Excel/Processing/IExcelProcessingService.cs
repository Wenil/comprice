﻿using System.Collections.Generic;

namespace Com.Price.Infrastructure.Services.ProcessFiles.Excel.Processing
{
	public interface IExcelProcessingService <in TRequest, TResponse>
	{
		public IList<TResponse> FindProductByArticles(IList<string> articlesList, TRequest excelProcessingRequest);


	}
}