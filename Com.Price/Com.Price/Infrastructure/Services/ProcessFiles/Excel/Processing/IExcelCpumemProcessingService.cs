﻿using System.Collections.Generic;
using Com.Price.Data.Suppliers;
using Com.Price.Infrastructure.Services.TaskManager.Model;

namespace Com.Price.Infrastructure.Services.ProcessFiles.Excel.Processing
{
	public interface IExcelCpumemProcessingService<in TRequest, TResponse>
	{
		List<TResponse> ReadArticlesAndStatistics(TRequest excelProcessingRequest);

		List<TResponse> ReadArticlesAndStatistics(string filePath);

		List<ComPriceProduct> UpdateFindArticles(string filePath, ListPriceTask task, List<SupplierBase> suppliers);
	}
}