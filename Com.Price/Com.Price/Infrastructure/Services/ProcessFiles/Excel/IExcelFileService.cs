﻿namespace Com.Price.Infrastructure.Services.ProcessFiles.Excel
{
	public interface IExcelFileService<TFileHandler>
	{
		public TFileHandler OpenFile(string filePath);

		public void CloseFile(TFileHandler excelFileHandler);
	}
}