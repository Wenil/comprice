﻿namespace Com.Price.Infrastructure.Services.Api
{
	public interface IEleprodApiService<T>
	{
		/// <summary>
		/// Получение данных о товаре.
		/// </summary>
		/// <param name="article">Парт номер товара.</param>
		/// <param name="name">Название товара.</param>
		/// <returns>Словарь с данными о товаре.</returns>
		public T GetProductInfo(string article, string name);

		/// <summary>
		/// Обновление наличия.
		/// </summary>
		/// <param name="productInfo">Данные товара.</param>
		public void UpdateAvailability(T productInfo);


		/// <summary>
		/// Обновление цен.
		/// </summary>
		/// <param name="productInfo">Данные товара.</param>
		public void UpdatePrice(T productInfo);

		/// <summary>
		/// Обновление количества.
		/// </summary>
		/// <param name="productInfo">Данные товара.</param>
		public void UpdateQuantity(T productInfo);

		/// <summary>
		/// Обновление дней на доставку.
		/// </summary>
		/// <param name="productInfo">Данные товара.</param>
		/// <param name="days">Количество дней на доставку товара.</param>
		public void UpdateDeliveryDay(T productInfo, int? days);
	}
}