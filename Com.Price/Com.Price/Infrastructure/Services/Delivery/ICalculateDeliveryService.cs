﻿using Com.Price.Data.Suppliers;

namespace Com.Price.Infrastructure.Services.Delivery
{
	public interface ICalculateDeliveryService
	{
		public void CalculateDeliveryDays(ComPriceProduct comPriceProduct, int inStockDeliveryDay, int underOrderDeliveryDay, int specifyDeliveryDay);
	}
}