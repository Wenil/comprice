﻿using System.Collections.Generic;
using Com.Price.Infrastructure.Services.Exchange.Data;

namespace Com.Price.Infrastructure.Services.Exchange
{
	public interface IExchangeRateService
	{
		public Dictionary<Currency, double> ExchangeRates { get; set; }

		public Dictionary<Currency, double> ExchangeRatesForTomorrow { get; set; }

		public Dictionary<Currency, double> LoadExchangeRates(int numberOfAttempts = 3);
	}
}