﻿namespace Com.Price.Infrastructure.Services.Exchange.Data
{
	public enum Currency
	{
		USD,
		EUR,
		RUB
	}
}