﻿using System;

namespace Com.Price.Infrastructure.Services
{
	public interface ILoggerService
	{
		public void Error(string message);

		public void ErrorFormat(string message, params object[] args);

		public void Error(string message, Exception exception);

		public void Information(string message);
	}
}