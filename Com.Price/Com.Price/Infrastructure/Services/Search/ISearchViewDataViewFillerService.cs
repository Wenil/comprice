﻿using System.Collections.Generic;
using System.Data;
using Com.Price.Data.Suppliers;

namespace Com.Price.Infrastructure.Services.Search
{
	public interface ISearchViewDataViewFillerService <T>
	{
		public DataTable ProcessFullResult(List<T> articles, List<SupplierBase> suppliers);

		public DataTable ProcessShortResult(List<T> articles, List<SupplierBase> suppliers, double markup);
	}
}