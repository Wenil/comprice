﻿using Catel.Windows;

namespace Com.Price.Views.Menu.Administration
{
	/// <summary>
	/// Interaction logic for SystemConfigurationView.xaml
	/// </summary>
	public partial class SystemConfigurationView : DataWindow
	{
		public SystemConfigurationView() : base(DataWindowMode.Custom)
		{
			InitializeComponent();
		}
	}
}