﻿using Com.Price.Data.TaskManager;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;

namespace Com.Price.Views
{
	[ValueConversion(typeof(object), typeof(string))]
	public class ListToStringConverter : MarkupExtension, IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is List<MarkupRangeModel> values && values.Count > 0)
			{ 
				var stringBuilder = new StringBuilder();
				foreach (var markupRangeItem in values)
				{
					if(!markupRangeItem.IsEmpty())
						stringBuilder.Append($"{markupRangeItem.RangeStart} - {markupRangeItem.RangeEnd} = {markupRangeItem.Markup}; ");
				}

				return stringBuilder.ToString();
			}

			return string.Empty;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
