﻿using Catel.Windows;

namespace Com.Price.Views.TaskManager
{
	/// <summary>
	/// Interaction logic for ComPriceTaskView.xaml
	/// </summary>
	public partial class ComPriceTaskView : DataWindow
	{
		public ComPriceTaskView() : base( DataWindowMode.Custom)
		{
			InitializeComponent();
		}
	}
}
