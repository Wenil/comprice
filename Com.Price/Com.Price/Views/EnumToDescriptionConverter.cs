﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using Com.Price.Data.Extensions;

namespace Com.Price.Views
{
	[ValueConversion(typeof(Enum), typeof(string))]
	public class EnumToDescriptionConverter : MarkupExtension, IValueConverter
	{
		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is Enum enumValue)
				return enumValue.Description();

			return string.Empty;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw null;
		}
	}
}