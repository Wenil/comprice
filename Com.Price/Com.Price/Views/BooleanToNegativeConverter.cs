﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace Com.Price.Views
{
	[ValueConversion(typeof(object), typeof(bool))]
	public class BooleanToNegativeConverter : MarkupExtension, IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value != null && (bool)value)
				return false;

			return true;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw null;
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}