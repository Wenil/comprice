﻿using Catel.Windows;

namespace Com.Price.Views
{
	/// <summary>
	/// Interaction logic for ShellView.xaml
	/// </summary>
	public partial class ShellView : DataWindow
	{
		public ShellView() : base(DataWindowMode.Custom)
		{
			InitializeComponent();
		}
	}
}