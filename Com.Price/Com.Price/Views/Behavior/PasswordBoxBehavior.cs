﻿using System.Windows;
using System.Windows.Controls;
using Microsoft.Xaml.Behaviors;

namespace Com.Price.Views.Behavior
{
	public class PasswordBoxBehavior : Behavior<PasswordBox>
	{
		public static readonly DependencyProperty PasswordProperty =
			DependencyProperty.Register("Password", typeof(string), typeof(PasswordBoxBehavior), new PropertyMetadata(default(string)));

		private bool _skipUpdate;

		public string Password
		{
			get => (string) GetValue(PasswordProperty);
			set => SetValue(PasswordProperty, value);
		}

		protected override void OnAttached()
		{
			if (AssociatedObject != null)
				AssociatedObject.PasswordChanged += PasswordBox_PasswordChanged;
		}

		protected override void OnDetaching()
		{
			if (AssociatedObject != null)
				AssociatedObject.PasswordChanged -= PasswordBox_PasswordChanged;
		}

		protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
		{
			base.OnPropertyChanged(e);

			if (e.Property == PasswordProperty && !_skipUpdate)
			{
				_skipUpdate = true;
				if (AssociatedObject != null && e.NewValue != null)
					AssociatedObject.Password = e.NewValue as string ?? string.Empty;
				_skipUpdate = false;
			}
		}

		private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
		{
			_skipUpdate = true;
			Password = AssociatedObject.Password;
			_skipUpdate = false;
		}
	}
}