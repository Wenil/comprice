﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Catel.IoC;
using Catel.MVVM;
using Catel.Services;
using Com.Price.Data;
using Com.Price.Data.Article;
using Com.Price.Data.Menu.Administration;
using Com.Price.Data.ProcessFiles.Xml;
using Com.Price.Data.Search;
using Com.Price.Data.Services;
using Com.Price.Data.Services.Api;
using Com.Price.Data.Services.Delivery;
using Com.Price.Data.Services.Download;
using Com.Price.Data.Services.Exchange;
using Com.Price.Data.Services.ProcessFiles.Excel;
using Com.Price.Data.Services.ProcessFiles.Excel.FileProcessing;
using Com.Price.Data.Services.ProcessFiles.Excel.Request;
using Com.Price.Data.Services.ProcessFiles.Excel.Response;
using Com.Price.Data.Services.ProcessFiles.Xml;
using Com.Price.Data.Services.Search;
using Com.Price.Data.Services.SystemConfiguration;
using Com.Price.Data.Services.TaskManager;
using Com.Price.Data.Suppliers;
using Com.Price.Data.TaskManager;
using Com.Price.Infrastructure.Services;
using Com.Price.Infrastructure.Services.Api;
using Com.Price.Infrastructure.Services.Delivery;
using Com.Price.Infrastructure.Services.Download;
using Com.Price.Infrastructure.Services.Exchange;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel;
using Com.Price.Infrastructure.Services.ProcessFiles.Excel.Processing;
using Com.Price.Infrastructure.Services.ProcessFiles.Xml;
using Com.Price.Infrastructure.Services.Search;
using Com.Price.Infrastructure.Services.SystemConfiguration;
using Com.Price.Infrastructure.Services.TaskManager;
using Com.Price.Infrastructure.Suppliers;
using Com.Price.Views;
using Com.Price.Views.Menu.Administration;
using Com.Price.Views.Search;
using Com.Price.Views.TaskManager;
using ControlzEx.Theming;
using MahApps.Metro.Theming;

namespace Com.Price
{
	/// <inheritdoc />
	/// <summary>
	/// Логика взаимодействия для App.xaml
	/// </summary>
	public sealed partial class App : Application
	{
		private ILoggerService _loggerService;

		protected override void OnStartup(StartupEventArgs e)
		{
#if DEBUG
			Catel.Logging.LogManager.AddDebugListener();
#endif

			var theme = ThemeManager.Current.AddLibraryTheme(
				new LibraryTheme(
					new Uri("pack://application:,,,/Com.Price;component/MetroUI/ApplicationTheme.xaml"),
					MahAppsLibraryThemeProvider.DefaultInstance
				)
			);

			ThemeManager.Current.ChangeTheme(this, theme);

			var serviceLocator = ServiceLocator.Default;
			serviceLocator.RegisterType<ILoggerService, LoggerService>();
			serviceLocator.RegisterType<ISystemConfigurationService, SystemConfigurationService>();
			serviceLocator.RegisterType<IExchangeRateService, ExchangeRateService>();

			serviceLocator.RegisterType<ISupplierLoaderService, SupplierLoaderService>();
			serviceLocator.RegisterType<IPriceListDownloadService, PriceListDownloadService>();
			serviceLocator.RegisterType<ICheckConditionService, CheckConditionService>();
			serviceLocator.RegisterType<IListPriceOptimalArticleFindService, ListPriceOptimalArticleFindService>();
			serviceLocator.RegisterType<ICalculateDeliveryService, CalculateDeliveryService>();

			serviceLocator.RegisterType<IExcelFileService<ExcelFileHandler>, ExcelFileService>();
			serviceLocator.RegisterType<IExcelProcessingService<ExcelProcessingRequest, ExcelProcessingResponse>, ExcelProcessingService>();
			serviceLocator.RegisterType<IExcelCpumemProcessingService<ExcelCpumemProcessingRequest, ExcelCpumemProcessingResponse>, ExcelCpumemProcessingService>();

			serviceLocator.RegisterType<IXmlProcessingService<XmlProcessingRequest, XmlProcessingResponse>, XmlProcessingService>();

			serviceLocator.RegisterType<ISupplierProviderService, SupplierProviderService>();

			serviceLocator.RegisterType<ISearchViewDataViewFillerService<ComPriceArticle>, SearchViewDataViewFillerService>();

			serviceLocator.RegisterType<IEleprodApiService<EleprodProductInfo>, EleprodApiService>();

			serviceLocator.RegisterType<IComPriceTaskConfigurationService, ComPriceTaskConfigurationService>();
			serviceLocator.RegisterType<IComPriceTaskManagerService, ComPriceTaskManagerService>();
			serviceLocator.RegisterType<IComPriceTaskExecuterService, ComPriceTaskExecuterService>();

			var viewModelLocator = ServiceLocator.Default.ResolveType<IViewModelLocator>();
			viewModelLocator.Register(typeof(ShellView), typeof(ShellViewModel));

			viewModelLocator.Register(typeof(SearchView), typeof(SearchViewModel));

			viewModelLocator.Register(typeof(SystemConfigurationView), typeof(SystemConfigurationViewModel));
			viewModelLocator.Register(typeof(GeneralSettingsView), typeof(GeneralSettingsViewModel));
			viewModelLocator.Register(typeof(SubstandardProductNamesView), typeof(SubstandardProductNamesViewModel));

			viewModelLocator.Register(typeof(TaskManagerView), typeof(TaskManagerViewModel));
			viewModelLocator.Register(typeof(ComPriceTaskView), typeof(ComPriceTaskViewModel));

			var uiVisualizerService = serviceLocator.ResolveType<IUIVisualizerService>();
			uiVisualizerService.Register(typeof(SystemConfigurationViewModel), typeof(SystemConfigurationView));
			uiVisualizerService.Register(typeof(ComPriceTaskViewModel), typeof(ComPriceTaskView));

			_loggerService = (ILoggerService) serviceLocator.GetService(typeof(ILoggerService));

			AppDomain.CurrentDomain.UnhandledException += (sender, args) => CurrentDomainOnUnhandledException(args, _loggerService);
			Current.DispatcherUnhandledException += (sender, args) => CurrentOnDispatcherUnhandledException(args, _loggerService);
			TaskScheduler.UnobservedTaskException += (sender, args) => TaskSchedulerOnUnobservedTaskException(args, _loggerService);

			base.OnStartup(e);
		}

		private void TaskSchedulerOnUnobservedTaskException(UnobservedTaskExceptionEventArgs e, ILoggerService loggerService)
		{
			loggerService.Error("UnhandledException", e.Exception);
			e.SetObserved();
		}

		private void CurrentOnDispatcherUnhandledException(DispatcherUnhandledExceptionEventArgs e, ILoggerService loggerService)
		{
			loggerService.Error("UnhandledException", e.Exception);
			// e.Handled = true;
		}

		private void CurrentDomainOnUnhandledException(UnhandledExceptionEventArgs e, ILoggerService loggerService)
		{
			loggerService.Error("UnhandledException", (Exception) e.ExceptionObject);
			loggerService.Error($"Runtime terminating: {e.IsTerminating}");
		}

		private void AppOnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			_loggerService.Error("UnhandledException", e.Exception);
			// e.Handled = true;
		}
	}
}